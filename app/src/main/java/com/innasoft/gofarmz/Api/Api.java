package com.innasoft.gofarmz.Api;

import com.innasoft.gofarmz.Response.AddAddressResponse;
import com.innasoft.gofarmz.Response.AddressDeleteResponse;
import com.innasoft.gofarmz.Response.AddressGetResponse;
import com.innasoft.gofarmz.Response.AddressListResponse;
import com.innasoft.gofarmz.Response.AppVersionResponse;
import com.innasoft.gofarmz.Response.BaseResponse;
import com.innasoft.gofarmz.Response.BottomOfferAddResponse;
import com.innasoft.gofarmz.Response.CartCountResponse;
import com.innasoft.gofarmz.Response.CartDeleteResponse;
import com.innasoft.gofarmz.Response.CartIncreaseResponse;
import com.innasoft.gofarmz.Response.CartResponse;
import com.innasoft.gofarmz.Response.CheckoutAddressResponse;
import com.innasoft.gofarmz.Response.CheckoutDataResponse;
import com.innasoft.gofarmz.Response.CommunityResponse;
import com.innasoft.gofarmz.Response.CustomBoxResponse;
import com.innasoft.gofarmz.Response.DeliverSlotResponse;
import com.innasoft.gofarmz.Response.DeliveryDetailsDataResponse;
import com.innasoft.gofarmz.Response.EditAddressResponse;
import com.innasoft.gofarmz.Response.FormarResponse;
import com.innasoft.gofarmz.Response.HomeAreasResponse;
import com.innasoft.gofarmz.Response.HomeBoxProductsResponse;
import com.innasoft.gofarmz.Response.HomeCategoriesResponse;
import com.innasoft.gofarmz.Response.ImageNotificationResponse;
import com.innasoft.gofarmz.Response.LoginMobileResponse;
import com.innasoft.gofarmz.Response.LoginResponse;
import com.innasoft.gofarmz.Response.MyOrderDetailsResponse;
import com.innasoft.gofarmz.Response.MyOrderResponse;
import com.innasoft.gofarmz.Response.MyPreOrderDetailsResponse;
import com.innasoft.gofarmz.Response.MyPreOrderResponse;
import com.innasoft.gofarmz.Response.NotificationResponse;
import com.innasoft.gofarmz.Response.PlaceOrderResponse;
import com.innasoft.gofarmz.Response.PreOrderAddCartResponse;
import com.innasoft.gofarmz.Response.PreOrderCartResponse;
import com.innasoft.gofarmz.Response.PreOrderCheckoutDataResponse;
import com.innasoft.gofarmz.Response.PreOrderDeleteCartResponse;
import com.innasoft.gofarmz.Response.PreOrderPlaceResponse;
import com.innasoft.gofarmz.Response.PreOrderProductResponse;
import com.innasoft.gofarmz.Response.PreOrderResponse;
import com.innasoft.gofarmz.Response.PreOrderUpdateCartResponse;
import com.innasoft.gofarmz.Response.ProfileResponse;
import com.innasoft.gofarmz.Response.PromoCodeResponse;
import com.innasoft.gofarmz.Response.SliderTopBannerResponse;
import com.innasoft.gofarmz.Response.StateResponse;
import com.innasoft.gofarmz.Response.ValidateCoupenResponse;
import com.innasoft.gofarmz.Response.ViewCartIncreaseResponse;
import com.innasoft.gofarmz.Response.ViewDetailsResponse;
import com.innasoft.gofarmz.Response.WalletAmountResponse;
import com.innasoft.gofarmz.Response.WalletgetResponse;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface Api {

    @FormUrlEncoded
    @POST("verify-login")
    Call<LoginResponse> userLogin(@Field("email") String email,
                                  @Field("password") String password,
                                  @Field("browser_id") String brower_id);

    @FormUrlEncoded
    @POST("registration")
    Call<BaseResponse> userRegistrationRequest(@Header("X-Deviceid") String deviceId, @Field("name") String name, @Field("email") String email,
                                               @Field("mobile") String mobile, @Field("conf_pwd") String conf_pwd);

    //login with mobile number

    @FormUrlEncoded
    @POST("login-otp")
    Call<LoginMobileResponse> LoginMobile(@Field("mobile") String mobile,
                                          @Header("X-Deviceid") String brower_id);


    @FormUrlEncoded
    @POST("otp-verification-login")
    Call<LoginResponse> LoginMobileWithOTP(@Field("mobile") String mobile,
                                           @Field("browser_id") String brower_id,
                                           @Field("otp") String otp);


    @FormUrlEncoded
    @POST("verify-login-social")
    Call<LoginResponse> socialLogin(@Header("X-Deviceid") String deviceId, @Field("name") String name, @Field("email") String email,
                                    @Field("browser_id") String browser_id, @Field("provider") String provider, @Field("identifier") String identifier
    );

    @FormUrlEncoded
    @POST("verify-otp")
    Call<LoginResponse> userOtpRequest(@Header("X-Deviceid") String deviceId, @Field("mobile") String mobile,
                                       @Field("otp") String otp,
                                       @Field("browser_id") String browser_id);

    @FormUrlEncoded
    @POST("resend-otp")
    Call<BaseResponse> ResendOtpRequest(@Header("X-Deviceid") String deviceId, @Field("mobile") String mobile);

    @FormUrlEncoded
    @POST("forgot-password")
    Call<BaseResponse> userForgotPassword(@Header("X-Deviceid") String deviceId, @Field("mobile") String mobile);


    @FormUrlEncoded
    @POST("verify-otp-forgot-password")
    Call<BaseResponse> userVerifyForgotPassword(@Header("X-Deviceid") String deviceId, @Field("mobile") String mobile,
                                                @Field("otp") String otp,
                                                @Field("browser_id") String browser_id,
                                                @Field("new_pwd") String new_pwd);


    @FormUrlEncoded
    @POST("user/change-password")
    Call<BaseResponse> userChangePassword(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String deviceId, @Field("user_id") String id, @Field("old_pwd") String old_pwd, @Field("new_pwd") String new_pwd);


    @GET("user/profile")
    Call<ProfileResponse> getUserProfile(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String deviceId, @Query("user_id") String userid);

    @FormUrlEncoded
    @POST("user/logout")
    Call<BaseResponse> userLogout(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String deviceId, @Field("user_id") String user_id, @Field("browser_id") String browser_id);


    @FormUrlEncoded
    @POST("user/profile")
    Call<BaseResponse> updateProfile(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String deviceId,
                                     @Field("user_id") String user_id, @Field("name") String name,
                                     @Field("email") String email, @Field("mobile") String mobile,
                                     @Field("gender") String gender
    );

    @FormUrlEncoded
    @POST("refer_farmer")
    Call<BaseResponse> referFarmer(@Header("X-Deviceid") String deviceId,
                                   @Field("user_id") String user_id,
                                   @Field("name") String name,
                                   @Field("state") String state,
                                   @Field("mobile") String mobile,
                                   @Field("city") String city
    );


    @GET("states")
    Call<StateResponse> stateList(@Header("Authorization-Basic") String tokenValue, @Query("country_id") String country_id);

    @GET("cities")
    Call<StateResponse> citiesList(@Header("Authorization-Basic") String tokenValue, @Query("state_id") String state_id);



    @Headers("Content-Type: application/json")
    @GET("urls/{type}")
    Call<ResponseBody> datafromwebview(@Path("type") String type);


    /*@Headers("Content-Type: application/json")
    @GET("urls/{type}")
    Call<ResponseBody> privacyPolicy(@Path("type") String type);
*/
    @Headers("Content-Type: application/json")
    @GET("urls/faqs")
    Call<ResponseBody> faq();

    @Headers("Content-Type: application/json")
    @GET("urls/")
    Call<ResponseBody> knowYourFormer();

    @Headers("Content-Type: application/json")
    @GET("urls/our-story")
    Call<ResponseBody> ourStory();

    @Headers("Content-Type: application/json")
    @GET("urls/farmer-network")
    Call<ResponseBody> formerNetwork();

    @GET("app/version")
    Call<AppVersionResponse> CheckAppUpdate(@Header("Authorization-Basic") String tokenValue);


    @FormUrlEncoded
    @POST("cart/delete")
    Call<CartDeleteResponse> CartDelete(@Field("user_id") String user_id,
                                        @Field("cart_id") String cart_id,
                                        @Field("browser_id") String browser_id);

    @FormUrlEncoded
    @POST("cart/quantity")
    Call<CartIncreaseResponse> CartIncrease(@Field("purchase_quantity") int purchase_quantity,
                                            @Field("cart_id") String cart_id);


    @GET("user/{userId}/notification")
    Call<NotificationResponse> notifications(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String deviceId, @Path("userId") String userId);

    @PUT("user/{userId}/notification/{sendId}/read")
    Call<BaseResponse> readNotifications(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String deviceId, @Path("userId") String userId, @Path("sendId") String sendId);


    @GET("user/{id}/orders")
    Call<MyOrderResponse> MyOrders(@Header("Authorization-Basic") String tokenValue, @Path("id") String userId);

    @GET("user/{id}/pre_orders")
    Call<MyPreOrderResponse> MyPreOrders(@Header("Authorization-Basic") String tokenValue, @Path("id") String userId);

    @GET("user/{id}/orders/{id1}")
    Call<MyOrderDetailsResponse> MyOrdersDetails(@Header("Authorization-Basic") String tokenValue, @Path("id") String userId, @Path("id1") String orderId);

    @GET("user/{id}/pre_orders/{id1}")
    Call<MyPreOrderDetailsResponse> MyPreOrderDetails(@Header("Authorization-Basic") String tokenValue, @Path("id") String userId, @Path("id1") String orderId);

    @GET("cart")
    Call<CartResponse> CartData(@Header("Authorization-Basic") String tokenValue, @Query("user_id") String userId, @Query("browser_id") String browserId);


    @GET("delivery-details")
    Call<DeliveryDetailsDataResponse> DeliverDetailsData(@Header("Authorization-Basic") String tokenValue, @Query("user_id") String userId);


    @GET("checkout")
    Call<CheckoutDataResponse> CheckoutData(@Header("Authorization-Basic") String tokenValue, @Query("user_id") String userId, @Query("address_id") String address_id);

    @FormUrlEncoded
    @POST("checkout")
    Call<PlaceOrderResponse>
    PlaceOrder(@Header("Authorization-Basic") String tokenValue,
               @Field("code") String code,
               @Field("payment_gateway_id") String payment_gateway_id,
               @Field("platform") String platform,
               @Field("user_id") String user_id,
               @Field("address_id") String address_id,
               @Field("delivery_slot_id") String delivery_slot_id,
               @Field("instructions") String instructions,
               @Field("is_wallet_used") String wallet_used);

    @GET("user/{id}/addresses")
    Call<CheckoutAddressResponse> CheckoutAddress(@Header("Authorization-Basic") String tokenValue, @Path("id") String userId);


    @FormUrlEncoded
    @PUT("fcm-token")
    Call<BaseResponse> sendFcmToken(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String deviceId, @Field("user_id") String userId, @Field("fcm_token") String fcmToken);


    @GET("farmers/products/{id}")
    Call<FormarResponse> getFormar(@Header("X-Deviceid") String deviceId, @Path("id") String id);

    @GET("promocodes")
    Call<PromoCodeResponse> getPromoCodes(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String deviceId);

    @FormUrlEncoded
    @POST("user/{id}/coupon/validate")
    Call<ValidateCoupenResponse> validCoupenCode(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String deviceId,
                                                 @Path("id") String id,
                                                 @Field("user_id") String user_id,
                                                 @Field("code") String code,
                                                 @Field("order_amount") String order_amount);

    @GET("user/{id}/notification/{id1}/details")
    Call<ImageNotificationResponse> imageNotification(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String deviceId, @Path("id") String userId, @Path("id1") String id1);


    @GET("delivery-details")
    Call<DeliverSlotResponse> deliverySlot(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String deviceId, @Query("user_id") String userId);


    @FormUrlEncoded
    @POST("cart")
    Call<BaseResponse> addtocart(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String deviceId,
                                 @Field("user_id") String user_id,
                                 @Field("product_id") String product_id,
                                 @Field("browser_id") String browser_id,
                                 @Field("purchase_quantity") String purchase_quantity,
                                 @Field("addOnProducts[]") List<String> items);


    @GET("cart/delete-product-addon")
    Call<BaseResponse> deleteCart(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String deviceId, @Query("cart_product_addon_id") String cart_product_addon_id);


    @GET("cart/count")
    Call<CartCountResponse> CartCount(@Header("Authorization-Basic") String tokenValue, @Query("user_id") String userId, @Query("browser_id") String browserId);

    @GET("banners")
    Call<SliderTopBannerResponse> TopBanners(@Header("Authorization-Basic") String tokenValue);

    @GET("category/main")
    Call<HomeCategoriesResponse> HomeCategories(@Header("Authorization-Basic") String tokenValue);


    @GET("banners/promo")
    Call<BottomOfferAddResponse> BottomOfferAdd(@Header("Authorization-Basic") String tokenValue);


    @GET("products")
    Call<HomeBoxProductsResponse> HomeBoxProducts(@Header("Authorization-Basic") String tokenValue, @Query("main_category") String mainCategory, @Query("type") String type,
                                                  @Query("page") String page, @Query("user_id") String user_id,
                                                  @Query("browser_id") String browser_id);

    @GET("products")
    Call<CustomBoxResponse> CustomBoxProducts(@Header("Authorization-Basic") String tokenValue, @Query("main_category") String mainCategory, @Query("type") String type,
                                              @Query("page") String page, @Query("user_id") String user_id,
                                              @Query("browser_id") String browser_id);
    @FormUrlEncoded
    @POST("user/{id}/addresses")
    Call<AddAddressResponse> AddAddress(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String Deviceid, @Path("id") String userId,
                                        @Field("name") String name, @Field("address_line1") String address_line1, @Field("address_line2") String address_line2,
                                        @Field("area") String area, @Field("city") String city, @Field("state") String state, @Field("pincode") String pincode,
                                        @Field("contact_no") String contact_no, @Field("alternate_contact_no") String alternate_contact_no, @Field("is_default") String is_default,
                                        @Field("latitude") String latitude, @Field("longitude") String longitude);

    @GET("user/{id}/addresses")
    Call<AddressListResponse> AddressList(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String deviceId, @Path("id") String userId);


    @DELETE("user/{id}/addresses/{id1}")
    Call<AddressDeleteResponse> DeleteAddress(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String deviceId, @Path("id") String userId, @Path("id1") String addressId);

    @GET("user/{id}/addresses/{id1}")
    Call<AddressGetResponse> GetAddress(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String deviceId, @Path("id") String userId, @Path("id1") String addressId);


    @FormUrlEncoded
    @POST("user/{id}/addresses/{id1}")
    Call<EditAddressResponse> EditAddress(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String Deviceid, @Path("id") String userId, @Path("id1") String addressId,
                                          @Field("name") String name, @Field("address_line1") String address_line1, @Field("address_line2") String address_line2,
                                          @Field("area") String area, @Field("city") String city, @Field("state") String state, @Field("pincode") String pincode,
                                          @Field("contact_no") String contact_no, @Field("alternate_contact_no") String alternate_contact_no, @Field("is_default") String is_default,
                                          @Field("latitude") String latitude, @Field("longitude") String longitude);

    //preorder

    @GET("pre_order_products/{id}")
    Call<PreOrderResponse> PreOrders(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String deviceId, @Path("id") String userId);

    @GET("pre_order_product/{id}/{id1}")
    Call<PreOrderProductResponse> PreOrderProducts(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String deviceId, @Path("id") String pId, @Path("id1") String userId);

    @FormUrlEncoded
    @POST("add_to_pre_order_cart")
    Call<PreOrderAddCartResponse> PreOrderAddCart(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String deviceId, @Field("user_id") String userId,
                                                  @Field("product_id") String product_id, @Field("product_quantity") String product_quantity);

    @FormUrlEncoded
    @POST("update_pre_order_cart")
    Call<PreOrderUpdateCartResponse> PreOrderUpdateCart(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String deviceId, @Field("user_id") String userId,
                                                        @Field("product_id") String product_id, @Field("product_quantity") String product_quantity);

    @GET("pre_order_cart/{id}")
    Call<PreOrderCartResponse> PreOrderCartData(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String deviceId, @Path("id") String userId);

    @FormUrlEncoded
    @POST("delete_from_pre_order_cart")
    Call<PreOrderDeleteCartResponse> PreOrderDeleteCart(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String deviceId, @Field("user_id") String userId, @Field("product_id") String product_id);

    @FormUrlEncoded
    @POST("update_pre_order_cart")
    Call<PreOrderUpdateCartResponse> PreOrderUpdateCart1(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String deviceId, @Field("user_id") String userId,
                                                         @Field("product_id") String product_id, @Field("product_quantity") String product_quantity);

    @GET("pre_order_checkout")
    Call<PreOrderCheckoutDataResponse> PreOrderCheckoutData(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String browserId, @Query("user_id") String userId,@Query("address_id") String address_id);

    @FormUrlEncoded
    @POST("pre_order_checkout")
    Call<PreOrderPlaceResponse> PreOrderPlace(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String browserId, @Field("user_id") String userId,
                                              @Field("address_id") String address_id, @Field("code") String code, @Field("payment_gateway_id") String payment_gateway_id,
                                              @Field("platform") String platform);

    @GET("cart/items")
    Call<ViewDetailsResponse> ViewDetailsData(@Header("Authorization-Basic") String tokenValue, @Query("user_id") String userId,
                                              @Query("browser_id") String browser_id, @Query("cart_id") String cart_id);

    @FormUrlEncoded
    @POST("cart/update-product-addon")
    Call<ViewCartIncreaseResponse> ViewCartIncreaseData(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String browserId, @Field("cart_product_addon_id") String cart_product_addon_id,
                                                        @Field("purchase_quantity") String purchase_quantity);



    @GET("service/areas")
    Call<HomeAreasResponse> HomeAreas();


    @FormUrlEncoded
    @PUT("user/{userId}/orders/{orderId}/delivery/{slotId}")
    Call<BaseResponse> updateDeliverydate(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String browserId,
                                          @Path("userId") String userId,
                                          @Path("orderId") String orderId,
                                          @Path("slotId") String slotId,
                                          @Field("user_id") String user_id,
                                          @Field("order_id") String order_id,
                                          @Field("delivery_slot_id") String delivery_slot_id);


  /*  @FormUrlEncoded
    @POST("payment/razorpay_success/{Id}")
    Call<ResponseBody> paymentSuccess(@Header("Authorization-Basic") String tokenValue,
                                      @Header("X-Deviceid") String browserId,
                                          @Path("Id") String Id,
                                          @Field("order_id") String orderid,
                                          @Field("payment_reference_id") String payment_reference_id,
                                          @Field("status") String status
                                          );
*/

    @FormUrlEncoded
    @POST("payment/inn_success/{id}")
    Call<ResponseBody> paymentSuccess(
            @Path("id") String encryptOrderid,
            @Field("gateway_name") String gateway_name,
            @Field("txnid") String txnid,
            @Field("status") String status,
            @Field("message") String message,
            @Field("mode") String mode,
            @Field("amount") String amount,
            @Field("card_type") String card_type
    );


    @GET("user/{userId}/wallet/amount")
    Call<WalletAmountResponse> getWalletAmount(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String browserId, @Path("userId") String userId);

    @GET("user/{userId}/wallet/transactions")
    Call<WalletgetResponse> getWalletTransactions(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String browserId, @Path("userId") String userId, @Query("page") int page);

    @GET
    Call<WalletgetResponse> getWalletTransactionsUrl(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String browserId,@Url String url);


    @FormUrlEncoded
    @POST("user/{userId}/wallet/add/money")
    Call<BaseResponse> addMoney(
            @Header("Authorization-Basic") String tokenValue,
            @Header("X-Deviceid") String browserId,
            @Path("userId") String userId,
            @Field("payment_id") String payment_id,
            @Field("amount") String amount);


    @GET("community")
    Call<CommunityResponse> getCommunityname(@Header("Authorization-Basic") String tokenValue, @Header("X-Deviceid") String browserId);

}
