package com.innasoft.gofarmz.Api;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    //Live URL's
    /*public static String BASE_URL = "http://13.232.141.21/";
    public static String API_URL = BASE_URL + "api/";
    public static String IMAGE_URL = BASE_URL + "images/products/";
    public static String BASEIMAGEURL_70x70 = IMAGE_URL + "70x70/";
    public static String BASEIMAGEURL_400x400 = IMAGE_URL + "400x400/";
    public static String BASEIMAGEURL_280x280 = IMAGE_URL + "280x280/";
    public static String KNOW_YOUR_FORMER = "know-your-farmer";
    public static String OUR_STORY = "our-story";
    public static String FORMER_NETWORK = "farmer-network";
    public static String FAQ = "faqs";
    public static String TERMS_CONDITION = "terms-conditions";
    public static String PRIVACY_POLICY = "privacy-policy";*/

    /*Test URL's*/
    // http://gofarmzv2.learningslot.in/

  /*  public static String API_URL = "http://gofarmzv2.learningslot.in/api/";
    public static String IMAGE_URL = "http://gofarmzv2.learningslot.in//images/products/";
    public static String CATEGORY_IMAGE_URL = "http://gofarmzv2.learningslot.in/";
    public static String BASEIMAGEURL_70x70 = "http://gofarmzv2.learningslot.in//images/products/70x70/";
    public static String BASEIMAGEURL_400x400 = "http://gofarmzv2.learningslot.in//images/products/400x400/";
    public static String BASEIMAGEURL_280x280 = "http://gofarmzv2.learningslot.in//images/products/280x280/";

 */

    public static String BASE_URL = "https://www.innasoft.in/gofarmz/";
    public static String API_URL = BASE_URL + "api/";
    public static String IMAGE_URL = BASE_URL + "images/products/";
    public static String BASEIMAGEURL_70x70 = IMAGE_URL + "70x70/";
    public static String BASEIMAGEURL_400x400 = IMAGE_URL + "400x400/";
    public static String BASEIMAGEURL_280x280 = IMAGE_URL + "280x280/";
    public static String KNOW_YOUR_FORMER = "know-your-farmer";
    public static String OUR_STORY = "our-story";
    public static String FORMER_NETWORK = "farmer-network";
    public static String FAQ = "faqs";
    public static String TERMS_CONDITION = "terms-conditions";
    public static String PRIVACY_POLICY = "privacy-policy";




    public static String GOFARMZ_API_KEY = "EF04794F718A3FC8C6DFA0B215B2CF24";


    private static RetrofitClient mInstance;
    public static Retrofit retrofit;

    private RetrofitClient() {

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(
                        new Interceptor() {
                            @Override
                            public Response intercept(Interceptor.Chain chain) throws IOException {
                                Request request = chain.request().newBuilder()
                                        .addHeader("Accept", "Application/JSON")
                                        .addHeader("Gofarmz-Api-Key", GOFARMZ_API_KEY)
                                        .addHeader("X-Platform", "ANDROID")


                                        .build();
                                return chain.proceed(request);
                            }
                        }).build();

        retrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    public static synchronized RetrofitClient getInstance() {
        if (mInstance == null) {
            mInstance = new RetrofitClient();
        }
        return mInstance;
    }

    public Api getApi() {
        return retrofit.create(Api.class);
    }

}
