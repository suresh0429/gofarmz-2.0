package com.innasoft.gofarmz.Api;

import java.util.ArrayList;

public class APIError {
    private String status;
    private String message;

    public APIError() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
