package com.innasoft.gofarmz.Api;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

import static com.android.volley.VolleyLog.TAG;

public class ErrorUtils {
    public static APIError parseError(Response<?> response) {
        Converter<ResponseBody, APIError> converter =
                RetrofitClient.retrofit.responseBodyConverter(APIError.class, new Annotation[0]);

        APIError error;

        try {
            Log.d(TAG, "parseError: "+response.body());
            error = converter.convert((ResponseBody) response.body());
        } catch (IOException e) {
            return new APIError();
        }

        return error;
    }

}
