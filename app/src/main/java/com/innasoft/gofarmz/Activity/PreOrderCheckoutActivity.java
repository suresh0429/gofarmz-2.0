package com.innasoft.gofarmz.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.gofarmz.Adapter.PreOrderPaymentTypeAdapter;
import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.Preferences.UserSessionManager;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.DeliverSlotResponse;
import com.innasoft.gofarmz.Response.PreOrderCheckoutDataResponse;
import com.innasoft.gofarmz.Response.PreOrderPlaceResponse;
import com.innasoft.gofarmz.Utilis.Util;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Utilis.Constants.ADD_ADDRESS;
import static com.innasoft.gofarmz.Utilis.Constants.BOTTAM_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.CART_COUNT;
import static com.innasoft.gofarmz.Utilis.Constants.CHANGE_ADDRESS;
import static com.innasoft.gofarmz.Utilis.Constants.COUNT;
import static com.innasoft.gofarmz.Utilis.Constants.MODULE;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_ID;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_STATUS;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_TITLE;
import static com.innasoft.gofarmz.Utilis.Constants.PRE_ORDER_CART;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;
import static com.innasoft.gofarmz.Utilis.Constants.TYPE;

public class PreOrderCheckoutActivity extends AppCompatActivity implements InternetConnectivityListener, PreOrderPaymentTypeAdapter.PaymentTypeInterface {

    @BindView(R.id.btnAddAddress)
    TextView btnAddAddress;
    @BindView(R.id.addressLayout)
    LinearLayout addressLayout;
    @BindView(R.id.txtName)
    TextView txtName;
    @BindView(R.id.txtChange)
    TextView txtChange;
    @BindView(R.id.txtAddress)
    TextView txtAddress;
    @BindView(R.id.txtCart)
    TextView txtCart;
    @BindView(R.id.txtCartTotal)
    TextView txtCartTotal;
    @BindView(R.id.txtCoupon)
    TextView txtCoupon;
    @BindView(R.id.txtCouponPrice)
    TextView txtCouponPrice;
    @BindView(R.id.txtDelivery)
    TextView txtDelivery;
    @BindView(R.id.txtDeliveryPrice)
    TextView txtDeliveryPrice;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.txtTotal)
    TextView txtTotal;
    @BindView(R.id.txtTotalPrice)
    TextView txtTotalPrice;
    @BindView(R.id.paymenttype)
    TextView paymenttype;
    @BindView(R.id.Paymentrecycler)
    RecyclerView Paymentrecycler;
    @BindView(R.id.paymenttypeLayout)
    LinearLayout paymenttypeLayout;
    @BindView(R.id.txtPayblAmount)
    TextView txtPayblAmount;
    @BindView(R.id.btnPay)
    Button btnPay;
    @BindView(R.id.bottamLayout)
    CardView bottamLayout;
    @BindView(R.id.mainLayout)
    RelativeLayout mainLayout;
    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;
    @BindView(R.id.txtRetry)
    TextView txtRetry;
    @BindView(R.id.no_internetLayout)
    LinearLayout noInternetLayout;
    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    boolean internet;
    ArrayList<Object> defaultAddressList = new ArrayList<Object>();
    String title, type, module, deviceId, token, user_id, slotId, address_id, paymentgatewayId = "", couponCode = "";
    Double carttotalPrice, deliveryCharge, grandTotal,Totalprice;
    UserSessionManager userSessionManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_order_checkout);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Checkout");

        if (getIntent() != null) {
            title = getIntent().getStringExtra(TITLE);
            type = getIntent().getStringExtra(TYPE);
            module = getIntent().getStringExtra(MODULE);
           // position = getIntent().getIntExtra(BOTTAM_TAB_POSITION, 0);

        }

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.KEY_DEVICEID);
        user_id = userDetails.get(UserSessionManager.KEY_ID);
        token = userDetails.get(UserSessionManager.KEY_TOKEN);

        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
            internet = isConnected;
            addressAndDeliveryData();
            noInternetLayout.setVisibility(View.GONE);
        } else {
            internet = false;
            noInternetLayout.setVisibility(View.VISIBLE);
            //Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }

    private void addressAndDeliveryData() {
        progressLayout.setVisibility(View.VISIBLE);
        Call<DeliverSlotResponse> call = RetrofitClient.getInstance().getApi().deliverySlot(token, deviceId, user_id);
        call.enqueue(new Callback<DeliverSlotResponse>() {
            @Override
            public void onResponse(Call<DeliverSlotResponse> call, Response<DeliverSlotResponse> response) {
                progressLayout.setVisibility(View.GONE);

                DeliverSlotResponse loginResponse = response.body();

                if (loginResponse.getStatus().equals("10100")) {

                    if (loginResponse.getData().getAddress() instanceof Boolean) {

                        addressLayout.setVisibility(View.VISIBLE);
                        mainLayout.setVisibility(View.GONE);

                    } else {

                        addressLayout.setVisibility(View.GONE);
                        mainLayout.setVisibility(View.VISIBLE);

                        defaultAddressList = (ArrayList<Object>) loginResponse.getData().getAddress();
                        Map<String, Object> recordPos = (Map<String, Object>) defaultAddressList.get(0);
                        address_id = (String) recordPos.get("id");

                        preOrderCheckOutData(address_id);

                    }

                    // deliverySlot = (ArrayList<Object>) loginResponse.getData().getAvailable_delivery_slots();

                } else if (loginResponse.getStatus().equalsIgnoreCase("10200")) {

                    // progressLayout.setVisibility(View.GONE);
                    Toast.makeText(PreOrderCheckoutActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                } else if (loginResponse.getStatus().equalsIgnoreCase("10400")) {
                    // progressLayout.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure(Call<DeliverSlotResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
            }
        });
    }

    //preOrder CheckOut Data
    private void preOrderCheckOutData(String address_id) {
        progressLayout.setVisibility(View.VISIBLE);
        Call<PreOrderCheckoutDataResponse> checkoutDataResponseCall = RetrofitClient.getInstance().getApi().PreOrderCheckoutData(token, deviceId, user_id,address_id);
        checkoutDataResponseCall.enqueue(new Callback<PreOrderCheckoutDataResponse>() {
            @Override
            public void onResponse(Call<PreOrderCheckoutDataResponse> call, Response<PreOrderCheckoutDataResponse> response) {
                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);
                    PreOrderCheckoutDataResponse preOrderCheckoutDataResponse = response.body();
                    if (preOrderCheckoutDataResponse.getStatus().equals("10100")) {

                        PreOrderCheckoutDataResponse.DataBean dataBean = preOrderCheckoutDataResponse.getData();
                        // List<PreOrderCheckoutDataResponse.DataBean.AddressBean> addressBeanList = dataBean.getAddress();
                        List<PreOrderCheckoutDataResponse.DataBean.ProductsBean> productsBeanList = dataBean.getProducts();
                        List<PreOrderCheckoutDataResponse.DataBean.PaymentGatewayBean> paymentGatewayBeanList = dataBean.getPayment_gateway();

                        if (preOrderCheckoutDataResponse.getData().getAddress() instanceof Boolean) {

                            //  addAddress_txt.setVisibility(View.VISIBLE);
                        } else {

                            //delivery_ll.setVisibility(View.VISIBLE);
                            defaultAddressList = (ArrayList<Object>) preOrderCheckoutDataResponse.getData().getAddress();
                            Map<String, Object> recordPos = (Map<String, Object>) defaultAddressList.get(0);
                            PreOrderCheckoutActivity.this.address_id = (String) recordPos.get("id");
                            String name = (String) recordPos.get("name");
                            String address_line1 = (String) recordPos.get("address_line1");
                            String address_line2 = (String) recordPos.get("address_line2");
                            String area = (String) recordPos.get("area");
                            String city = (String) recordPos.get("city");
                            String state = (String) recordPos.get("state");
                            String pincode = (String) recordPos.get("pincode");
                            String contact_no = (String) recordPos.get("contact_no");
                            String delivery_charges = (String) recordPos.get("delivery_charges");
                            String alternate_contact_no = (String) recordPos.get("alternate_contact_no");
                            boolean delivery_status = (Boolean) recordPos.get("delivery_status");

                            txtName.setText(name);
                            txtAddress.setText(address_line1 + ", " + address_line2 + ", " + area + ", " + city + ", " + state + ", " + pincode + "\n" + "Mobile No : " + contact_no);

                            // delivery Charges
                            if (delivery_charges.equals("0"))
                                txtDeliveryPrice.setText("\u20B9" + String.format("%.2f", delivery_charges));
                            else {
                                Double charge = Double.parseDouble(delivery_charges);
                                txtDeliveryPrice.setText("\u20B9" + String.format("%.2f", charge));
                            }

                            deliveryCharge = Double.parseDouble(delivery_charges);

                        }

                        // cart carttotalPrice
                        txtCartTotal.setText("\u20B9" + String.format("%.2f", Double.parseDouble(String.valueOf(dataBean.getFinalTotal()))));


                        // final price
                        carttotalPrice = Double.valueOf(dataBean.getFinalTotal());
                        grandTotal = deliveryCharge + carttotalPrice;
                        txtTotalPrice.setText("\u20B9" + String.format("%.2f", grandTotal));

                        txtPayblAmount.setText("\u20B9" + String.format("%.2f", grandTotal));


                        // payment gateway
                        PreOrderPaymentTypeAdapter paymentTypeAdapter = new PreOrderPaymentTypeAdapter(PreOrderCheckoutActivity.this, paymentGatewayBeanList, PreOrderCheckoutActivity.this);
                        Paymentrecycler.setHasFixedSize(true);
                        Paymentrecycler.setLayoutManager(new LinearLayoutManager(PreOrderCheckoutActivity.this, LinearLayoutManager.VERTICAL, false));
                        Paymentrecycler.setItemAnimator(new DefaultItemAnimator());
                        Paymentrecycler.setAdapter(paymentTypeAdapter);


                    } else if (preOrderCheckoutDataResponse.getStatus().equals("10200")) {

                        Toast.makeText(PreOrderCheckoutActivity.this, preOrderCheckoutDataResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<PreOrderCheckoutDataResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Toast.makeText(PreOrderCheckoutActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }


    @OnClick({ R.id.btnPay, R.id.txtChange, R.id.btnAddAddress})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.btnPay:
                if (internet) {
                     doPreOrderCheckoutPayment();
                } else {
                    Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
                }
                break;
            case R.id.txtChange:
                Intent intentAddress = new Intent(PreOrderCheckoutActivity.this, AddressListActivity.class);
                intentAddress.putExtra(TITLE, CHANGE_ADDRESS);
                intentAddress.putExtra(MODULE, PRE_ORDER_CART);
                startActivityForResult(intentAddress, 2);
                break;
            case R.id.btnAddAddress:
                Intent intentAddressBtn = new Intent(PreOrderCheckoutActivity.this, AddAddressActivity.class);
                intentAddressBtn.putExtra(TITLE, ADD_ADDRESS);
                intentAddressBtn.putExtra(MODULE, PRE_ORDER_CART);
                startActivity(intentAddressBtn);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
         if (requestCode == 2 && resultCode == RESULT_OK && data != null) {
            address_id = data.getStringExtra("AddressId");
            Log.d(TAG, "onActivityResult: " + address_id);
            preOrderCheckOutData(address_id);

        }

    }


    private void doPreOrderCheckoutPayment() {
        progressLayout.setVisibility(View.VISIBLE);
        Call<PreOrderPlaceResponse> call = RetrofitClient.getInstance().getApi().PreOrderPlace(token,deviceId,user_id,address_id,"",paymentgatewayId,"ANDROID");
        call.enqueue(new Callback<PreOrderPlaceResponse>() {
            @Override
            public void onResponse(Call<PreOrderPlaceResponse> call, Response<PreOrderPlaceResponse> response) {
                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);
                    final PreOrderPlaceResponse placeOrderResponse = response.body();
                    PreOrderPlaceResponse.DataBean dataBean = placeOrderResponse.getData();
                    Log.d(TAG, "onResponse: " + dataBean.getOrderId());
                    if (placeOrderResponse.getStatus().equals("10100")) {

                        // alert Dilogue
                        //before inflating the custom alert dialog layout, we will get the current title viewgroup
                        ViewGroup viewGroup = findViewById(android.R.id.content);
                        //then we will inflate the custom alert dialog xml that we created
                        View dialogView = LayoutInflater.from(PreOrderCheckoutActivity.this).inflate(R.layout.custom_alert_diloge, viewGroup, false);

                        Button btnOk = dialogView.findViewById(R.id.buttonOk);
                        btnOk.setOnClickListener(view -> {
                            Intent intent = new Intent(PreOrderCheckoutActivity.this, OrderDetailsActivity.class);
                            intent.putExtra(ORDER_ID, String.valueOf(dataBean.getOrderId()));
                            intent.putExtra(ORDER_STATUS, "Confirmed");
                            intent.putExtra(TITLE, ORDER_TITLE);
                            intent.putExtra(MODULE,PRE_ORDER_CART);
                            intent.putExtra(ORDER_TAB_POSITION, 0);
                            intent.putExtra(BOTTAM_TAB_POSITION, 3);
                            startActivity(intent);
                            // finish();

                            setCartCount(0);
                        });
                        //Now we need an AlertDialog.Builder object
                        AlertDialog.Builder builder = new AlertDialog.Builder(PreOrderCheckoutActivity.this);

                        //setting the view of the builder to our custom view that we already inflated
                        builder.setView(dialogView);

                        //finally creating the alert dialog and displaying it
                        AlertDialog alertDialog = builder.create();
                        alertDialog.setCancelable(false);
                        alertDialog.show();



                    } else if (placeOrderResponse.getStatus().equals("10300")) {

                        Toast.makeText(PreOrderCheckoutActivity.this, "Unable to process your request..! ", Toast.LENGTH_LONG).show();
                    } else if (placeOrderResponse.getStatus().equals("10400")) {

                        if (paymentgatewayId.equalsIgnoreCase("6")) {
                            Log.d(TAG, "onResponse:" + dataBean.getOrderRefNo());
                            Intent order_detail = new Intent(PreOrderCheckoutActivity.this, PaymentGatewayActivity.class);
                            order_detail.putExtra(ORDER_ID, dataBean.getOrderId());
                            order_detail.putExtra(MODULE,PRE_ORDER_CART);
                            order_detail.putExtra("TOTAL", grandTotal);
                            order_detail.putExtra("ENCRYPTORDERID", dataBean.getEncryptedOrderid());
                            order_detail.putExtra("ORDER_REF_NO", dataBean.getOrderRefNo());
                            order_detail.putExtra(TITLE, ORDER_TITLE);
                            startActivity(order_detail);



                        } else {
                            Intent order_detail = new Intent(PreOrderCheckoutActivity.this, PaymentGatewayActivity.class);
                            order_detail.putExtra("payurl", dataBean.getPaymentUrl());
                            order_detail.putExtra(ORDER_ID, dataBean.getOrderId());
                            order_detail.putExtra(MODULE,PRE_ORDER_CART);
                            order_detail.putExtra("TOTAL", grandTotal);
                            startActivity(order_detail);

                        }
                    } else if (placeOrderResponse.getStatus().equals("10500")) {
                        Toast.makeText(PreOrderCheckoutActivity.this, "10500" + placeOrderResponse.getMessage(), Toast.LENGTH_LONG).show();

                    } else if (placeOrderResponse.getStatus().equals("10600")) {

                        Toast.makeText(PreOrderCheckoutActivity.this, "Coupon is invalid..!", Toast.LENGTH_LONG).show();
                    } else if (placeOrderResponse.getStatus().equals("10700")) {

                        Toast.makeText(PreOrderCheckoutActivity.this, "10700" + placeOrderResponse.getMessage(), Toast.LENGTH_LONG).show();
                    } else if (placeOrderResponse.getStatus().equals("10110")) {

                        Toast.makeText(PreOrderCheckoutActivity.this, "10110" + placeOrderResponse.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<PreOrderPlaceResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Log.d("TAG", "onFailure: " + t.getMessage());
                Toast.makeText(PreOrderCheckoutActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }


    @Override
    public void onItemClick(List<PreOrderCheckoutDataResponse.DataBean.PaymentGatewayBean> paymentGatewayBean, int position) {
        Log.d(TAG, "onItemClick: " + paymentGatewayBean.get(position).getId() + "---" + paymentGatewayBean.get(position).getName());
        paymentgatewayId = paymentGatewayBean.get(position).getId();

        if (paymentgatewayId.equalsIgnoreCase("1")) {
            btnPay.setText("Place Order");
        } else {
            btnPay.setText("Continue");
        }
        btnPay.setEnabled(true);
        btnPay.setAlpha(0.9f);
    }

    @Override
    public void onBackPressed() {
        /*Intent intent = new Intent(PreOrderCheckoutActivity.this, PreOrderCartActivity.class);
        intent.putExtra(BOTTAM_TAB_POSITION, 2);
        startActivity(intent);*/
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
               /* Intent intent = new Intent(PreOrderCheckoutActivity.this, PreOrderCartActivity.class);
                intent.putExtra(BOTTAM_TAB_POSITION, 2);
                startActivity(intent);*/
               finish();

                break;
        }
        return super.onOptionsItemSelected(item);
    }
    private void setCartCount(int count) {
        SharedPreferences preferences = getSharedPreferences(CART_COUNT, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(COUNT, count);
        editor.apply();

        invalidateOptionsMenu();
    }
}
