package com.innasoft.gofarmz.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.Preferences.UserSessionManager;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.PreOrderAddCartResponse;
import com.innasoft.gofarmz.Response.PreOrderProductResponse;
import com.innasoft.gofarmz.Response.PreOrderUpdateCartResponse;
import com.innasoft.gofarmz.Utilis.Converter;
import com.innasoft.gofarmz.Utilis.Util;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Utilis.Constants.CART_COUNT;
import static com.innasoft.gofarmz.Utilis.Constants.COUNT;
import static com.innasoft.gofarmz.Utilis.Constants.PRE_ORDER_ID;

public class PreOrderProductViewActivity extends AppCompatActivity implements InternetConnectivityListener {
    @BindView(R.id.product_img)
    ImageView productImg;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.txtDescription)
    TextView txtDescription;
    @BindView(R.id.exp_date_txt)
    TextView expDateTxt;
    @BindView(R.id.txtDate)
    TextView txtDate;
    @BindView(R.id.available_units_txt)
    TextView availableUnitsTxt;
    @BindView(R.id.txtAvalibleUnits)
    TextView txtAvalibleUnits;
    @BindView(R.id.price_units_txt)
    TextView priceUnitsTxt;
    @BindView(R.id.txtUnitPrice)
    TextView txtUnitPrice;
    @BindView(R.id.unit_value_txt)
    TextView unitValueTxt;
    @BindView(R.id.txtUnits)
    TextView txtUnits;
    @BindView(R.id.txtDecrease)
    TextView txtDecrease;
    @BindView(R.id.txtQty)
    TextView txtQty;
    @BindView(R.id.txtIncrease)
    TextView txtIncrease;
    @BindView(R.id.txtPrice)
    TextView txtPrice;
    @BindView(R.id.btnUpdateToCart)
    Button btnUpdateToCart;
    @BindView(R.id.btnAddToCart)
    Button btnAddToCart;
    @BindView(R.id.btnViewCart)
    Button btnViewCart;
    @BindView(R.id.bottomLayout)
    LinearLayout bottomLayout;
    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;
    @BindView(R.id.txtRetry)
    TextView txtRetry;
    @BindView(R.id.no_internetLayout)
    LinearLayout noInternetLayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    UserSessionManager userSessionManager;
    boolean internet;
    String deviceId, user_id, token, preOrderId;
    float totalAmount = 0;
    float unitPlusminus = 0;
    float maxavaliblUnits = 0;
    int cartCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_order_product_view);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Pre Orders Details");
        if (getIntent() != null) {
            preOrderId = getIntent().getStringExtra(PRE_ORDER_ID);

        }

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.KEY_DEVICEID);
        user_id = userDetails.get(UserSessionManager.KEY_ID);
        token = userDetails.get(UserSessionManager.KEY_TOKEN);

        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
            getPreOrderViewData();
            noInternetLayout.setVisibility(View.GONE);
        } else {
            // internet = false;
            noInternetLayout.setVisibility(View.VISIBLE);
            Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }

    private void getPreOrderViewData() {
        progressLayout.setVisibility(View.VISIBLE);
        Call<PreOrderProductResponse> call = RetrofitClient.getInstance().getApi().PreOrderProducts(token, deviceId, preOrderId, user_id);
        call.enqueue(new Callback<PreOrderProductResponse>() {
            @Override
            public void onResponse(Call<PreOrderProductResponse> call, Response<PreOrderProductResponse> response) {
                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);
                    PreOrderProductResponse preOrderProductResponse = response.body();
                    if (preOrderProductResponse.getStatus().equals("10100")) {


                        PreOrderProductResponse.DataBean dataBean = preOrderProductResponse.getData();

                        preOrderId = dataBean.getProduct_id();

                        Glide.with(PreOrderProductViewActivity.this)
                                .load(dataBean.getPop_image())
                                .into(productImg);

                        txtTitle.setText(dataBean.getProduct_name());
                        txtDescription.setText(dataBean.getPop_description());
                        txtDate.setText(dataBean.getPop_expected_delivery_date());
                        txtAvalibleUnits.setText(dataBean.getPop_available_units());
                        txtUnits.setText(dataBean.getPop_unit_value());
                        txtUnitPrice.setText("\u20B9 " + dataBean.getPop_unit_price());

                               /* String price = preOrderViewResponse.getData().getPopUnitPrice();
                                Double d = Double.parseDouble(price);
                                txtPrice.setText("" + Math.round(d) + "/-");*/

                        totalAmount = Float.valueOf(dataBean.getPop_unit_price());
                        maxavaliblUnits = Float.valueOf(dataBean.getPop_available_units());
                        unitPlusminus = Float.valueOf(dataBean.getQuantity());
                        txtQty.setText("" + Math.round(unitPlusminus));

                        double result = Math.round(totalAmount * unitPlusminus);
                        String priceResult = String.format("%.2f", result);
                        txtPrice.setText("\u20B9" + priceResult);

                        // cart count label
                        cartCount = preOrderProductResponse.getCart_count();
                        Log.d(TAG, "onResponse: "+cartCount);


                        if (unitPlusminus == 0) {
                            btnAddToCart.setEnabled(false);
                            btnViewCart.setEnabled(false);
                            txtDecrease.setEnabled(false);
                            btnAddToCart.setAlpha(.5f);
                            btnViewCart.setAlpha(.5f);
                            txtDecrease.setAlpha(.5f);

                        } else {
                            btnAddToCart.setText("UPDATE CART");
                            btnAddToCart.setEnabled(true);
                            btnViewCart.setEnabled(true);
                            txtDecrease.setEnabled(true);
                        }

                        setCartCount(cartCount);

                    }

                    if (preOrderProductResponse.getStatus().equals("10200")) {

                        Toast.makeText(PreOrderProductViewActivity.this, preOrderProductResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    if (preOrderProductResponse.getStatus().equals("10300")) {

                        Toast.makeText(PreOrderProductViewActivity.this, preOrderProductResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    if (preOrderProductResponse.getStatus().equals("10400")) {
                        Toast.makeText(PreOrderProductViewActivity.this, preOrderProductResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }


                }
            }

            @Override
            public void onFailure(Call<PreOrderProductResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Toast.makeText(PreOrderProductViewActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @OnClick({R.id.txtDecrease, R.id.txtIncrease, R.id.btnAddToCart, R.id.btnViewCart})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnViewCart:
                addToCart(preOrderId);
                Intent i1 = new Intent(PreOrderProductViewActivity.this, PreOrderCartActivity.class);
                startActivity(i1);
                break;

            case R.id.txtDecrease:

                float avalibleUnits2 = Float.parseFloat(txtAvalibleUnits.getText().toString());

                if (unitPlusminus >= 1) {

                    unitPlusminus = unitPlusminus - 1;
                    txtQty.setText("" + Math.round(unitPlusminus));

                    if (avalibleUnits2 <= maxavaliblUnits) {

                        avalibleUnits2 = avalibleUnits2 + 1;
                        txtAvalibleUnits.setText("" + Math.round(avalibleUnits2));


                    }


                }
                double result1 = Math.round(totalAmount * unitPlusminus);
                String priceResult1 = String.format("%.2f", result1);
                txtPrice.setText("\u20B9" + priceResult1);

                if (txtQty.getText().equals("0")) {
                    btnAddToCart.setEnabled(false);
                    btnViewCart.setEnabled(false);
                    txtDecrease.setEnabled(false);

                    btnAddToCart.setAlpha(.5f);
                    btnViewCart.setAlpha(.5f);
                    txtDecrease.setAlpha(.5f);
                }

                break;

            case R.id.txtIncrease:

                float avalibleUnits = Float.parseFloat(txtAvalibleUnits.getText().toString());

                if (avalibleUnits > 0) {

                    unitPlusminus = unitPlusminus + 1;
                    txtQty.setText("" + Math.round(unitPlusminus));

                    avalibleUnits = avalibleUnits - 1;
                    txtAvalibleUnits.setText("" + Math.round(avalibleUnits));

                    double result = Math.round(totalAmount * unitPlusminus);
                    String priceResult = String.format("%.2f", result);
                    txtPrice.setText("\u20B9" + priceResult);

                    btnAddToCart.setEnabled(true);
                    btnViewCart.setEnabled(true);
                    txtDecrease.setEnabled(true);
                    btnAddToCart.setAlpha(.9f);
                    btnViewCart.setAlpha(.9f);
                    txtDecrease.setAlpha(.9f);
                }

                break;


            case R.id.btnAddToCart:

                if (btnAddToCart.getText().toString().equalsIgnoreCase("UPDATE CART")) {
                    updateCart(preOrderId);
                } else if (btnAddToCart.getText().toString().equalsIgnoreCase("ADD TO CART")) {
                    addToCart(preOrderId);
                }

                break;
        }
    }

    private void addToCart(final String pr_id) {

        progressBar.setVisibility(View.VISIBLE);
        Call<PreOrderAddCartResponse> cartResponseCall = RetrofitClient.getInstance().getApi().PreOrderAddCart(token, deviceId, user_id, pr_id, txtQty.getText().toString());
        cartResponseCall.enqueue(new Callback<PreOrderAddCartResponse>() {
            @Override
            public void onResponse(Call<PreOrderAddCartResponse> call, Response<PreOrderAddCartResponse> response) {
                if (response.isSuccessful()) {
                    PreOrderAddCartResponse preOrderAddCartResponse = response.body();
                    if (preOrderAddCartResponse.getStatus().equals("10100")) {
                        progressBar.setVisibility(View.GONE);
                        cartCount = preOrderAddCartResponse.getCart_count();

                        setCartCount(cartCount);
                        //getCartCount();
                        btnAddToCart.setText("UPDATE CART");

                        Toast.makeText(PreOrderProductViewActivity.this, preOrderAddCartResponse.getMessage(), Toast.LENGTH_LONG).show();

                    }
                    if (preOrderAddCartResponse.getStatus().equalsIgnoreCase("10200")) {

                        Toast.makeText(PreOrderProductViewActivity.this, preOrderAddCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    if (preOrderAddCartResponse.getStatus().equalsIgnoreCase("10300")) {

                        Toast.makeText(PreOrderProductViewActivity.this, preOrderAddCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    if (preOrderAddCartResponse.getStatus().equalsIgnoreCase("10400")) {

                        Toast.makeText(PreOrderProductViewActivity.this, preOrderAddCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<PreOrderAddCartResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(PreOrderProductViewActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void updateCart(final String pr_id) {

        progressBar.setVisibility(View.VISIBLE);

        Call<PreOrderUpdateCartResponse> cartResponseCall = RetrofitClient.getInstance().getApi().PreOrderUpdateCart(token, deviceId, user_id, pr_id, txtQty.getText().toString());
        cartResponseCall.enqueue(new Callback<PreOrderUpdateCartResponse>() {
            @Override
            public void onResponse(Call<PreOrderUpdateCartResponse> call, Response<PreOrderUpdateCartResponse> response) {
                if (response.isSuccessful()) {
                    progressBar.setVisibility(View.GONE);
                    PreOrderUpdateCartResponse preOrderUpdateCartResponse = response.body();
                    if (preOrderUpdateCartResponse.getStatus().equals("10100")) {

                        //getCartCount();
                        btnAddToCart.setText("UPDATE CART");
                        setCartCount(preOrderUpdateCartResponse.getCart_count());
                        Toast.makeText(PreOrderProductViewActivity.this, preOrderUpdateCartResponse.getMessage(), Toast.LENGTH_LONG).show();

                    }
                    if (preOrderUpdateCartResponse.getStatus().equalsIgnoreCase("10200")) {

                        Toast.makeText(PreOrderProductViewActivity.this, preOrderUpdateCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    if (preOrderUpdateCartResponse.getStatus().equalsIgnoreCase("10300")) {

                        Toast.makeText(PreOrderProductViewActivity.this, preOrderUpdateCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    if (preOrderUpdateCartResponse.getStatus().equalsIgnoreCase("10400")) {

                        Toast.makeText(PreOrderProductViewActivity.this, preOrderUpdateCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<PreOrderUpdateCartResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(PreOrderProductViewActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getPreOrderViewData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.preorder_menu, menu);

        final MenuItem menuItem = menu.findItem(R.id.cart);

        SharedPreferences preferences = getSharedPreferences(CART_COUNT,0);
        int count = preferences.getInt(COUNT,0);
        Log.d(TAG, "onCreateOptionsMenu: "+count);
        menuItem.setIcon(Converter.convertLayoutToImage(PreOrderProductViewActivity.this, count, R.drawable.ic_shopping_cart_black_24dp));
        return super.onCreateOptionsMenu(menu);
    }



    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.cart:
                Log.d(TAG, "onOptionsItemSelected: "+cartCount);
                if (cartCount == 0) {
                    Util.snackBar(progressLayout, "No Items Found !", Color.YELLOW);
                } else {
                    Intent i = new Intent(PreOrderProductViewActivity.this, PreOrderCartActivity.class);
                    startActivity(i);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void setCartCount(int count){
        SharedPreferences preferences = getSharedPreferences(CART_COUNT,0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(COUNT,count);
        editor.apply();

        invalidateOptionsMenu();
    }
}
