package com.innasoft.gofarmz.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.innasoft.gofarmz.Adapter.FBHAdpter;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.models.FBhItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.innasoft.gofarmz.Utilis.Constants.BOTTAM_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;
import static com.innasoft.gofarmz.Utilis.Constants.TYPE;

public class VirtualShopsActivity extends AppCompatActivity {
    @BindView(R.id.gvsRecycler)
    RecyclerView gvsRecycler;
    private List<FBhItem> gvsItems = new ArrayList<>();
    String title, type;
    int position;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_virtual_shops);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        if (getIntent() != null) {
            title = getIntent().getStringExtra(TITLE);
            type = getIntent().getStringExtra(TYPE);
            position = getIntent().getIntExtra(BOTTAM_TAB_POSITION,0);
            getSupportActionBar().setTitle(title);
        }
        gvsList();
    }

    private void gvsList() {
        RecyclerView.LayoutManager manager = new GridLayoutManager(VirtualShopsActivity.this, 3);
        gvsRecycler.setLayoutManager(manager);
        FBHAdpter adapter = new FBHAdpter(gvsItems, VirtualShopsActivity.this);
        gvsRecycler.setAdapter(adapter);

        FBhItem fBhItem = new FBhItem("https://www.ashianahousing.com/assets/images/homeall.jpg", "Manmadha shop");
        gvsItems.add(fBhItem);

        fBhItem = new FBhItem("https://assets-news.housing.com/news/wp-content/uploads/2018/02/24194719/The-pros-and-cons-of-gated-communities-and-standalone-buildings-FB-1200x628-compressed.jpg", "Madhava Shop");
        gvsItems.add(fBhItem);

        fBhItem = new FBhItem("https://www.myhomebhooja.com/img/banner2.png", "Mahesh shop");
        gvsItems.add(fBhItem);

        fBhItem = new FBhItem("https://c8.alamy.com/comp/MCX785/hyderabad-india-december-202016-children-at-play-area-inside-a-gated-community-with-highrise-buildings-in-hyderabadindia-MCX785.jpg", "Ramesh shop");
        gvsItems.add(fBhItem);

        fBhItem = new FBhItem("https://www.track2realty.track2media.com/wp-content/uploads/2016/03/Sobha-Group-launches-Sobha-Hartland-in-Dubai-high-rise-apartments.jpg", "Reddy shop");
        gvsItems.add(fBhItem);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(VirtualShopsActivity.this,HomeActivity.class);
        intent.putExtra(BOTTAM_TAB_POSITION,position);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(VirtualShopsActivity.this,HomeActivity.class);
                intent.putExtra(BOTTAM_TAB_POSITION,position);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
