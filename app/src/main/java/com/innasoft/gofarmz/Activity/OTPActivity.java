package com.innasoft.gofarmz.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.Preferences.UserSessionManager;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.BaseResponse;
import com.innasoft.gofarmz.Response.LoginResponse;
import com.innasoft.gofarmz.Utilis.Constants;
import com.innasoft.gofarmz.Utilis.Util;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.aabhasjindal.otptextview.OTPListener;
import in.aabhasjindal.otptextview.OtpTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Utilis.Constants.MOBILE_KEY;
import static com.innasoft.gofarmz.Utilis.Constants.BOTTAM_TAB_POSITION;

public class OTPActivity extends Activity implements InternetConnectivityListener {

    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;
    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    boolean internet;
    String deviceId, refreshedToken, otpText, mobile;
    UserSessionManager userSessionManager;

    @BindView(R.id.logo_img)
    ImageView logoImg;
    @BindView(R.id.textView)
    TextView textView;
    @BindView(R.id.otp_view)
    OtpTextView otpView;
    @BindView(R.id.verify_btn)
    Button verifyBtn;
    @BindView(R.id.txtresend)
    TextView txtresend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        ButterKnife.bind(this);
        progressLayout.setVisibility(View.GONE);

        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);

        // device Id
        deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        userSessionManager = new UserSessionManager(this);
       /* HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);*/

        //progressIndicator.start();
        if (getIntent() != null) {
            mobile = getIntent().getStringExtra(MOBILE_KEY);

            char first = mobile.charAt(0);
            String substring = mobile.substring(Math.max(mobile.length() - 2, 0));
            textView.setText("Please type the verification code sent to\n +91" + first + "XXXXXXX" + substring);
        }

        otpView.setOtpListener(new OTPListener() {
            @Override
            public void onInteractionListener() {
                // fired when user types something in the Otpbox
            }

            @Override
            public void onOTPComplete(String otp) {
                // fired when user has entered the OTP fully.
                otpText = otp;
            }
        });


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
            internet = isConnected;

        } else {
            internet = false;
            Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }

    @OnClick({R.id.verify_btn, R.id.txtresend})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.verify_btn:
                if (internet){
                    if ((otpText == null || otpText.equals("")) || otpText.length() != 6) {
                        Util.snackBar(progressLayout, "Enter OTP to Verify", Color.YELLOW);
                        return;
                    } else {
                        otpVerify();
                    }
                }else {

                    Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
                }
                break;
            case R.id.txtresend:
                if (internet){
                   resendOtp();
                }else {
                    Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
                }
                break;
        }
    }

    private void otpVerify() {
        progressLayout.setVisibility(View.VISIBLE);
        Call<LoginResponse> call = RetrofitClient.getInstance().getApi().LoginMobileWithOTP(mobile, deviceId, otpText);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);
                    LoginResponse verifyOtpResponse = response.body();

                    if (verifyOtpResponse.getStatus().equals("10100")) {
                        Toast.makeText(getApplicationContext(), "You have been logged in successfully.", Toast.LENGTH_SHORT).show();

                        Log.d(TAG, "onResponse: " + verifyOtpResponse.getData().getUserId());

                        userSessionManager.createLogin(verifyOtpResponse.getData().getUserId(),
                                verifyOtpResponse.getData().getUserName()
                                , verifyOtpResponse.getData().getEmail()
                                , verifyOtpResponse.getData().getMobile(),null,
                                deviceId,verifyOtpResponse.getData().getJwt(),verifyOtpResponse.getData().getGender());


                        Intent intent = new Intent(OTPActivity.this, HomeActivity.class);
                        intent.putExtra(BOTTAM_TAB_POSITION,0);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();

                        setDefaults();
                        displayFirebaseRegId(verifyOtpResponse.getData().getUserId(), verifyOtpResponse.getData().getJwt());





                    } else if (verifyOtpResponse.getStatus().equals("10200")) {

                        Toast.makeText(OTPActivity.this, verifyOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (verifyOtpResponse.getStatus().equals("10300")) {
                        Toast.makeText(OTPActivity.this, verifyOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (verifyOtpResponse.getStatus().equals("10400")) {
                        Toast.makeText(OTPActivity.this, verifyOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(getApplicationContext(), jObjError.getJSONObject("message").getString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Toast.makeText(OTPActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
    
    private void resendOtp(){
        progressLayout.setVisibility(View.VISIBLE);
        Call<BaseResponse> call1 = RetrofitClient.getInstance().getApi().ResendOtpRequest(deviceId, mobile);
        call1.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);
                    BaseResponse resendOtpResponse = response.body();

                    if (resendOtpResponse.getStatus().equals("10100")) {

                        Toast.makeText(OTPActivity.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (resendOtpResponse.getStatus().equals("10200")) {

                        Toast.makeText(OTPActivity.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (resendOtpResponse.getStatus().equals("10300")) {

                        Toast.makeText(OTPActivity.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (resendOtpResponse.getStatus().equals("10400")) {

                        Toast.makeText(OTPActivity.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(getApplicationContext(), jObjError.getJSONObject("message").getString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Toast.makeText(OTPActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();


            }
        });
    }


    public void setDefaults() {
        SharedPreferences preferences = getSharedPreferences(Constants.PREFS_LOCATION, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Constants.KEY_LOCATIONFIRSTTIME, true);
        editor.apply();
    }

    private void displayFirebaseRegId(String user_id, String jwt) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Constants.SHARED_PREF, 0);
        // String regId = pref.getString("regId", null);
        // Fcm Token
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("TAG", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        refreshedToken = task.getResult().getToken();

                        // Log and toast

                        Log.d("TAG", refreshedToken);
                        // Toast.makeText(MainActivity.this, refreshedToken, Toast.LENGTH_SHORT).show();

                        Log.w("notificationToken", "" + refreshedToken);
                        if (!TextUtils.isEmpty(refreshedToken)) {
                            //Toast.makeText(getApplicationContext(), "Firebase Reg Id: " + regId, Toast.LENGTH_LONG).show();
                            Log.d("FCM", refreshedToken);
                            sendFCMTokes(user_id, refreshedToken, "ANDROID", jwt);
                        } else {
                            Toast.makeText(getApplicationContext(), "Firebase Reg Id is not received yet!", Toast.LENGTH_LONG).show();
                        }

                    }
                });


    }

    public void sendFCMTokes(final String user_id, final String token_fcm, final String platform, String jwt) {

            Call<BaseResponse> call = RetrofitClient.getInstance().getApi().sendFcmToken(jwt, deviceId, user_id, token_fcm);
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    BaseResponse loginResponse = response.body();
                    Log.d("Tag", "onResponseFCM: " + loginResponse.getStatus());


                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });



        }
}

