package com.innasoft.gofarmz.Activity;

import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro;
import com.innasoft.gofarmz.Fragments.ScreenFragment1;
import com.innasoft.gofarmz.Fragments.ScreenFragment2;
import com.innasoft.gofarmz.Fragments.ScreenFragment3;
import com.innasoft.gofarmz.Fragments.ScreenFragment4;
import com.innasoft.gofarmz.Preferences.PrefManager;

public class AppIntroActivity extends AppIntro {
    private PrefManager prefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_app_intro);

        // Checking for first time launch - before calling setContentView()
        prefManager = new PrefManager(this);
        if (!prefManager.isFirstTimeLaunch()) {
            launchHomeScreen();
            finish();
        }


        addSlide(new ScreenFragment1());
        addSlide(new ScreenFragment2());
        addSlide(new ScreenFragment3());
        addSlide(new ScreenFragment4());
       /* addSlide(AppIntroFragment.newInstance("First App Into", "First App Intro Details",
                R.drawable.ic_app_icon, ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary)));
        addSlide(AppIntroFragment.newInstance("Second App Into", "Second App Intro Details",
                R.drawable.ic_app_icon, ContextCompat.getColor(getApplicationContext(), R.color.colorAccent)));
        addSlide(AppIntroFragment.newInstance("Third App Into", "Third App Intro Details",
                R.drawable.ic_app_icon, ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryDark)));
*/
        setBarColor(Color.parseColor("#0f9350"));
       // setColorDoneText(Color.MAGENTA);
       // setColorSkipButton(Color.MAGENTA);
       // setNextArrowColor(Color.MAGENTA);
        //setSeparatorColor(Color.parseColor("#2196F3"));

    }

    private void launchHomeScreen() {
        prefManager.setFirstTimeLaunch(false);
        startActivity(new Intent(AppIntroActivity.this, SplashActivity.class));
        finish();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
       /* Intent intent=new Intent(getApplicationContext(),SplashActivity.class);
        startActivity(intent);
        finish();*/

       launchHomeScreen();
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        /*Intent intent=new Intent(getApplicationContext(),SplashActivity.class);
        startActivity(intent);
        finish();*/
        launchHomeScreen();
    }
}
