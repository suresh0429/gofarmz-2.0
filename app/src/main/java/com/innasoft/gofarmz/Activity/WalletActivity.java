package com.innasoft.gofarmz.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputLayout;
import com.innasoft.gofarmz.Adapter.PaginationWalletAdapter;
import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.Preferences.UserSessionManager;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.WalletAmountResponse;
import com.innasoft.gofarmz.Response.WalletTransresponse;
import com.innasoft.gofarmz.Response.WalletgetResponse;
import com.innasoft.gofarmz.Utilis.PaginationScrollListener;
import com.innasoft.gofarmz.Utilis.Util;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Utilis.Constants.BOTTAM_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.MODULE;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_ID;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;
import static com.innasoft.gofarmz.Utilis.Constants.TYPE;

public class WalletActivity extends AppCompatActivity implements InternetConnectivityListener {

    @BindView(R.id.txtRetry)
    TextView txtRetry;
    @BindView(R.id.no_internetLayout)
    LinearLayout noInternetLayout;
    @BindView(R.id.walletEmptyLayout)
    LinearLayout walletEmptyLayout;
    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    boolean internet;
    String title, type;
    int position;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token, user_name, mobile;

    PaginationWalletAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    // limiting to 5 for this tutorial, since carttotalPrice pages in actual API is very large. Feel free to modify.
    private int TOTAL_PAGES = 5;
    private int currentPage = PAGE_START;

    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;
    @BindView(R.id.txtName)
    TextView txtName;
    @BindView(R.id.txtAmount)
    TextView txtAmount;
    @BindView(R.id.txtAddAmount)
    TextView txtAddAmount;
    @BindView(R.id.transactionsList)
    RecyclerView transactionsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Wallet");
        if (getIntent() != null) {
            title = getIntent().getStringExtra(TITLE);
            type = getIntent().getStringExtra(TYPE);
            position = getIntent().getIntExtra(BOTTAM_TAB_POSITION, 0);

        }
        progressLayout.setVisibility(View.GONE);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.KEY_DEVICEID);
        user_id = userDetails.get(UserSessionManager.KEY_ID);
        token = userDetails.get(UserSessionManager.KEY_TOKEN);
        user_name = userDetails.get(UserSessionManager.KEY_NAME);
        mobile = userDetails.get(UserSessionManager.KEY_MOBILE);


        adapter = new PaginationWalletAdapter(this);

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        transactionsList.setLayoutManager(linearLayoutManager);

        transactionsList.setItemAnimator(new DefaultItemAnimator());

        transactionsList.setAdapter(adapter);

        transactionsList.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                // mocking network delay for API call
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadNextPage();
                    }
                }, 1000);
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });


        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);

        txtName.setText("Hi ! " + user_name + " your Wallet Balance");


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
            internet = isConnected;

            // doApiCall();
            getWalletAmount();
            loadFirstPage();

            noInternetLayout.setVisibility(View.GONE);

        } else {
            internet = false;
            noInternetLayout.setVisibility(View.VISIBLE);
            // Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }

    @OnClick(R.id.txtAddAmount)
    public void onViewClicked() {
        if (internet) {
            addmoneyDiloge();
        } else {
            Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }

    private void getWalletAmount() {

        progressLayout.setVisibility(View.VISIBLE);

        Call<WalletAmountResponse> call = RetrofitClient.getInstance().getApi().getWalletAmount(token, deviceId, user_id);
        call.enqueue(new Callback<WalletAmountResponse>() {
            @Override
            public void onResponse(Call<WalletAmountResponse> call, Response<WalletAmountResponse> response) {
                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);
                    WalletAmountResponse addressGetResponse = response.body();
                    if (addressGetResponse.getStatus().equals("10100")) {

                        if (addressGetResponse.getData().getAmount() != null) {
                            txtAmount.setText("\u20B9 " + addressGetResponse.getData().getAmount());
                        } else {
                            txtAmount.setText("\u20B9 " + "0.00");
                        }

                    }

                    if (addressGetResponse.getStatus().equals("10200")) {

                        Toast.makeText(getApplicationContext(), addressGetResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    if (addressGetResponse.getStatus().equals("10300")) {

                        Toast.makeText(getApplicationContext(), addressGetResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(getApplicationContext(), jObjError.getJSONObject("message").getString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<WalletAmountResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Toast.makeText(WalletActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    private void loadFirstPage() {
        Log.d(TAG, "loadFirstPage: ");

        callWalletTransactionsApi().enqueue(new Callback<WalletgetResponse>() {
            @Override
            public void onResponse(Call<WalletgetResponse> call, Response<WalletgetResponse> response) {
                // Got data. Send it to adapter

                if (response.isSuccessful()) {
                    WalletgetResponse walletgetResponse = response.body();

                    if (walletgetResponse.getStatus().equalsIgnoreCase("10100")) {
                        List<WalletTransresponse> results = fetchResults(response);
                        progressLayout.setVisibility(View.GONE);
                        adapter.addAll(results);



                        if (currentPage <= TOTAL_PAGES) adapter.addLoadingFooter();
                        else isLastPage = true;
                        adapter.removeLoadingFooter();
                    } else if (walletgetResponse.getStatus().equalsIgnoreCase("10300")) {
                        progressLayout.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), walletgetResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }

            }

            @Override
            public void onFailure(Call<WalletgetResponse> call, Throwable t) {
                t.printStackTrace();
                // TODO: 08/11/16 handle failure
                walletEmptyLayout.setVisibility(View.VISIBLE);
            }
        });

    }

    /**
     * @param response extracts List<{@link WalletTransresponse >} from response
     * @return
     */
    private List<WalletTransresponse> fetchResults(Response<WalletgetResponse> response) {
        WalletgetResponse topRatedMovies = response.body();
        return topRatedMovies.getData();
    }

    private void loadNextPage() {
        Log.d(TAG, "loadNextPage: " + currentPage);

        callWalletTransactionsApi().enqueue(new Callback<WalletgetResponse>() {
            @Override
            public void onResponse(Call<WalletgetResponse> call, Response<WalletgetResponse> response) {

                if (response.isSuccessful()) {
                    WalletgetResponse walletgetResponse = response.body();

                    if (walletgetResponse.getStatus().equalsIgnoreCase("10100")) {
                        adapter.removeLoadingFooter();
                        isLoading = false;

                        List<WalletTransresponse> results = fetchResults(response);
                        adapter.addAll(results);

                        if (currentPage != TOTAL_PAGES) adapter.addLoadingFooter();
                        else isLastPage = true;


                    } else if (walletgetResponse.getStatus().equalsIgnoreCase("10300")) {

                        adapter.removeLoadingFooter();
                        isLoading = false;
                        walletEmptyLayout.setVisibility(View.VISIBLE);
                    }

                }

            }

            @Override
            public void onFailure(Call<WalletgetResponse> call, Throwable t) {
                t.printStackTrace();
                adapter.removeLoadingFooter();
                // TODO: 08/11/16 handle failure
                Log.d(TAG, "onFailure: " + t.getMessage());

            }
        });
    }


    /**
     * Performs a Retrofit call to the top rated movies API.
     * Same API call for Pagination.
     * As {@link #currentPage} will be incremented automatically
     * by @{@link PaginationScrollListener} to load next page.
     */
    private Call<WalletgetResponse> callWalletTransactionsApi() {
        return RetrofitClient.getInstance().getApi().getWalletTransactions(token, deviceId, user_id, currentPage);
    }


    private void addmoneyDiloge() {

        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(WalletActivity.this);
        View sheetView = WalletActivity.this.getLayoutInflater().inflate(R.layout.custom_money_dialog, null);

        final EditText editText = sheetView.findViewById(R.id.edt_comment);
        final ImageView closeImage = sheetView.findViewById(R.id.closeImage);
        final TextView txtClear = sheetView.findViewById(R.id.txtClear);
        final TextInputLayout txtinput = sheetView.findViewById(R.id.txtInput);
        Button button = (Button) sheetView.findViewById(R.id.buttonSubmit);

        closeImage.setOnClickListener(view -> {
            mBottomSheetDialog.dismiss();
        });
        txtClear.setOnClickListener(view -> mBottomSheetDialog.dismiss());

        button.setOnClickListener(view -> {
            // DO SOMETHINGS

            if (!editText.getText().toString().isEmpty()) {

                String totalvalue = editText.getText().toString();

                Intent order_detail = new Intent(WalletActivity.this, PaymentGatewayActivity.class);

                order_detail.putExtra(ORDER_ID, 0);
                order_detail.putExtra("TOTAL", Double.parseDouble(totalvalue));
                order_detail.putExtra("ENCRYPTORDERID", "");
                order_detail.putExtra("ORDER_REF_NO", "");
                order_detail.putExtra(TITLE, "WalletActivity");
                order_detail.putExtra(MODULE, "WalletActivity");
                order_detail.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(order_detail);

                mBottomSheetDialog.dismiss();
            } else {
                txtinput.setError("Please Enter Amount");
                // Toast.makeText(WalletActivity.this, "Please Enter Amount", Toast.LENGTH_SHORT).show();
            }

        });
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();


    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(WalletActivity.this, HomeActivity.class);
        intent.putExtra(BOTTAM_TAB_POSITION, position);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(WalletActivity.this, HomeActivity.class);
                intent.putExtra(BOTTAM_TAB_POSITION, position);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
