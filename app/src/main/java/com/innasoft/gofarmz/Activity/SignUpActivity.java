package com.innasoft.gofarmz.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.Constraints;

import com.google.android.material.textfield.TextInputEditText;
import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.BaseResponse;
import com.innasoft.gofarmz.Response.LoginMobileResponse;
import com.innasoft.gofarmz.Utilis.Util;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.innasoft.gofarmz.Utilis.Constants.MOBILE_KEY;

public class SignUpActivity extends Activity implements InternetConnectivityListener {
    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    boolean internet;
    String deviceId, mobile, terms;

    @BindView(R.id.logoimage)
    RelativeLayout logoimage;
    @BindView(R.id.close_img)
    ImageView closeImg;
    @BindView(R.id.linear_mobile)
    LinearLayout linearMobile;
    @BindView(R.id.mobile_edt)
    TextInputEditText mobileEdt;
    @BindView(R.id.name_edt)
    TextInputEditText nameEdt;
    @BindView(R.id.email_edt)
    TextInputEditText emailEdt;
    @BindView(R.id.t_c_chk)
    CheckBox tCChk;
    @BindView(R.id.t_c_textview)
    TextView tCTextview;
    @BindView(R.id.term_conditon_layout)
    RelativeLayout termConditonLayout;
    @BindView(R.id.btn_signup)
    Button btnSignup;

    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        progressLayout.setVisibility(View.GONE);

        if (getIntent() != null) {
            mobile = getIntent().getStringExtra(MOBILE_KEY);
            mobileEdt.setText(mobile);
        }

        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);

        // device Id
        deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        btnSignup.setEnabled(false);
        btnSignup.setAlpha(0.5f);
        btnSignup.setClickable(false);
        terms = "";
        tCChk.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                btnSignup.setEnabled(true);
                btnSignup.setAlpha(0.9f);
                btnSignup.setClickable(true);
                terms = "terms";
            } else {
                btnSignup.setEnabled(false);
                btnSignup.setAlpha(0.5f);
                btnSignup.setClickable(false);
                terms = "";
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(Constraints.TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
            internet = isConnected;
        } else {
            internet = false;
            Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }

    @OnClick({R.id.close_img, R.id.btn_signup})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.close_img:
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_signup:
                String mobile = mobileEdt.getText().toString();
                String name = nameEdt.getText().toString();
                String email = emailEdt.getText().toString();

                if (internet) {
                    Log.d(Constraints.TAG, "onInternetConnectivityChanged: " + internet);
                    if (name.isEmpty()) {
                        nameEdt.setError("Enter Your Name");
                        return;
                    }

                    if (email.isEmpty()) {
                        emailEdt.setError("Enter Your Email");
                        return;
                    }
                    userRegistration(name, email, mobile);
                } else {
                    Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);

                }

                break;
        }
    }


    private void userRegistration(String name, String email, String mobile) {
        progressLayout.setVisibility(View.VISIBLE);
        Call<BaseResponse> call = RetrofitClient.getInstance().getApi().userRegistrationRequest(deviceId, name, email, mobile, "");
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);
                    BaseResponse registerResponse = response.body();
                    if (registerResponse.getStatus().equalsIgnoreCase("10100")) {

                        otpRequest(mobile);

                    } else if (registerResponse.getStatus().equals("10200")) {

                        Util.snackBar(progressLayout, registerResponse.getMessage(), Color.RED);
                    } else if (registerResponse.getStatus().equals("10300")) {

                        Util.snackBar(progressLayout, registerResponse.getMessage(), Color.RED);
                    } else if (registerResponse.getStatus().equals("10400")) {

                    }
                }else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(getApplicationContext(), jObjError.getJSONObject("message").getString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }


            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Util.snackBar(progressLayout, t.getMessage(), Color.RED);
            }
        });
    }

    private void otpRequest(String mobile) {
        progressLayout.setVisibility(View.VISIBLE);
        Call<LoginMobileResponse> call = RetrofitClient.getInstance().getApi().LoginMobile(mobile, deviceId);
        call.enqueue(new Callback<LoginMobileResponse>() {
            @Override
            public void onResponse(Call<LoginMobileResponse> call, Response<LoginMobileResponse> response) {
                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);
                    LoginMobileResponse loginMobileResponse = response.body();
                    if (loginMobileResponse.getStatus().equals("10100")) {
                        Toast.makeText(SignUpActivity.this, loginMobileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(SignUpActivity.this, OTPActivity.class);
                        intent.putExtra(MOBILE_KEY, mobile);
                        startActivity(intent);


                    } else if (loginMobileResponse.getStatus().equals("10200")) {

                        Util.snackBar(progressLayout, loginMobileResponse.getMessage(), Color.RED);
                    } else if (loginMobileResponse.getStatus().equals("10300")) {
                        Util.snackBar(progressLayout, loginMobileResponse.getMessage(), Color.RED);
                    } else if (loginMobileResponse.getStatus().equals("10400")) {

                        Util.snackBar(progressLayout, loginMobileResponse.getMessage(), Color.RED);
                    }
                }else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(getApplicationContext(), jObjError.getJSONObject("message").getString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginMobileResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Util.snackBar(progressLayout, t.getMessage(), Color.RED);
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
        startActivity(intent);
    }
}
