package com.innasoft.gofarmz.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.innasoft.gofarmz.Fragments.OrderFragment;
import com.innasoft.gofarmz.Fragments.PreOrderFragment;
import com.innasoft.gofarmz.R;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Utilis.Constants.BOTTAM_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;

public class OrdersActivity extends AppCompatActivity implements InternetConnectivityListener {
    private InternetAvailabilityChecker mInternetAvailabilityChecker;

    @BindView(R.id.result_tabs)
    TabLayout resultTabs;
    @BindView(R.id.viewpager)
    ViewPager viewpager;

    String title, type;
    int position, order_position;
    @BindView(R.id.txtRetry)
    TextView txtRetry;
    @BindView(R.id.no_internetLayout)
    LinearLayout noInternetLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Orders");

        if (getIntent() != null) {
            title = getIntent().getStringExtra(TITLE);
            position = getIntent().getIntExtra(BOTTAM_TAB_POSITION, 0);
            order_position = getIntent().getIntExtra(ORDER_TAB_POSITION, 0);
            getSupportActionBar().setTitle(title);
            Log.d("TAG", "onCreate: " + position);
        }

        setupViewPager(viewpager);
        viewpager.setCurrentItem(order_position);
        resultTabs.setupWithViewPager(viewpager);

        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
           noInternetLayout.setVisibility(View.GONE);
        } else {
            noInternetLayout.setVisibility(View.VISIBLE);
            //Util.snackBar(noInternetLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }
    // Add Fragments to Tabs
    private void setupViewPager(ViewPager viewPager) {


        Adapter adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(new OrderFragment(), "Orders");
        adapter.addFragment(new PreOrderFragment(), "PreOrders");
        viewPager.setAdapter(adapter);


    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(OrdersActivity.this, HomeActivity.class);
        intent.putExtra(BOTTAM_TAB_POSITION, position);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(OrdersActivity.this, HomeActivity.class);
                intent.putExtra(BOTTAM_TAB_POSITION, position);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
