package com.innasoft.gofarmz.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.gofarmz.Adapter.OrderHistoryAdapter;
import com.innasoft.gofarmz.Adapter.PreOrderHistoryadapter;
import com.innasoft.gofarmz.Adapter.TrackingAdapter;
import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.Preferences.UserSessionManager;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.MyOrderDetailsResponse;
import com.innasoft.gofarmz.Response.MyPreOrderDetailsResponse;
import com.innasoft.gofarmz.Utilis.Util;

import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Utilis.Constants.BOTTAM_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.MODULE;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_CART;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_ID;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_STATUS;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;

public class OrderDetailsActivity extends AppCompatActivity implements InternetConnectivityListener {

    @BindView(R.id.txtOrderDate)
    TextView txtOrderDate;
    @BindView(R.id.txtExpectedDate)
    TextView txtExpectedDate;
    @BindView(R.id.expectedDateLayout)
    LinearLayout expectedDateLayout;
    @BindView(R.id.recyclerTracking)
    RecyclerView recyclerTracking;
    @BindView(R.id.txtTrackOrder)
    TextView txtTrackOrder;
    @BindView(R.id.trackCard)
    CardView trackCard;


    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    boolean internet;
    UserSessionManager userSessionManager;
    String title, status, orderId, deviceId, user_id, token,module;
    int order_position, position;
    @BindView(R.id.txtorder_id)
    TextView txtorder_id;
    @BindView(R.id.order_status)
    TextView orderStatus;
    @BindView(R.id.productRecycler)
    RecyclerView productRecycler;
    @BindView(R.id.txtName)
    TextView txtName;
    @BindView(R.id.txtMobile)
    TextView txtMobile;
    @BindView(R.id.txtAddress)
    TextView txtAddress;
    @BindView(R.id.textView9)
    TextView textView9;
    @BindView(R.id.txtPrice)
    TextView txtPrice;
    @BindView(R.id.textView12)
    TextView textView12;
    @BindView(R.id.txtDiscount)
    TextView txtDiscount;
    @BindView(R.id.textView15)
    TextView textView15;
    @BindView(R.id.txtShippingChargesd)
    TextView txtShippingChargesd;
    @BindView(R.id.textView17)
    TextView textView17;
    @BindView(R.id.txtGrandTotal)
    TextView txtGrandTotal;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.textView21)
    TextView textView21;
    @BindView(R.id.txtPaymentMode)
    TextView txtPaymentMode;
    @BindView(R.id.orderRelative)
    RelativeLayout orderRelative;
    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressLayout.setVisibility(View.GONE);
        if (getIntent() != null) {
            title = getIntent().getStringExtra(TITLE);
            module = getIntent().getStringExtra(MODULE);
            status = getIntent().getStringExtra(ORDER_STATUS);
            orderId = getIntent().getStringExtra(ORDER_ID);
            order_position = getIntent().getIntExtra(ORDER_TAB_POSITION, 0);
            position = getIntent().getIntExtra(BOTTAM_TAB_POSITION, 0);
            getSupportActionBar().setTitle(title);
        }

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.KEY_DEVICEID);
        user_id = userDetails.get(UserSessionManager.KEY_ID);
        token = userDetails.get(UserSessionManager.KEY_TOKEN);


        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
            internet = isConnected;

            if (module.equalsIgnoreCase(ORDER_CART)) {
                getOrderDetails(orderId);
            } else {
                getPreOrderDetails(orderId);
            }


        } else {
            internet = false;
            Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }


    public void getOrderDetails(String orderId) {

        progressLayout.setVisibility(View.VISIBLE);
        Call<MyOrderDetailsResponse> call = RetrofitClient.getInstance().getApi().MyOrdersDetails(token, user_id, orderId);
        call.enqueue(new Callback<MyOrderDetailsResponse>() {
            @Override
            public void onResponse(Call<MyOrderDetailsResponse> call, Response<MyOrderDetailsResponse> response) {
                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);
                    MyOrderDetailsResponse myOrderDetailsResponse = response.body();
                    if (myOrderDetailsResponse.getStatus().equals("10100")) {

                        MyOrderDetailsResponse.DataBean dataBean = myOrderDetailsResponse.getData();
                        MyOrderDetailsResponse.DataBean.OrderDetailsBean orderDetailsBean = dataBean.getOrderDetails();
                        MyOrderDetailsResponse.DataBean.AddressBean addressBean = dataBean.getAddress();

                        List<MyOrderDetailsResponse.DataBean.StatusBean> statusBeanList = dataBean.getStatus();
                        List<MyOrderDetailsResponse.DataBean.ProductsBean> productsBeanList = dataBean.getProducts();


                        trackCard.setVisibility(View.VISIBLE);
                        txtTrackOrder.setVisibility(View.VISIBLE);

                        txtorder_id.setText(orderDetailsBean.getOrderConfirmationReferenceId());

                        if (orderDetailsBean.getOrderStatus().equalsIgnoreCase("Confirmed") ||
                                orderDetailsBean.getOrderStatus().equalsIgnoreCase("Dispatched") ||
                                orderDetailsBean.getOrderStatus().equalsIgnoreCase("Shipped") ||
                                orderDetailsBean.getOrderStatus().equalsIgnoreCase("Delivered")) {

                            orderStatus.setText(orderDetailsBean.getOrderStatus());
                            orderStatus.setTextColor(Color.parseColor("#0f9350"));
                            txtExpectedDate.setText(orderDetailsBean.getExpectedDelivery() + "\n" + orderDetailsBean.getExpectedDeliverySlot());
                        } else if (orderDetailsBean.getOrderStatus().equalsIgnoreCase("Failed")
                                || orderDetailsBean.getOrderStatus().equalsIgnoreCase("Cancelled")
                                || orderDetailsBean.getOrderStatus().equalsIgnoreCase("Not Delivered")) {
                            orderStatus.setText(orderDetailsBean.getOrderStatus());
                            orderStatus.setTextColor(Color.RED);
                            txtExpectedDate.setText("--------");

                        } else if (orderDetailsBean.getOrderStatus().equalsIgnoreCase("Pending")) {
                            orderStatus.setText(orderDetailsBean.getOrderStatus());
                            orderStatus.setTextColor(Color.parseColor("#FF8000"));
                            txtExpectedDate.setText("--------");
                        } else if (orderDetailsBean.getOrderStatus().equalsIgnoreCase("Received")) {
                            orderStatus.setText(orderDetailsBean.getOrderStatus());
                            orderStatus.setTextColor(Color.BLUE);
                            txtExpectedDate.setText("--------");
                        }

                        txtOrderDate.setText(orderDetailsBean.getOrderDate());

                        txtPrice.setText("\u20B9" + orderDetailsBean.getTotalMrpPrice() + "/-");
                        int dis = orderDetailsBean.getDiscount().compareTo("0");
                        if (dis > 0) {
                            // row_discount.setVisibility(View.VISIBLE);
                            txtDiscount.setText("\u20B9" + orderDetailsBean.getDiscount() + "/-");
                        } else {
                            txtDiscount.setVisibility(View.GONE);
                            textView12.setVisibility(View.GONE);

                        }
                        txtShippingChargesd.setText("\u20B9" + orderDetailsBean.getShippingCharges() + "/-");
                        txtGrandTotal.setText("\u20B9" + orderDetailsBean.getFinalPrice() + "/-");
                        txtPaymentMode.setText(orderDetailsBean.getGetawayName());

                        //SHIPPING ADDRESS
                        txtName.setText(addressBean.getName());
                        txtMobile.setText(addressBean.getMobile() + " " + addressBean.getAlternateContactNo());
                        txtAddress.setText(addressBean.getArea() + ", " + addressBean.getAddress() + ", " +
                                addressBean.getState() + ", " + addressBean.getCountry() + "\n" +
                                addressBean.getCity() + " - " + addressBean.getPincode());


                        //PRODUCTS LIST DETAIL
                        OrderHistoryAdapter orderHisDetailAdapter = new OrderHistoryAdapter(productsBeanList, OrderDetailsActivity.this);
                        productRecycler.setHasFixedSize(true);
                        productRecycler.setLayoutManager(new LinearLayoutManager(OrderDetailsActivity.this, LinearLayoutManager.VERTICAL, false));
                        productRecycler.setItemAnimator(new DefaultItemAnimator());
                        productRecycler.setAdapter(orderHisDetailAdapter);

                        //Track Status LIST DETAIL
                        TrackingAdapter orderTrackingAdapter = new TrackingAdapter(statusBeanList, OrderDetailsActivity.this);
                        recyclerTracking.setHasFixedSize(true);
                        recyclerTracking.setLayoutManager(new LinearLayoutManager(OrderDetailsActivity.this, LinearLayoutManager.VERTICAL, false));
                        recyclerTracking.setItemAnimator(new DefaultItemAnimator());
                        recyclerTracking.setAdapter(orderTrackingAdapter);


                    } else if (myOrderDetailsResponse.getStatus().equals("10200")) {

                        Toast.makeText(OrderDetailsActivity.this, myOrderDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (myOrderDetailsResponse.getStatus().equals("10300")) {

                        Toast.makeText(OrderDetailsActivity.this, myOrderDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (myOrderDetailsResponse.getStatus().equals("10400")) {

                        Toast.makeText(OrderDetailsActivity.this, myOrderDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(getApplicationContext(), jObjError.getJSONObject("message").getString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<MyOrderDetailsResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
              //  Toast.makeText(OrderDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    private void getPreOrderDetails(String orderId) {
        progressLayout.setVisibility(View.VISIBLE);
        Call<MyPreOrderDetailsResponse> call = RetrofitClient.getInstance().getApi().MyPreOrderDetails(token, user_id, orderId);
        call.enqueue(new Callback<MyPreOrderDetailsResponse>() {
            @Override
            public void onResponse(Call<MyPreOrderDetailsResponse> call, Response<MyPreOrderDetailsResponse> response) {
                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);
                    MyPreOrderDetailsResponse myPreOrderDetailsResponse = response.body();
                    if (myPreOrderDetailsResponse.getStatus().equals("10100")) {
                        MyPreOrderDetailsResponse.DataBean dataBean = myPreOrderDetailsResponse.getData();

                        MyPreOrderDetailsResponse.DataBean.OrderDetailsBean orderDetailsBean = dataBean.getOrder_details();
                        MyPreOrderDetailsResponse.DataBean.AddressBean addressBean = dataBean.getAddress();
                        List<MyPreOrderDetailsResponse.DataBean.ProductsBean> productsBeanList = dataBean.getProducts();

                        txtDiscount.setVisibility(View.GONE);
                        textView12.setVisibility(View.GONE);
                        expectedDateLayout.setVisibility(View.GONE);
                        trackCard.setVisibility(View.GONE);
                        txtTrackOrder.setVisibility(View.GONE);

                        txtorder_id.setText(orderDetailsBean.getRef_no());
                        if (orderDetailsBean.getPre_order_status().equalsIgnoreCase("Confirmed") ||
                                orderDetailsBean.getPre_order_status().equalsIgnoreCase("Dispatched") ||
                                orderDetailsBean.getPre_order_status().equalsIgnoreCase("Shipped") ||
                                orderDetailsBean.getPre_order_status().equalsIgnoreCase("Delivered")) {

                            orderStatus.setText(orderDetailsBean.getPre_order_status());
                            orderStatus.setTextColor(Color.parseColor("#0f9350"));
                            //txtExpectedDate.setText(orderDetailsBean.getExpected_delivery() + "\n"+orderDetailsBean.getExpected_delivery_slot());
                        } else if (orderDetailsBean.getPre_order_status().equalsIgnoreCase("Failed")
                                || orderDetailsBean.getPre_order_status().equalsIgnoreCase("Cancelled")
                                || orderDetailsBean.getPre_order_status().equalsIgnoreCase("Not Delivered")) {
                            orderStatus.setText(orderDetailsBean.getPre_order_status());
                            orderStatus.setTextColor(Color.RED);
                            // txtExpectedDate.setText("--------");

                        } else if (orderDetailsBean.getPre_order_status().equalsIgnoreCase("Pending")) {
                            orderStatus.setText(orderDetailsBean.getPre_order_status());
                            orderStatus.setTextColor(Color.parseColor("#FF8000"));
                            txtExpectedDate.setText("--------");
                        } else if (orderDetailsBean.getPre_order_status().equalsIgnoreCase("Received")) {
                            orderStatus.setText(orderDetailsBean.getPre_order_status());
                            orderStatus.setTextColor(Color.BLUE);
                            // txtExpectedDate.setText("--------");
                        }

                        txtOrderDate.setText(orderDetailsBean.getOrder_date());
                        txtPrice.setText("\u20B9" + orderDetailsBean.getTotal_price() + "/-");
                        txtShippingChargesd.setText("\u20B9" + orderDetailsBean.getShipping_charges() + "/-");
                        txtGrandTotal.setText("\u20B9" + orderDetailsBean.getGrand_total() + "/-");
                        txtPaymentMode.setText(orderDetailsBean.getGetaway_name());

                        //SHIPPING ADDRESS
                        txtName.setText(addressBean.getName());
                        txtMobile.setText(addressBean.getMobile() + " " + addressBean.getAlternate_contact_no());
                        txtAddress.setText(addressBean.getArea() + ", " + addressBean.getAddress() + ", " +
                                addressBean.getState() + ", " + addressBean.getCountry() + "\n" +
                                addressBean.getCity() + " - " + addressBean.getPincode());


                        //PRODUCTS LIST DETAIL
                        PreOrderHistoryadapter orderHisDetailAdapter = new PreOrderHistoryadapter(productsBeanList, OrderDetailsActivity.this);
                        productRecycler.setHasFixedSize(true);
                        productRecycler.setLayoutManager(new LinearLayoutManager(OrderDetailsActivity.this, LinearLayoutManager.VERTICAL, false));
                        productRecycler.setItemAnimator(new DefaultItemAnimator());
                        productRecycler.setAdapter(orderHisDetailAdapter);

                       /* //Track Status LIST DETAIL

                        orderTrackingAdapter = new OrderTrackingAdapter(OrderDetailsActivity.this, statusBeanList, mWithLinePadding);
                        recycler_view_track.setHasFixedSize(true);
                        recycler_view_track.setLayoutManager(new LinearLayoutManager(OrderDetailsActivity.this, LinearLayoutManager.VERTICAL, false));
                        recycler_view_track.setItemAnimator(new DefaultItemAnimator());
                        recycler_view_track.setAdapter(orderTrackingAdapter);*/


                    } else if (myPreOrderDetailsResponse.getStatus().equals("10200")) {

                        Toast.makeText(OrderDetailsActivity.this, myPreOrderDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (myPreOrderDetailsResponse.getStatus().equals("10300")) {

                        Toast.makeText(OrderDetailsActivity.this, myPreOrderDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (myPreOrderDetailsResponse.getStatus().equals("10400")) {

                        Toast.makeText(OrderDetailsActivity.this, myPreOrderDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(getApplicationContext(), jObjError.getJSONObject("message").getString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<MyPreOrderDetailsResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
             //   Toast.makeText(OrderDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(OrderDetailsActivity.this, OrdersActivity.class);
        intent.putExtra(BOTTAM_TAB_POSITION, position);
        intent.putExtra(ORDER_TAB_POSITION, order_position);
        intent.putExtra(TITLE, "Orders");
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(OrderDetailsActivity.this, OrdersActivity.class);
                intent.putExtra(BOTTAM_TAB_POSITION, position);
                intent.putExtra(ORDER_TAB_POSITION, order_position);
                intent.putExtra(TITLE, "Orders");
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
