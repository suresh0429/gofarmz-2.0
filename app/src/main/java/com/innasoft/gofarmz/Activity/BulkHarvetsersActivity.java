package com.innasoft.gofarmz.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.innasoft.gofarmz.Adapter.FBHAdpter;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.models.FBhItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.innasoft.gofarmz.Utilis.Constants.BOTTAM_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;
import static com.innasoft.gofarmz.Utilis.Constants.TYPE;

public class BulkHarvetsersActivity extends AppCompatActivity {
    private List<FBhItem> fBhItems = new ArrayList<>();
    @BindView(R.id.fbhRecycler)
    RecyclerView fbhRecycler;
    String title, type;
    int position;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bulk_harvetsers);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent() != null) {
            title = getIntent().getStringExtra(TITLE);
            type = getIntent().getStringExtra(TYPE);
            position = getIntent().getIntExtra(BOTTAM_TAB_POSITION,0);
            getSupportActionBar().setTitle(title);
        }

        fbhList();
    }

    private void fbhList() {
        RecyclerView.LayoutManager manager = new GridLayoutManager(BulkHarvetsersActivity.this, 2);
        fbhRecycler.setLayoutManager(manager);
        FBHAdpter adapter = new FBHAdpter(fBhItems, BulkHarvetsersActivity.this);
        fbhRecycler.setAdapter(adapter);

        FBhItem fBhItem = new FBhItem("https://i0.wp.com/images-prod.healthline.com/hlcmsresource/images/AN_images/tomatoes-1296x728-feature.jpg?w=1155&h=1528", "Madhava Reddy");
        fBhItems.add(fBhItem);

        fBhItem = new FBhItem("https://cdn-prod.medicalnewstoday.com/content/images/articles/280/280579/potatoes-can-be-healthful.jpg", "Manmadha rao");
        fBhItems.add(fBhItem);

        fBhItem = new FBhItem("https://ichef.bbci.co.uk/wwfeatures/live/976_549/images/live/p0/7v/2w/p07v2wjn.jpg", "Mahesh");
        fBhItems.add(fBhItem);

        fBhItem = new FBhItem("https://img.etimg.com/thumb/width-640,height-480,imgsize-876498,resizemode-1,msid-67172581/despite-scheme-onions-plunge-to-rs-1-50-in-maharashtra.jpg", "Ramesh");
        fBhItems.add(fBhItem);

        fBhItem = new FBhItem("https://tiimg.tistatic.com/fp/1/004/859/fresh-green-chilli-mirchi--316.jpg", "Madhava Reddy");
        fBhItems.add(fBhItem);

    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(BulkHarvetsersActivity.this,HomeActivity.class);
        intent.putExtra(BOTTAM_TAB_POSITION,position);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(BulkHarvetsersActivity.this,HomeActivity.class);
                intent.putExtra(BOTTAM_TAB_POSITION,position);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
