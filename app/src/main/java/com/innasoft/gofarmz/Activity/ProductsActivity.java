package com.innasoft.gofarmz.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.gofarmz.Adapter.CustomBoxWeightAdapter;
import com.innasoft.gofarmz.Adapter.ProductAdapter;
import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.Interface.ProductListner;
import com.innasoft.gofarmz.Preferences.UserSessionManager;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.BaseResponse;
import com.innasoft.gofarmz.Response.CustomBoxResponse;
import com.innasoft.gofarmz.Utilis.Util;
import com.innasoft.gofarmz.models.OptionsModel;
import com.innasoft.gofarmz.models.Product;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Utilis.Constants.BOTTAM_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.CATID;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;

public class ProductsActivity extends AppCompatActivity implements InternetConnectivityListener, ProductListner {
    @BindView(R.id.productRecycler)
    RecyclerView productRecycler;
    @BindView(R.id.txtTotal)
    TextView txtTotal;
    @BindView(R.id.txtCartTotal)
    TextView txtCartTotal;
    @BindView(R.id.btnAddtocart)
    Button btnAddtocart;
    @BindView(R.id.bottamLayout)
    CardView bottamLayout;
    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;
    @BindView(R.id.txtRetry)
    TextView txtRetry;
    @BindView(R.id.no_internetLayout)
    LinearLayout noInternetLayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    UserSessionManager userSessionManager;
    boolean internet;
    private String title, catId, type, deviceId, token, user_id, basketName, basketId;
    List<Product> productsList = new ArrayList<Product>();
    ArrayList<OptionsModel> optionsModels = new ArrayList<>();
    ProductListner productListner;
    ProductAdapter productAdapter;

    ArrayList<String> cartArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setTitle("Products");

        if (getIntent() != null) {
            title = getIntent().getStringExtra(TITLE);
            catId = getIntent().getStringExtra(CATID);
            getSupportActionBar().setTitle(title);
        }

        productListner = this;

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.KEY_DEVICEID);
        user_id = userDetails.get(UserSessionManager.KEY_ID);
        token = userDetails.get(UserSessionManager.KEY_TOKEN);

        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
            customBoxProducts();
            noInternetLayout.setVisibility(View.GONE);
        } else {
            // internet = false;
            noInternetLayout.setVisibility(View.VISIBLE);
            Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }

    private void customBoxProducts() {
        progressLayout.setVisibility(View.VISIBLE);
        Call<CustomBoxResponse> cartCountResponseCall = RetrofitClient.getInstance().getApi().CustomBoxProducts(token, catId, "COMBO_CUSTOM", "1", user_id, deviceId);
        cartCountResponseCall.enqueue(new Callback<CustomBoxResponse>() {
            @Override
            public void onResponse(Call<CustomBoxResponse> call, Response<CustomBoxResponse> response) {
                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);
                    CustomBoxResponse homeBoxProductsResponse = response.body();

                    if (homeBoxProductsResponse.getStatus().equals("10100")) {

                        List<CustomBoxResponse.DataBean> dataBeanList = homeBoxProductsResponse.getData();

                        for (CustomBoxResponse.DataBean dataBean : dataBeanList) {
                            basketName = dataBean.getPdtName();
                            basketId = dataBean.getId();
                            List<CustomBoxResponse.DataBean.OPTIONALBean> optionalBeans = dataBean.getOPTIONAL();

                            productsList.clear();
                            int i = 0;
                            for (CustomBoxResponse.DataBean.OPTIONALBean optionsBean : optionalBeans) {


                                optionsModels.clear();
                                for (CustomBoxResponse.DataBean.OPTIONALBean.OptionsBean optionsBean1 : optionsBean.getOptions()) {

                                   /* optionsModels.add(new OptionsModel(optionsBean1.getOptId(),optionsBean1.getPdtId(),optionsBean1.getOptName(),optionsBean1.getPrice(),
                                            optionsBean1.getIsInCart(),optionsBean1.getCartQty(),optionsBean1.getQtyPrice()));
*/
                                    Log.d(TAG, "onResponse: " + optionsBean1.getOptName());


                                }

                                Log.d(TAG, "cartQty: " + optionalBeans.get(i).getOptions().get(0).getPrice());
                                productsList.add(new Product(optionsBean.getId(), optionsBean.getPdtName(), optionsBean.getImages(), optionsBean.getAvailability(),
                                        optionalBeans.get(i).getOptions().get(0).getOptId(),
                                        optionalBeans.get(i).getOptions().get(0).getPdtId(),
                                        optionalBeans.get(i).getOptions().get(0).getOptName(),
                                        Double.parseDouble(optionalBeans.get(i).getOptions().get(0).getPrice()),
                                        optionalBeans.get(i).getOptions().get(0).getIsInCart(),
                                        // optionalBeans.get(i).getOptions().get(0).getCartQty(),
                                        0,
                                        optionalBeans.get(i).getOptions().get(0).getQtyPrice()));


                                i++;

                            }

                            //  productsList();
                            productAdapter = new ProductAdapter(productsList, optionalBeans, ProductsActivity.this, productListner);
                            GridLayoutManager linearLayoutManager = new GridLayoutManager(ProductsActivity.this, 2);
                            productRecycler.setLayoutManager(linearLayoutManager);
                            productRecycler.setAdapter(productAdapter);
                            productAdapter.notifyDataSetChanged();

                        }


                    }

                }
            }

            @Override
            public void onFailure(Call<CustomBoxResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }


    @Override
    public void OnItemClick(int position, Product product, CustomBoxResponse.DataBean.OPTIONALBean optionalBean) {
        Log.d(TAG, "OnItemClick: " + optionalBean.getOptions());

        List<CustomBoxResponse.DataBean.OPTIONALBean.OptionsBean> childProductsBeanList = optionalBean.getOptions();

       /* CustomBasketItemFragment bottomSheetFragment = new CustomBasketItemFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ARRAYLIST, (Serializable) childProductsBeanList);
        bundle.putSerializable(ARRAYLIST1, (Serializable) productsList);
        bundle.putString(TITLE, product.getPdtName());
        bundle.putString(FRAGMENT_FROM, String.valueOf(product));
        bottomSheetFragment.setArguments(bundle);
        bottomSheetFragment.show(((FragmentActivity) this).getSupportFragmentManager(), bottomSheetFragment.getTag());
*/


        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(ProductsActivity.this).inflate(R.layout.custom_box_item_dialog, viewGroup, false);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(ProductsActivity.this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();


       /* RecyclerView recyclerView = (RecyclerView) dialogView.findViewById(R.id.childItemsRecycler);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(ProductsActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager1);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
*/

        ListView listView = dialogView.findViewById(R.id.childItemsRecycler);
        TextView txtTitleProduct = dialogView.findViewById(R.id.txtTitle);
        ImageView closeImage = dialogView.findViewById(R.id.closeImage);
        txtTitleProduct.setText(product.getPdtName());
        closeImage.setOnClickListener(view -> alertDialog.dismiss());
        //CustomBoxItemAdapter adapter = new CustomBoxItemAdapter(childProductsBeanList,getApplicationContext(),alertDialog);
        CustomBoxWeightAdapter adapter = new CustomBoxWeightAdapter(ProductsActivity.this, (ArrayList<CustomBoxResponse.DataBean.OPTIONALBean.OptionsBean>) childProductsBeanList);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                CustomBoxResponse.DataBean.OPTIONALBean.OptionsBean optionsBean = (CustomBoxResponse.DataBean.OPTIONALBean.OptionsBean) adapterView.getItemAtPosition(position);

                int i = productsList.indexOf(product);
                Product updatedProduct = new Product(
                        product.getId(), product.getPdtName(), product.getImages(), product.getAvailability(),
                        optionsBean.getOptId(), product.getPdtId(), optionsBean.getOptName(), Double.parseDouble(optionsBean.getPrice()), product.getIsInCart(),
                        product.getCartQty(), product.getQtyPrice()
                );

                productsList.remove(product);
                productsList.add(i, updatedProduct);

                productAdapter.notifyDataSetChanged();


                calculateCartTotal();

                alertDialog.dismiss();
            }
        });


        // cartstr = pdtId + "_" + optId + "_" + qty;
    }

    @Override
    public void onMinusClick(int position, Product product) {

        int i = productsList.indexOf(product);
        if (product.getCartQty() > 0) {

            Product updatedProduct = new Product(
                    product.getId(), product.getPdtName(), product.getImages(), product.getAvailability(),
                    product.getOptId(), product.getPdtId(), product.getOptName(), product.getPrice(), product.getIsInCart(),
                    (product.getCartQty() - 1), product.getQtyPrice()
            );

            productsList.remove(product);
            productsList.add(i, updatedProduct);

            productAdapter.notifyDataSetChanged();

            calculateCartTotal();
        }

    }

    @Override
    public void onPlusClick(int position, Product product) {
        Log.d(TAG, "onPlusClick: " + product.getPrice());

        int i = productsList.indexOf(product);
        Product updatedProduct = new Product(
                product.getId(), product.getPdtName(), product.getImages(), product.getAvailability(),
                product.getOptId(), product.getPdtId(), product.getOptName(), product.getPrice(), product.getIsInCart(),
                (product.getCartQty() + 1), product.getQtyPrice()
        );

        productsList.remove(product);
        productsList.add(i, updatedProduct);

        productAdapter.notifyDataSetChanged();


        calculateCartTotal();
    }

    @Override
    public void onAddClick(int position, Product product) {

        int i = productsList.indexOf(product);
        Product updatedProduct = new Product(
                product.getId(), product.getPdtName(), product.getImages(), product.getAvailability(),
                product.getOptId(), product.getPdtId(), product.getOptName(), product.getPrice(), product.getIsInCart(),
                1, product.getQtyPrice()
        );

        productsList.remove(product);
        productsList.add(i, updatedProduct);

        productAdapter.notifyDataSetChanged();


        calculateCartTotal();
    }


    // total Amount
    public void calculateCartTotal() {

        cartArray = new ArrayList<>();
        int grandTotal = 0;

        for (Product order : productsList) {

            grandTotal += order.getPrice() * order.getCartQty();

            if (order.getCartQty() > 0) {
                cartArray.add(order.getPdtId() + "_" + order.getOptId() + "_" + order.getCartQty());
                Log.d(TAG, "calculateCartTotal: " + order.getPdtId() + "______" + order.getOptId() + "-----" + order.getCartQty());
            }

        }

        Log.e("TOTAL", "" + productsList.size() + "Items | " + "  DISCOUNT : " + grandTotal);

        if (grandTotal != 0) {
            txtTotal.setText("\u20B9 " + grandTotal);
            txtCartTotal.setText(basketName);
            bottamLayout.setVisibility(View.VISIBLE);
        } else {
            bottamLayout.setVisibility(View.GONE);
        }

        btnAddtocart.setOnClickListener(view -> {

            if (btnAddtocart.getText().toString().equalsIgnoreCase("GOTO CART")){
                Intent intent = new Intent(ProductsActivity.this,HomeActivity.class);
                intent.putExtra(BOTTAM_TAB_POSITION,2);
                startActivity(intent);
            }else {
                addToCart(cartArray);
            }

        });


    }


    private void addToCart(ArrayList<String> cartarr) {
        progressBar.setVisibility(View.VISIBLE);
        for (int i = 0; i < cartarr.size(); i++) {
            cartarr.get(i);
        }

        Call<BaseResponse> cartCountResponseCall = RetrofitClient.getInstance().getApi().addtocart(token, deviceId, user_id, basketId, deviceId, "1", cartarr);
        cartCountResponseCall.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    progressBar.setVisibility(View.GONE);
                    BaseResponse cartCountResponse = response.body();
                    if (cartCountResponse.getStatus().equals("10100")) {
                        // makeFlyAnimation(total_price_txt);
                        btnAddtocart.setText("GOTO CART");
                        Toast.makeText(ProductsActivity.this, "Add to Cart Successfully ", Toast.LENGTH_LONG).show();
                    } else if (cartCountResponse.getStatus().equals("10200")) {
                        Toast.makeText(getApplicationContext(), cartCountResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    private double ParseDouble(String strNumber) {
        if (strNumber != null && strNumber.length() > 0) {
            try {
                return Double.parseDouble(strNumber);
            } catch (Exception e) {
                return -1;   // or some value to mark this field is wrong. or make a function validates field first ...
            }
        } else return 0;
    }


    public void updatePrice(Product product) {

        Log.d(TAG, "updatePrice: " + product.getQtyPrice());
        int i = productsList.indexOf(product);
        Product updatedProduct = new Product(
                product.getId(), product.getPdtName(),
                product.getImages(), product.getAvailability(),
                product.getOptId(), product.getPdtId(), product.getOptName()
                , product.getPrice(), product.getIsInCart(),
                product.getCartQty(), product.getQtyPrice()
        );

        productsList.remove(product);
        productsList.add(i, updatedProduct);

        productAdapter.notifyDataSetChanged();


        calculateCartTotal();

    }


    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                /*Intent intent = new Intent(PromoCodesActivity.this, CheckoutActivity.class);
                intent.putExtra(BOTTAM_TAB_POSITION, position);
                startActivity(intent);*/
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
