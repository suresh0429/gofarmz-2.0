package com.innasoft.gofarmz.Activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.Fragments.AcountFragment;
import com.innasoft.gofarmz.Fragments.BasketFragment;
import com.innasoft.gofarmz.Fragments.CartFragment;
import com.innasoft.gofarmz.Fragments.HomeFragment;
import com.innasoft.gofarmz.Preferences.UserSessionManager;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.CartCountResponse;
import com.innasoft.gofarmz.Response.NotificationResponse;
import com.innasoft.gofarmz.Utilis.Converter;
import com.innasoft.gofarmz.Utilis.Util;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Api.RetrofitClient.FORMER_NETWORK;
import static com.innasoft.gofarmz.Utilis.Constants.BOTTAM_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.COUNT;
import static com.innasoft.gofarmz.Utilis.Constants.HOMEACTIVITY;
import static com.innasoft.gofarmz.Utilis.Constants.LOCATIONNAME;
import static com.innasoft.gofarmz.Utilis.Constants.LOCATION_PREF;
import static com.innasoft.gofarmz.Utilis.Constants.NOTIFY_COUNT;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;
import static com.innasoft.gofarmz.Utilis.Constants.TYPE;

public class HomeActivity extends AppCompatActivity implements InternetConnectivityListener {
    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    UserSessionManager userSessionManager;
    private String deviceId, token, user_id;
    Location currentLocation;
    FusedLocationProviderClient fusedLocationProviderClient;
    FusedLocationProviderClient mFusedLocationClient;
    boolean doubleBackToExitPressedOnce = false;
    private View badge;
    ActionBar actionBar;
    @BindView(R.id.main_container)
    FrameLayout mainContainer;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottom_navigation;

    TextView titleTxtView;
    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        // custom actionbar
        actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(R.layout.locations);
        View v = actionBar.getCustomView();
        titleTxtView = v.findViewById(R.id.txtLocation);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.KEY_DEVICEID);
        user_id = userDetails.get(UserSessionManager.KEY_ID);
        token = userDetails.get(UserSessionManager.KEY_TOKEN);


        if (getIntent() != null) {
            position = getIntent().getIntExtra(BOTTAM_TAB_POSITION, 0);
           // position = getIntent().getIntExtra(BOTTAM_TAB_POSITION, 0);
            Log.d("TAG", "onCreate: " + position);
            if (position == 3) {
                loadFragment(new AcountFragment());
                bottom_navigation.setSelectedItemId(R.id.navigation_account);
            } else if (position == 2) {
                loadFragment(new CartFragment());
                bottom_navigation.setSelectedItemId(R.id.navigation_cart);
                //removeCartBadge(bottom_navigation, R.id.navigation_cart);
            } else {
                loadFragment(new HomeFragment());
                bottom_navigation.setSelectedItemId(R.id.navigation_home);
            }

        } else {
            loadFragment(new HomeFragment());
            bottom_navigation.setSelectedItemId(R.id.navigation_home);
        }


        bottom_navigation.setOnNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.navigation_home:
                    actionBar.setTitle("Home");
                    loadFragment(new HomeFragment());
                    return true;
                case R.id.navigation_basket:
                    actionBar.setTitle("Basket");
                    loadFragment(new BasketFragment());
                    return true;
                case R.id.navigation_cart:
                    actionBar.setTitle("Cart");
                    loadFragment(new CartFragment());
                    //removeCartBadge(bottom_navigation, R.id.navigation_cart);
                    return true;
                case R.id.navigation_account:
                    actionBar.setTitle("Account");
                    loadFragment(new AcountFragment());
                    return true;

            }
            return false;
        });


        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {

            requestStoragePermission();
            getCartCount();
            getnotificationCount();



        } else {
            // internet = false;
            Util.snackBar(bottom_navigation, getResources().getString(R.string.notconnected), Color.YELLOW);
            //removeCartBadge(bottom_navigation, R.id.navigation_cart);
        }
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    public static void showBadge(Context context, BottomNavigationView
            bottomNavigationView, @IdRes int itemId, String value) {
        removeBadge(bottomNavigationView, itemId);
        BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
        View badge = LayoutInflater.from(context).inflate(R.layout.cart_badge, bottomNavigationView, false);

        TextView text = badge.findViewById(R.id.badge_text_view);

        if (!value.equalsIgnoreCase("0")){
            text.setText(value);
            itemView.addView(badge);
        }else {
            removeBadge(bottomNavigationView, itemId);
        }

    }

    public static void removeBadge(BottomNavigationView bottomNavigationView, @IdRes int itemId) {
        BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
        if (itemView.getChildCount() == 3) {
            itemView.removeViewAt(2);
        }
    }


    // location text
    private void setTexts(Location location) {

        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (addresses != null) {
            Log.d("max", " " + addresses.get(0).getMaxAddressLineIndex());

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String area = addresses.get(0).getSubLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

            addresses.get(0).getAdminArea();

       /* textView.setText(address);
        tvFeature.setText(knownName);
        tvCountry.setText(country);
        tvState.setText(state);
        tvCity.setText(city);
        tvPincode.setText(postalCode);*/


            SharedPreferences preferences = getSharedPreferences(LOCATION_PREF,0);
            String locationName = preferences.getString(LOCATIONNAME,"");

            if (!locationName.equalsIgnoreCase("")){

                titleTxtView.setText(locationName);
            }else {

                if (area != null) {
                    titleTxtView.setText(area);
                } else {
                    titleTxtView.setText("Select Location");
                }
            }


            titleTxtView.setOnClickListener(view ->{
                Intent intent = new Intent(HomeActivity.this,AddressListActivity.class);
                intent.putExtra(BOTTAM_TAB_POSITION,0);
                intent.putExtra(TITLE,HOMEACTIVITY);
                startActivity(intent);
            });

        }
    }

    private void requestStoragePermission() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            // getLastLocation();
                            fetchLocation();
                          //  Toast.makeText(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void fetchLocation() {

        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    currentLocation = location;
                    setTexts(currentLocation);
                   // Toast.makeText(getApplicationContext(), currentLocation.getLatitude() + "" + currentLocation.getLongitude(), Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    public void getCartCount() {

        Log.d(TAG, "getCartCount: " + "Count");
        Call<CartCountResponse> cartCountResponseCall = RetrofitClient.getInstance().getApi().CartCount(token, user_id, deviceId);
        cartCountResponseCall.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, retrofit2.Response<CartCountResponse> response) {
                if (response.isSuccessful()) ;
                CartCountResponse cartCountResponse = response.body();
                if (cartCountResponse.getStatus().equals("10100")) {

                    showBadge( HomeActivity.this,bottom_navigation,R.id.navigation_cart, String.valueOf(cartCountResponse.getData()));

                } else if (cartCountResponse.getStatus().equals("10200")) {
                    Toast.makeText(HomeActivity.this, cartCountResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {
                Toast.makeText(HomeActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    public void getnotificationCount() {

        Call<NotificationResponse> call = RetrofitClient.getInstance().getApi().notifications(token, deviceId, user_id);
        call.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {

                if (response.isSuccessful()) {

                    NotificationResponse loginResponse = response.body();

                    if (loginResponse.getStatus().equals("10100")) {

                        ArrayList<String> countList = new ArrayList<>();
                        List<NotificationResponse.DataBean> dataBeanList = loginResponse.getData();
                        for (NotificationResponse.DataBean dataBean : dataBeanList){
                            if (dataBean.getIsRead().equalsIgnoreCase("0")){
                                Log.d(TAG, "onResponseNotification:  "+dataBean.getIsRead());

                                countList.add(dataBean.getIsRead());
                                int notificatationCount = countList.size();
                                setNotifyCount(notificatationCount);

                            }else {
                                setNotifyCount(0);
                                Log.d(TAG, "onResponseNotification:  "+dataBean.getIsRead().length());
                            }

                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getnotificationCount();
        invalidateOptionsMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds countries to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);

        SharedPreferences preferences = getSharedPreferences(NOTIFY_COUNT,0);
        int notificatationCount = preferences.getInt(COUNT,0);

        final MenuItem menuItem = menu.findItem(R.id.notification);
        menuItem.setIcon(Converter.convertLayoutToImage(HomeActivity.this, notificatationCount, R.drawable.ic_notifications_black_24dp));
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent title in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.notification) {
            Intent intent = new Intent(HomeActivity.this, NotificationsActivity.class);
            intent.putExtra(TITLE, "Notifications");
            intent.putExtra(TYPE, FORMER_NETWORK);
            intent.putExtra(BOTTAM_TAB_POSITION, -1);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {


        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            moveTaskToBack(true);
        } else {
            Toast.makeText(getApplicationContext(), "Press Back again to Exit.", Toast.LENGTH_SHORT).show();
            doubleBackToExitPressedOnce = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2 * 1000);

        }
    }

    private void setNotifyCount(int count){
        SharedPreferences preferences = getSharedPreferences(NOTIFY_COUNT,0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(COUNT,count);
        editor.apply();

        invalidateOptionsMenu();
    }
}
