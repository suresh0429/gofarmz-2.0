package com.innasoft.gofarmz.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.Preferences.UserSessionManager;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.BaseResponse;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.innasoft.gofarmz.Utilis.Constants.BOTTAM_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.CART_COUNT;
import static com.innasoft.gofarmz.Utilis.Constants.COUNT;
import static com.innasoft.gofarmz.Utilis.Constants.MODULE;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_CART;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_ID;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_STATUS;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.PRE_ORDER_CART;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;

public class PaymentGatewayActivity extends Activity implements PaymentResultListener {
    private static final String TAG = PaymentGatewayActivity.class.getSimpleName();
    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;

    UserSessionManager userSessionManager;
    String title, module, name, email, mobile, encryptOrderId, order_ref_no, token, deviceId, user_id;
    int orderId;
    Handler mHandler = new Handler();
    Double totalvalue;
    private boolean checkInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_gateway);
        ButterKnife.bind(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        email = userDetails.get(UserSessionManager.KEY_EMAIL);
        mobile = userDetails.get(UserSessionManager.KEY_MOBILE);
        name = userDetails.get(UserSessionManager.KEY_NAME);
        token = userDetails.get(UserSessionManager.KEY_TOKEN);
        deviceId = userDetails.get(UserSessionManager.KEY_DEVICEID);
        user_id = userDetails.get(UserSessionManager.KEY_ID);

        Checkout.preload(getApplicationContext());

        if (getIntent() != null) {

            orderId = getIntent().getIntExtra(ORDER_ID, 0);
            module = getIntent().getStringExtra(MODULE);
            Double total = getIntent().getDoubleExtra("TOTAL", 0.00);
            order_ref_no = getIntent().getStringExtra("ORDER_REF_NO");
            encryptOrderId = getIntent().getStringExtra("ENCRYPTORDERID");
            title = getIntent().getStringExtra(TITLE);
            totalvalue = total * 100;


        }
        startPayment(orderId, totalvalue, encryptOrderId);

    }


    public void startPayment(int orderId, Double total, String encryptOrderId) {


        /*
          You need to pass current title in order to let Razorpay create CheckoutActivity
         */
        final Activity activity = this;

        final Checkout co = new Checkout();

        try {
            JSONObject data = new JSONObject();
            data.put("name", "Gofarmz");
            data.put("description", "Demoing Charges");
            //You can omit the image option to fetch the image from dashboard
            data.put("image", "https://www.gofarmz.com/assets-v1/img/gofarmz.png");
            data.put("currency", "INR");
            data.put("amount", total);

            JSONObject notes = new JSONObject();
            notes.put("email", email);
            notes.put("contact", mobile);
            notes.put("order_id", orderId);
            notes.put("order_ref_no", order_ref_no);
            data.put("notes", notes);
            data.put("prefill", notes);

            co.open(activity, data);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unused")
    @Override
    public void onPaymentSuccess(final String razorpayPaymentID) {
        try {
            //Toast.makeText(this, "Payment Successful: " + razorpayPaymentID, Toast.LENGTH_SHORT).show();
            mHandler.post(new Runnable() {
                public void run() {
                    mHandler = null;

                    if (module.equalsIgnoreCase(ORDER_CART) || module.equalsIgnoreCase(PRE_ORDER_CART)){

                        sendingToServerData(razorpayPaymentID,module);

                    } else {

                        Double finaltotal = (double) totalvalue;
                        finaltotal = finaltotal / 100;

                        String total = String.valueOf(finaltotal);
                        addMoneyToWallet(razorpayPaymentID, total);
                    }

                   /* // if (title.equalsIgnoreCase("PaymentDetails")) {
                    if (title.equalsIgnoreCase("Order Details") || title.equalsIgnoreCase("PreOrder Details")) {

                        sendingToServerData(razorpayPaymentID, module);
                    } else {

                        Double finaltotal = (double) totalvalue;
                        finaltotal = finaltotal / 100;

                        String total = String.valueOf(finaltotal);
                        addMoneyToWallet(razorpayPaymentID, total);
                    }*/

                }
            });

        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentSuccess", e);
        }
    }

    @SuppressWarnings("unused")
    @Override
    public void onPaymentError(int code, String response) {
        try {
            Log.d(TAG, "onPaymentError: " + code + "__" + response);
            //Toast.makeText(getApplicationContext(),"Cancel payment" , Toast.LENGTH_SHORT).show();


            if (code == 0) {
                Toast.makeText(this, response, Toast.LENGTH_SHORT).show();

                if (title.equalsIgnoreCase("WalletActivity")) {
                    Intent intent = new Intent(PaymentGatewayActivity.this, WalletActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                } else {

                    finish();
                }

            } else {
                failedDilogue();
            }


        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentError", e);
        }
    }

    private void successDilogue(String razorpayPaymentID, String module) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(PaymentGatewayActivity.this).inflate(R.layout.success_alert_diloge, viewGroup, false);

        TextView txttransactionId = dialogView.findViewById(R.id.txttransactionId);
        txttransactionId.setText("" + razorpayPaymentID);
        TextView txtOrderId = dialogView.findViewById(R.id.txtOrderId);
        txtOrderId.setText("" + orderId);

        Button btnOk = dialogView.findViewById(R.id.buttonOk);
        btnOk.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), OrderDetailsActivity.class);
            intent.putExtra(ORDER_ID, String.valueOf(orderId));
            intent.putExtra(ORDER_STATUS, "Confirmed");
            intent.putExtra(TITLE, title);
            intent.putExtra(MODULE, module);
            intent.putExtra(ORDER_TAB_POSITION, 0);
            intent.putExtra(BOTTAM_TAB_POSITION, 3);
            startActivity(intent);

        });
        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(PaymentGatewayActivity.this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    private void failedDilogue() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(PaymentGatewayActivity.this).inflate(R.layout.failed_alert_diloge, viewGroup, false);

        Button btnOk = dialogView.findViewById(R.id.buttonOk);
        btnOk.setOnClickListener(view -> {
            finish();
        });
        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(PaymentGatewayActivity.this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    private void sendingToServerData(String razorpayPaymentID, String module) {
        progressLayout.setVisibility(View.VISIBLE);
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().paymentSuccess(encryptOrderId, "Razorpay", razorpayPaymentID, "success", "Order Successful", "Online", String.valueOf(totalvalue), "cc");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                progressLayout.setVisibility(View.GONE);

                successDilogue(razorpayPaymentID,module);

                if (module.equalsIgnoreCase(PRE_ORDER_CART)) {
                  setCartCount(0);
                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
            }
        });


    }

    private void addMoneyToWallet(String razorpayPaymentID, String total) {
        progressLayout.setVisibility(View.VISIBLE);

        Call<BaseResponse> call = RetrofitClient.getInstance().getApi().addMoney(token, deviceId, user_id, razorpayPaymentID, total);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);
                    BaseResponse baseResponse = response.body();

                    if (baseResponse.getStatus().equalsIgnoreCase("10100")) {
                        Intent order_detail = new Intent(PaymentGatewayActivity.this, WalletActivity.class);
                        order_detail.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(order_detail);
                        finish();
                        Toast.makeText(PaymentGatewayActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    if (baseResponse.getStatus().equalsIgnoreCase("10200")) {
                        Toast.makeText(PaymentGatewayActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    if (baseResponse.getStatus().equalsIgnoreCase("10300")) {
                        Toast.makeText(PaymentGatewayActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }


                }


            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Toast.makeText(PaymentGatewayActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }


    private void setCartCount(int count) {
        SharedPreferences preferences = getSharedPreferences(CART_COUNT, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(COUNT, count);
        editor.apply();

        invalidateOptionsMenu();
    }
}
