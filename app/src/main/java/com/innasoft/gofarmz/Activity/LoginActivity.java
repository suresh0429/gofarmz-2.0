package com.innasoft.gofarmz.Activity;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.Credentials;
import com.google.android.gms.auth.api.credentials.CredentialsOptions;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.material.textfield.TextInputEditText;
import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.Preferences.UserSessionManager;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.LoginMobileResponse;
import com.innasoft.gofarmz.Utilis.Util;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import org.json.JSONObject;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Utilis.Constants.MOBILE_KEY;

public class LoginActivity extends Activity implements InternetConnectivityListener {

    private static final int RESOLVE_HINT = 1;
    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    boolean internet;
    String deviceId;

    @BindView(R.id.logoimage)
    ImageView logoimage;
    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;
    @BindView(R.id.btn_continue1)
    Button btnContinue;
    @BindView(R.id.edt_mobile)
    TextInputEditText edtMobile;


    @SuppressLint("HardwareIds")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_actity);
        ButterKnife.bind(this);
        progressLayout.setVisibility(View.GONE);

        UserSessionManager session = new UserSessionManager(this);
        Log.d(TAG, "onCreate: "+session.isLoggedIn());
        if (session.isLoggedIn()) {
            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
            finish();
        }

        try {
            requestHint();
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }

        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);

        // device Id
        deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        btnContinue.setAlpha(.5f);
        btnContinue.setEnabled(false);
        edtMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String value = String.valueOf(charSequence.length());

                 if (value.equals("10")) {

                    btnContinue.setEnabled(true);
                    btnContinue.setAlpha(.9f);
                    btnContinue.setText("CONTINUE");
                    btnContinue.setClickable(true);

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edtMobile.getWindowToken(), 0);

                } else {
                    btnContinue.setAlpha(.5f);
                    btnContinue.setEnabled(false);
                    btnContinue.setText("ENTER MOBILE NUMBER");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
            internet = isConnected;

        } else {
            internet = false;
            Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }


    @OnClick(R.id.btn_continue1)
    public void onViewClicked() {
        Log.d(TAG, "onViewClicked: "+internet);
        if (internet) {
            String mobile = edtMobile.getText().toString();
            if (mobile.equals("")) {
                Util.snackBar(progressLayout, "Enter Mobile Number", Color.RED);
                // Toast.makeText(LoginActivity.this, "Enter Mobile Number", Toast.LENGTH_SHORT).show();
            } else {
                login(mobile);
            }
        } else {
            Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);

        }
    }

    private void login(String mobile) {

        progressLayout.setVisibility(View.VISIBLE);
        Call<LoginMobileResponse> call = RetrofitClient.getInstance().getApi().LoginMobile(mobile, deviceId);
        call.enqueue(new Callback<LoginMobileResponse>() {
            @Override
            public void onResponse(Call<LoginMobileResponse> call, Response<LoginMobileResponse> response) {
                if (response.isSuccessful()) {


                    LoginMobileResponse loginMobileResponse = response.body();

                    if (loginMobileResponse.getStatus().equals("10100")) {
                        progressLayout.setVisibility(View.GONE);
                        Toast.makeText(LoginActivity.this, loginMobileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(LoginActivity.this, OTPActivity.class);
                        intent.putExtra(MOBILE_KEY, mobile);
                        startActivity(intent);

                        //setDefaults();

                    } else if (loginMobileResponse.getStatus().equals("10200")) {
                        progressLayout.setVisibility(View.GONE);
                        Util.snackBar(progressLayout, loginMobileResponse.getMessage(), Color.RED);
                        // Toast.makeText(LoginActivity.this, loginMobileResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    } else if (loginMobileResponse.getStatus().equals("10300")) {
                        progressLayout.setVisibility(View.GONE);
                        Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                        intent.putExtra(MOBILE_KEY, mobile);
                        startActivity(intent);

                    } else if (loginMobileResponse.getStatus().equals("10400")) {
                        progressLayout.setVisibility(View.GONE);
                        Util.snackBar(progressLayout, loginMobileResponse.getMessage(), Color.RED);
                        // Toast.makeText(LoginActivity.this, loginMobileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(getApplicationContext(), jObjError.getJSONObject("message").getString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<LoginMobileResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Util.snackBar(progressLayout, t.getMessage(), Color.BLUE);
                //Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }


    // Construct a request for phone numbers and show the picker
    private void requestHint() throws IntentSender.SendIntentException {
        HintRequest hintRequest = new HintRequest.Builder()
                .setPhoneNumberIdentifierSupported(true)
                .build();

        CredentialsOptions options = new CredentialsOptions.Builder()
                .forceEnableSaveDialog()
                .build();
        PendingIntent pendingIntent =  Credentials.getClient(this, options).getHintPickerIntent(hintRequest);

        startIntentSenderForResult(pendingIntent.getIntentSender(), RESOLVE_HINT, null, 0, 0, 0);

    }


    // Obtain the phone number from the result
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESOLVE_HINT) {
            if (resultCode == RESULT_OK) {
                Credential credential = data.getParcelableExtra(Credential.EXTRA_KEY);

                String phoneNumber = credential.getId();
               // edtMobile.setText(credential.getId());
                // credential.getId(); <-- E.164 format phone number on 10.2.+ devices

                if(phoneNumber.startsWith("+")) {
                    if(phoneNumber.length()==13) {
                        String str_getMOBILE=phoneNumber.substring(3);
                        edtMobile.setText(str_getMOBILE);
                    }
                    else if(phoneNumber.length()==14) {
                        String str_getMOBILE=phoneNumber.substring(4);
                        edtMobile.setText(str_getMOBILE);
                    }
                }
                else {
                    edtMobile.setText(phoneNumber);
                }
            }
        }
    }


    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }
}
