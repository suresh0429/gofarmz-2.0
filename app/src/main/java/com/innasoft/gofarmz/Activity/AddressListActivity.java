package com.innasoft.gofarmz.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.gofarmz.Adapter.AddressAdapter;
import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.Preferences.UserSessionManager;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.AddressDeleteResponse;
import com.innasoft.gofarmz.Response.AddressListResponse;
import com.innasoft.gofarmz.Utilis.Util;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Utilis.Constants.ADD_ADDRESS;
import static com.innasoft.gofarmz.Utilis.Constants.BOTTAM_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.CHANGE_ADDRESS;
import static com.innasoft.gofarmz.Utilis.Constants.HOMEACTIVITY;
import static com.innasoft.gofarmz.Utilis.Constants.LOCATIONNAME;
import static com.innasoft.gofarmz.Utilis.Constants.LOCATION_PREF;
import static com.innasoft.gofarmz.Utilis.Constants.MODULE;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_CART;
import static com.innasoft.gofarmz.Utilis.Constants.PRE_ORDER_CART;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;
import static com.innasoft.gofarmz.Utilis.Constants.TYPE;

public class AddressListActivity extends AppCompatActivity implements InternetConnectivityListener {

    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;
    @BindView(R.id.recyclerAddress)
    RecyclerView recyclerAddress;
    @BindView(R.id.addAddressLayout)
    LinearLayout addAddressLayout;
    @BindView(R.id.addressLayout)
    LinearLayout addressLayout;
    @BindView(R.id.txtRetry)
    TextView txtRetry;
    @BindView(R.id.no_internetLayout)
    LinearLayout noInternetLayout;
    @BindView(R.id.btnAddAddress)
    TextView btnAddAddress;
    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    boolean internet;
    private String title, type, module,deviceId, token, user_id;
    UserSessionManager userSessionManager;
    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_list);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Address");
        if (getIntent() != null) {
            title = getIntent().getStringExtra(TITLE);
            type = getIntent().getStringExtra(TYPE);
            module = getIntent().getStringExtra(MODULE);
            position = getIntent().getIntExtra(BOTTAM_TAB_POSITION, 0);

        }

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.KEY_DEVICEID);
        user_id = userDetails.get(UserSessionManager.KEY_ID);
        token = userDetails.get(UserSessionManager.KEY_TOKEN);

        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
            internet = isConnected;
            addressListData();
            noInternetLayout.setVisibility(View.GONE);
        } else {
            internet = false;
            noInternetLayout.setVisibility(View.VISIBLE);
            //Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }

    private void addressListData() {
        progressLayout.setVisibility(View.VISIBLE);
        Call<AddressListResponse> call = RetrofitClient.getInstance().getApi().AddressList(token, deviceId, user_id);
        call.enqueue(new Callback<AddressListResponse>() {
            @Override
            public void onResponse(Call<AddressListResponse> call, Response<AddressListResponse> response) {
                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);
                    AddressListResponse addressListRespons = response.body();
                    if (addressListRespons.getStatus().equals("10100")) {
                        List<AddressListResponse.DataBean> dataBeanList = addressListRespons.getData();

                        AddressAdapter addressesAdapter = new AddressAdapter(dataBeanList, AddressListActivity.this, title,module);
                        recyclerAddress.setLayoutManager(new LinearLayoutManager(AddressListActivity.this, LinearLayoutManager.VERTICAL, false));
                        recyclerAddress.setItemAnimator(new DefaultItemAnimator());
                        recyclerAddress.setAdapter(addressesAdapter);

                    } else if (addressListRespons.getStatus().equals("10200")) {
                        addressLayout.setVisibility(View.VISIBLE);
                        btnAddAddress.setVisibility(View.GONE);
                        recyclerAddress.setVisibility(View.GONE);

                        Toast.makeText(AddressListActivity.this, addressListRespons.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (addressListRespons.getStatus().equals("10300")) {
                        addressLayout.setVisibility(View.VISIBLE);
                        btnAddAddress.setVisibility(View.GONE);
                        recyclerAddress.setVisibility(View.GONE);
                        // Toast.makeText(AddressListActivity.this, addressListRespons.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<AddressListResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Toast.makeText(AddressListActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void deleteAddress(String id) {
        progressLayout.setVisibility(View.VISIBLE);
        Call<AddressDeleteResponse> call = RetrofitClient.getInstance().getApi().DeleteAddress(token, deviceId, user_id, id);
        call.enqueue(new Callback<AddressDeleteResponse>() {
            @Override
            public void onResponse(Call<AddressDeleteResponse> call, Response<AddressDeleteResponse> response) {
                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);

                    AddressDeleteResponse addressDeleteResponse = response.body();
                    if (addressDeleteResponse.getStatus().equals("10100")) {
                        addressListData();
                        Util.snackBar(progressLayout, addressDeleteResponse.getMessage(), Color.YELLOW);
                    } else if (addressDeleteResponse.getStatus().equals("10200")) {
                        Util.snackBar(progressLayout, addressDeleteResponse.getMessage(), Color.YELLOW);
                    } else if (addressDeleteResponse.getStatus().equals("10300")) {
                        Util.snackBar(progressLayout, addressDeleteResponse.getMessage(), Color.YELLOW);
                    } else if (addressDeleteResponse.getStatus().equals("10400")) {
                        Util.snackBar(progressLayout, addressDeleteResponse.getMessage(), Color.YELLOW);
                    }
                }
            }

            @Override
            public void onFailure(Call<AddressDeleteResponse> call, Throwable t) {
                Toast.makeText(AddressListActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                progressLayout.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public void onBackPressed() {

        if (title.equalsIgnoreCase(CHANGE_ADDRESS) || title.equalsIgnoreCase(ADD_ADDRESS)) {

            if (module.equalsIgnoreCase(ORDER_CART)) {
                Intent intent = new Intent(AddressListActivity.this, CheckoutActivity.class);
                intent.putExtra(BOTTAM_TAB_POSITION, position);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }else {
                Intent intent = new Intent(AddressListActivity.this, PreOrderCheckoutActivity.class);
                intent.putExtra(MODULE, PRE_ORDER_CART);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }

        } else if (title.equalsIgnoreCase(HOMEACTIVITY)) {
            Intent intent = new Intent(AddressListActivity.this, HomeActivity.class);
            intent.putExtra(BOTTAM_TAB_POSITION, position);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }else {
            //onBackPressed();
            Intent intent = new Intent(AddressListActivity.this, HomeActivity.class);
            intent.putExtra(BOTTAM_TAB_POSITION, position);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                if (title.equalsIgnoreCase(CHANGE_ADDRESS) || title.equalsIgnoreCase(ADD_ADDRESS)) {
                    if (module.equalsIgnoreCase(ORDER_CART)) {
                        Intent intent = new Intent(AddressListActivity.this, CheckoutActivity.class);
                        intent.putExtra(BOTTAM_TAB_POSITION, position);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }else {
                        Intent intent = new Intent(AddressListActivity.this, PreOrderCheckoutActivity.class);
                        intent.putExtra(MODULE, PRE_ORDER_CART);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                } else if (title.equalsIgnoreCase(HOMEACTIVITY)) {
                    Intent intent = new Intent(AddressListActivity.this, HomeActivity.class);
                    intent.putExtra(BOTTAM_TAB_POSITION, position);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                }else {
                    //onBackPressed();
                    Intent intent = new Intent(AddressListActivity.this, HomeActivity.class);
                    intent.putExtra(BOTTAM_TAB_POSITION, position);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.addAddressLayout)
    public void onViewClicked() {
        Intent intent = new Intent(AddressListActivity.this, AddAddressActivity.class);
        intent.putExtra(TITLE, title);
        intent.putExtra(MODULE, module);
        intent.putExtra(BOTTAM_TAB_POSITION, position);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void chooseAddress(String id) {
        Intent output = new Intent();
        output.putExtra("AddressId", id);
        setResult(RESULT_OK, output);
        finish();
    }

    private void setLocationPref(String locationName){
        SharedPreferences preferences = getSharedPreferences(LOCATION_PREF,0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(LOCATIONNAME,locationName);
        editor.apply();
    }
}
