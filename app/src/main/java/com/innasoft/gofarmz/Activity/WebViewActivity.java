package com.innasoft.gofarmz.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.Constraints;

import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.R;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;
import static com.innasoft.gofarmz.Utilis.Constants.BOTTAM_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;
import static com.innasoft.gofarmz.Utilis.Constants.TYPE;

public class WebViewActivity extends AppCompatActivity implements InternetConnectivityListener {
    @BindView(R.id.webview)
    WebView webview;
    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;
    @BindView(R.id.about_us_tittle)
    TextView aboutUsTittle;
    @BindView(R.id.about_us_desp)
    TextView aboutUsDesp;
    @BindView(R.id.mail)
    TextView mail;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.supportLayout)
    LinearLayout supportLayout;
    @BindView(R.id.txtRetry)
    TextView txtRetry;
    @BindView(R.id.no_internetLayout)
    LinearLayout noInternetLayout;
    private InternetAvailabilityChecker mInternetAvailabilityChecker;

    String title, type;
    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        if (getIntent() != null) {
            title = getIntent().getStringExtra(TITLE);
            type = getIntent().getStringExtra(TYPE);
            position = getIntent().getIntExtra(BOTTAM_TAB_POSITION, 0);
            getSupportActionBar().setTitle(title);
        }


        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);
    }


    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(Constraints.TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
            initControls();
            noInternetLayout.setVisibility(View.GONE);
        } else {
            noInternetLayout.setVisibility(View.VISIBLE);
           // Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }

    private void initControls() {

        progressLayout.setVisibility(View.VISIBLE);

        webview.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //To open hyperlink in existing WebView
                view.loadUrl(url);
                return false;
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(getApplicationContext(), description, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                progressLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                progressLayout.setVisibility(View.GONE);
            }
        });


        /*if (title.equalsIgnoreCase("Terms & Conditions")) {

        } else if (title.equalsIgnoreCase("Privacy Policy")) {
            loadPrivacyPolicy(type);
        }*/

        if (title.equalsIgnoreCase("Support")) {
            supportLayout.setVisibility(View.VISIBLE);
            webview.setVisibility(View.GONE);
            progressLayout.setVisibility(View.GONE);
        } else {
            supportLayout.setVisibility(View.GONE);
            webview.setVisibility(View.VISIBLE);
            loadData(type);
        }
    }

    private void loadData(String type) {

        progressLayout.setVisibility(View.VISIBLE);
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().datafromwebview(type);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLayout.setVisibility(View.GONE);
                try {
                    assert response.body() != null;
                    String html = response.body().string();
                    Document document = Jsoup.parse(html);
                    Log.d(TAG, "onResponse: " + document);
                    webview.getSettings().setJavaScriptEnabled(true);
                    webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                    webview.getSettings().setBuiltInZoomControls(false);
                    // webview.loadUrl("https://zatackcoder.com/webview-based-android-app/");
                    //webview.loadData(String.valueOf(document), "text/html", null);
                    webview.loadDataWithBaseURL(null, html, "text/html", "utf-8", null);

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(WebViewActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                progressLayout.setVisibility(View.GONE);
            }
        });


    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webview.canGoBack()) {
            webview.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(WebViewActivity.this, HomeActivity.class);
        intent.putExtra(BOTTAM_TAB_POSITION, position);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(WebViewActivity.this, HomeActivity.class);
                intent.putExtra(BOTTAM_TAB_POSITION, position);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.mail, R.id.phone})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mail:
                String[] recipients = new String[]{"support@gofarmz.com", ""};
                Intent testIntent = new Intent(Intent.ACTION_SEND);
                testIntent.setType("message/rfc822");
                testIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                testIntent.putExtra(Intent.EXTRA_TEXT, "");
                testIntent.putExtra(Intent.EXTRA_EMAIL, recipients);
                startActivity(testIntent);
                break;
            case R.id.phone:
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + Uri.encode("+91 8106670757")));
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callIntent);
                break;
        }
    }
}
