package com.innasoft.gofarmz.Activity;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.innasoft.gofarmz.Adapter.FormarAdapter;
import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.Preferences.UserSessionManager;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.FormarResponse;
import com.innasoft.gofarmz.Utilis.Util;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Utilis.Constants.IMAGE;
import static com.innasoft.gofarmz.Utilis.Constants.PRODUCTID;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;

public class FormarActivity extends AppCompatActivity implements InternetConnectivityListener {
    @BindView(R.id.mainImage)
    ImageView mainImage;
    @BindView(R.id.txtProductName)
    TextView txtProductName;
    @BindView(R.id.txtDescription)
    TextView txtDescription;
    @BindView(R.id.farmerRecycler)
    RecyclerView farmerRecycler;
    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;
    @BindView(R.id.txtRetry)
    TextView txtRetry;
    @BindView(R.id.no_internetLayout)
    LinearLayout noInternetLayout;
    @BindView(R.id.no_dataFound)
    LinearLayout noDataFound;
    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    String title, productId, image, deviceId, token, user_id;
    UserSessionManager userSessionManager;
    boolean internet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formar);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Know Your Farmer");

        if (getIntent() != null) {
            title = getIntent().getStringExtra(TITLE);
            productId = getIntent().getStringExtra(PRODUCTID);
            image = getIntent().getStringExtra(IMAGE);
            Log.d(TAG, "onCreate: " + image);

            Glide.with(this).load(image).into(mainImage);
            txtProductName.setText(title);

        }
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.KEY_DEVICEID);
        user_id = userDetails.get(UserSessionManager.KEY_ID);
        token = userDetails.get(UserSessionManager.KEY_TOKEN);

        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
            getFormersList();
            noInternetLayout.setVisibility(View.GONE);
        } else {
            // internet = false;
            noInternetLayout.setVisibility(View.VISIBLE);
            Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }


    private void getFormersList() {
        progressLayout.setVisibility(View.VISIBLE);

        Call<FormarResponse> call = RetrofitClient.getInstance().getApi().getFormar(deviceId, productId);
        call.enqueue(new Callback<FormarResponse>() {
            @Override
            public void onResponse(Call<FormarResponse> call, Response<FormarResponse> response) {

                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);
                    FormarResponse baseResponse = response.body();

                    if (baseResponse.getStatus().equals("10100")) {

                        List<FormarResponse.DataBean> formerresponse = baseResponse.getData();
                        FormarAdapter formarAdapter = new FormarAdapter(formerresponse, FormarActivity.this);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(FormarActivity.this, RecyclerView.VERTICAL, false);
                        farmerRecycler.setLayoutManager(linearLayoutManager);
                        farmerRecycler.setAdapter(formarAdapter);


                    } else if (baseResponse.getStatus().equals("10200")) {

                        Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (baseResponse.getStatus().equals("10300")) {

                        noDataFound.setVisibility(View.VISIBLE);
                       // Toast.makeText(FormarActivity.this, "No Data Found.", Toast.LENGTH_SHORT).show();
                    } else if (baseResponse.getStatus().equals("10400")) {

                        Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<FormarResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
