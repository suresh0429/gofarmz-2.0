package com.innasoft.gofarmz.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.innasoft.gofarmz.Adapter.CityAdapter;
import com.innasoft.gofarmz.Adapter.StateAdapter;
import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.Preferences.UserSessionManager;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.BaseResponse;
import com.innasoft.gofarmz.Response.StateResponse;
import com.innasoft.gofarmz.Utilis.Util;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Preferences.UserSessionManager.KEY_DEVICEID;
import static com.innasoft.gofarmz.Preferences.UserSessionManager.KEY_ID;
import static com.innasoft.gofarmz.Preferences.UserSessionManager.KEY_TOKEN;
import static com.innasoft.gofarmz.Utilis.Constants.BOTTAM_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;
import static com.innasoft.gofarmz.Utilis.Constants.TYPE;

public class ReferFarmerActivity extends AppCompatActivity implements InternetConnectivityListener {
    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    boolean internet;
    UserSessionManager userSessionManager;
    private String title, type, deviceId, token, user_id;
    int position;
    List<StateResponse.DataBean> stateDataList;
    List<StateResponse.DataBean> citydataList;

    @BindView(R.id.etstate)
    TextInputEditText etstate;
    @BindView(R.id.stateTill)
    TextInputLayout stateTill;
    @BindView(R.id.etcity)
    TextInputEditText etcity;
    @BindView(R.id.cityTill)
    TextInputLayout cityTill;
    @BindView(R.id.etfarmerName)
    TextInputEditText etfarmerName;
    @BindView(R.id.farmerNameTill)
    TextInputLayout farmerNameTill;
    @BindView(R.id.etfarmerMobile)
    TextInputEditText etfarmerMobile;
    @BindView(R.id.farmMobileTill)
    TextInputLayout farmMobileTill;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refer_farmer);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressLayout.setVisibility(View.GONE);
        if (getIntent() != null) {
            title = getIntent().getStringExtra(TITLE);
            type = getIntent().getStringExtra(TYPE);
            position = getIntent().getIntExtra(BOTTAM_TAB_POSITION, 0);
            getSupportActionBar().setTitle(title);
        }
        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(KEY_DEVICEID);
        token = userDetails.get(KEY_TOKEN);
        user_id = userDetails.get(KEY_ID);


        etfarmerName.addTextChangedListener(new MyTextWatcher(etfarmerName));
        etfarmerMobile.addTextChangedListener(new MyTextWatcher(etfarmerMobile));
        getStateList();

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ReferFarmerActivity.this, HomeActivity.class);
        intent.putExtra(BOTTAM_TAB_POSITION, position);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
            internet = isConnected;

        } else {
            internet = false;
            Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }


    @OnClick({R.id.etstate, R.id.etcity, R.id.btnSubmit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.etstate:
                if (!etfarmerName.getText().toString().isEmpty() || !etfarmerMobile.getText().toString().isEmpty()){
                    stateDilogue();
                    etcity.setText(null);
                }else {
                    etstate.setClickable(false);
                    etstate.setFocusable(false);
                }

                break;
            case R.id.etcity:
                if (!etstate.getText().toString().isEmpty()){
                    cityDilogue();
                }else {
                    etcity.setClickable(false);
                    etcity.setFocusable(false);
                    Util.snackBar(progressLayout, "Select State First", Color.YELLOW);

                }

                break;
            case R.id.btnSubmit:
                if (internet){
                    referFarmer();
                }else {
                    Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
                }
                break;
        }
    }

    private void getStateList() {
        progressLayout.setVisibility(View.VISIBLE);
        Call<StateResponse> call = RetrofitClient.getInstance().getApi().stateList(token, "1");
        call.enqueue(new Callback<StateResponse>() {
            @Override
            public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {
                StateResponse baseResponse = response.body();
                if (baseResponse.getStatus().equals("10100")) {
                    progressLayout.setVisibility(View.GONE);
                    stateDataList = baseResponse.getData();

                }

                if (baseResponse.getStatus().equals("10200")) {

                    Toast.makeText(getApplicationContext(), "Invalid Details.", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<StateResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                progressLayout.setVisibility(View.GONE);
            }
        });

    }

    public void stateData(String id, String name) {
        etstate.setText(name);
        getCityList(id);

    }

    private void getCityList(final String cityList) {
        progressLayout.setVisibility(View.VISIBLE);
        Call<StateResponse> call = RetrofitClient.getInstance().getApi().citiesList(token, cityList);
        call.enqueue(new Callback<StateResponse>() {
            @Override
            public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {
                StateResponse baseResponse = response.body();
                if (baseResponse.getStatus().equals("10100")) {
                    progressLayout.setVisibility(View.GONE);
                    citydataList = baseResponse.getData();

                }

                if (baseResponse.getStatus().equals("10200")) {
                    progressLayout.setVisibility(View.GONE);
                    Toast.makeText(ReferFarmerActivity.this, "Invalid Details.", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<StateResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                progressLayout.setVisibility(View.GONE);
            }
        });

    }

    public void cityData(String id, String name) {
        etcity.setText(name);

    }

    private void stateDilogue() {
        //before inflating the custom alert dialog layout, we will get the current title viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(ReferFarmerActivity.this).inflate(R.layout.place_list_dialog, viewGroup, false);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(ReferFarmerActivity.this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();

        TextView txtTitle = (TextView) dialogView.findViewById(R.id.txtTitle);
        txtTitle.setText("Select State");
        RecyclerView recyclerView = (RecyclerView) dialogView.findViewById(R.id.placesRecycler);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(ReferFarmerActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager1);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        StateAdapter adapter = new StateAdapter(ReferFarmerActivity.this, stateDataList, alertDialog);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void cityDilogue() {
        //before inflating the custom alert dialog layout, we will get the current title viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(ReferFarmerActivity.this).inflate(R.layout.place_list_dialog, viewGroup, false);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(ReferFarmerActivity.this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();

        TextView txtTitle = (TextView) dialogView.findViewById(R.id.txtTitle);
        txtTitle.setText("Select City");
        RecyclerView recyclerView = (RecyclerView) dialogView.findViewById(R.id.placesRecycler);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(ReferFarmerActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager1);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        CityAdapter adapter = new CityAdapter(ReferFarmerActivity.this, citydataList, alertDialog);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void referFarmer(){
        final String name = etfarmerName.getText().toString().trim();
        final String mobile = etfarmerMobile.getText().toString().trim();
        final String state = etstate.getText().toString().trim();
        final String city = etcity.getText().toString().trim();

        if ((!Util.isValidName(name, farmerNameTill, etfarmerName, ReferFarmerActivity.this))) {
            return;
        }
        if ((!Util.isValidPhoneNumber(mobile, farmMobileTill, etfarmerMobile, ReferFarmerActivity.this))) {
            return;
        }
        if (state.isEmpty() || city.isEmpty()){
            Util.snackBar(progressLayout, "Enter Valid Details", Color.YELLOW);
           return;
        }
        progressLayout.setVisibility(View.VISIBLE);
        Call<BaseResponse> call = RetrofitClient.getInstance().getApi().referFarmer(deviceId,user_id,name,state,mobile,city);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, retrofit2.Response<BaseResponse> response) {
                BaseResponse baseResponse = response.body();
                progressLayout.setVisibility(View.GONE);
                if (baseResponse.getStatus().equals("10100")) {

                    Toast.makeText(ReferFarmerActivity.this, "Refer Farmer Successfully..!", Toast.LENGTH_SHORT).show();
                    finish();
                    /*Intent intent = new Intent(ReferFarmerActivity.this, HomeActivity.class);
                    intent.putExtra(BOTTAM_TAB_POSITION, position);
                    startActivity(intent);*/

                }

                if (baseResponse.getStatus().equals("10200")) {

                    Toast.makeText(ReferFarmerActivity.this, "Invalid Details.", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                progressLayout.setVisibility(View.GONE);            }
        });
    }


    // text input layout class
    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.etName:
                    Util.isValidName(etfarmerName.getText().toString().trim(), farmerNameTill, etfarmerName, ReferFarmerActivity.this);
                    break;
                case R.id.etMobile:
                    Util.isValidPhoneNumber(etfarmerMobile.getText().toString().trim(), farmMobileTill, etfarmerMobile, ReferFarmerActivity.this);
                    break;


            }
        }
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(ReferFarmerActivity.this, HomeActivity.class);
                intent.putExtra(BOTTAM_TAB_POSITION, position);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
