package com.innasoft.gofarmz.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.gofarmz.Adapter.PreOrderListAdapter;
import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.Preferences.UserSessionManager;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.PreOrderResponse;
import com.innasoft.gofarmz.Utilis.Converter;
import com.innasoft.gofarmz.Utilis.Util;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Utilis.Constants.BOTTAM_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.CART_COUNT;
import static com.innasoft.gofarmz.Utilis.Constants.COUNT;

public class PreOrdersActivity extends AppCompatActivity implements InternetConnectivityListener {

    @BindView(R.id.txtRetry)
    TextView txtRetry;
    @BindView(R.id.no_internetLayout)
    LinearLayout noInternetLayout;
    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;
    @BindView(R.id.txtGotoHome)
    TextView txtGotoHome;
    @BindView(R.id.no_dataFound)
    LinearLayout noDataFound;
    @BindView(R.id.prOrderRecycler)
    RecyclerView prOrderRecycler;
    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    UserSessionManager userSessionManager;
    boolean internet;
    int position,cartCount;
    private String title, catId, type, deviceId, token, user_id, basketName, basketId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_orders);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Pre Orders");

        if (getIntent() != null) {
            position = getIntent().getIntExtra(BOTTAM_TAB_POSITION, 0);

        }

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.KEY_DEVICEID);
        user_id = userDetails.get(UserSessionManager.KEY_ID);
        token = userDetails.get(UserSessionManager.KEY_TOKEN);




        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
            getPreOrderData();
            getCartCount();
            noInternetLayout.setVisibility(View.GONE);
        } else {
            // internet = false;
            noInternetLayout.setVisibility(View.VISIBLE);
            Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }

    private void getCartCount(){
        SharedPreferences preferences = getSharedPreferences(CART_COUNT,0);
        cartCount = preferences.getInt(COUNT,0);
        Log.d(TAG, "onCreateOptionsMenu: "+cartCount);
        invalidateOptionsMenu();
    }

    private void getPreOrderData() {
        progressLayout.setVisibility(View.VISIBLE);
        Call<PreOrderResponse> call = RetrofitClient.getInstance().getApi().PreOrders(token, deviceId, user_id);
        call.enqueue(new Callback<PreOrderResponse>() {
            @Override
            public void onResponse(Call<PreOrderResponse> call, Response<PreOrderResponse> response) {
                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);

                    PreOrderResponse preOrderResponse = response.body();
                    if (preOrderResponse.getStatus().equals("10100")) {

                        PreOrderResponse.DataBean dataBean = preOrderResponse.getData();
                       // cartCountTxt.setText(String.valueOf(preOrderResponse.getCart_count()));

                        PreOrderResponse.VersionsBean versionsBeanList = preOrderResponse.getVersions();
                        List<PreOrderResponse.DataBean.ProductsBean> productsBeanList = dataBean.getProducts();

                        prOrderRecycler.setLayoutManager(new GridLayoutManager(PreOrdersActivity.this, 2));
                        prOrderRecycler.setItemAnimator(new DefaultItemAnimator());
                        PreOrderListAdapter preOrderListAdapter = new PreOrderListAdapter(PreOrdersActivity.this, productsBeanList);
                        prOrderRecycler.setAdapter(preOrderListAdapter);
                    } else if (preOrderResponse.getStatus().equals("10200")) {

                        Toast.makeText(getApplicationContext(), preOrderResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (preOrderResponse.getStatus().equals("10300")) {


                        Toast.makeText(getApplicationContext(), preOrderResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        noDataFound.setVisibility(View.VISIBLE);
                        txtGotoHome.setOnClickListener(view -> {
                            Intent intent = new Intent(PreOrdersActivity.this, HomeActivity.class);
                            intent.putExtra(BOTTAM_TAB_POSITION,0);
                            startActivity(intent);
                        });


                    } else if (preOrderResponse.getStatus().equals("10400")) {

                        Toast.makeText(getApplicationContext(), preOrderResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else {

                        noDataFound.setVisibility(View.VISIBLE);
                        txtGotoHome.setOnClickListener(view -> {
                            Intent intent = new Intent(PreOrdersActivity.this,HomeActivity.class);
                            intent.putExtra(BOTTAM_TAB_POSITION,0);
                            startActivity(intent);
                        });
                        //preOrderListAdapter.notifyDataSetChanged();

                    }

                }
            }

            @Override
            public void onFailure(Call<PreOrderResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCartCount();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.preorder_menu, menu);

        final MenuItem menuItem = menu.findItem(R.id.cart);
        Log.d(TAG, "onCreateOptionsMenu: "+"1233");

        menuItem.setIcon(Converter.convertLayoutToImage(PreOrdersActivity.this, cartCount, R.drawable.ic_shopping_cart_black_24dp));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        /*Intent intent = new Intent(PreOrdersActivity.this, HomeActivity.class);
        intent.putExtra(BOTTAM_TAB_POSITION, position);
        startActivity(intent);*/

        finish();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
               /* Intent intent = new Intent(PreOrdersActivity.this, HomeActivity.class);
                intent.putExtra(BOTTAM_TAB_POSITION, position);
                startActivity(intent);*/
                finish();
                break;
            case R.id.cart:
                Log.d(TAG, "onOptionsItemSelected: "+cartCount);
                if (cartCount == 0) {
                    Util.snackBar(progressLayout, "No Items Found !", Color.YELLOW);
                } else {
                    Intent i = new Intent(PreOrdersActivity.this, PreOrderCartActivity.class);
                    startActivity(i);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
