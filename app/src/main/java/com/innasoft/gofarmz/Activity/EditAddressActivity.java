package com.innasoft.gofarmz.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.Preferences.UserSessionManager;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.EditAddressResponse;
import com.innasoft.gofarmz.Utilis.AnchorSheetBehavior;
import com.innasoft.gofarmz.Utilis.MapRipple;
import com.innasoft.gofarmz.Utilis.Util;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Utilis.Constants.BOTTAM_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.MODULE;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;

public class EditAddressActivity extends AppCompatActivity  implements InternetConnectivityListener, OnMapReadyCallback, GoogleMap.OnCameraMoveListener, GoogleMap.OnCameraIdleListener{
    @BindView(R.id.marker_image)
    ImageView markerImage;
    @BindView(R.id.txtArea)
    TextView txtArea;
    @BindView(R.id.txtaddressline)
    TextView txtaddressline;
    @BindView(R.id.textSearch)
    TextView textSearch;
    @BindView(R.id.topLayout)
    RelativeLayout topLayout;
    @BindView(R.id.etName)
    TextInputEditText etName;
    @BindView(R.id.etAddress)
    TextInputEditText etAddress;
    @BindView(R.id.etLandmark)
    TextInputEditText etLandmark;
    @BindView(R.id.etMobile)
    TextInputEditText etMobile;
    @BindView(R.id.swithDefalut)
    Switch swithDefalut;
    @BindView(R.id.btnSave)
    Button btnSave;
    @BindView(R.id.nameTill)
    TextInputLayout nameTill;
    @BindView(R.id.addressTill)
    TextInputLayout addressTill;
    @BindView(R.id.landmarkTill)
    TextInputLayout landmarkTill;
    @BindView(R.id.mobileTill)
    TextInputLayout mobileTill;
    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;

    private Geocoder geocoder;
    private AnchorSheetBehavior bsBehavior;
    View bottomSheet;

    private GoogleMap mMap;
    private static final int AUTOCOMPLETE_REQUEST_CODE = 22;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private PlacesClient placesClient;
    private List<AutocompletePrediction> predictionList;

    private Location mLastKnownLocation;
    private LocationCallback locationCallback;

    private View mapView;

    private final float DEFAULT_ZOOM = 16;

    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    boolean internet;
    private String title,module, type, deviceId, token, user_id,addressId, city, state, pincode, isDefalut;
    private double latitude, longitude;
    UserSessionManager userSessionManager;
    int position;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_address);
        ButterKnife.bind(this);

        if (getIntent() != null) {
            title = getIntent().getStringExtra(TITLE);
            module = getIntent().getStringExtra(MODULE);
            addressId = getIntent().getStringExtra("id");
            latitude = getIntent().getDoubleExtra("latitude",0.0);
            longitude = getIntent().getDoubleExtra("longitude",0.0);
            String addressline = getIntent().getStringExtra("addressline1");
            String landmark = getIntent().getStringExtra("landmark");
            String name = getIntent().getStringExtra("name");
            String contactNo = getIntent().getStringExtra("contactNo");
            isDefalut = getIntent().getStringExtra("isDefault");

            etName.setText(name);
            etAddress.setText(addressline);
            etLandmark.setText(landmark);
            etMobile.setText(contactNo);


        }
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.KEY_DEVICEID);
        user_id = userDetails.get(UserSessionManager.KEY_ID);
        token = userDetails.get(UserSessionManager.KEY_TOKEN);

        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapView = mapFragment.getView();

        if (isDefalut.equalsIgnoreCase("Yes")){
            swithDefalut.setChecked(true);
            isDefalut = "Yes";
        }
        else {
            swithDefalut.setChecked(false);
            isDefalut = "No";
        }
        swithDefalut.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                isDefalut = "Yes";
            } else {
                isDefalut = "No";

            }
        });


        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(EditAddressActivity.this);
        Places.initialize(EditAddressActivity.this, getString(R.string.googleapikey));
        placesClient = Places.createClient(this);



        bottomSheet = findViewById(R.id.bottomsheet_map);
        bsBehavior = AnchorSheetBehavior.from(bottomSheet);
        bsBehavior.setState(AnchorSheetBehavior.STATE_COLLAPSED);

        //anchor offset. any value between 0 and 1 depending upon the position u want
        bsBehavior.setAnchorOffset(0.5f);
        bsBehavior.setAnchorSheetCallback(new AnchorSheetBehavior.AnchorSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == AnchorSheetBehavior.STATE_COLLAPSED) {
                    //action if needed
                }

                if (newState == AnchorSheetBehavior.STATE_EXPANDED) {

                }

                if (newState == AnchorSheetBehavior.STATE_DRAGGING) {

                }

                if (newState == AnchorSheetBehavior.STATE_ANCHOR) {

                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                float h = bottomSheet.getHeight();
                float off = h * slideOffset;

                switch (bsBehavior.getState()) {
                    case AnchorSheetBehavior.STATE_DRAGGING:
                        setMapPaddingBotttom(off);
                        //reposition marker at the center
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,longitude), DEFAULT_ZOOM));

                        // if (mLoc != null) mMap.moveCamera(CameraUpdateFactory.newLatLng(mLoc));
                        break;
                    case AnchorSheetBehavior.STATE_SETTLING:
                        setMapPaddingBotttom(off);
                        //reposition marker at the center
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,longitude), DEFAULT_ZOOM));

                        // if (mLoc != null) mMap.moveCamera(CameraUpdateFactory.newLatLng(mLoc));
                        break;
                    case AnchorSheetBehavior.STATE_HIDDEN:
                        break;
                    case AnchorSheetBehavior.STATE_EXPANDED:
                        break;
                    case AnchorSheetBehavior.STATE_COLLAPSED:
                        break;
                }
            }
        });

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
            internet = isConnected;

        } else {
            internet = false;
            Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }

    @OnClick({R.id.textSearch, R.id.btnSave})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.textSearch:
                List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.FULLSCREEN, fields).setCountry("IN") //INDIA
                        .build(this);
                startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
                break;
            case R.id.btnSave:
                if (internet) {
                    editAddress();
                } else {
                    Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
                }
                break;
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        if (mapView != null && mapView.findViewById(Integer.parseInt("1")) != null) {
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 60, 40, 0);
        }

       // setTexts(latitude, longitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,longitude), DEFAULT_ZOOM));

        //check if gps is enabled or not and then request user to enable it
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);

        SettingsClient settingsClient = LocationServices.getSettingsClient(EditAddressActivity.this);
        Task<LocationSettingsResponse> task = settingsClient.checkLocationSettings(builder.build());

        task.addOnSuccessListener(EditAddressActivity.this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                //getDeviceLocation();
            }
        });

        task.addOnFailureListener(EditAddressActivity.this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    ResolvableApiException resolvable = (ResolvableApiException) e;
                    try {
                        resolvable.startResolutionForResult(EditAddressActivity.this, 51);
                    } catch (IntentSender.SendIntentException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {

                return false;
            }
        });


        // maps events we need to respond to
        mMap.setOnCameraMoveListener(this);
        mMap.setOnCameraIdleListener(this);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Objects.requireNonNull(place.getLatLng()).latitude, place.getLatLng().longitude), DEFAULT_ZOOM));
                setTexts(place.getLatLng().latitude, place.getLatLng().longitude);

                bsBehavior.setAnchorOffset(0.5f);
                bsBehavior.setAnchorSheetCallback(new AnchorSheetBehavior.AnchorSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        if (newState == AnchorSheetBehavior.STATE_COLLAPSED) {
                            //action if needed
                        }

                        if (newState == AnchorSheetBehavior.STATE_EXPANDED) {

                        }

                        if (newState == AnchorSheetBehavior.STATE_DRAGGING) {

                        }

                        if (newState == AnchorSheetBehavior.STATE_ANCHOR) {

                        }
                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                        float h = bottomSheet.getHeight();
                        float off = h * slideOffset;

                        switch (bsBehavior.getState()) {
                            case AnchorSheetBehavior.STATE_DRAGGING:
                                setMapPaddingBotttom(off);
                                //reposition marker at the center

                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Objects.requireNonNull(place.getLatLng()).latitude, place.getLatLng().longitude), DEFAULT_ZOOM));
                                setTexts(place.getLatLng().latitude, place.getLatLng().longitude);

                                // if (mLoc != null) mMap.moveCamera(CameraUpdateFactory.newLatLng(mLoc));
                                break;
                            case AnchorSheetBehavior.STATE_SETTLING:
                                setMapPaddingBotttom(off);
                                //reposition marker at the center
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Objects.requireNonNull(place.getLatLng()).latitude, place.getLatLng().longitude), DEFAULT_ZOOM));
                                setTexts(place.getLatLng().latitude, place.getLatLng().longitude);

                                // if (mLoc != null) mMap.moveCamera(CameraUpdateFactory.newLatLng(mLoc));
                                break;
                            case AnchorSheetBehavior.STATE_HIDDEN:
                                break;
                            case AnchorSheetBehavior.STATE_EXPANDED:
                                break;
                            case AnchorSheetBehavior.STATE_COLLAPSED:
                                break;
                        }
                    }
                });
            }
        }
    }

    @SuppressLint("MissingPermission")
    private void getDeviceLocation() {
        mFusedLocationProviderClient.getLastLocation()
                .addOnCompleteListener(new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            mLastKnownLocation = task.getResult();

                            Log.d(TAG, "onLocationResult: " + mLastKnownLocation.getLatitude() + "--" + mLastKnownLocation.getLongitude());
                            if (mLastKnownLocation != null) {
                                setTexts(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                            } else {
                                final LocationRequest locationRequest = LocationRequest.create();
                                locationRequest.setInterval(10000);
                                locationRequest.setFastestInterval(5000);
                                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                                locationCallback = new LocationCallback() {
                                    @Override
                                    public void onLocationResult(LocationResult locationResult) {
                                        super.onLocationResult(locationResult);
                                        if (locationResult == null) {
                                            return;
                                        }
                                        mLastKnownLocation = locationResult.getLastLocation();
                                        Log.d(TAG, "onLocationResult: " + mLastKnownLocation.getLatitude() + "--" + mLastKnownLocation.getLongitude());
                                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                                        mFusedLocationProviderClient.removeLocationUpdates(locationCallback);

                                        setTexts(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
                                    }
                                };
                                mFusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);

                            }
                        } else {
                            Toast.makeText(EditAddressActivity.this, "unable to get last location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public void onCameraIdle() {
        markerImage.setVisibility(View.GONE);

        MapRipple mapRipple = new MapRipple(mMap, new LatLng(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude), this);
        mapRipple.stopRippleMapAnimation();
        mapRipple.withNumberOfRipples(1);
        // mapRipple.withFillColor(Color.parseColor("#FFA3D2E4"));
        mapRipple.withStrokeColor(Color.BLACK);
        mapRipple.withStrokewidth(3);
        mapRipple.withDistance(200);
        mapRipple.withRippleDuration(1800);
        // mapRipple.withDurationBetweenTwoRipples(1000);
        mapRipple.withTransparency(0.5f);
        mapRipple.startRippleMapAnimation();

        mMap.addMarker(new MarkerOptions()
                .position(mMap.getCameraPosition().target)
                .draggable(true)
                .anchor(0.5f, 0.6f)
                .icon(bitmapDescriptorFromVector(EditAddressActivity.this, R.drawable.ic_placeholder)));

        setTexts(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude);
        // addPulsatingEffect(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude);


    }

    @Override
    public void onCameraMove() {
        mMap.clear();
        markerImage.setVisibility(View.VISIBLE);
    }

    private void setMapPaddingBotttom(Float offset) {
        //From 0.0 (min) - 1.0 (max) // bsExpanded - bsCollapsed;
        Float maxMapPaddingBottom = 1.0f;
        mMap.setPadding(0, 0, 0, Math.round(offset * maxMapPaddingBottom));

    }

    private void setTexts(double latitudeValue, double longitudeValue) {

        try {
            List<Address> addresses;
            geocoder = new Geocoder(EditAddressActivity.this, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(latitudeValue, longitudeValue, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                String address1 = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String address2 = addresses.get(0).getAddressLine(1); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String areaName = addresses.get(0).getSubLocality();
                String cityName = addresses.get(0).getLocality();
                String stateName = addresses.get(0).getAdminArea();
                String countryName = addresses.get(0).getCountryName();
                String postalCodeName = addresses.get(0).getPostalCode();

                Log.e("Address1: ", "" + address1);
                Log.e("Address2: ", "" + address2);
                Log.e("AddressCity: ", "" + cityName);
                Log.e("AddressArea: ", "" + areaName);
                Log.e("AddressState: ", "" + stateName);
                Log.e("AddressCountry: ", "" + countryName);
                Log.e("AddressPostal: ", "" + postalCodeName);
                Log.e("AddressLatitude: ", "" + latitude);
                Log.e("AddressLongitude: ", "" + longitude);

                if (areaName != null) {
                    txtArea.setText(areaName);
                } else {
                    txtArea.setText("Select Location");
                }
                txtaddressline.setText(address1);

                latitude = latitudeValue;
                longitude = longitudeValue;
                city = cityName;
                state = stateName;
                pincode = postalCodeName;

            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
            //setMarker(latLng);
        }


    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }


    private void editAddress() {

        String name = etName.getText().toString();
        String addressLine = etAddress.getText().toString();
        String landmark = etLandmark.getText().toString();
        String mobile = etMobile.getText().toString();

        if (name.isEmpty()) {
            nameTill.setError("Please Enter Name");
            return;
        }else {
            nameTill.setError(null);
        }
        if (addressLine.isEmpty()) {
            addressTill.setError("Please Enter House / Flat / Block No. / Apartment Name");
            return;
        }else {
            addressTill.setError(null);
        }
        if (landmark.isEmpty()) {
            landmarkTill.setError("Please Enter Landmark");
            return;
        }else {
            landmarkTill.setError(null);
        }
        if (mobile.isEmpty()) {
            mobileTill.setError("Please Enter Mobile");
            return;
        }else {
            mobileTill.setError(null);
        }

        progressLayout.setVisibility(View.VISIBLE);
        Call<EditAddressResponse> call = RetrofitClient.getInstance().getApi().EditAddress(token, deviceId, user_id,addressId, name, addressLine, landmark, txtArea.getText().toString(), city,
                state, pincode, mobile, "", isDefalut, String.valueOf(latitude), String.valueOf(longitude));
        call.enqueue(new Callback<EditAddressResponse>() {
            @Override
            public void onResponse(Call<EditAddressResponse> call, Response<EditAddressResponse> response) {
                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);

                    EditAddressResponse addAddressResponse = response.body();
                    if (addAddressResponse.getStatus().equals("10100")) {

                        Intent intent = new Intent(EditAddressActivity.this, AddressListActivity.class);
                        intent.putExtra(TITLE, title);
                        intent.putExtra(MODULE, module);
                        intent.putExtra(BOTTAM_TAB_POSITION, 3);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);

                    } else if (addAddressResponse.getStatus().equals("10200")) {

                        Toast.makeText(EditAddressActivity.this, addAddressResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (addAddressResponse.getStatus().equals("10300")) {

                        Toast.makeText(EditAddressActivity.this, addAddressResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (addAddressResponse.getStatus().equals("10400")) {

                        Toast.makeText(EditAddressActivity.this, addAddressResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (addAddressResponse.getStatus().equals("10500")) {
                        Toast.makeText(EditAddressActivity.this, addAddressResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<EditAddressResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Toast.makeText(EditAddressActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

   /* @Override
    public void onBackPressed() {
        if (!title.equalsIgnoreCase("checkout")){
            Intent intent = new Intent(EditAddressActivity.this, AddressListActivity.class);
            intent.putExtra(BOTTAM_TAB_POSITION, 3);
            intent.putExtra(TITLE, title);
            startActivity(intent);
        }else {
            Intent intent = new Intent(EditAddressActivity.this, AddressListActivity.class);
            intent.putExtra(TITLE, title);
            startActivity(intent);
        }
    }*/
}
