package com.innasoft.gofarmz.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.gofarmz.Adapter.CoupensAdapter;
import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.Preferences.UserSessionManager;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.PromoCodeResponse;
import com.innasoft.gofarmz.Response.ValidateCoupenResponse;
import com.innasoft.gofarmz.Utilis.Util;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;
import static com.innasoft.gofarmz.Utilis.Constants.TYPE;

public class PromoCodesActivity extends AppCompatActivity implements InternetConnectivityListener {

    @BindView(R.id.txtRetry)
    TextView txtRetry;
    @BindView(R.id.no_internetLayout)
    LinearLayout noInternetLayout;
    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    boolean internet;
    private String title, type, deviceId, token, user_id;
    int position;
    double total;
    UserSessionManager userSessionManager;
    @BindView(R.id.txtNodata)
    TextView txtNodata;
    @BindView(R.id.etCode)
    EditText etCode;
    @BindView(R.id.txtApply)
    TextView txtApply;
    @BindView(R.id.txtAvlCoupons)
    TextView txtAvlCoupons;
    @BindView(R.id.childItemsRecycler)
    RecyclerView childItemsRecycler;
    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promo_codes);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Apply Coupons");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);

        if (getIntent() != null) {
            title = getIntent().getStringExtra(TITLE);
            type = getIntent().getStringExtra(TYPE);
            total = getIntent().getDoubleExtra("FINAL_AMOUNT", 0.00);

        }

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.KEY_DEVICEID);
        user_id = userDetails.get(UserSessionManager.KEY_ID);
        token = userDetails.get(UserSessionManager.KEY_TOKEN);

        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);

        txtApply.setAlpha(.4f);
        txtApply.setEnabled(false);
        etCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String value = String.valueOf(charSequence.length());

                if (charSequence.length() >= 1) {

                    txtApply.setEnabled(true);
                    txtApply.setAlpha(.9f);
                    txtApply.setClickable(true);


                } else {
                    txtApply.setAlpha(.5f);
                    txtApply.setEnabled(false);
                    txtApply.setText("Apply");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
            internet = isConnected;
            couponList();
            noInternetLayout.setVisibility(View.GONE);

        } else {
            internet = false;
            noInternetLayout.setVisibility(View.VISIBLE);
           // Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }

    @OnClick(R.id.txtApply)
    public void onViewClicked() {

        if (internet) {
            String coupon = etCode.getText().toString();
            validateCouponCode(coupon);
        } else {
            Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }


    private void couponList() {
        progressLayout.setVisibility(View.VISIBLE);
        Call<PromoCodeResponse> call = RetrofitClient.getInstance().getApi().getPromoCodes(token, deviceId);
        call.enqueue(new Callback<PromoCodeResponse>() {
            @Override
            public void onResponse(Call<PromoCodeResponse> call, Response<PromoCodeResponse> response) {

                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);
                    PromoCodeResponse promoCodeResponse = response.body();

                    if (promoCodeResponse.getStatus().equals("10100")) {

                        List<PromoCodeResponse.DataBean> dataBeanList = promoCodeResponse.getData();

                        CoupensAdapter adapter = new CoupensAdapter(dataBeanList, PromoCodesActivity.this);
                        LinearLayoutManager layoutManager = new LinearLayoutManager(PromoCodesActivity.this, RecyclerView.VERTICAL, false);
                        childItemsRecycler.setNestedScrollingEnabled(false);
                        childItemsRecycler.setLayoutManager(layoutManager);
                        childItemsRecycler.setAdapter(adapter);

                    } else if (promoCodeResponse.getStatus().equals("10200")) {

                        Toast.makeText(PromoCodesActivity.this, "Invalid Input..!", Toast.LENGTH_SHORT).show();
                    } else if (promoCodeResponse.getStatus().equals("10300")) {

                        txtNodata.setVisibility(View.VISIBLE);
                        txtAvlCoupons.setVisibility(View.GONE);
                        Toast.makeText(PromoCodesActivity.this, "No Coupon Found..!", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<PromoCodeResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Log.d("MESSAGE", "onFailure: " + t.getMessage() + " " + t.toString());
            }
        });
    }

    public void validateCouponCode(String coupon) {
        Call<ValidateCoupenResponse> call = RetrofitClient.getInstance().getApi().validCoupenCode(token, deviceId, user_id, user_id, coupon, String.valueOf(total));
        call.enqueue(new Callback<ValidateCoupenResponse>() {
            @Override
            public void onResponse(Call<ValidateCoupenResponse> call, Response<ValidateCoupenResponse> response) {
                if (response.isSuccessful()) {
                    ValidateCoupenResponse promoCodeResponse = response.body();

                    if (promoCodeResponse.getStatus().equals("10100")) {

                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("CouponCode", coupon);
                        returnIntent.putExtra("DiscountAmount", promoCodeResponse.getData());
                        PromoCodesActivity.this.setResult(Activity.RESULT_OK, returnIntent);
                        PromoCodesActivity.this.finish();

                    } else if (promoCodeResponse.getStatus().equals("10200")) {
                        Toast.makeText(PromoCodesActivity.this, "Invalid Input..!", Toast.LENGTH_SHORT).show();
                    } else if (promoCodeResponse.getStatus().equals("10300")) {

                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("CouponCode", "2");
                        PromoCodesActivity.this.setResult(Activity.RESULT_OK, returnIntent);
                        PromoCodesActivity.this.finish();
                        Toast.makeText(PromoCodesActivity.this, "No Coupon Found..!", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ValidateCoupenResponse> call, Throwable t) {

                Log.d("MESSAGE", "onFailure: " + t.getMessage() + " " + t.toString());
            }
        });
    }


    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                /*Intent intent = new Intent(PromoCodesActivity.this, CheckoutActivity.class);
                intent.putExtra(BOTTAM_TAB_POSITION, position);
                startActivity(intent);*/
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
