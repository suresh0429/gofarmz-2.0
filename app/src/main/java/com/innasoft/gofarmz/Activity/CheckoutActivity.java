package com.innasoft.gofarmz.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.innasoft.gofarmz.Adapter.PaymentTypeAdapter;
import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.Fragments.DeliverySlotFragment;
import com.innasoft.gofarmz.Preferences.UserSessionManager;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.CheckoutDataResponse;
import com.innasoft.gofarmz.Response.DeliverSlotResponse;
import com.innasoft.gofarmz.Response.PlaceOrderResponse;
import com.innasoft.gofarmz.Response.WalletAmountResponse;
import com.innasoft.gofarmz.Utilis.Util;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Utilis.Constants.ADD_ADDRESS;
import static com.innasoft.gofarmz.Utilis.Constants.ARRAYLIST;
import static com.innasoft.gofarmz.Utilis.Constants.BOTTAM_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.CHANGE_ADDRESS;
import static com.innasoft.gofarmz.Utilis.Constants.MODULE;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_CART;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_ID;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_STATUS;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_TITLE;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;
import static com.innasoft.gofarmz.Utilis.Constants.TYPE;

public class CheckoutActivity extends AppCompatActivity implements InternetConnectivityListener, PaymentTypeAdapter.PaymentTypeInterface {

    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    boolean internet;
    private String title, type, deviceId, token, user_id, slotId, address_id, paymentgatewayId = "", couponCode = "";
    Double carttotalPrice, deliveryCharge, grandTotal, walletPrice, paybleAmount;
    String wallet_Used = "0";
    boolean isWallet = true;
    UserSessionManager userSessionManager;
    int position;
    PaymentTypeAdapter paymentTypeAdapter;

    ArrayList<Object> defaultAddressList = new ArrayList<Object>();
    ArrayList<Object> deliverySlot = new ArrayList<>();

    @BindView(R.id.btnAddAddress)
    TextView btnAddAddress;
    @BindView(R.id.addressLayout)
    LinearLayout addressLayout;
    @BindView(R.id.mainLayout)
    RelativeLayout mainLayout;
    @BindView(R.id.txtRetry)
    TextView txtRetry;
    @BindView(R.id.no_internetLayout)
    LinearLayout noInternetLayout;
    @BindView(R.id.deliverySlotInput)
    TextInputLayout deliverySlotInput;
    @BindView(R.id.couponImage)
    ImageView couponImage;
    @BindView(R.id.txtApplyCoupon)
    TextView txtApplyCoupon;
    @BindView(R.id.forwardImage)
    ImageView forwardImage;
    @BindView(R.id.relativeCoupon)
    RelativeLayout relativeCoupon;
    @BindView(R.id.txtOfferDetails)
    TextView txtOfferDetails;
    @BindView(R.id.paymenttype)
    TextView paymenttype;
    @BindView(R.id.paymenttypeLayout)
    LinearLayout paymenttypeLayout;
    @BindView(R.id.txtPayblAmount)
    TextView txtPayblAmount;
    @BindView(R.id.bottamLayout)
    CardView bottamLayout;
    @BindView(R.id.etDeliverySlots)
    TextInputEditText etDeliverySlots;
    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;
    @BindView(R.id.txtName)
    TextView txtName;
    @BindView(R.id.txtChange)
    TextView txtChange;
    @BindView(R.id.txtAddress)
    TextView txtAddress;
    @BindView(R.id.txtExpecteddate)
    TextView txtExpecteddate;
    @BindView(R.id.txtCart)
    TextView txtCart;
    @BindView(R.id.txtCartTotal)
    TextView txtCartTotal;
    @BindView(R.id.txtCoupon)
    TextView txtCoupon;
    @BindView(R.id.txtCouponPrice)
    TextView txtCouponPrice;
    @BindView(R.id.txtDelivery)
    TextView txtDelivery;
    @BindView(R.id.txtDeliveryPrice)
    TextView txtDeliveryPrice;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.txtTotal)
    TextView txtTotal;
    @BindView(R.id.txtTotalPrice)
    TextView txtTotalPrice;
    @BindView(R.id.checkboxWallet)
    CheckBox checkboxWallet;
    @BindView(R.id.Paymentrecycler)
    RecyclerView Paymentrecycler;
    @BindView(R.id.btnPay)
    Button btnPay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Checkout");

        if (getIntent() != null) {
            title = getIntent().getStringExtra(TITLE);
            type = getIntent().getStringExtra(TYPE);
            position = getIntent().getIntExtra(BOTTAM_TAB_POSITION, 0);

        }

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.KEY_DEVICEID);
        user_id = userDetails.get(UserSessionManager.KEY_ID);
        token = userDetails.get(UserSessionManager.KEY_TOKEN);

        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
            internet = isConnected;
            addressAndDeliveryData();
            noInternetLayout.setVisibility(View.GONE);
        } else {
            internet = false;
            noInternetLayout.setVisibility(View.VISIBLE);
            //Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }

    private void addressAndDeliveryData() {
        progressLayout.setVisibility(View.VISIBLE);
        Call<DeliverSlotResponse> call = RetrofitClient.getInstance().getApi().deliverySlot(token, deviceId, user_id);
        call.enqueue(new Callback<DeliverSlotResponse>() {
            @Override
            public void onResponse(Call<DeliverSlotResponse> call, Response<DeliverSlotResponse> response) {
                progressLayout.setVisibility(View.GONE);

                DeliverSlotResponse loginResponse = response.body();

                if (loginResponse.getStatus().equals("10100")) {

                    if (loginResponse.getData().getAddress() instanceof Boolean) {

                        addressLayout.setVisibility(View.VISIBLE);
                        mainLayout.setVisibility(View.GONE);

                    } else {

                        addressLayout.setVisibility(View.GONE);
                        mainLayout.setVisibility(View.VISIBLE);

                        defaultAddressList = (ArrayList<Object>) loginResponse.getData().getAddress();
                        Map<String, Object> recordPos = (Map<String, Object>) defaultAddressList.get(0);
                        address_id = (String) recordPos.get("id");

                        checkOutData(address_id);

                    }

                    deliverySlot = (ArrayList<Object>) loginResponse.getData().getAvailable_delivery_slots();

                } else if (loginResponse.getStatus().equalsIgnoreCase("10200")) {

                    // progressLayout.setVisibility(View.GONE);
                    Toast.makeText(CheckoutActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                } else if (loginResponse.getStatus().equalsIgnoreCase("10400")) {
                    // progressLayout.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure(Call<DeliverSlotResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
            }
        });
    }

    public void slotEditText(String id, String dateslot, String startTime, String endTime) {
        slotId = id;
        etDeliverySlots.setText(startTime + " - " + endTime);
        txtExpecteddate.setText("" + dateslot + "  " + startTime + " - " + endTime);
        deliverySlotInput.setErrorEnabled(false);
        deliverySlotInput.setError(null);
    }

    // order checkoutData
    private void checkOutData(String addressId) {
        progressLayout.setVisibility(View.VISIBLE);
        Call<CheckoutDataResponse> checkoutDataResponseCall = RetrofitClient.getInstance().getApi().CheckoutData(token, user_id, addressId);
        checkoutDataResponseCall.enqueue(new Callback<CheckoutDataResponse>() {
            @Override
            public void onResponse(Call<CheckoutDataResponse> call, Response<CheckoutDataResponse> response) {
                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);
                    CheckoutDataResponse checkoutDataResponse = response.body();
                    if (checkoutDataResponse.getStatus().equals("10100")) {
                        CheckoutDataResponse.DataBean dataBean = checkoutDataResponse.getData();
                        // List<CheckoutDataResponse.DataBean.AddressBean> addressBeanList = dataBean.getAddress();
                        List<CheckoutDataResponse.DataBean.ProductsBean> productsBeanList = dataBean.getProducts();
                        List<CheckoutDataResponse.DataBean.PaymentGatewayBean> paymentGatewayBeanList = dataBean.getPaymentGateway();

                        if (checkoutDataResponse.getData().getAddress() instanceof Boolean) {

                            //  addAddress_txt.setVisibility(View.VISIBLE);
                        } else {

                            //delivery_ll.setVisibility(View.VISIBLE);
                            defaultAddressList = (ArrayList<Object>) checkoutDataResponse.getData().getAddress();
                            Map<String, Object> recordPos = (Map<String, Object>) defaultAddressList.get(0);
                            address_id = (String) recordPos.get("id");
                            String name = (String) recordPos.get("name");
                            String address_line1 = (String) recordPos.get("address_line1");
                            String address_line2 = (String) recordPos.get("address_line2");
                            String area = (String) recordPos.get("area");
                            String city = (String) recordPos.get("city");
                            String state = (String) recordPos.get("state");
                            String pincode = (String) recordPos.get("pincode");
                            String contact_no = (String) recordPos.get("contact_no");
                            String delivery_charges = (String) recordPos.get("delivery_charges");
                            String alternate_contact_no = (String) recordPos.get("alternate_contact_no");
                            boolean delivery_status = (Boolean) recordPos.get("delivery_status");

                            txtName.setText(name);
                            txtAddress.setText(address_line1 + ", " + address_line2 + ", " + area + ", " + city + ", " + state + ", " + pincode + "\n" + "Mobile No : " + contact_no);

                            // delivery Charges
                            if (delivery_charges.equals("0"))
                                txtDeliveryPrice.setText("\u20B9" + String.format("%.2f", delivery_charges));
                            else {
                                Double charge = Double.parseDouble(delivery_charges);
                                txtDeliveryPrice.setText("\u20B9" + String.format("%.2f", charge));
                            }

                            deliveryCharge = Double.parseDouble(delivery_charges);

                        }

                        // cart carttotalPrice
                        txtCartTotal.setText("\u20B9" + String.format("%.2f", Double.parseDouble(String.valueOf(dataBean.getFinalTotal()))));


                        // final price
                        carttotalPrice = Double.valueOf(dataBean.getFinalTotal());
                        grandTotal = deliveryCharge + carttotalPrice;
                        txtTotalPrice.setText("\u20B9" + String.format("%.2f", grandTotal));

                        getWalletAmount(carttotalPrice, deliveryCharge, grandTotal);

                        // payment gateway
                        paymentTypeAdapter = new PaymentTypeAdapter(CheckoutActivity.this, paymentGatewayBeanList, CheckoutActivity.this);
                        Paymentrecycler.setHasFixedSize(true);
                        Paymentrecycler.setLayoutManager(new LinearLayoutManager(CheckoutActivity.this, LinearLayoutManager.VERTICAL, false));
                        Paymentrecycler.setItemAnimator(new DefaultItemAnimator());
                        Paymentrecycler.setAdapter(paymentTypeAdapter);


                    } else if (checkoutDataResponse.getStatus().equals("10200")) {
                        Toast.makeText(CheckoutActivity.this, checkoutDataResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }

            }

            @Override
            public void onFailure(Call<CheckoutDataResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Log.d(TAG, "onFailure1: " + t.getMessage());
                // Toast.makeText(CheckoutActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void getWalletAmount(Double total, Double deliveryCharge, Double grandTotal) {
        progressLayout.setVisibility(View.VISIBLE);
        Call<WalletAmountResponse> call = RetrofitClient.getInstance().getApi().getWalletAmount(token, deviceId, user_id);
        call.enqueue(new Callback<WalletAmountResponse>() {
            @Override
            public void onResponse(Call<WalletAmountResponse> call, Response<WalletAmountResponse> response) {
                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);
                    WalletAmountResponse addressGetResponse = response.body();
                    if (addressGetResponse.getStatus().equals("10100")) {

                        if (Double.parseDouble(addressGetResponse.getData().getAmount()) == 0.0) {
                            checkboxWallet.setEnabled(false);
                            checkboxWallet.setText("\u20B9" + "0.00");
                            walletPrice = 0.00;
                            Log.d(TAG, "onResponse: " + "equals");
                            calculationWallet(false, grandTotal);
                            // txtTotalPrice.setText("\u20B9" + String.format("%.2f", grandTotal));
                        } else if (Double.parseDouble(addressGetResponse.getData().getAmount()) > 0.0) {
                            checkboxWallet.setEnabled(true);
                            checkboxWallet.setChecked(true);
                            checkboxWallet.setText("\u20B9" + addressGetResponse.getData().getAmount());
                            walletPrice = Double.valueOf(addressGetResponse.getData().getAmount());


                            calculationWallet(true, grandTotal);
                            // getCheckOutData(true, title, total_price_after_coupon);
                            checkboxWallet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                                    if (isChecked) {
                                        // getCheckOutData(true, title, total_price_after_coupon);
                                        isWallet = true;
                                        wallet_Used = "1";
                                        calculationWallet(isWallet, grandTotal);
                                    } else {

                                        // getCheckOutData(false, title, total_price_after_coupon);
                                        isWallet = false;
                                        wallet_Used = "0";
                                        calculationWallet(isWallet, grandTotal);
                                    }
                                }
                            });


                        }


                    } else if (addressGetResponse.getStatus().equals("10200")) {

                        Toast.makeText(getApplicationContext(), addressGetResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (addressGetResponse.getStatus().equals("10300")) {

                        Toast.makeText(getApplicationContext(), addressGetResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<WalletAmountResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Toast.makeText(CheckoutActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void calculationWallet(boolean isWallet, Double grandTotal) {

        if (isWallet) {
            if (walletPrice > grandTotal) {
                paymentgatewayId = "4";
                // paybleAmount = walletPrice-grandTotal;
                paybleAmount = grandTotal;
                wallet_Used = "1";
                paymenttype.setVisibility(View.GONE);
                paymenttypeLayout.setVisibility(View.GONE);
                btnPay.setEnabled(true);
                btnPay.setAlpha(0.9f);
                btnPay.setText("Place Order");
                Log.d(TAG, "onResponse: " + "high" + paybleAmount);
            } else if (walletPrice < grandTotal) {
                wallet_Used = "1";
                paymentgatewayId = "";
                paybleAmount = grandTotal - walletPrice;

                paymenttype.setVisibility(View.VISIBLE);
                paymenttypeLayout.setVisibility(View.VISIBLE);

                Log.d(TAG, "onResponse: " + "low" + paybleAmount);
            }
        } else {

            paymentgatewayId = "";
            paybleAmount = grandTotal;
            wallet_Used = "0";
            paymenttype.setVisibility(View.VISIBLE);
            paymenttypeLayout.setVisibility(View.VISIBLE);
            btnPay.setEnabled(false);
            btnPay.setAlpha(0.5f);
            btnPay.setText("Continue");
            Log.d(TAG, "onResponse: " + "equal" + paybleAmount);
        }

        txtPayblAmount.setText("\u20B9" + String.format("%.2f", paybleAmount));
    }


    @OnClick({R.id.etDeliverySlots, R.id.btnPay, R.id.relativeCoupon, R.id.txtChange, R.id.btnAddAddress,})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.etDeliverySlots:
                DeliverySlotFragment bottomSheetFragment = new DeliverySlotFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable(ARRAYLIST, (Serializable) deliverySlot);
                bottomSheetFragment.setArguments(bundle);
                bottomSheetFragment.show(((FragmentActivity) this).getSupportFragmentManager(), bottomSheetFragment.getTag());
                break;
            case R.id.relativeCoupon:
                Intent intent = new Intent(CheckoutActivity.this, PromoCodesActivity.class);
                intent.putExtra("FINAL_AMOUNT", carttotalPrice);
                startActivityForResult(intent, 1);
                break;
            case R.id.btnPay:
                if (internet) {

                        String deliverySlot = etDeliverySlots.getText().toString();
                        if (!deliverySlot.isEmpty()) {
                            doOrderCheckout();
                        } else {
                            deliverySlotInput.setError("Choose Delivery Slot");
                            Util.snackBar(progressLayout, "Choose Delivery Slot", Color.YELLOW);
                        }
                } else {
                    Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
                }
                break;
            case R.id.txtChange:
                Intent intentAddress = new Intent(CheckoutActivity.this, AddressListActivity.class);
                intentAddress.putExtra(TITLE, CHANGE_ADDRESS);
                intentAddress.putExtra(MODULE,ORDER_CART);
                startActivityForResult(intentAddress, 2);
                break;
            case R.id.btnAddAddress:
                Intent intentAddressBtn = new Intent(CheckoutActivity.this, AddAddressActivity.class);
                intentAddressBtn.putExtra(TITLE, ADD_ADDRESS);
                intentAddressBtn.putExtra(MODULE, ORDER_CART);
                startActivity(intentAddressBtn);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK && data != null) {

            couponCode = data.getStringExtra("CouponCode");
            Double discountAmount = data.getDoubleExtra("DiscountAmount", 0.00);

            if (couponCode.equalsIgnoreCase("2")) {
                txtCoupon.setVisibility(View.GONE);
                txtCouponPrice.setVisibility(View.GONE);
            } else {
                txtCoupon.setVisibility(View.VISIBLE);
                txtCouponPrice.setVisibility(View.VISIBLE);
                forwardImage.setImageResource(R.drawable.ic_close_black_24dp);
                relativeCoupon.setClickable(false);

                txtCouponPrice.setText("- \u20B9" + String.format("%.2f", discountAmount));
                txtApplyCoupon.setText(couponCode);
                txtApplyCoupon.setTextColor(getResources().getColor(R.color.colorPrimary));

                Log.d(TAG, "onActivityResult: " + carttotalPrice + "=" + deliveryCharge + "=" + discountAmount);
                grandTotal = (carttotalPrice + deliveryCharge) - discountAmount;
                txtTotalPrice.setText("\u20B9" + String.format("%.2f", grandTotal));

                getWalletAmount(carttotalPrice, deliveryCharge, grandTotal);

                forwardImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d(TAG, "onClick: " + "click");
                        forwardImage.setImageResource(R.drawable.ic_forward_arrow);
                        relativeCoupon.setClickable(true);
                        txtApplyCoupon.setText("Apply Coupon");
                        txtApplyCoupon.setTextColor(getResources().getColor(R.color.black));
                        txtCoupon.setVisibility(View.GONE);
                        txtCouponPrice.setVisibility(View.GONE);

                        grandTotal = carttotalPrice + deliveryCharge;
                        txtTotalPrice.setText("\u20B9" + String.format("%.2f", grandTotal));
                        getWalletAmount(carttotalPrice, deliveryCharge, grandTotal);

                    }
                });

                Log.d(TAG, "onActivityResult: " + data.getStringExtra("CouponCode"));
                Log.d(TAG, "onActivityResult1: " + data.getDoubleExtra("DiscountAmount", 0.00));
            }
        } else if (requestCode == 2 && resultCode == RESULT_OK && data != null) {
            address_id = data.getStringExtra("AddressId");
            Log.d(TAG, "onActivityResult: " + address_id);
            checkOutData(address_id);

            forwardImage.setImageResource(R.drawable.ic_forward_arrow);
            relativeCoupon.setClickable(true);
            txtApplyCoupon.setText("Apply Coupon");
            txtCoupon.setVisibility(View.GONE);
            txtCouponPrice.setVisibility(View.GONE);

            grandTotal = carttotalPrice + deliveryCharge;
            txtTotalPrice.setText("\u20B9" + String.format("%.2f", grandTotal));

        }

    }

    @Override
    public void onItemClick(List<CheckoutDataResponse.DataBean.PaymentGatewayBean> paymentGatewayBean, int position) {
        Log.d(TAG, "onItemClick: " + paymentGatewayBean.get(position).getId() + "---" + paymentGatewayBean.get(position).getName());
        paymentgatewayId = paymentGatewayBean.get(position).getId();

        if (paymentgatewayId.equalsIgnoreCase("1")) {
            btnPay.setText("Place Order");
        } else {
            btnPay.setText("Continue");
        }
        btnPay.setEnabled(true);
        btnPay.setAlpha(0.9f);
    }

    private void doOrderCheckout() {
        progressLayout.setVisibility(View.VISIBLE);
        Call<PlaceOrderResponse> call = RetrofitClient.getInstance().getApi().PlaceOrder(token, couponCode, paymentgatewayId, "ANDROID", user_id, address_id, slotId, "", wallet_Used);
        call.enqueue(new Callback<PlaceOrderResponse>() {
            @Override
            public void onResponse(Call<PlaceOrderResponse> call, Response<PlaceOrderResponse> response) {
                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);
                    final PlaceOrderResponse placeOrderResponse = response.body();
                    PlaceOrderResponse.DataBean dataBean = placeOrderResponse.getData();
                    Log.d(TAG, "onResponse: " + dataBean.getOrderId());
                    if (placeOrderResponse.getStatus().equals("10100")) {

                        // alert Dilogue
                        //before inflating the custom alert dialog layout, we will get the current title viewgroup
                        ViewGroup viewGroup = findViewById(android.R.id.content);
                        //then we will inflate the custom alert dialog xml that we created
                        View dialogView = LayoutInflater.from(CheckoutActivity.this).inflate(R.layout.custom_alert_diloge, viewGroup, false);

                        Button btnOk = dialogView.findViewById(R.id.buttonOk);
                        btnOk.setOnClickListener(view -> {
                            Intent intent = new Intent(CheckoutActivity.this, OrderDetailsActivity.class);
                            intent.putExtra(ORDER_ID, String.valueOf(dataBean.getOrderId()));
                            intent.putExtra(ORDER_STATUS, "Confirmed");
                            intent.putExtra(TITLE, ORDER_TITLE);
                            intent.putExtra(MODULE,ORDER_CART);
                            intent.putExtra(ORDER_TAB_POSITION, 0);
                            intent.putExtra(BOTTAM_TAB_POSITION, 3);
                            startActivity(intent);
                            // finish();
                        });
                        //Now we need an AlertDialog.Builder object
                        AlertDialog.Builder builder = new AlertDialog.Builder(CheckoutActivity.this);

                        //setting the view of the builder to our custom view that we already inflated
                        builder.setView(dialogView);

                        //finally creating the alert dialog and displaying it
                        AlertDialog alertDialog = builder.create();
                        alertDialog.setCancelable(false);
                        alertDialog.show();


                    } else if (placeOrderResponse.getStatus().equals("10300")) {

                        Toast.makeText(CheckoutActivity.this, "Unable to process your request..! ", Toast.LENGTH_LONG).show();
                    } else if (placeOrderResponse.getStatus().equals("10400")) {

                        if (paymentgatewayId.equalsIgnoreCase("6")) {
                            Intent order_detail = new Intent(CheckoutActivity.this, PaymentGatewayActivity.class);
                            order_detail.putExtra(ORDER_ID, dataBean.getOrderId());
                            order_detail.putExtra(MODULE,ORDER_CART);
                            order_detail.putExtra("TOTAL", paybleAmount);
                            order_detail.putExtra("ENCRYPTORDERID", dataBean.getEncryptedOrderid());
                            order_detail.putExtra("ORDER_REF_NO", dataBean.getOrderRefNo());
                            order_detail.putExtra(TITLE, ORDER_TITLE);
                            startActivity(order_detail);

                        } else {
                            Intent order_detail = new Intent(CheckoutActivity.this, PaymentGatewayActivity.class);
                            order_detail.putExtra("payurl", dataBean.getPaymentUrl());
                            order_detail.putExtra(ORDER_ID, dataBean.getOrderId());
                            order_detail.putExtra(MODULE,ORDER_CART);
                            order_detail.putExtra("TOTAL", paybleAmount);
                            startActivity(order_detail);

                        }
                    } else if (placeOrderResponse.getStatus().equals("10500")) {
                        Toast.makeText(CheckoutActivity.this, "10500" + placeOrderResponse.getMessage(), Toast.LENGTH_LONG).show();

                    } else if (placeOrderResponse.getStatus().equals("10600")) {

                        Toast.makeText(CheckoutActivity.this, "Coupon is invalid..!", Toast.LENGTH_LONG).show();
                    } else if (placeOrderResponse.getStatus().equals("10700")) {

                        Toast.makeText(CheckoutActivity.this, "10700" + placeOrderResponse.getMessage(), Toast.LENGTH_LONG).show();
                    } else if (placeOrderResponse.getStatus().equals("10110")) {

                        Toast.makeText(CheckoutActivity.this, "10110" + placeOrderResponse.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<PlaceOrderResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Log.d("TAG", "onFailure: " + t.getMessage());
                Toast.makeText(CheckoutActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(CheckoutActivity.this, HomeActivity.class);
        intent.putExtra(BOTTAM_TAB_POSITION, 2);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(CheckoutActivity.this, HomeActivity.class);
                intent.putExtra(BOTTAM_TAB_POSITION, 2);
                startActivity(intent);

                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
