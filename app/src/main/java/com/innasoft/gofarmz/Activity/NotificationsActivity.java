package com.innasoft.gofarmz.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.innasoft.gofarmz.Adapter.NotificationAdapter;
import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.Preferences.UserSessionManager;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.BaseResponse;
import com.innasoft.gofarmz.Response.NotificationResponse;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Utilis.Constants.BOTTAM_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.COUNT;
import static com.innasoft.gofarmz.Utilis.Constants.NOTIFY_COUNT;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;
import static com.innasoft.gofarmz.Utilis.Constants.TYPE;

public class NotificationsActivity extends AppCompatActivity implements InternetConnectivityListener {

    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;
    @BindView(R.id.recyclerNotification)
    RecyclerView recyclerNotification;
    @BindView(R.id.txtRetry)
    TextView txtRetry;
    @BindView(R.id.no_internetLayout)
    LinearLayout noInternetLayout;
    @BindView(R.id.txtGotoHome)
    TextView txtGotoHome;
    @BindView(R.id.notificationLayout)
    LinearLayout notificationLayout;
    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    boolean internet;
    private String title, type, deviceId, token, user_id;
    UserSessionManager userSessionManager;
    int position,notificatationCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent() != null) {
            title = getIntent().getStringExtra(TITLE);
            type = getIntent().getStringExtra(TYPE);
            position = getIntent().getIntExtra(BOTTAM_TAB_POSITION, 0);
            getSupportActionBar().setTitle(title);
        }

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.KEY_DEVICEID);
        user_id = userDetails.get(UserSessionManager.KEY_ID);
        token = userDetails.get(UserSessionManager.KEY_TOKEN);

        // notification Count
        SharedPreferences preferences = getSharedPreferences(NOTIFY_COUNT,0);
        notificatationCount = preferences.getInt(COUNT,0);


        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
            internet = isConnected;
            notificationData();
            noInternetLayout.setVisibility(View.GONE);
        } else {
            internet = false;
            noInternetLayout.setVisibility(View.VISIBLE);
            // Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }

    private void notificationData() {
        progressLayout.setVisibility(View.VISIBLE);
        Call<NotificationResponse> call = RetrofitClient.getInstance().getApi().notifications(token, deviceId, user_id);
        call.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {

                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);

                    NotificationResponse loginResponse = response.body();

                    if (loginResponse.getStatus().equals("10100")) {

                        List<NotificationResponse.DataBean> dataBeanList = loginResponse.getData();

                        NotificationAdapter notificationAdapter = new NotificationAdapter(dataBeanList, NotificationsActivity.this);
                        recyclerNotification.setNestedScrollingEnabled(false);
                        recyclerNotification.setLayoutManager(new LinearLayoutManager(NotificationsActivity.this, LinearLayoutManager.VERTICAL, false));
                        recyclerNotification.setItemAnimator(new DefaultItemAnimator());
                        recyclerNotification.setAdapter(notificationAdapter);

                    } else {
                        notificationLayout.setVisibility(View.VISIBLE);
                        txtGotoHome.setOnClickListener(view -> {
                            Intent intent = new Intent(NotificationsActivity.this,HomeActivity.class);
                            intent.putExtra(BOTTAM_TAB_POSITION,0);
                            startActivity(intent);
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
            }
        });
    }

    public void notificationReadStatus(String message, String title, String id) {



        new AlertDialog.Builder(NotificationsActivity.this, R.style.CustomDialogTheme)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        progressLayout.setVisibility(View.VISIBLE);
                        Call<BaseResponse> call = RetrofitClient.getInstance().getApi().readNotifications(token, deviceId, user_id, id);
                        call.enqueue(new Callback<BaseResponse>() {
                            @Override
                            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                                if (response.isSuccessful()) {
                                    progressLayout.setVisibility(View.GONE);

                                    BaseResponse loginResponse = response.body();
                                    if (loginResponse.getStatus().equals("10100")) {

                                        notificationData();

                                        if (notificatationCount>0) {
                                            notificatationCount = notificatationCount - 1;
                                            setNotifyCount(notificatationCount);
                                        }

                                        if (getApplicationContext() instanceof HomeActivity) {
                                            ((HomeActivity) getApplicationContext()).getnotificationCount();
                                        }

                                    } else if (loginResponse.getStatus().equals("10200")) {

                                        Toast.makeText(NotificationsActivity.this, "Invalid Input..!", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<BaseResponse> call, Throwable t) {
                                progressLayout.setVisibility(View.GONE);
                            }
                        });
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                })
                .show();


    }


    @Override
    public void onBackPressed() {
        if (position != -1) {
            Intent intent = new Intent(NotificationsActivity.this, HomeActivity.class);
            intent.putExtra(BOTTAM_TAB_POSITION, position);
            startActivity(intent);
        } else {
            finish();
        }

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                if (position != -1) {
                    Intent intent = new Intent(NotificationsActivity.this, HomeActivity.class);
                    intent.putExtra(BOTTAM_TAB_POSITION, position);
                    startActivity(intent);
                } else {
                    finish();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setNotifyCount(int count){
        SharedPreferences preferences = getSharedPreferences(NOTIFY_COUNT,0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(COUNT,count);
        editor.apply();


        invalidateOptionsMenu();
    }
}
