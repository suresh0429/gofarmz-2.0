package com.innasoft.gofarmz.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.gofarmz.Adapter.PreOrderCartAdapter;
import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.Preferences.UserSessionManager;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.PreOrderCartResponse;
import com.innasoft.gofarmz.Response.PreOrderDeleteCartResponse;
import com.innasoft.gofarmz.Response.PreOrderUpdateCartResponse;
import com.innasoft.gofarmz.Utilis.Util;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Utilis.Constants.BOTTAM_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.CART_COUNT;
import static com.innasoft.gofarmz.Utilis.Constants.COUNT;
import static com.innasoft.gofarmz.Utilis.Constants.MODULE;
import static com.innasoft.gofarmz.Utilis.Constants.PRE_ORDER_CART;
import static com.innasoft.gofarmz.Utilis.Constants.PRE_ORDER_ID;

public class PreOrderCartActivity extends AppCompatActivity implements InternetConnectivityListener {

    @BindView(R.id.prOrderCartRecycler)
    RecyclerView prOrderCartRecycler;
    @BindView(R.id.txtTotal)
    TextView txtTotal;
    @BindView(R.id.txtCartTotal)
    TextView txtCartTotal;
    @BindView(R.id.btnCheckOut)
    Button btnCheckOut;
    @BindView(R.id.bottamLayout)
    CardView bottamLayout;
    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;
    @BindView(R.id.txtRetry)
    TextView txtRetry;
    @BindView(R.id.no_internetLayout)
    LinearLayout noInternetLayout;
    @BindView(R.id.txtAddProduct)
    TextView txtAddProduct;
    @BindView(R.id.no_dataFound)
    LinearLayout noDataFound;
    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    UserSessionManager userSessionManager;
    boolean internet;
    String deviceId, user_id, token, preOrderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_order_cart);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Pre Orders Cart");
        if (getIntent() != null) {
            preOrderId = getIntent().getStringExtra(PRE_ORDER_ID);

        }

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.KEY_DEVICEID);
        user_id = userDetails.get(UserSessionManager.KEY_ID);
        token = userDetails.get(UserSessionManager.KEY_TOKEN);

        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
            getPreOrderCartData();
            noInternetLayout.setVisibility(View.GONE);
        } else {
            // internet = false;
            noInternetLayout.setVisibility(View.VISIBLE);
            Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }

    private void getPreOrderCartData() {
        progressLayout.setVisibility(View.VISIBLE);
        Call<PreOrderCartResponse> cartResponseCall = RetrofitClient.getInstance().getApi().PreOrderCartData(token, deviceId, user_id);
        cartResponseCall.enqueue(new Callback<PreOrderCartResponse>() {
            @Override
            public void onResponse(Call<PreOrderCartResponse> call, Response<PreOrderCartResponse> response) {
                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);
                    PreOrderCartResponse preOrderCartResponse = response.body();
                    if (preOrderCartResponse.getStatus().equals("10100")) {

                        List<PreOrderCartResponse.DataBean> dataBeanList = preOrderCartResponse.getData();


                        if (dataBeanList.size() != 0) {
                            txtTotal.setText("Total : " + String.valueOf(preOrderCartResponse.getTotal()));

                            prOrderCartRecycler.setHasFixedSize(true);
                            prOrderCartRecycler.setLayoutManager(new LinearLayoutManager(PreOrderCartActivity.this, LinearLayoutManager.VERTICAL, false));
                            prOrderCartRecycler.setItemAnimator(new DefaultItemAnimator());
                            PreOrderCartAdapter preOrderCartAdapter = new PreOrderCartAdapter(dataBeanList, PreOrderCartActivity.this);
                            prOrderCartRecycler.setAdapter(preOrderCartAdapter);
                        } else {
                            prOrderCartRecycler.setVisibility(View.GONE);
                            noDataFound.setVisibility(View.VISIBLE);
                            bottamLayout.setVisibility(View.GONE);

                            txtAddProduct.setOnClickListener(view -> {
                                startActivity(new Intent(PreOrderCartActivity.this, PreOrdersActivity.class));
                            });
                        }


                    } else if (preOrderCartResponse.getStatus().equals("10200")) {
                        noDataFound.setVisibility(View.VISIBLE);

                        Toast.makeText(getApplicationContext(), preOrderCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (preOrderCartResponse.getStatus().equals("10300")) {

                        prOrderCartRecycler.setVisibility(View.GONE);
                        noDataFound.setVisibility(View.VISIBLE);
                        bottamLayout.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), preOrderCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (preOrderCartResponse.getStatus().equals("10400")) {
                        noDataFound.setVisibility(View.VISIBLE);
                        Toast.makeText(getApplicationContext(), preOrderCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<PreOrderCartResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Toast.makeText(PreOrderCartActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void changeQuatity(String product_id, int qty) {

        Call<PreOrderUpdateCartResponse> cartResponseCall = RetrofitClient.getInstance().getApi().PreOrderUpdateCart1(token, deviceId, user_id, product_id, String.valueOf(qty));
        cartResponseCall.enqueue(new Callback<PreOrderUpdateCartResponse>() {
            @Override
            public void onResponse(Call<PreOrderUpdateCartResponse> call, Response<PreOrderUpdateCartResponse> response) {
                if (response.isSuccessful()) {
                    PreOrderUpdateCartResponse preOrderUpdateCartResponse = response.body();
                    if (preOrderUpdateCartResponse.getStatus().equals("10100")) {

                        getPreOrderCartData();

                        Toast.makeText(PreOrderCartActivity.this, preOrderUpdateCartResponse.getMessage(), Toast.LENGTH_LONG).show();

                    }
                    if (preOrderUpdateCartResponse.getStatus().equalsIgnoreCase("10200")) {
                        //  progressDialog.dismiss();
                        Toast.makeText(PreOrderCartActivity.this, preOrderUpdateCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    if (preOrderUpdateCartResponse.getStatus().equalsIgnoreCase("10300")) {
                        //    progressDialog.dismiss();
                        Toast.makeText(PreOrderCartActivity.this, preOrderUpdateCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    if (preOrderUpdateCartResponse.getStatus().equalsIgnoreCase("10400")) {
                        //  progressDialog.dismiss();
                        Toast.makeText(PreOrderCartActivity.this, preOrderUpdateCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<PreOrderUpdateCartResponse> call, Throwable t) {
                // progressDialog.dismiss();
                Toast.makeText(PreOrderCartActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    public void deleteCartItem(String product_id) {

        new AlertDialog.Builder(PreOrderCartActivity.this, R.style.CustomDialogTheme)
                .setTitle("REMOVE ?")
                .setMessage("Are you sure want to Delete this Item.")
                .setPositiveButton("OK", (dialog, which) -> {
                    Log.d("MainActivity", "Sending atomic bombs to Jupiter");

                    progressLayout.setVisibility(View.VISIBLE);
                    Call<PreOrderDeleteCartResponse> cartResponseCall = RetrofitClient.getInstance().getApi().PreOrderDeleteCart(token, deviceId, user_id, product_id);
                    cartResponseCall.enqueue(new Callback<PreOrderDeleteCartResponse>() {
                        @Override
                        public void onResponse(Call<PreOrderDeleteCartResponse> call, Response<PreOrderDeleteCartResponse> response) {
                            if (response.isSuccessful()) {
                                progressLayout.setVisibility(View.GONE);
                                PreOrderDeleteCartResponse preOrderDeleteCartResponse = response.body();
                                if (preOrderDeleteCartResponse.getStatus().equals("10100")) {

                                    getPreOrderCartData();
                                    setCartCount(preOrderDeleteCartResponse.getCart_count());
                                    Toast.makeText(PreOrderCartActivity.this, preOrderDeleteCartResponse.getMessage(), Toast.LENGTH_SHORT).show();

                                }
                                if (preOrderDeleteCartResponse.getStatus().equals("10200")) {
                                    Toast.makeText(PreOrderCartActivity.this, preOrderDeleteCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<PreOrderDeleteCartResponse> call, Throwable t) {
                            Toast.makeText(PreOrderCartActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            progressLayout.setVisibility(View.GONE);
                        }
                    });

                })
                .setNegativeButton("CANCEL", (dialog, which) -> {
                    Log.d("MainActivity", "Aborting mission...");
                    dialog.dismiss();
                })
                .show();
    }

    @OnClick(R.id.btnCheckOut)
    public void onViewClicked() {
        Intent intent2 = new Intent(PreOrderCartActivity.this, PreOrderCheckoutActivity.class);
        intent2.putExtra(MODULE,PRE_ORDER_CART);
        intent2.putExtra(BOTTAM_TAB_POSITION, 0);
        intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent2);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void setCartCount(int count) {
        SharedPreferences preferences = getSharedPreferences(CART_COUNT, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(COUNT, count);
        editor.apply();

        invalidateOptionsMenu();
    }


}
