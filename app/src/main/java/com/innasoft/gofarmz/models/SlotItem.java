package com.innasoft.gofarmz.models;

public class SlotItem {

    public static final int HEADER_TYPE = 0;
    public static final int CHILD_TYPE = 1;

    String Dateslot;
    String startTime;
    String endTime;
    String id;
    int type;

    public SlotItem(String dateslot, String startTime, String endTime, String id, int type) {
        Dateslot = dateslot;
        this.startTime = startTime;
        this.endTime = endTime;
        this.id = id;
        this.type = type;
    }

    public String getDateslot() {
        return Dateslot;
    }

    public void setDateslot(String dateslot) {
        Dateslot = dateslot;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
