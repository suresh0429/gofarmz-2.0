package com.innasoft.gofarmz.models;

public class OptionsModel {
    private String optId;
    private String pdtId;
    private String optName;
    private Double price;
    private int isInCart;
    private int cartQty;
    private int qtyPrice;

    public OptionsModel(String optId, String pdtId, String optName, Double price, int isInCart, int cartQty, int qtyPrice) {
        this.optId = optId;
        this.pdtId = pdtId;
        this.optName = optName;
        this.price = price;
        this.isInCart = isInCart;
        this.cartQty = cartQty;
        this.qtyPrice = qtyPrice;
    }

    public String getOptId() {
        return optId;
    }

    public void setOptId(String optId) {
        this.optId = optId;
    }

    public String getPdtId() {
        return pdtId;
    }

    public void setPdtId(String pdtId) {
        this.pdtId = pdtId;
    }

    public String getOptName() {
        return optName;
    }

    public void setOptName(String optName) {
        this.optName = optName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public int getIsInCart() {
        return isInCart;
    }

    public void setIsInCart(int isInCart) {
        this.isInCart = isInCart;
    }

    public int getCartQty() {
        return cartQty;
    }

    public void setCartQty(int cartQty) {
        this.cartQty = cartQty;
    }

    public int getQtyPrice() {
        return qtyPrice;
    }

    public void setQtyPrice(int qtyPrice) {
        this.qtyPrice = qtyPrice;
    }
}
