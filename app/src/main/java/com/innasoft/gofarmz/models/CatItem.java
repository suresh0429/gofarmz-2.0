package com.innasoft.gofarmz.models;

public class CatItem {
    String catName;
    String catId;
    int image;

    public CatItem(String catName, String catId, int image) {
        this.catName = catName;
        this.catId = catId;
        this.image = image;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
