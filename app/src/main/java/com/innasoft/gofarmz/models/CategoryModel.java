package com.innasoft.gofarmz.models;

public class CategoryModel
{
    private String id;
    private String name;
    private String image;
    private String type_avail;

    public String getType_avail() {
        return type_avail;
    }

    public void setType_avail(String type_avail) {
        this.type_avail = type_avail;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
