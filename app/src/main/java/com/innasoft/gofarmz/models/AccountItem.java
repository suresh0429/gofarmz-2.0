package com.innasoft.gofarmz.models;

public class AccountItem {

    public static final int HEADER_TYPE = 0;
    public static final int CHILD_TYPE = 1;

    int icon;
    String name;
    int type;

    public AccountItem(int icon, String name, int type) {
        this.icon = icon;
        this.name = name;
        this.type = type;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
