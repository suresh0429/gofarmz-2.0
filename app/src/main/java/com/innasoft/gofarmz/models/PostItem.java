package com.innasoft.gofarmz.models;

public class PostItem {

    String id,title,amount,txt_type,event_flag,internal_ref_no,created_on;

    /*public PostItem(String id, String title, String amount, String txt_type, String event_flag, String internal_ref_no, String created_on) {
        this.id = id;
        this.title = title;
        this.amount = amount;
        this.txt_type = txt_type;
        this.event_flag = event_flag;
        this.internal_ref_no = internal_ref_no;
        this.created_on = created_on;
    }*/

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTxt_type() {
        return txt_type;
    }

    public void setTxt_type(String txt_type) {
        this.txt_type = txt_type;
    }

    public String getEvent_flag() {
        return event_flag;
    }

    public void setEvent_flag(String event_flag) {
        this.event_flag = event_flag;
    }

    public String getInternal_ref_no() {
        return internal_ref_no;
    }

    public void setInternal_ref_no(String internal_ref_no) {
        this.internal_ref_no = internal_ref_no;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }
}
