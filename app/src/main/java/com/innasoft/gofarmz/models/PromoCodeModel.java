package com.innasoft.gofarmz.models;



import org.json.JSONObject;

public class PromoCodeModel {

    public String code, name, description;


    public PromoCodeModel(JSONObject jsonObject) {

        try {
            if (jsonObject.has("code") && !jsonObject.isNull("code"))
                this.code = jsonObject.getString("code");
            if (jsonObject.has("name") && !jsonObject.isNull("name"))
                this.name = jsonObject.getString("name");
            if (jsonObject.has("description") && !jsonObject.isNull("description"))
                this.description = jsonObject.getString("description");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}