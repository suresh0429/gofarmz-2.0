package com.innasoft.gofarmz.models;
import org.json.JSONObject;

import static com.innasoft.gofarmz.Api.RetrofitClient.BASEIMAGEURL_70x70;


public class OrderHistoryModel
{
    public String id,reference_id,order_id,final_price,order_status,order_date,order_time,discount,total_mrp_price,coupon_code,status,shipping_charges,images;
    public OrderHistoryModel(JSONObject jsonObject) {
        try {
            if (jsonObject.has("id") && !jsonObject.isNull("id"))
                this.id = jsonObject.getString("id");

            if (jsonObject.has("reference_id") && !jsonObject.isNull("reference_id"))
                this.reference_id = jsonObject.getString("reference_id");

            if (jsonObject.has("order_id") && !jsonObject.isNull("order_id"))
                this.order_id = jsonObject.getString("order_id");

            if (jsonObject.has("images") && !jsonObject.isNull("images"))
                this.images = BASEIMAGEURL_70x70 + jsonObject.getString("images");

            if (jsonObject.has("final_price") && !jsonObject.isNull("final_price"))
                this.final_price = jsonObject.getString("final_price");

            if (jsonObject.has("order_status") && !jsonObject.isNull("order_status"))
                this.order_status = jsonObject.getString("order_status");

            if (jsonObject.has("order_date") && !jsonObject.isNull("order_date"))
                this.order_date = jsonObject.getString("order_date");

            if (jsonObject.has("order_time") && !jsonObject.isNull("order_time"))
                this.order_time = jsonObject.getString("order_time");

            if (jsonObject.has("discount") && !jsonObject.isNull("discount"))
                this.discount = jsonObject.getString("discount");

            if (jsonObject.has("total_mrp_price") && !jsonObject.isNull("total_mrp_price"))
                this.total_mrp_price = jsonObject.getString("total_mrp_price");

            if (jsonObject.has("coupon_code") && !jsonObject.isNull("coupon_code"))
                this.coupon_code = jsonObject.getString("coupon_code");

            if (jsonObject.has("shipping_charges") && !jsonObject.isNull("shipping_charges"))
                this.shipping_charges = jsonObject.getString("shipping_charges");

            if (jsonObject.has("status") && !jsonObject.isNull("status"))
                this.status = jsonObject.getString("status");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
