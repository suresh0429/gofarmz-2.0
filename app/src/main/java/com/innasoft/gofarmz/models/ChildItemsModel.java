package com.innasoft.gofarmz.models;

public class ChildItemsModel {
   private String image;
   private String finalPrice;
   private String quantity;
   private String productName;
   private String unitValue;
   private String unitName;

    public ChildItemsModel(String image, String finalPrice, String quantity, String productName, String unitValue, String unitName) {
        this.image = image;
        this.finalPrice = finalPrice;
        this.quantity = quantity;
        this.productName = productName;
        this.unitValue = unitValue;
        this.unitName = unitName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(String finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getUnitValue() {
        return unitValue;
    }

    public void setUnitValue(String unitValue) {
        this.unitValue = unitValue;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }
}
