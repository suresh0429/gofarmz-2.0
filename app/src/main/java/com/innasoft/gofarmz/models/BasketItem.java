package com.innasoft.gofarmz.models;

public class BasketItem {

    private String childPdtName;
    private String weight;
    private String images;

    public BasketItem(String childPdtName, String weight, String images) {
        this.childPdtName = childPdtName;
        this.weight = weight;
        this.images = images;
    }

    public String getChildPdtName() {
        return childPdtName;
    }

    public void setChildPdtName(String childPdtName) {
        this.childPdtName = childPdtName;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }
}
