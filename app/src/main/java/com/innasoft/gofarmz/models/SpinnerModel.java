package com.innasoft.gofarmz.models;

public class SpinnerModel {

    private String optId;
    private String pdtId;
    private String optName;
    private String price;
    private int quantity;

    public SpinnerModel(String optId, String pdtId, String optName, String price, int quantity) {
        this.optId = optId;
        this.pdtId = pdtId;
        this.optName = optName;
        this.price = price;
        this.quantity = quantity;
    }

    public String getOptId() {
        return optId;
    }

    public void setOptId(String optId) {
        this.optId = optId;
    }

    public String getPdtId() {
        return pdtId;
    }

    public void setPdtId(String pdtId) {
        this.pdtId = pdtId;
    }

    public String getOptName() {
        return optName;
    }

    public void setOptName(String optName) {
        this.optName = optName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
