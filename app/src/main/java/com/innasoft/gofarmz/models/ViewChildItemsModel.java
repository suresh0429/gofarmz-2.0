package com.innasoft.gofarmz.models;

import java.io.Serializable;

public class ViewChildItemsModel implements Serializable {
    String type,product_name,unit_name,unit_value,price,discount,final_price,quantity,created_on,images;

    public ViewChildItemsModel(String type, String product_name, String unit_name, String unit_value, String price, String discount, String final_price, String quantity, String created_on, String images) {
        this.type = type;
        this.product_name = product_name;
        this.unit_name = unit_name;
        this.unit_value = unit_value;
        this.price = price;
        this.discount = discount;
        this.final_price = final_price;
        this.quantity = quantity;
        this.created_on = created_on;
        this.images = images;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getUnit_name() {
        return unit_name;
    }

    public void setUnit_name(String unit_name) {
        this.unit_name = unit_name;
    }

    public String getUnit_value() {
        return unit_value;
    }

    public void setUnit_value(String unit_value) {
        this.unit_value = unit_value;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getFinal_price() {
        return final_price;
    }

    public void setFinal_price(String final_price) {
        this.final_price = final_price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }
}
