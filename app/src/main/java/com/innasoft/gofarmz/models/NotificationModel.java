package com.innasoft.gofarmz.models;

import org.json.JSONObject;

public class NotificationModel
{
    public String id,title,message,created_on,is_read,image_path,notification_id;

    public NotificationModel(JSONObject jsonObject) {
        try {
            if (jsonObject.has("id") && !jsonObject.isNull("id"))
                this.id = jsonObject.getString("id");

            if (jsonObject.has("title") && !jsonObject.isNull("title"))
                this.title = jsonObject.getString("title");

            if (jsonObject.has("message") && !jsonObject.isNull("message"))
                this.message = jsonObject.getString("message");

            if (jsonObject.has("created_on") && !jsonObject.isNull("created_on"))
                this.created_on = jsonObject.getString("created_on");

            if (jsonObject.has("is_read") && !jsonObject.isNull("is_read"))
                this.is_read = jsonObject.getString("is_read");

            if (jsonObject.has("image") && !jsonObject.isNull("image"))
                this.image_path = jsonObject.getString("image");

            if (jsonObject.has("notification_id") && !jsonObject.isNull("notification_id"))
                this.notification_id = jsonObject.getString("notification_id");




        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
