package com.innasoft.gofarmz.models;

public class Options {

    private String optName;
    private String price;
    private int cartQty;

    public Options(String optName, String price, int cartQty) {
        this.optName = optName;
        this.price = price;
        this.cartQty = cartQty;
    }

    public String getOptName() {
        return optName;
    }

    public void setOptName(String optName) {
        this.optName = optName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getCartQty() {
        return cartQty;
    }

    public void setCartQty(int cartQty) {
        this.cartQty = cartQty;
    }
}
