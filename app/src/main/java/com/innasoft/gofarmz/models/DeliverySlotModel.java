package com.innasoft.gofarmz.models;

import java.util.ArrayList;

public class DeliverySlotModel {

    private String day;
    private String date;
    private String id;
    private String start_time;
    private String end_time;
    String option;
    ArrayList<Object> options;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }


    public ArrayList<Object> getOptions() {

        return options;
    }

    public void setOptions(ArrayList<Object> options) {
        this.options = options;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }
}
