package com.innasoft.gofarmz.models;

public class TrackingItem {

  private Boolean isActive;
  private String  formattedDate;
  private String  title;
  private String  status;

    public TrackingItem( String formattedDate, String title, String status) {
        this.formattedDate = formattedDate;
        this.title = title;
        this.status = status;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getFormattedDate() {
        return formattedDate;
    }

    public void setFormattedDate(String formattedDate) {
        this.formattedDate = formattedDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
