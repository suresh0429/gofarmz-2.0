package com.innasoft.gofarmz.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.models.FBhItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class FBHAdpter extends RecyclerView.Adapter<CatAdapter.MyViewHolder> {
    List<FBhItem> model;
    Context context;



    public FBHAdpter(List<FBhItem> model, Context context) {
        this.model = model;
        this.context = context;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgCat)
        ImageView imgCat;
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.parentLayout)
        LinearLayout parentLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    @NonNull
    @Override
    public CatAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.fbh_list_item, parent, false);
        CatAdapter.MyViewHolder holder = new CatAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull CatAdapter.MyViewHolder holder, int position) {
        Glide.with(context).load(model.get(position).getImage()).into(holder.imgCat);
        Log.d(TAG, "onBindViewHolder: " + model.get(position).getImage());
        holder.txtTitle.setText(model.get(position).getName());
        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return model.size();

    }
}
