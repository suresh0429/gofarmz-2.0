package com.innasoft.gofarmz.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.innasoft.gofarmz.Activity.NotificationsActivity;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.NotificationResponse;
import com.innasoft.gofarmz.Utilis.TimeAgo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {
    List<NotificationResponse.DataBean> model;
    Context context;

    public NotificationAdapter(List<NotificationResponse.DataBean> model, Context context) {
        this.model = model;
        this.context = context;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imag_notification)
        ImageView imagNotification;
        @BindView(R.id.notify_title_name)
        TextView notifyTitleName;
        @BindView(R.id.notify_description)
        TextView notifyDescription;
        @BindView(R.id.notify_dateandtime)
        TextView notifyDateandtime;
        @BindView(R.id.card_view)
        LinearLayout cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.notification_list_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        @SuppressLint("SimpleDateFormat") DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = (Date)formatter.parse(model.get(position).getCreatedOn());
            Log.d(TAG, "onBindViewHolder: "+model.get(position).getCreatedOn());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println("Today is " +date.getTime());


        TimeAgo timeAgo2 = new TimeAgo();
        String timeAgo = timeAgo2.getTimeAgo(date.getTime());

        holder.notifyDateandtime.setText(timeAgo);
        holder.notifyTitleName.setText(model.get(position).getMsgTitle());
        holder.notifyDescription.setText(model.get(position).getMsgTxt());

        Log.d(TAG, "onBindViewHolderNotify: "+model.get(position).getMsgImage());
        if (!model.get(position).getMsgImage().equalsIgnoreCase("") || !model.get(position).getMsgImage().isEmpty() || model.get(position).getMsgImage() != null ){
            Glide.with(context).load(model.get(position).getMsgImage()).error(R.drawable.ic_notification).into(holder.imagNotification);
        }else {
            Log.d(TAG, "onBindViewHolderImage: "+"image");
            holder.imagNotification.setImageResource(R.drawable.placeholder);
        }


        if(model.get(position).getIsRead().equalsIgnoreCase("0"))
        {
            holder.cardView.setBackgroundColor(Color.parseColor("#d1f2e2"));
        }else {
            holder.cardView.setBackgroundColor(Color.parseColor("#ffffff"));

        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (context instanceof NotificationsActivity) {
                    ((NotificationsActivity)context).notificationReadStatus(model.get(position).getMsgTxt(),model.get(position).getMsgTitle(),model.get(position).getId());
                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return model.size();

    }
}
