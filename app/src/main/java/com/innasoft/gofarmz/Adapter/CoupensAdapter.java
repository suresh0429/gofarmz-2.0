package com.innasoft.gofarmz.Adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.gofarmz.Activity.PromoCodesActivity;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.PromoCodeResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CoupensAdapter extends RecyclerView.Adapter<CoupensAdapter.MyViewHolder> {

    private List<PromoCodeResponse.DataBean> mList;
    private Context mContext;
    private ListAdapterListener mListener;


    public interface ListAdapterListener { // create an interface
        void onClickAtOKButton(int position); // create callback function
    }

    public CoupensAdapter(List<PromoCodeResponse.DataBean> mList, Context mContext) {
        this.mList = mList;
        this.mContext = mContext;
        //this.mListener = mListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.coupon_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final PromoCodeResponse.DataBean home = mList.get(position);

        holder.txtPromoCode.setText(home.getCode());
        holder.txtDescription.setText(Html.fromHtml(home.getDescription()));
        holder.txtApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mContext instanceof PromoCodesActivity) {
                    ((PromoCodesActivity) mContext).validateCouponCode(home.getCode());
                }
            }
        });


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtPromoCode)
        TextView txtPromoCode;
        @BindView(R.id.txtDescription)
        TextView txtDescription;
        @BindView(R.id.txtApply)
        TextView txtApply;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }


}