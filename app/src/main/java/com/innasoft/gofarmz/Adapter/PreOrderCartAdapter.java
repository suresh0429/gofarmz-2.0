package com.innasoft.gofarmz.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.innasoft.gofarmz.Activity.PreOrderCartActivity;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.PreOrderCartResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.google.firebase.remoteconfig.FirebaseRemoteConfig.TAG;

public class PreOrderCartAdapter extends RecyclerView.Adapter<PreOrderCartAdapter.MyViewHolder> {
    List<PreOrderCartResponse.DataBean> model;
    Context context;

    public PreOrderCartAdapter(List<PreOrderCartResponse.DataBean> model, Context context) {
        this.model = model;
        this.context = context;

    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtPrice)
        TextView txtPrice;
        @BindView(R.id.txtType)
        TextView txtType;
        @BindView(R.id.priceLayout)
        LinearLayout priceLayout;
        @BindView(R.id.product_minus)
        TextView productMinus;
        @BindView(R.id.product_quantity)
        TextView productQuantity;
        @BindView(R.id.product_plus)
        TextView productPlus;
        @BindView(R.id.quantityLayout)
        LinearLayout quantityLayout;
        @BindView(R.id.view)
        View view;
        @BindView(R.id.txtRemove)
        TextView txtRemove;
        @BindView(R.id.txtViewDetails)
        TextView txtViewDetails;
        @BindView(R.id.parentLayout)
        CardView parentLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    @NonNull
    @Override
    public PreOrderCartAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cart_list_item, parent, false);
        PreOrderCartAdapter.MyViewHolder holder = new PreOrderCartAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull PreOrderCartAdapter.MyViewHolder holder, int position) {
        Glide.with(context).load( model.get(position).getPop_image()).into(holder.image);
        holder.txtName.setText(model.get(position).getProduct_name());

        Log.d(TAG, "onBindViewHolder: "+model.get(position).getPop_image());
        Double totalvalue = Double.parseDouble( model.get(position).getTotal_price());
        holder.txtPrice.setText("\u20B9" + String.format("%.2f", totalvalue));
        holder.txtType.setVisibility(View.GONE);
        holder.productQuantity.setText(model.get(position).getQuantity());

        holder.productPlus.setOnClickListener(view -> {

            if (model.get(position).getQuantity().equals("8")) {
                Toast.makeText(context, "Sorry,You Can't add more qunatity!!!", Toast.LENGTH_LONG).show();
            } else {
                int qty = Integer.parseInt(model.get(position).getQuantity());
                if (qty != 0)
                    qty++;

                ((PreOrderCartActivity) context).changeQuatity(model.get(position).getProduct_id(),qty);
            }
        });

        holder.productMinus.setOnClickListener(view -> {
            int qty = Integer.parseInt(model.get(position).getQuantity());
            if (qty > 1) {
                qty--;

                ((PreOrderCartActivity) context).changeQuatity(model.get(position).getProduct_id(), qty);
            }
        });

        holder.txtRemove.setOnClickListener(view -> ((PreOrderCartActivity) context).deleteCartItem(model.get(position).getProduct_id()));

        holder.txtViewDetails.setVisibility(View.GONE);


       /* holder.txtViewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (model.get(position).getType().equalsIgnoreCase("COMBO_CUSTOM")){

                    List<CartResponse.DataBean.RecordsBean.ADDONSBean> customProductsBeanList1 = model.get(position).getADDONS();
                    ChildItemsBottomSheetFragment bottomSheetFragment = new ChildItemsBottomSheetFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(ARRAYLIST, (Serializable) customProductsBeanList1);
                    bundle.putString(TITLE, model.get(position).getProductName());
                    bundle.putString(TYPE, model.get(position).getType());
                    bundle.putString(FRAGMENT_FROM, CART);
                    bottomSheetFragment.setArguments(bundle);
                    bottomSheetFragment.show(((FragmentActivity) context).getSupportFragmentManager(), bottomSheetFragment.getTag());

                }else {

                    List<CartResponse.DataBean.RecordsBean.ESSENTIALBean> normalProductsBeanList = model.get(position).getESSENTIAL();
                    ChildItemsBottomSheetFragment bottomSheetFragment = new ChildItemsBottomSheetFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(ARRAYLIST, (Serializable) normalProductsBeanList);
                    bundle.putString(TITLE, model.get(position).getProductName());
                    bundle.putString(TYPE, model.get(position).getType());
                    bundle.putString(FRAGMENT_FROM, CART);
                    bottomSheetFragment.setArguments(bundle);
                    bottomSheetFragment.show(((FragmentActivity) context).getSupportFragmentManager(), bottomSheetFragment.getTag());

                }


            }
        });*/

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return model.size();

    }
}
