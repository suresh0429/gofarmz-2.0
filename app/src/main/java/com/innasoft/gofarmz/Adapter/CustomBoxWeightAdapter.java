package com.innasoft.gofarmz.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.CustomBoxResponse;

import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class CustomBoxWeightAdapter extends BaseAdapter {
    Context context;
    ArrayList<CustomBoxResponse.DataBean.OPTIONALBean.OptionsBean> arrayList;

    public CustomBoxWeightAdapter(Context context, ArrayList<CustomBoxResponse.DataBean.OPTIONALBean.OptionsBean> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return arrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return arrayList.size();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        View rowView= LayoutInflater.from(context).inflate(R.layout.child_box_list_item, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txtTitle);

        CustomBoxResponse.DataBean.OPTIONALBean.OptionsBean optionsBean = arrayList.get(position);

        txtTitle.setText(optionsBean.getOptName() +"  for  "+"\u20B9"+optionsBean.getPrice());
        Log.d(TAG, "getView: "+optionsBean.getOptId());
        return rowView;
    }
}
