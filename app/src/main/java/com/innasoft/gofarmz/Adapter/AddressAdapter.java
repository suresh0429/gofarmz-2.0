package com.innasoft.gofarmz.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.gofarmz.Activity.AddressListActivity;
import com.innasoft.gofarmz.Activity.EditAddressActivity;
import com.innasoft.gofarmz.Activity.HomeActivity;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.AddressListResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Utilis.Constants.BOTTAM_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.CHANGE_ADDRESS;
import static com.innasoft.gofarmz.Utilis.Constants.HOMEACTIVITY;
import static com.innasoft.gofarmz.Utilis.Constants.LOCATIONNAME;
import static com.innasoft.gofarmz.Utilis.Constants.LOCATION_PREF;
import static com.innasoft.gofarmz.Utilis.Constants.MODULE;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.MyViewHolder> {
    List<AddressListResponse.DataBean> model;
    Context context;
    String title,module;

    public AddressAdapter(List<AddressListResponse.DataBean> model, Context context, String title, String module) {
        this.model = model;
        this.context = context;
        this.title = title;
        this.module = module;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageIcon)
        ImageView imageIcon;
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.txtAddress)
        TextView txtAddress;
        @BindView(R.id.txtEdit)
        TextView txtEdit;
        @BindView(R.id.txtDelete)
        TextView txtDelete;
        @BindView(R.id.txtDefault)
        TextView txtDefault;
        @BindView(R.id.txtSelect)
        TextView txtSelect;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.address_list_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        if (model.get(position).getIs_default().equalsIgnoreCase("Yes")){
            holder.txtDefault.setText("Default");
        }else {
            holder.txtDefault.setVisibility(View.GONE);
        }

        holder.txtTitle.setText(model.get(position).getName());

        holder.txtAddress.setText(model.get(position).getAddress_line1()+", "+model.get(position).getAddress_line2()+", "+
                model.get(position).getArea()+", "+model.get(position).getCity()+", "+model.get(position).getCountry()+", "+
                model.get(position).getPincode());

        holder.txtEdit.setOnClickListener(view -> {

            Intent intent = new Intent(context, EditAddressActivity.class);
            intent.putExtra(TITLE,  title);
            intent.putExtra(MODULE,  module);
            intent.putExtra("id",  model.get(position).getId());
            intent.putExtra("name", model.get(position).getName());
            intent.putExtra("addressline1", model.get(position).getAddress_line1());
            intent.putExtra("landmark", model.get(position).getAddress_line2());
            intent.putExtra("area", model.get(position).getArea());
            intent.putExtra("city", model.get(position).getCity());
            intent.putExtra("state", model.get(position).getState());
            intent.putExtra("pincode", model.get(position).getPincode());
            intent.putExtra("contactNo", model.get(position).getContact_no());
            intent.putExtra("alternatecontactNo", model.get(position).getAlternate_contact_no());
            intent.putExtra("latitude", Double.parseDouble(model.get(position).getLatitude()));
            Log.d(TAG, "onBindViewHolder: "+model.get(position).getLatitude());
            intent.putExtra("longitude", Double.parseDouble(model.get(position).getLongitude()));
            intent.putExtra("isDefault", model.get(position).getIs_default());
            context.startActivity(intent);
        });

        holder.txtDelete.setOnClickListener(view -> {
            if (context instanceof AddressListActivity) {
                ((AddressListActivity) context).deleteAddress(model.get(position).getId());
            }
        });

        // from home activity
        if (title.equalsIgnoreCase(HOMEACTIVITY)){
            holder.txtSelect.setVisibility(View.GONE);
            holder.txtDelete.setVisibility(View.GONE);
            holder.txtEdit.setVisibility(View.GONE);

            holder.itemView.setOnClickListener(view -> {
                Intent intent = new Intent(context, HomeActivity.class);
                intent.putExtra(BOTTAM_TAB_POSITION, position);
                intent.putExtra(LOCATIONNAME, model.get(position).getArea());
                context.startActivity(intent);

                setLocationPref(model.get(position).getArea());
            });
        }

        // from checkout activity
        if (title.equalsIgnoreCase(CHANGE_ADDRESS)){
            holder.txtSelect.setVisibility(View.VISIBLE);
        }else {
            holder.txtSelect.setVisibility(View.GONE);
        }

        holder.txtSelect.setOnClickListener(view -> {
            if (context instanceof AddressListActivity) {
                ((AddressListActivity) context).chooseAddress(model.get(position).getId());
            }
        });



    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return model.size();

    }

    private void setLocationPref(String locationName){

        SharedPreferences preferences = context.getSharedPreferences(LOCATION_PREF,0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(LOCATIONNAME,locationName);
        editor.apply();
    }
}
