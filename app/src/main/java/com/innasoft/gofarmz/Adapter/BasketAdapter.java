package com.innasoft.gofarmz.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.models.BasketItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Api.RetrofitClient.BASEIMAGEURL_70x70;

public class BasketAdapter extends RecyclerView.Adapter<BasketAdapter.MyViewHolder> {
    List<BasketItem> model;
    Context context;

    public BasketAdapter(List<BasketItem> model, Context context) {
        this.model = model;
        this.context = context;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageProduct)
        ImageView imageProduct;
        @BindView(R.id.txtProductname)
        TextView txtProductname;
        @BindView(R.id.txtcapacity)
        TextView txtcapacity;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.basket_list_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Glide.with(context).load(BASEIMAGEURL_70x70+model.get(position).getImages()).into(holder.imageProduct);
        Log.d(TAG, "onBindViewHolder: " + model.get(position).getImages());
        holder.txtProductname.setText(model.get(position).getChildPdtName());
        holder.txtcapacity.setText(model.get(position).getWeight());

        /*holder.itemView.setOnClickListener(view -> {
            Intent it = new Intent(context, FormarActivity.class);
            it.putExtra(PRODUCTID, model.get(position).get());
            it.putExtra(TITLE, customBoxProductsModels.get(position).getPdtName());
            it.putExtra(IMAGE, BASEIMAGEURL_70x70+customBoxProductsModels.get(position).getImages());
            context.startActivity(it);
        });*/
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return model.size();

    }
}
