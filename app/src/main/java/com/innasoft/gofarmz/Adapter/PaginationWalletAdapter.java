package com.innasoft.gofarmz.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;

import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.WalletTransresponse;
import com.innasoft.gofarmz.Utilis.TimeAgo;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.constraintlayout.widget.Constraints.TAG;


public class PaginationWalletAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private static final String BASE_URL_IMG = "https://image.tmdb.org/t/p/w150";

    private List<WalletTransresponse> movieResults;
    private Context context;

    private boolean isLoadingAdded = false;

    public PaginationWalletAdapter(Context context) {
        this.context = context;
        movieResults = new ArrayList<>();
    }

    public List<WalletTransresponse> getMovies() {
        return movieResults;
    }

    public void setMovies(List<WalletTransresponse> movieResults) {
        this.movieResults = movieResults;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(v2);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.row_wallet, parent, false);
        viewHolder = new MovieVH(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        WalletTransresponse result = movieResults.get(position); // Movie

        switch (getItemViewType(position)) {
            case ITEM:
                final MovieVH movieVH = (MovieVH) holder;

                movieVH.txtAmount.setText("\u20B9"+result.getAmount());
                movieVH.txtRefno.setText("Ref No : "+result.getInternalRefNo());
                movieVH.txtTitle.setText(result.getTitle());


                @SuppressLint("SimpleDateFormat") DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = null;
                try {
                    date = (Date)formatter.parse(result.getCreatedOn());
                    Log.d(TAG, "onBindViewHolder: "+result.getCreatedOn());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                System.out.println("Today is " +date.getTime());
                TimeAgo timeAgo2 = new TimeAgo();
                String timeAgo = timeAgo2.getTimeAgo(date.getTime());
                movieVH.txtDate.setText(timeAgo);

               /* TimeAgo timeAgo2 = new TimeAgo();
                String timeAgo = timeAgo2.covertTimeToText(result.getCreatedOn());
                movieVH.txtDate.setText(timeAgo);
                Log.d(TAG, "onBindViewHolder: "+timeAgo);
*/
                if (result.getTxnType().equalsIgnoreCase("DEBIT")){
                    movieVH.txtType.setText(result.getTxnType());
                    movieVH.txtType.setTextColor(Color.RED);
                }else {
                    movieVH.txtType.setText(result.getTxnType());
                    movieVH.txtType.setTextColor(Color.parseColor("#05914E"));
                }

                break;

            case LOADING:
//                Do nothing
                break;


        }

    }

    @Override
    public int getItemCount() {
        return movieResults == null ? 0 : movieResults.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == movieResults.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }


    /*
   Helpers
   _________________________________________________________________________________________________
    */

    public void add(WalletTransresponse r) {
        movieResults.add(r);
        notifyItemInserted(movieResults.size() - 1);
    }

    public void addAll(List<WalletTransresponse> moveResults) {
        for (WalletTransresponse result : moveResults) {
            add(result);
        }
    }

    public void remove(WalletTransresponse r) {
        int position = movieResults.indexOf(r);
        if (position > -1) {
            movieResults.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new WalletTransresponse());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = movieResults.size() - 1;
        WalletTransresponse result = getItem(position);

        if (result != null) {
            movieResults.remove(position);
            notifyItemRemoved(position);
        }
    }

    public WalletTransresponse getItem(int position) {
        return movieResults.get(position);
    }


   /*
   View Holders
   _________________________________________________________________________________________________
    */

    /**
     * Main list's content ViewHolder
     */
    protected class MovieVH extends RecyclerView.ViewHolder {
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.txtAmount)
        TextView txtAmount;
        @BindView(R.id.txtRefno)
        TextView txtRefno;
        @BindView(R.id.txtType)
        TextView txtType;
        @BindView(R.id.txtDate)
        TextView txtDate;

        public MovieVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }


    protected class LoadingVH extends RecyclerView.ViewHolder {

        public LoadingVH(View itemView) {
            super(itemView);
        }
    }


}
