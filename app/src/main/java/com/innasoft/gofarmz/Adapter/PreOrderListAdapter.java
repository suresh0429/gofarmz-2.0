package com.innasoft.gofarmz.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.innasoft.gofarmz.Activity.PreOrderProductViewActivity;
import com.innasoft.gofarmz.Preferences.UserSessionManager;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.PreOrderResponse;

import java.util.HashMap;
import java.util.List;

import static com.innasoft.gofarmz.Utilis.Constants.PRE_ORDER_ID;

public class PreOrderListAdapter extends RecyclerView.Adapter<PreOrderListAdapter.Holder> {

    Context context;
    List<PreOrderResponse.DataBean.ProductsBean> productsBeanList;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token, price;

    public PreOrderListAdapter(Context context,List<PreOrderResponse.DataBean.ProductsBean> productsBeanList) {
        this.context=context;
        this.productsBeanList = productsBeanList;

    }

    @NonNull
    @Override
    public PreOrderListAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_preorder_products, null);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull PreOrderListAdapter.Holder holder, final int i) {


        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.KEY_DEVICEID);
        user_id = userDetails.get(UserSessionManager.KEY_ID);
        token = userDetails.get(UserSessionManager.KEY_TOKEN);
        Log.d("SESSIONDATA", deviceId + "\n" + user_id + "\n" + token);

        holder.preorder_name_txt.setText(productsBeanList.get(i).getProduct_name());

        holder.txtAvalibleUnits.setText("Available Units : "+productsBeanList.get(i).getPop_available_units());

        holder.ord_his_unit_txt.setText(productsBeanList.get(i).getPop_unit_value() + " For Rs. ");

        String price = productsBeanList.get(i).getPop_unit_price();
        Double d = Double.parseDouble(price);
        holder.ord_his_price_txt.setText(""+Math.round(d)+"/-");

        Glide.with(context)
                .load(productsBeanList.get(i).getPop_image())
                .into(holder.preorder_img);



        holder.view_txt.setOnClickListener(v -> {
            Intent intent=new Intent(context, PreOrderProductViewActivity.class);
            intent.putExtra(PRE_ORDER_ID,productsBeanList.get(i).getProduct_id());
            context.startActivity(intent);
        });

        holder.parentLayout.setOnClickListener(v -> {
            Intent intent=new Intent(context, PreOrderProductViewActivity.class);
            intent.putExtra(PRE_ORDER_ID,productsBeanList.get(i).getProduct_id());
            context.startActivity(intent);
        });

    }

    @Override
    public int getItemCount() {
        return productsBeanList.size();
    }
    class Holder extends RecyclerView.ViewHolder{

        ImageView preorder_img;
        TextView preorder_name_txt,ord_his_price_txt,ord_his_unit_txt,txtAvalibleUnits,view_txt;
        RelativeLayout parentLayout;


        public Holder(@NonNull View itemView) {
            super(itemView);

            view_txt=itemView.findViewById(R.id.view_txt);
            parentLayout=itemView.findViewById(R.id.parentLayout);
            ord_his_price_txt=itemView.findViewById(R.id.ord_his_price_txt);
            preorder_name_txt=itemView.findViewById(R.id.preorder_name_txt);
            ord_his_unit_txt=itemView.findViewById(R.id.ord_his_unit_txt);
            txtAvalibleUnits=itemView.findViewById(R.id.txtAvalibleUnits);
            preorder_img=itemView.findViewById(R.id.preorder_img);
        }



    }
}

