package com.innasoft.gofarmz.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.gofarmz.Activity.AddressListActivity;
import com.innasoft.gofarmz.Activity.BulkHarvetsersActivity;
import com.innasoft.gofarmz.Activity.NotificationsActivity;
import com.innasoft.gofarmz.Activity.OrdersActivity;
import com.innasoft.gofarmz.Activity.ReferFarmerActivity;
import com.innasoft.gofarmz.Activity.VirtualShopsActivity;
import com.innasoft.gofarmz.Activity.WalletActivity;
import com.innasoft.gofarmz.Activity.WebViewActivity;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.models.AccountItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Api.RetrofitClient.FAQ;
import static com.innasoft.gofarmz.Api.RetrofitClient.FORMER_NETWORK;
import static com.innasoft.gofarmz.Api.RetrofitClient.KNOW_YOUR_FORMER;
import static com.innasoft.gofarmz.Api.RetrofitClient.OUR_STORY;
import static com.innasoft.gofarmz.Api.RetrofitClient.PRIVACY_POLICY;
import static com.innasoft.gofarmz.Api.RetrofitClient.TERMS_CONDITION;
import static com.innasoft.gofarmz.Utilis.Constants.BOTTAM_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;
import static com.innasoft.gofarmz.Utilis.Constants.TYPE;
import static com.innasoft.gofarmz.models.AccountItem.CHILD_TYPE;
import static com.innasoft.gofarmz.models.AccountItem.HEADER_TYPE;

public class AccountAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<AccountItem> mList;
    private Context mContext;

    public AccountAdapter(List<AccountItem> mList, Context mContext) {
        this.mList = mList;
        this.mContext = mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case HEADER_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_margin, parent, false);
                return new CityViewHolder(view);
            case CHILD_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.account_list_item, parent, false);
                return new EventViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        AccountItem object = mList.get(position);
        if (object != null) {
            switch (object.getType()) {
                case HEADER_TYPE:
                    ((CityViewHolder) holder).mTitle.setBackgroundColor(Color.parseColor("#4CE5E5E5"));
                    break;
                case CHILD_TYPE:
                    ((EventViewHolder) holder).txtTitle.setText(object.getName());
                    ((EventViewHolder) holder).imageIcon.setImageResource(object.getIcon());
                    ((EventViewHolder) holder).parentLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Log.d(TAG, "onClick: "+position);

                            if (position == 0) {
                                Intent intent = new Intent(mContext, OrdersActivity.class);
                                intent.putExtra(TITLE, object.getName());
                                //intent.putExtra(TYPE,"know-your-farmer");
                                intent.putExtra(BOTTAM_TAB_POSITION, 3);
                                 mContext.startActivity(intent);
                            } else if (position == 1) {
                                Intent intent = new Intent(mContext, WalletActivity.class);
                                intent.putExtra(TITLE, object.getName());
                                //intent.putExtra(TYPE,"know-your-farmer");
                                intent.putExtra(BOTTAM_TAB_POSITION, 3);
                                mContext.startActivity(intent);
                            } else if (position == 2) {
                                Intent intent = new Intent(mContext, AddressListActivity.class);
                                intent.putExtra(TITLE, object.getName());
                                // intent.putExtra(TYPE,"know-your-farmer");
                                intent.putExtra(BOTTAM_TAB_POSITION, 3);
                                mContext.startActivity(intent);
                            } else if (position == 4) {
                                Intent intent = new Intent(mContext, VirtualShopsActivity.class);
                                intent.putExtra(TITLE, object.getName());
                                // intent.putExtra(TYPE,"know-your-farmer");
                                intent.putExtra(BOTTAM_TAB_POSITION, 3);
                                mContext.startActivity(intent);
                            } else if (position == 5) {
                                Intent intent = new Intent(mContext, BulkHarvetsersActivity.class);
                                intent.putExtra(TITLE, object.getName());
                                // intent.putExtra(TYPE,"know-your-farmer");
                                intent.putExtra(BOTTAM_TAB_POSITION, 3);
                                mContext.startActivity(intent);
                            } else if (position == 6) {
                                Intent intent = new Intent(mContext, WebViewActivity.class);
                                intent.putExtra(TITLE, object.getName());
                                intent.putExtra(TYPE, KNOW_YOUR_FORMER);
                                intent.putExtra(BOTTAM_TAB_POSITION, 3);
                                mContext.startActivity(intent);
                            } else if (position == 7) {
                                Intent intent = new Intent(mContext, WebViewActivity.class);
                                intent.putExtra(TITLE, object.getName());
                                intent.putExtra(TYPE, OUR_STORY);
                                intent.putExtra(BOTTAM_TAB_POSITION, 3);
                                mContext.startActivity(intent);
                            } else if (position == 8) {
                                Intent intent = new Intent(mContext, WebViewActivity.class);
                                intent.putExtra(TITLE, object.getName());
                                intent.putExtra(TYPE, FORMER_NETWORK);
                                intent.putExtra(BOTTAM_TAB_POSITION, 3);
                                mContext.startActivity(intent);
                            }else if (position == 9) {
                                Intent intent = new Intent(mContext, ReferFarmerActivity.class);
                                intent.putExtra(TITLE, object.getName());
                               // intent.putExtra(TYPE, FORMER_NETWORK);
                                intent.putExtra(BOTTAM_TAB_POSITION, 3);
                                mContext.startActivity(intent);
                            } else if (position == 11) {
                                Intent intent = new Intent(mContext, NotificationsActivity.class);
                                intent.putExtra(TITLE, object.getName());
                                intent.putExtra(TYPE, FORMER_NETWORK);
                                intent.putExtra(BOTTAM_TAB_POSITION, 3);
                                mContext.startActivity(intent);
                            }else if (position == 12) {
                                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                                sharingIntent.setType("text/plain");
                                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "GoFarmz -");
                                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Hi, Here is an excellent app which supplies genuine naturally grown food products, sourced from local farmers, straight to your doorstep. Download to Check it out.  " + "https://play.google.com/store/apps/details?id=in.innasoft.gofarmz&hl=en");
                                mContext.startActivity(Intent.createChooser(sharingIntent, "Share via"));

                            } else if (position == 13) {
                                Intent intent = new Intent(mContext, WebViewActivity.class);
                                intent.putExtra(TITLE, object.getName());
                                intent.putExtra(TYPE, FORMER_NETWORK);
                                intent.putExtra(BOTTAM_TAB_POSITION, 3);
                                mContext.startActivity(intent);
                            } else if (position == 14) {
                                Intent intent = new Intent(mContext, WebViewActivity.class);
                                intent.putExtra(TITLE, object.getName());
                                intent.putExtra(TYPE, FAQ);
                                intent.putExtra(BOTTAM_TAB_POSITION, 3);
                                mContext.startActivity(intent);
                            } else if (position == 15) {
                                Intent intent = new Intent(mContext, WebViewActivity.class);
                                intent.putExtra(TITLE, object.getName());
                                intent.putExtra(TYPE, TERMS_CONDITION);
                                intent.putExtra(BOTTAM_TAB_POSITION, 3);
                                mContext.startActivity(intent);
                            } else if (position == 16) {
                                Intent intent = new Intent(mContext, WebViewActivity.class);
                                intent.putExtra(TITLE, object.getName());
                                intent.putExtra(TYPE, PRIVACY_POLICY);
                                intent.putExtra(BOTTAM_TAB_POSITION, 3);
                                mContext.startActivity(intent);
                            }

                        }
                    });

                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        if (mList == null)
            return 0;
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (mList != null) {
            AccountItem object = mList.get(position);
            if (object != null) {
                return object.getType();
            }
        }
        return 0;
    }

    public static class CityViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout mTitle;

        public CityViewHolder(View itemView) {
            super(itemView);
            mTitle = (LinearLayout) itemView.findViewById(R.id.headerLayout);
        }
    }

    public static class EventViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageIcon)
        ImageView imageIcon;
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.imageFarward)
        ImageView imageFarward;
        @BindView(R.id.parentLayout)
        RelativeLayout parentLayout;

        public EventViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
