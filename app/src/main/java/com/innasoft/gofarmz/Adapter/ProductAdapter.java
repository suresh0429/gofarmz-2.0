package com.innasoft.gofarmz.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.innasoft.gofarmz.Activity.FormarActivity;
import com.innasoft.gofarmz.Interface.ProductListner;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.CustomBoxResponse;
import com.innasoft.gofarmz.models.Product;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.google.firebase.remoteconfig.FirebaseRemoteConfig.TAG;
import static com.innasoft.gofarmz.Api.RetrofitClient.BASEIMAGEURL_70x70;
import static com.innasoft.gofarmz.Utilis.Constants.IMAGE;
import static com.innasoft.gofarmz.Utilis.Constants.PRODUCTID;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {
    private List<Product> customBoxProductsModels;
    List<CustomBoxResponse.DataBean.OPTIONALBean> optionalBeans;
    Context context;
    ProductListner productListner;

    public ProductAdapter(List<Product> customBoxProductsModels, List<CustomBoxResponse.DataBean.OPTIONALBean> optionalBeans, Context context, ProductListner productListner) {
        this.customBoxProductsModels = customBoxProductsModels;
        this.optionalBeans = optionalBeans;
        this.context = context;
        this.productListner = productListner;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgProduct)
        ImageView imgProduct;
        @BindView(R.id.txtProductName)
        TextView txtProductName;
        @BindView(R.id.txtProductPrice)
        TextView txtProductPrice;
        @BindView(R.id.txtProductWeight)
        TextView txtProductWeight;
        @BindView(R.id.customizeText)
        TextView customizeText;
        @BindView(R.id.product_minus)
        TextView productMinus;
        @BindView(R.id.product_quantity)
        TextView productQuantity;
        @BindView(R.id.product_plus)
        TextView productPlus;
        @BindView(R.id.quantityLayout)
        LinearLayout quantityLayout;
        @BindView(R.id.parentLayout)
        LinearLayout parentLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.product_list_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Product dataBean = customBoxProductsModels.get(position);
        CustomBoxResponse.DataBean.OPTIONALBean optionalBean = optionalBeans.get(position);

        holder.txtProductName.setText(dataBean.getPdtName());
        Glide.with(context).load(BASEIMAGEURL_70x70 + dataBean.getImages()).into(holder.imgProduct);

        holder.txtProductWeight.setText("" + dataBean.getOptName());
        holder.txtProductPrice.setText("\u20B9" + String.format("%.2f", dataBean.getPrice()));
        // holder.productQuantity.setText(""+dataBean.getCartQty());

        holder.customizeText.setText(dataBean.getOptName() + "  for  " + "\u20B9" + String.format("%.2f", dataBean.getPrice()));

        Log.d(TAG, "onBindViewHolder: " + dataBean.getCartQty());

        Log.d(TAG, "onBindViewHolder: " + dataBean.getCartQty());
        if (dataBean.getCartQty() == 0) {
            holder.productQuantity.setText("ADD");
            holder.productMinus.setVisibility(View.GONE);
            holder.productPlus.setVisibility(View.GONE);
            Log.d(TAG, "onBindViewHolder: " + dataBean.getCartQty());
        } else {
            holder.productQuantity.setText("" + dataBean.getCartQty());
            holder.productMinus.setVisibility(View.VISIBLE);
            holder.productPlus.setVisibility(View.VISIBLE);
            Log.d(TAG, "onBindViewHolder1: " + dataBean.getCartQty());
        }

        holder.productQuantity.setOnClickListener(view -> {
            if (holder.productQuantity.getText().toString().equalsIgnoreCase("ADD")) {
                holder.productQuantity.setText("" + dataBean.getCartQty());
                holder.productMinus.setVisibility(View.VISIBLE);
                holder.productPlus.setVisibility(View.VISIBLE);

                productListner.onAddClick(position, dataBean);
            }
        });

        holder.productMinus.setOnClickListener(view -> {

            productListner.onMinusClick(position, dataBean);

        });

        holder.productPlus.setOnClickListener(view -> {

            productListner.onPlusClick(position, dataBean);

        });

        holder.customizeText.setOnClickListener(view -> {
            productListner.OnItemClick(position, dataBean, optionalBean);
        });

        holder.parentLayout.setOnClickListener(view -> {
            Intent it = new Intent(context, FormarActivity.class);
            it.putExtra(PRODUCTID, customBoxProductsModels.get(position).getId());
            it.putExtra(TITLE, customBoxProductsModels.get(position).getPdtName());
            it.putExtra(IMAGE, BASEIMAGEURL_70x70+customBoxProductsModels.get(position).getImages());
            context.startActivity(it);
        });


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return customBoxProductsModels.size();

    }


}
