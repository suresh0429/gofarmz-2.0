package com.innasoft.gofarmz.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.innasoft.gofarmz.Fragments.ChildItemsBottomSheetFragment;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.MyOrderDetailsResponse;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Api.RetrofitClient.BASEIMAGEURL_70x70;
import static com.innasoft.gofarmz.Utilis.Constants.ARRAYLIST;
import static com.innasoft.gofarmz.Utilis.Constants.FRAGMENT_FROM;
import static com.innasoft.gofarmz.Utilis.Constants.PRODUCT_DETAILS;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;

public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.MyViewHolder> {
    List<MyOrderDetailsResponse.DataBean.ProductsBean> recordDataBeanList;
    Context context;



    public OrderHistoryAdapter(List<MyOrderDetailsResponse.DataBean.ProductsBean> recordDataBeanList, Context context) {
        this.recordDataBeanList = recordDataBeanList;
        this.context = context;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgView)
        ImageView imgView;
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.txtQty)
        TextView txtQty;
        @BindView(R.id.txtPrice)
        TextView txtPrice;
        @BindView(R.id.txtViewDetails)
        TextView txtViewDetails;
        @BindView(R.id.view)
        View view;
        @BindView(R.id.parentLayout)
        RelativeLayout parentLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.order_history_list_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Glide.with(context).load(BASEIMAGEURL_70x70 + recordDataBeanList.get(position).getImages())
                .apply(new RequestOptions().placeholder(R.drawable.cart).error(R.drawable.cart))
                .into(holder.imgView);

        Log.d(TAG, "onBindViewHolder: " + BASEIMAGEURL_70x70 + recordDataBeanList.get(position).getImages());
        holder.txtPrice.setText("\u20B9 " + recordDataBeanList.get(position).getTotalPrice());
        holder.txtQty.setText("Qty : " + recordDataBeanList.get(position).getPurchaseQuantity());
        holder.txtTitle.setText(recordDataBeanList.get(position).getProductName());

        if (recordDataBeanList.size()>1){
            holder.view.setVisibility(View.VISIBLE);
        }else {
            holder.view.setVisibility(View.GONE);
        }

        holder.txtViewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<MyOrderDetailsResponse.DataBean.ProductsBean.ChildProductsBean> childProductsBeanList=recordDataBeanList.get(position).getChildProducts();

                ChildItemsBottomSheetFragment bottomSheetFragment = new ChildItemsBottomSheetFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable(ARRAYLIST, (Serializable) childProductsBeanList);
                bundle.putString(TITLE, recordDataBeanList.get(position).getProductName());
                bundle.putString(FRAGMENT_FROM, PRODUCT_DETAILS);
                bottomSheetFragment.setArguments(bundle);
                bottomSheetFragment.show(((FragmentActivity)context).getSupportFragmentManager(), bottomSheetFragment.getTag());


            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return recordDataBeanList.size();

    }
}
