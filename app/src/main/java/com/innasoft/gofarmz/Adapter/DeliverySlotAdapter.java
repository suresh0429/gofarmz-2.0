package com.innasoft.gofarmz.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.gofarmz.Activity.CheckoutActivity;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.models.SlotItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.models.AccountItem.CHILD_TYPE;
import static com.innasoft.gofarmz.models.AccountItem.HEADER_TYPE;

public class DeliverySlotAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<SlotItem> mList;
    private Context mContext;

    private ListAdapterListener mListener;

    public interface ListAdapterListener { // create an interface
        void onClickAtOKButton(int position); // create callback function
    }

    public DeliverySlotAdapter(List<SlotItem> mList, Context mContext,ListAdapterListener mListener) {
        this.mList = mList;
        this.mContext = mContext;
        this.mListener = mListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case HEADER_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_header, parent, false);
                return new DeliverySlotAdapter.CityViewHolder(view);
            case CHILD_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.slot_list_item, parent, false);
                return new DeliverySlotAdapter.EventViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        SlotItem object = mList.get(position);
        if (object != null) {
            switch (object.getType()) {
                case HEADER_TYPE:
                    Log.d(TAG, "HEADER_TYPE: "+object.getDateslot());
                    ((DeliverySlotAdapter.CityViewHolder) holder).txtDate.setText(object.getDateslot());
                    ((DeliverySlotAdapter.CityViewHolder) holder).mTitle.setBackgroundColor(Color.parseColor("#4CE5E5E5"));
                    break;
                case CHILD_TYPE:
                    Log.d(TAG, "CHILD_TYPE: "+object.getDateslot());
                    ((DeliverySlotAdapter.EventViewHolder) holder).txtTitle.setText(object.getStartTime()+" - "+object.getEndTime());
                    //((DeliverySlotAdapter.EventViewHolder) holder).imageIcon.setImageResource(object.getIcon());
                    ((DeliverySlotAdapter.EventViewHolder) holder).parentLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mListener.onClickAtOKButton(position);
                            if (mContext instanceof CheckoutActivity) {

                                ((CheckoutActivity) mContext).slotEditText(object.getId(),object.getDateslot(),object.getStartTime(),object.getEndTime());
                            }

                        }
                    });

                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        if (mList == null)
            return 0;
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (mList != null) {
            SlotItem object = mList.get(position);
            if (object != null) {
                return object.getType();
            }
        }
        return 0;
    }

    public static class CityViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout mTitle;
        private TextView txtDate;

        public CityViewHolder(View itemView) {
            super(itemView);
            mTitle = (LinearLayout) itemView.findViewById(R.id.headerLayout);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
        }
    }

    public static class EventViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageIcon)
        ImageView imageIcon;
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.imageFarward)
        ImageView imageFarward;
        @BindView(R.id.parentLayout)
        RelativeLayout parentLayout;

        public EventViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }



}
