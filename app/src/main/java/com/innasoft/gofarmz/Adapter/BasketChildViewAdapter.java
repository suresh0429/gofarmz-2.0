package com.innasoft.gofarmz.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.models.ChildItemsModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Api.RetrofitClient.BASEIMAGEURL_70x70;

public class BasketChildViewAdapter extends RecyclerView.Adapter<BasketChildViewAdapter.MyViewHolder> {
    List<ChildItemsModel> model;
    Context context;



    public BasketChildViewAdapter(List<ChildItemsModel> model, Context context) {
        this.model = model;
        this.context = context;
    }

    public BasketChildViewAdapter(ArrayList<Object> transactionList, FragmentActivity activity) {
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgView)
        ImageView imgView;
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.txtQty)
        TextView txtQty;
        @BindView(R.id.txtPrice)
        TextView txtPrice;
        @BindView(R.id.txtCapacity)
        TextView txtCapacity;
        @BindView(R.id.view)
        View view;
        @BindView(R.id.parentLayout)
        RelativeLayout parentLayout;
        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.child_view_list_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Glide.with(context).load(BASEIMAGEURL_70x70 + model.get(position).getImage())
                .apply(new RequestOptions().placeholder(R.drawable.cart).error(R.drawable.cart))
                .into(holder.imgView);

        Log.d(TAG, "onBindViewHolder: " + BASEIMAGEURL_70x70 + model.get(position).getImage());
        holder.txtPrice.setText("\u20B9 " + model.get(position).getFinalPrice());

        if (model.get(position).getQuantity() != null){
            holder.txtQty.setText("Qty : " + model.get(position).getQuantity());
        }else {
            //holder.txtQty.setVisibility(View.GONE);
            holder.txtQty.setText("Qty : " + "1");
        }

        holder.txtTitle.setText(model.get(position).getProductName());
        holder.txtCapacity.setText(model.get(position).getUnitValue()+" "+model.get(position).getUnitName());

        if (model.size()>1){
            holder.view.setVisibility(View.VISIBLE);
        }else {
            holder.view.setVisibility(View.GONE);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return model.size();

    }
}
