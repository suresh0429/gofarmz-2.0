package com.innasoft.gofarmz.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.FormarResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.google.firebase.remoteconfig.FirebaseRemoteConfig.TAG;
import static com.innasoft.gofarmz.Api.RetrofitClient.BASE_URL;

public class FormarAdapter extends RecyclerView.Adapter<FormarAdapter.MyViewHolder> {
    List<FormarResponse.DataBean> model;
    Context context;


    public FormarAdapter(List<FormarResponse.DataBean> model, Context context) {
        this.model = model;
        this.context = context;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgFarmer)
        ImageView imgFarmer;
        @BindView(R.id.txtFarmerName)
        TextView txtFarmerName;
        @BindView(R.id.txtFarmName)
        TextView txtFarmName;
        @BindView(R.id.txtType)
        TextView txtType;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.farmerlist_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        holder.txtType.setText(model.get(position).getType());
        holder.txtFarmerName.setText(model.get(position).getFarmer_name());
        holder.txtFarmName.setText(model.get(position).getFarm_name());

        if (model.get(position).getType().equalsIgnoreCase("Farmer")){

            if (model.get(position).getFarm_photo().equalsIgnoreCase("")){
                holder.imgFarmer.setImageResource(R.drawable.ic_agriculture);
            }else {
                Glide.with(context).load(BASE_URL+model.get(position).getFarm_photo()).into(holder.imgFarmer);
                Log.d(TAG, "onBindViewHolder: "+model.get(position).getFarm_photo());
            }

        }else {

            if (model.get(position).getFarm_photo().equalsIgnoreCase("")){
                holder.imgFarmer.setImageResource(R.drawable.ic_supplier);
            }else {
                Glide.with(context).load(BASE_URL+model.get(position).getFarm_photo()).into(holder.imgFarmer);
                Log.d(TAG, "onBindViewHolder: "+model.get(position).getFarm_photo());
            }
        }


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return model.size();

    }
}
