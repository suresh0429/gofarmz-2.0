package com.innasoft.gofarmz.Adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.models.Options;

import java.util.List;

public class CustomSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

   // List<CustomBoxResponse.DataBean.OPTIONALBean.OptionsBean> company;
    List<Options> optionsArrayList;
    Context context;

    public CustomSpinnerAdapter(Context context, List<Options> optionsArrayList) {
       // this.company = company;
        this.context = context;
        this.optionsArrayList = optionsArrayList;
    }

    @Override
    public int getCount() {
        return optionsArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return optionsArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view =  View.inflate(context, R.layout.company_main, null);
        TextView textView = (TextView) view.findViewById(R.id.main);
        textView.setText(optionsArrayList.get(position).getOptName()+" - \u20B9"+optionsArrayList.get(position).getPrice());
        return textView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        View view;
        view =  View.inflate(context, R.layout.company_dropdown, null);
        final TextView textView = (TextView) view.findViewById(R.id.dropdown);
        textView.setText(optionsArrayList.get(position).getOptName()+" - \u20B9"+optionsArrayList.get(position).getPrice());

        return view;
    }
}
