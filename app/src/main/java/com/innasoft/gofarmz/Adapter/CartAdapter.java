package com.innasoft.gofarmz.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.innasoft.gofarmz.Fragments.CartFragment;
import com.innasoft.gofarmz.Fragments.ChildItemsBottomSheetFragment;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.CartResponse;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.innasoft.gofarmz.Api.RetrofitClient.BASEIMAGEURL_70x70;
import static com.innasoft.gofarmz.Utilis.Constants.ARRAYLIST;
import static com.innasoft.gofarmz.Utilis.Constants.CART;
import static com.innasoft.gofarmz.Utilis.Constants.FRAGMENT_FROM;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;
import static com.innasoft.gofarmz.Utilis.Constants.TYPE;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {
    List<CartResponse.DataBean.RecordsBean> model;
    Context context;
    CartFragment fragment;


    public CartAdapter(List<CartResponse.DataBean.RecordsBean> model, Context context, CartFragment fragment) {
        this.model = model;
        this.context = context;
        this.fragment = fragment;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtPrice)
        TextView txtPrice;
        @BindView(R.id.txtType)
        TextView txtType;
        @BindView(R.id.priceLayout)
        LinearLayout priceLayout;
        @BindView(R.id.product_minus)
        TextView productMinus;
        @BindView(R.id.product_quantity)
        TextView productQuantity;
        @BindView(R.id.product_plus)
        TextView productPlus;
        @BindView(R.id.quantityLayout)
        LinearLayout quantityLayout;
        @BindView(R.id.view)
        View view;
        @BindView(R.id.txtRemove)
        TextView txtRemove;
        @BindView(R.id.txtViewDetails)
        TextView txtViewDetails;
        @BindView(R.id.parentLayout)
        CardView parentLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cart_list_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Glide.with(context).load(BASEIMAGEURL_70x70 + model.get(position).getImages()).into(holder.image);
        holder.txtName.setText(model.get(position).getProductName());

        Double totalvalue = (double) model.get(position).getTotalPrice();
        holder.txtPrice.setText("\u20B9" + String.format("%.2f", totalvalue));
        holder.txtType.setText(model.get(position).getType());
        holder.productQuantity.setText(model.get(position).getPurchaseQuantity());

        holder.productPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (model.get(position).getPurchaseQuantity().equals("8")) {
                    Toast.makeText(context, "Sorry,You Can't add more qunatity!!!", Toast.LENGTH_LONG).show();
                } else {
                    int qty = Integer.parseInt(model.get(position).getPurchaseQuantity());
                    if (qty != 0)
                        qty++;

                    ((CartFragment) fragment).changeQuatity(model.get(position).getId(),qty);
                }
            }
        });

        holder.productMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int qty = Integer.parseInt(model.get(position).getPurchaseQuantity());
                if (qty > 1) {
                    qty--;

                    ((CartFragment) fragment).changeQuatity(model.get(position).getId(), qty);
                }
            }
        });

        holder.txtRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((CartFragment) fragment).deleteCartItem(model.get(position).getId());
            }
        });
        holder.txtViewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (model.get(position).getType().equalsIgnoreCase("COMBO_CUSTOM")){

                    List<CartResponse.DataBean.RecordsBean.ADDONSBean> customProductsBeanList1 = model.get(position).getADDONS();
                    ChildItemsBottomSheetFragment bottomSheetFragment = new ChildItemsBottomSheetFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(ARRAYLIST, (Serializable) customProductsBeanList1);
                    bundle.putString(TITLE, model.get(position).getProductName());
                    bundle.putString(TYPE, model.get(position).getType());
                    bundle.putString(FRAGMENT_FROM, CART);
                    bottomSheetFragment.setArguments(bundle);
                    bottomSheetFragment.show(((FragmentActivity) context).getSupportFragmentManager(), bottomSheetFragment.getTag());

                }else {

                    List<CartResponse.DataBean.RecordsBean.ESSENTIALBean> normalProductsBeanList = model.get(position).getESSENTIAL();
                    ChildItemsBottomSheetFragment bottomSheetFragment = new ChildItemsBottomSheetFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(ARRAYLIST, (Serializable) normalProductsBeanList);
                    bundle.putString(TITLE, model.get(position).getProductName());
                    bundle.putString(TYPE, model.get(position).getType());
                    bundle.putString(FRAGMENT_FROM, CART);
                    bottomSheetFragment.setArguments(bundle);
                    bottomSheetFragment.show(((FragmentActivity) context).getSupportFragmentManager(), bottomSheetFragment.getTag());

                }


            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return model.size();

    }
}
