package com.innasoft.gofarmz.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.gofarmz.Activity.ReferFarmerActivity;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.StateResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StateAdapter extends RecyclerView.Adapter<StateAdapter.MyViewHolder> {


    private Context mContext;
    private List<StateResponse.DataBean> homeList;
    private AlertDialog alertDialog;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtArea)
        TextView txtArea;
        @BindView(R.id.parentLayout)
        LinearLayout parentLayout;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    public StateAdapter(Context mContext, List<StateResponse.DataBean> homekitchenList, AlertDialog alertDialog) {
        this.mContext = mContext;
        this.homeList = homekitchenList;
        this.alertDialog = alertDialog;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.location_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final StateResponse.DataBean home = homeList.get(position);

        holder.txtArea.setText(home.getName());
        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                if (mContext instanceof ReferFarmerActivity) {
                    ((ReferFarmerActivity)mContext).stateData(home.getId(),home.getName());
                }

            }

        });


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return homeList.size();
    }


}
