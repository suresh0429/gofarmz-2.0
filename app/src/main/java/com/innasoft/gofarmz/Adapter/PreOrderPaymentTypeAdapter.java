package com.innasoft.gofarmz.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.innasoft.gofarmz.Activity.PreOrderCheckoutActivity;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.PreOrderCheckoutDataResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.innasoft.gofarmz.Api.RetrofitClient.BASE_URL;
import static com.innasoft.gofarmz.Utilis.Constants.capitalize;

public class PreOrderPaymentTypeAdapter extends RecyclerView.Adapter<PreOrderPaymentTypeAdapter.Holder> {
    Context context;
    public int lastSelectedPosition = -1;
    List<PreOrderCheckoutDataResponse.DataBean.PaymentGatewayBean> paymentGatewayBeans;

    private PreOrderPaymentTypeAdapter.PaymentTypeInterface paymentTypeInterface;

    public PreOrderPaymentTypeAdapter(PreOrderCheckoutActivity checkoutActivity, List<PreOrderCheckoutDataResponse.DataBean.PaymentGatewayBean> paymentGatewayBeans, PreOrderPaymentTypeAdapter.PaymentTypeInterface paymentTypeInterface) {
        this.context = checkoutActivity;
        this.paymentGatewayBeans = paymentGatewayBeans;
        this.paymentTypeInterface = paymentTypeInterface;
    }

    @NonNull
    @Override
    public PreOrderPaymentTypeAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_paymentmethods, viewGroup, false);
        return new PreOrderPaymentTypeAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PreOrderPaymentTypeAdapter.Holder holder, final int i) {


        holder.offerName.setText(capitalize(paymentGatewayBeans.get(i).getName()));
        holder.offerSelect.setChecked(lastSelectedPosition == i);

        Glide.with(context).load(BASE_URL + paymentGatewayBeans.get(i).getLogo()).into(holder.pImage);

        Log.d("IMAGE", "onBindViewHolder: " + BASE_URL + paymentGatewayBeans.get(i).getLogo());
        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastSelectedPosition = holder.getAdapterPosition();
                notifyDataSetChanged();

                paymentTypeInterface.onItemClick(paymentGatewayBeans, i);
                Log.d("POSITION", "onClick: "+paymentGatewayBeans.get(i).getId());


                // cash = paymentGatewayBeans.get(lastSelectedPosition).getId();
            }
        };
        holder.itemView.setOnClickListener(clickListener);
        holder.offerSelect.setOnClickListener(clickListener);

    }

    @Override
    public int getItemCount() {
        return paymentGatewayBeans.size();
    }

    class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.p_image)
        ImageView pImage;
        @BindView(R.id.offer_select)
        RadioButton offerSelect;
        @BindView(R.id.offer_name)
        TextView offerName;

        public Holder(@NonNull final View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

        }
    }


    public interface PaymentTypeInterface {

        void onItemClick(List<PreOrderCheckoutDataResponse.DataBean.PaymentGatewayBean> paymentGatewayBean, int position);
    }
}