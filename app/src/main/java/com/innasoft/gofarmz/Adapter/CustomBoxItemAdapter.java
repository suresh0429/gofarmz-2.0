package com.innasoft.gofarmz.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.gofarmz.Activity.ProductsActivity;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.CustomBoxResponse;
import com.innasoft.gofarmz.models.Product;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class CustomBoxItemAdapter extends RecyclerView.Adapter<CustomBoxItemAdapter.MyViewHolder> {
    List<CustomBoxResponse.DataBean.OPTIONALBean.OptionsBean> products;
    Context context;
    androidx.appcompat.app.AlertDialog alertDialog;

    private ListAdapterListener mListener;



    public interface ListAdapterListener { // create an interface
        void onClickAtOKButton(int position); // create callback function
    }

    public CustomBoxItemAdapter(List<CustomBoxResponse.DataBean.OPTIONALBean.OptionsBean> products, Context context,androidx.appcompat.app.AlertDialog alertDialog) {
        this.products = products;
        this.context = context;
        this.alertDialog = alertDialog;
       // this.mListener = mListener;
    }



    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.parentLayout)
        LinearLayout parentLayout;
        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    @NonNull
    @Override
    public CustomBoxItemAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.child_box_list_item, parent, false);
        CustomBoxItemAdapter.MyViewHolder holder = new CustomBoxItemAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull CustomBoxItemAdapter.MyViewHolder holder, int position) {
         CustomBoxResponse.DataBean.OPTIONALBean.OptionsBean product = products.get(position);
         holder.txtTitle.setText(product.getOptName()+" - "+product.getPrice());
         holder.parentLayout.setOnClickListener(view -> {

             Product product1 = new Product("","","","",
                     "","",product.getOptName(),Double.parseDouble(product.getPrice()),product.getIsInCart(),product.getCartQty(),product.getQtyPrice());

             Log.d(TAG, "onBindViewHolder: "+product.getOptName()+" - "+product.getPrice());
//             mListener.onClickAtOKButton(position);
             if (context instanceof ProductsActivity) {

                 ((ProductsActivity) context).updatePrice(product1);

                  alertDialog.dismiss();
             }


         });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return products.size();

    }
}