package com.innasoft.gofarmz.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.SliderTopBannerResponse;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.List;

import static com.innasoft.gofarmz.Api.RetrofitClient.BASE_URL;

public class SliderAdapter extends SliderViewAdapter<SliderAdapter.SliderAdapterVH> {

    private Context context;
    private List<SliderTopBannerResponse.DataBean> dataBeans;

    public SliderAdapter(Context context, List<SliderTopBannerResponse.DataBean> dataBeans) {
        this.context = context;
        this.dataBeans = dataBeans;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_layout_item, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {
        viewHolder.textViewDescription.setText(dataBeans.get(position).getHighlighted_text());
        Glide.with(viewHolder.itemView)
                .load(BASE_URL +dataBeans.get(position).getImage())
                .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.drawable.placeholder))
                .into(viewHolder.imageViewBackground);



    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size
        return dataBeans.size();
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;
        ImageView imageViewBackground;
        TextView textViewDescription;

        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);
            textViewDescription = itemView.findViewById(R.id.tv_auto_image_slider);
            this.itemView = itemView;
        }
    }
}
