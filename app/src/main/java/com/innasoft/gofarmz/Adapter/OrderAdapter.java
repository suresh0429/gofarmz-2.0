package com.innasoft.gofarmz.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.innasoft.gofarmz.Activity.OrderDetailsActivity;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.MyOrderResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Api.RetrofitClient.BASEIMAGEURL_70x70;
import static com.innasoft.gofarmz.Utilis.Constants.MODULE;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_CART;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_ID;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_TITLE;
import static com.innasoft.gofarmz.Utilis.Constants.ORDER_STATUS;
import static com.innasoft.gofarmz.Utilis.Constants.BOTTAM_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MyViewHolder> {
    List<MyOrderResponse.DataBean.RecordDataBean> recordDataBeanList;
    Context context;


    public OrderAdapter(List<MyOrderResponse.DataBean.RecordDataBean> recordDataBeanList, Context context) {
        this.recordDataBeanList = recordDataBeanList;
        this.context = context;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.view)
        View view;
        @BindView(R.id.order_id)
        TextView orderId;
        @BindView(R.id.order_date)
        TextView orderDate;
        @BindView(R.id.coupan)
        TextView coupan;
        @BindView(R.id.order_status)
        TextView orderStatus;
        @BindView(R.id.firstLayout)
        LinearLayout firstLayout;
        @BindView(R.id.ord_his_product_img)
        ImageView ordHisProductImg;
        @BindView(R.id.ord_his_price_txt)
        TextView ordHisPriceTxt;
        @BindView(R.id.secondLayout)
        LinearLayout secondLayout;
        @BindView(R.id.parentLayout)
        CardView parentLayout;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.order_list_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Glide.with(context).load(BASEIMAGEURL_70x70 + recordDataBeanList.get(position).getImages())
                .apply(new RequestOptions().placeholder(R.drawable.cart).error(R.drawable.cart))
                .into(holder.ordHisProductImg);
        Log.d(TAG, "onBindViewHolder: " + recordDataBeanList.get(position).getImages());
        holder.orderId.setText(recordDataBeanList.get(position).getOrder_id());
        holder.orderDate.setText(recordDataBeanList.get(position).getOrder_date());
        holder.orderStatus.setText(recordDataBeanList.get(position).getOrder_status());
        holder.ordHisPriceTxt.setText("\u20B9 " + recordDataBeanList.get(position).getFinal_price());

        if (recordDataBeanList.get(position).getCoupon_code() != null) {
            holder.coupan.setVisibility(View.VISIBLE);
            holder.coupan.setText(recordDataBeanList.get(position).getCoupon_code() + "  Coupon Applied");
        } else {
            holder.coupan.setVisibility(View.GONE);
        }

        if (recordDataBeanList.get(position).getOrder_status().equalsIgnoreCase("Confirmed") ||
                recordDataBeanList.get(position).getOrder_status().equalsIgnoreCase("Dispatched") ||
                recordDataBeanList.get(position).getOrder_status().equalsIgnoreCase("Shipped") ||
                recordDataBeanList.get(position).getOrder_status().equalsIgnoreCase("Delivered")) {

            holder.view.setBackgroundColor(Color.parseColor("#0f9350"));
            holder.orderStatus.setTextColor(Color.parseColor("#0f9350"));
        } else if (recordDataBeanList.get(position).getOrder_status().equalsIgnoreCase("Failed")
                || recordDataBeanList.get(position).getOrder_status().equalsIgnoreCase("Cancelled")
                || recordDataBeanList.get(position).getOrder_status().equalsIgnoreCase("Not Delivered")) {
            holder.view.setBackgroundColor(Color.RED);
            holder.orderStatus.setTextColor(Color.RED);
        } else if (recordDataBeanList.get(position).getOrder_status().equalsIgnoreCase("Pending")) {
            holder.view.setBackgroundColor(Color.parseColor("#FF8000"));
            holder.orderStatus.setTextColor(Color.parseColor("#FF8000"));
        } else if (recordDataBeanList.get(position).getOrder_status().equalsIgnoreCase("Received")) {
            holder.view.setBackgroundColor(Color.BLUE);
            holder.orderStatus.setTextColor(Color.BLUE);
        }

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, OrderDetailsActivity.class);
                intent.putExtra(ORDER_ID,recordDataBeanList.get(position).getId());
                intent.putExtra(ORDER_STATUS,recordDataBeanList.get(position).getOrder_status());
                intent.putExtra(TITLE,ORDER_TITLE);
                intent.putExtra(MODULE, ORDER_CART);
                intent.putExtra(ORDER_TAB_POSITION,0);
                intent.putExtra(BOTTAM_TAB_POSITION,3);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return recordDataBeanList.size();

    }
}
