package com.innasoft.gofarmz.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.MyOrderDetailsResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TrackingAdapter extends RecyclerView.Adapter<TrackingAdapter.MyViewHolder> {
    List<MyOrderDetailsResponse.DataBean.StatusBean> statusBeanList;
    Context context;



    public TrackingAdapter(List<MyOrderDetailsResponse.DataBean.StatusBean> statusBeanList, Context context) {
        this.statusBeanList = statusBeanList;
        this.context = context;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.dot_image)
        ImageView dotImage;
        @BindView(R.id.view)
        View view;
        @BindView(R.id.txtStatus)
        TextView txtStatus;
        @BindView(R.id.txtDate)
        TextView txtDate;
        @BindView(R.id.txtMessage)
        TextView txtMessage;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.tracking_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        /* if (recordDataBeanList.get(position).getPreOrderStatus().equalsIgnoreCase("Confirmed") ||
                recordDataBeanList.get(position).getPreOrderStatus().equalsIgnoreCase("Dispatched") ||
                recordDataBeanList.get(position).getPreOrderStatus().equalsIgnoreCase("Shipped") ||
                recordDataBeanList.get(position).getPreOrderStatus().equalsIgnoreCase("Delivered")) {

            holder.view.setBackgroundColor(Color.parseColor("#0f9350"));
            holder.orderStatus.setTextColor(Color.parseColor("#0f9350"));
        } else if (recordDataBeanList.get(position).getPreOrderStatus().equalsIgnoreCase("Failed")
                || recordDataBeanList.get(position).getPreOrderStatus().equalsIgnoreCase("Cancelled")
                || recordDataBeanList.get(position).getPreOrderStatus().equalsIgnoreCase("Not Delivered")) {
            holder.view.setBackgroundColor(Color.RED);
            holder.orderStatus.setTextColor(Color.RED);
        } else if (recordDataBeanList.get(position).getPreOrderStatus().equalsIgnoreCase("Pending")) {
            holder.view.setBackgroundColor(Color.parseColor("#FF8000"));
            holder.orderStatus.setTextColor(Color.parseColor("#FF8000"));
        } else if (recordDataBeanList.get(position).getPreOrderStatus().equalsIgnoreCase("Received")) {
            holder.view.setBackgroundColor(Color.BLUE);
            holder.orderStatus.setTextColor(Color.BLUE);
        }
*/
        Animation animation = new AlphaAnimation(1, 0); //to change visibility from visible to invisible
        animation.setDuration(1000); //1 second duration for each animation cycle
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE); //repeating indefinitely
        animation.setRepeatMode(Animation.REVERSE); //animation will start from end point once ended.
        holder.dotImage.startAnimation(animation); //to start animation
        holder.view.startAnimation(animation); //to start animation

        holder.txtMessage.setText(statusBeanList.get(position).getMessage());
        holder.txtStatus.setText(statusBeanList.get(position).getOrderStatus());
        holder.txtDate.setText(statusBeanList.get(position).getCreatedOn());

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return statusBeanList.size();

    }
}
