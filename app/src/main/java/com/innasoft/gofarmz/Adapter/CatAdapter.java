package com.innasoft.gofarmz.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.innasoft.gofarmz.Activity.ProductsActivity;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.models.CatItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Utilis.Constants.CATID;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;

public class CatAdapter extends RecyclerView.Adapter<CatAdapter.MyViewHolder> {
    List<CatItem> model;
    Context context;



    public CatAdapter(List<CatItem> model, Context context) {
        this.model = model;
        this.context = context;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgCat)
        ImageView imgCat;
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.parentLayout)
        LinearLayout parentLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cat_list_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Glide.with(context).load(model.get(position).getImage()).into(holder.imgCat);
        Log.d(TAG, "onBindViewHolder: " + model.get(position).getImage());
        holder.txtTitle.setText(model.get(position).getCatName());
        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ProductsActivity.class);
                intent.putExtra(TITLE,model.get(position).getCatName());
                intent.putExtra(CATID, model.get(position).getCatId());
                if (!model.get(position).getCatId().equalsIgnoreCase("")) {
                    context.startActivity(intent);
                }

            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        //return position == 6 ? 0 : 1;
        return position;
    }

    @Override
    public int getItemCount() {
        return model.size();

    }
}
