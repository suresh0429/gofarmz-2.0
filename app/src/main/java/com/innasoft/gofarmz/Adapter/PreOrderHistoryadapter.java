package com.innasoft.gofarmz.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.MyPreOrderDetailsResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class PreOrderHistoryadapter extends RecyclerView.Adapter<OrderHistoryAdapter.MyViewHolder> {
    List<MyPreOrderDetailsResponse.DataBean.ProductsBean> recordDataBeanList;
    Context context;


    public PreOrderHistoryadapter(List<MyPreOrderDetailsResponse.DataBean.ProductsBean> recordDataBeanList, Context context) {
        this.recordDataBeanList = recordDataBeanList;
        this.context = context;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgView)
        ImageView imgView;
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.txtQty)
        TextView txtQty;
        @BindView(R.id.txtPrice)
        TextView txtPrice;
        @BindView(R.id.txtViewDetails)
        TextView txtViewDetails;
        @BindView(R.id.view)
        View view;
        @BindView(R.id.parentLayout)
        RelativeLayout parentLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    @NonNull
    @Override
    public OrderHistoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.order_history_list_item, parent, false);
        OrderHistoryAdapter.MyViewHolder holder = new OrderHistoryAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull OrderHistoryAdapter.MyViewHolder holder, int position) {
        Glide.with(context).load( recordDataBeanList.get(position).getImage())
                .apply(new RequestOptions().placeholder(R.drawable.cart).error(R.drawable.cart))
                .into(holder.imgView);

        Log.d(TAG, "onBindViewHolder: " +  recordDataBeanList.get(position).getImage());
        holder.txtPrice.setText("\u20B9 " + recordDataBeanList.get(position).getTotal_price());
        holder.txtQty.setText("Qty : " + recordDataBeanList.get(position).getProduct_quantity());
        holder.txtTitle.setText(recordDataBeanList.get(position).getProduct_name());
        holder.txtViewDetails.setVisibility(View.GONE);

        if (recordDataBeanList.size() > 1) {
            holder.view.setVisibility(View.VISIBLE);
        } else {
            holder.view.setVisibility(View.GONE);
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return recordDataBeanList.size();

    }
}
