package com.innasoft.gofarmz.Fragments;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.gofarmz.Activity.LoginActivity;
import com.innasoft.gofarmz.Adapter.AccountAdapter;
import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.Preferences.UserSessionManager;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.BaseResponse;
import com.innasoft.gofarmz.Utilis.Util;
import com.innasoft.gofarmz.models.AccountItem;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Preferences.UserSessionManager.KEY_DEVICEID;
import static com.innasoft.gofarmz.Preferences.UserSessionManager.KEY_EMAIL;
import static com.innasoft.gofarmz.Preferences.UserSessionManager.KEY_ID;
import static com.innasoft.gofarmz.Preferences.UserSessionManager.KEY_MOBILE;
import static com.innasoft.gofarmz.Preferences.UserSessionManager.KEY_NAME;
import static com.innasoft.gofarmz.Preferences.UserSessionManager.KEY_TOKEN;
import static com.innasoft.gofarmz.models.AccountItem.CHILD_TYPE;
import static com.innasoft.gofarmz.models.AccountItem.HEADER_TYPE;


/**
 * A simple {@link Fragment} subclass.
 */
public class AcountFragment extends Fragment implements InternetConnectivityListener {
   
    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;
    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    UserSessionManager userSessionManager;

    List<AccountItem> accountItemList = new ArrayList<>();
    RecyclerView.LayoutManager manager;
    AccountAdapter adapter;

    String deviceId, token, user_id,name,email,mobile;
    boolean internet;

    View rootView;
    @BindView(R.id.img)
    ImageView img;
    @BindView(R.id.txtUserName)
    TextView txtUserName;
    @BindView(R.id.txtMobile)
    TextView txtMobile;
    @BindView(R.id.txtEmail)
    TextView txtEmail;
    @BindView(R.id.imgEdit)
    ImageView imgEdit;
    @BindView(R.id.btnLogout)
    TextView btnLogout;
    @BindView(R.id.accountRecycler)
    RecyclerView accountRecycler;


    public AcountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_acount, container, false);
        ButterKnife.bind(this, rootView);
        progressLayout.setVisibility(View.GONE);


        userSessionManager = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();

        deviceId = userDetails.get(KEY_DEVICEID);
        token = userDetails.get(KEY_TOKEN);
        user_id = userDetails.get(KEY_ID);
        name = userDetails.get(KEY_NAME);
        email = userDetails.get(KEY_EMAIL);
        mobile = userDetails.get(KEY_MOBILE);

        txtUserName.setText(name);
        txtEmail.setText(email);
        txtMobile.setText(mobile);

        accountData();

        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);


        return rootView;
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);

    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
            internet = isConnected;

        } else {
            internet = false;
            Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }


    @OnClick({R.id.imgEdit, R.id.btnLogout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgEdit:
                ProfileFragmentDilogue bottomSheetFragment = new ProfileFragmentDilogue();
                bottomSheetFragment.show(getActivity().getSupportFragmentManager(), bottomSheetFragment.getTag());
                break;
            case R.id.btnLogout:
                if (internet) {
                    logoutDilogue();
                } else {
                    Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);

                }

                break;
        }
    }


    private void accountData() {

        manager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        accountRecycler.setLayoutManager(manager);
        //accountRecycler.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        adapter = new AccountAdapter(accountItemList, getActivity());
        accountRecycler.setAdapter(adapter);
        adapter.notifyDataSetChanged();


        // account list stateData
        accountItemList.add(new AccountItem(R.drawable.ic_orders, "Orders", CHILD_TYPE));
        accountItemList.add(new AccountItem(R.drawable.ic_wallet, "Wallet", CHILD_TYPE));
        accountItemList.add(new AccountItem(R.drawable.ic_location, "Address", CHILD_TYPE));
        accountItemList.add(new AccountItem(R.drawable.ic_account, null, HEADER_TYPE));
        accountItemList.add(new AccountItem(R.drawable.ic_farmcom, "Gofarmz Community Shops", CHILD_TYPE));
        accountItemList.add(new AccountItem(R.drawable.ic_farmerbulk, "Farmer Bulk harvests", CHILD_TYPE));
        accountItemList.add(new AccountItem(R.drawable.ic_knowfarmer, "Know Your Farmer", CHILD_TYPE));
        accountItemList.add(new AccountItem(R.drawable.ic_story, "Our Story", CHILD_TYPE));
        accountItemList.add(new AccountItem(R.drawable.ic_network, "Farmer Network", CHILD_TYPE));
        accountItemList.add(new AccountItem(R.drawable.ic_refer, "Refer a Farmer", CHILD_TYPE));
        accountItemList.add(new AccountItem(R.drawable.ic_account, null, HEADER_TYPE));
        accountItemList.add(new AccountItem(R.drawable.ic_notifiy, "Notifications", CHILD_TYPE));
        accountItemList.add(new AccountItem(R.drawable.ic_share, "Share", CHILD_TYPE));
        accountItemList.add(new AccountItem(R.drawable.ic_support, "Support", CHILD_TYPE));
        accountItemList.add(new AccountItem(R.drawable.ic_faq, "Faq", CHILD_TYPE));
        accountItemList.add(new AccountItem(R.drawable.ic_terms_and_conditions, "Terms & Conditions", CHILD_TYPE));
        accountItemList.add(new AccountItem(R.drawable.ic_privacy_policy, "Privacy Policy", CHILD_TYPE));


    }


    private void logoutDilogue(){
        new AlertDialog.Builder(getActivity(), R.style.CustomDialogTheme)
                .setTitle("Logout?")
                .setMessage("Are you sure want to Logout this Profile.")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d("MainActivity", "Sending atomic bombs to Jupiter");
                        logout();
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d("MainActivity", "Aborting mission...");
                        dialog.dismiss();
                    }
                })
                .show();
    }


    private void logout() {
        Call<BaseResponse> call = RetrofitClient.getInstance().getApi().userLogout(token, deviceId, user_id, deviceId);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                if (response.isSuccessful()) ;
                BaseResponse baseResponse = response.body();

                if (baseResponse.getStatus().equals("10100")) {

                    SharedPreferences preferences = getContext().getSharedPreferences("LOCATION_PREF", 0);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.apply();
                    editor.clear();

                    userSessionManager.clearSession();

                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                    Toast.makeText(getContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();


                } else if (baseResponse.getStatus().equals("10200")) {

                    Toast.makeText(getContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (baseResponse.getStatus().equals("10300")) {

                  //  Toast.makeText(getContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (baseResponse.getStatus().equals("10400")) {

                    Toast.makeText(getContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
