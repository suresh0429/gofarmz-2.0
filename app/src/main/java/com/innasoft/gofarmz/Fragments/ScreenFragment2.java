package com.innasoft.gofarmz.Fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.innasoft.gofarmz.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ScreenFragment2 extends Fragment {


    public ScreenFragment2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_screen_fragment2, container, false);
    }

}
