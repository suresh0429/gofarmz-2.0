package com.innasoft.gofarmz.Fragments;


import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.innasoft.gofarmz.Activity.HomeActivity;
import com.innasoft.gofarmz.Adapter.BasketAdapter;
import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.Preferences.UserSessionManager;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.BaseResponse;
import com.innasoft.gofarmz.Response.CartCountResponse;
import com.innasoft.gofarmz.Response.HomeBoxProductsResponse;
import com.innasoft.gofarmz.Utilis.Util;
import com.innasoft.gofarmz.models.BasketItem;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class MediumBasket extends Fragment implements InternetConnectivityListener {
    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    View rootView;
    boolean internet;
    private UserSessionManager userSessionManager;
    String deviceId,user_id,token,productId;
    ArrayList<BasketItem> basketItems = new ArrayList<>();

    @BindView(R.id.txtMessage)
    TextView txtMessage;
    @BindView(R.id.txtPrice)
    TextView txtPrice;
    @BindView(R.id.topLayout)
    LinearLayout topLayout;
    @BindView(R.id.Basketrecycler)
    RecyclerView Basketrecycler;
    @BindView(R.id.txtTotal)
    TextView txtTotal;
    @BindView(R.id.btnAddtocart)
    Button btnAddtocart;
    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;


    public MediumBasket() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_medium_basket, container, false);
        ButterKnife.bind(this, rootView);
        progressLayout.setVisibility(View.GONE);

        userSessionManager = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.KEY_DEVICEID);
        user_id = userDetails.get(UserSessionManager.KEY_ID);
        token = userDetails.get(UserSessionManager.KEY_TOKEN);

        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);

        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);

    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
            internet = isConnected;
            BasketData();
        } else {
            internet = false;
            // Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }

    private void BasketData() {

        progressLayout.setVisibility(View.VISIBLE);
        Call<HomeBoxProductsResponse> cartCountResponseCall = RetrofitClient.getInstance().getApi().HomeBoxProducts(token, "2", "COMBO", "1", user_id, deviceId);
        cartCountResponseCall.enqueue(new Callback<HomeBoxProductsResponse>() {
            @Override
            public void onResponse(Call<HomeBoxProductsResponse> call, Response<HomeBoxProductsResponse> response) {
                if (response.isSuccessful()) {

                    progressLayout.setVisibility(View.GONE);
                    HomeBoxProductsResponse homeBoxProductsResponse = response.body();
                    if (homeBoxProductsResponse.getStatus().equalsIgnoreCase("10100")) {
                        List<HomeBoxProductsResponse.DataBean> recordDataBeanList = homeBoxProductsResponse.getData();

                        basketItems.clear();
                        for (HomeBoxProductsResponse.DataBean dataBean : recordDataBeanList){

                            if (dataBean.getPdtName().equalsIgnoreCase("Medium Basket")) {
                                txtMessage.setText(dataBean.getAbout());
                                txtPrice.setText("\u20B9 "+dataBean.getPrice());
                                txtTotal.setText("\u20B9 "+dataBean.getPrice());

                                productId = dataBean.getId();
                                List<HomeBoxProductsResponse.DataBean.ESSENTIALBean> essentialBeans = dataBean.getESSENTIAL();
                                for (HomeBoxProductsResponse.DataBean.ESSENTIALBean essentialBean : essentialBeans) {
                                    basketItems.add(new BasketItem(essentialBean.getChildPdtName(), essentialBean.getOptName(), essentialBean.getImages()));
                                }
                            }

                        }

                        BasketAdapter adapter = new BasketAdapter(basketItems, getActivity());
                        Basketrecycler.setLayoutManager(new GridLayoutManager(getActivity(),4));
                        Basketrecycler.setItemAnimator(new DefaultItemAnimator());
                        Basketrecycler.setNestedScrollingEnabled(false);
                        Basketrecycler.setAdapter(adapter);

                    } else if (homeBoxProductsResponse.getStatus().equalsIgnoreCase("10200")) {
                        Toast.makeText(getContext(), homeBoxProductsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (homeBoxProductsResponse.getStatus().equalsIgnoreCase("10300")) {
                       // Toast.makeText(getContext(), homeBoxProductsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<HomeBoxProductsResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @OnClick(R.id.btnAddtocart)
    public void onViewClicked() {

        if (internet) {
            addToCart();
        } else {
            Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }

    private void addToCart(){
        progressLayout.setVisibility(View.VISIBLE);
        Call<BaseResponse> cartCountResponseCall = RetrofitClient.getInstance().getApi().addtocart(token,deviceId, user_id,productId, deviceId,"1",null);
        cartCountResponseCall.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, retrofit2.Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);
                    BaseResponse cartCountResponse = response.body();
                    if (cartCountResponse.getStatus().equals("10100")) {
                        if (getContext() instanceof HomeActivity) {
                            ((HomeActivity) getContext()).getCartCount();
                        }
                        Util.snackBar(progressLayout, "Add to Cart Successfully ", Color.YELLOW);
                    } else if (cartCountResponse.getStatus().equals("10200")) {
                        Util.snackBar(progressLayout, cartCountResponse.getMessage(), Color.YELLOW);
                    }
                    else if (cartCountResponse.getStatus().equalsIgnoreCase("10300")) {
                        Util.snackBar(progressLayout, "Sorry Try Again.", Color.YELLOW);
                    }
                    else if (cartCountResponse.getStatus().equalsIgnoreCase("10400")) {
                        Util.snackBar(progressLayout, "Already added to cart", Color.YELLOW);
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void getCartCount() {

        Call<CartCountResponse> cartCountResponseCall = RetrofitClient.getInstance().getApi().CartCount(token, user_id, deviceId);
        cartCountResponseCall.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, retrofit2.Response<CartCountResponse> response) {
                if (response.isSuccessful()) ;
                CartCountResponse cartCountResponse = response.body();
                if (cartCountResponse.getStatus().equals("10100")) {
                    // cart_count_txt.setText(String.valueOf(cartCountResponse.getData()));
                } else if (cartCountResponse.getStatus().equals("10200")) {
                    Toast.makeText(getActivity(), cartCountResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }
}