package com.innasoft.gofarmz.Fragments;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.gofarmz.Activity.HomeActivity;
import com.innasoft.gofarmz.Adapter.PreOrderAdapter;
import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.Preferences.UserSessionManager;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.MyPreOrderResponse;
import com.innasoft.gofarmz.Utilis.Util;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Utilis.Constants.BOTTAM_TAB_POSITION;


/**
 * A simple {@link Fragment} subclass.
 */
public class PreOrderFragment extends Fragment implements InternetConnectivityListener {
    @BindView(R.id.txtGotoHome)
    TextView txtGotoHome;
    @BindView(R.id.ordersLayout)
    LinearLayout ordersLayout;
    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    View rootView;
    boolean internet;
    private UserSessionManager userSessionManager;
    String deviceId, user_id, token;

    @BindView(R.id.preorder_recyclerview)
    RecyclerView preorderRecyclerview;
    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;


    public PreOrderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_pre_order, container, false);
        ButterKnife.bind(this, rootView);
        progressLayout.setVisibility(View.GONE);

        userSessionManager = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.KEY_DEVICEID);
        user_id = userDetails.get(UserSessionManager.KEY_ID);
        token = userDetails.get(UserSessionManager.KEY_TOKEN);
        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);

        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);

    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
            internet = isConnected;
            getPreOrderHisotryData();
        } else {
            internet = false;
            Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);

        }
    }

    private void getPreOrderHisotryData() {
        progressLayout.setVisibility(View.VISIBLE);
        Call<MyPreOrderResponse> call = RetrofitClient.getInstance().getApi().MyPreOrders(token, user_id);
        call.enqueue(new Callback<MyPreOrderResponse>() {
            @Override
            public void onResponse(Call<MyPreOrderResponse> call, Response<MyPreOrderResponse> response) {
                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);
                    MyPreOrderResponse myPreOrderResponse = response.body();
                    if (myPreOrderResponse.getStatus().equals("10100")) {

                        List<MyPreOrderResponse.DataBean.RecordDataBean> recordDataBeanList = myPreOrderResponse.getData().getRecordData();
                        PreOrderAdapter preOrderAdapter = new PreOrderAdapter(recordDataBeanList, getActivity());
                        preorderRecyclerview.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                        preorderRecyclerview.setItemAnimator(new DefaultItemAnimator());
                        preorderRecyclerview.setNestedScrollingEnabled(false);
                        preorderRecyclerview.setAdapter(preOrderAdapter);
                        ordersLayout.setVisibility(View.GONE);
                    } else if (myPreOrderResponse.getStatus().equals("10200")) {

                        ordersLayout.setVisibility(View.VISIBLE);

                    } else if (myPreOrderResponse.getStatus().equals("10300")) {
                        ordersLayout.setVisibility(View.VISIBLE);
                        txtGotoHome.setOnClickListener(view -> {
                            Intent intent = new Intent(getContext(), HomeActivity.class);
                            intent.putExtra(BOTTAM_TAB_POSITION, 0);
                            startActivity(intent);
                        });

                    }

                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(getActivity(), jObjError.getJSONObject("message").getString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<MyPreOrderResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }
}
