package com.innasoft.gofarmz.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.innasoft.gofarmz.Adapter.CustomBoxItemAdapter;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.CustomBoxResponse;
import com.innasoft.gofarmz.models.Product;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.innasoft.gofarmz.Utilis.Constants.ARRAYLIST;
import static com.innasoft.gofarmz.Utilis.Constants.FRAGMENT_FROM;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;

public class CustomBasketItemFragment extends BottomSheetDialogFragment implements CustomBoxItemAdapter.ListAdapterListener {
    private CustomBoxItemAdapter adapter;
    View rootView;
    @BindView(R.id.childItemsRecycler)
    RecyclerView childItemsRecycler;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.closeImage)
    ImageView closeImage;
    @BindView(R.id.txtClear)
    TextView txtClear;
    ArrayList<Product> childItemsModels = new ArrayList<>();


    public CustomBasketItemFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_delivery_slot, container, false);
        ButterKnife.bind(this, rootView);

        Bundle bundle = this.getArguments();
        if (bundle != null) {

            String title = bundle.getString(TITLE);
            String fragmentType = bundle.getString(FRAGMENT_FROM);
            ArrayList<CustomBoxResponse.DataBean.OPTIONALBean> transactionList = (ArrayList<CustomBoxResponse.DataBean.OPTIONALBean>) getArguments().getSerializable(ARRAYLIST);
            txtTitle.setText(title);

            childItemsModels.clear();
            for (CustomBoxResponse.DataBean.OPTIONALBean childProductsBean :transactionList){

                for (CustomBoxResponse.DataBean.OPTIONALBean.OptionsBean optionalBean : childProductsBean.getOptions()){

                    childItemsModels.add(new Product(childProductsBean.getId(),
                            childProductsBean.getPdtName(),childProductsBean.getImages(),childProductsBean.getAvailability(),
                            optionalBean.getOptId(),optionalBean.getPdtId(),optionalBean.getOptName(),Double.parseDouble(optionalBean.getPrice()),optionalBean.getIsInCart(),
                            optionalBean.getCartQty(),optionalBean.getQtyPrice()));
                }

            }

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
            childItemsRecycler.setLayoutManager(linearLayoutManager);
           /* adapter = new CustomBoxItemAdapter(childItemsModels,getActivity(),this);
            childItemsRecycler.setAdapter(adapter);
*/
        }




        return rootView;
    }

    @OnClick({R.id.closeImage, R.id.txtClear})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.closeImage:
                dismiss();
                break;
            case R.id.txtClear:
                dismiss();
                break;
        }
    }

    @Override
    public void onClickAtOKButton(int position) {
        dismiss();
    }
}
