package com.innasoft.gofarmz.Fragments;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.innasoft.gofarmz.Activity.CheckoutActivity;
import com.innasoft.gofarmz.Activity.HomeActivity;
import com.innasoft.gofarmz.Adapter.CartAdapter;
import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.Preferences.UserSessionManager;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.BaseResponse;
import com.innasoft.gofarmz.Response.CartDeleteResponse;
import com.innasoft.gofarmz.Response.CartIncreaseResponse;
import com.innasoft.gofarmz.Response.CartResponse;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Api.RetrofitClient.OUR_STORY;
import static com.innasoft.gofarmz.Utilis.Constants.BOTTAM_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.CART_COUNT;
import static com.innasoft.gofarmz.Utilis.Constants.CHECKOUT;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;
import static com.innasoft.gofarmz.Utilis.Constants.TYPE;


/**
 * A simple {@link Fragment} subclass.
 */
public class CartFragment extends Fragment implements InternetConnectivityListener {
    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;
    @BindView(R.id.recyclercart)
    RecyclerView recyclercart;
    @BindView(R.id.txtTotal)
    TextView txtTotal;
    @BindView(R.id.btnAddtocart)
    Button btnAddtocart;
    @BindView(R.id.bottamLayout)
    CardView bottamLayout;
    @BindView(R.id.txtRetry)
    TextView txtRetry;
    @BindView(R.id.no_internetLayout)
    LinearLayout noInternetLayout;
    @BindView(R.id.txtCartTotal)
    TextView txtCartTotal;
    @BindView(R.id.txtAddProduct)
    TextView txtAddProduct;
    @BindView(R.id.no_dataFound)
    LinearLayout noDataFound;
    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    View rootView;
    boolean internet;
    private UserSessionManager userSessionManager;
    String deviceId, user_id, token, productId;
    CartAdapter myCartAdapter;
    SharedPreferences preferences;
    int cartCount;

    public CartFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_cart, container, false);
        ButterKnife.bind(this, rootView);
        progressLayout.setVisibility(View.GONE);

        userSessionManager = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.KEY_DEVICEID);
        user_id = userDetails.get(UserSessionManager.KEY_ID);
        token = userDetails.get(UserSessionManager.KEY_TOKEN);

        preferences =getActivity().getSharedPreferences("CARTCOUNT",0);
        cartCount = preferences.getInt(CART_COUNT,0);

        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);

        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);

    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
            internet = isConnected;
            cartData();

            noInternetLayout.setVisibility(View.GONE);
        } else {
            internet = false;
            noInternetLayout.setVisibility(View.VISIBLE);
            // Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }

    private void cartData() {
        progressLayout.setVisibility(View.VISIBLE);
        Call<CartResponse> cartResponseCall = RetrofitClient.getInstance().getApi().CartData(token, user_id, deviceId);
        cartResponseCall.enqueue(new Callback<CartResponse>() {
            @Override
            public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {
                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);

                    Gson gson = new Gson();
                    CartResponse cartResponse = response.body();

                    if (cartResponse.getStatus().equals("10100")) {

                        CartResponse.DataBean dataBean = cartResponse.getData();
                        List<CartResponse.DataBean.RecordsBean> recordsBeanList = dataBean.getRecords();

                        myCartAdapter = new CartAdapter(recordsBeanList, getActivity(), CartFragment.this);
                        recyclercart.setHasFixedSize(true);
                        recyclercart.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                        recyclercart.setItemAnimator(new DefaultItemAnimator());
                        recyclercart.setAdapter(myCartAdapter);
                        myCartAdapter.notifyDataSetChanged();

                        Double totalvalue = (double) dataBean.getFinalTotal();
                        txtTotal.setText("\u20B9 " + String.format("%.2f", totalvalue));

                        bottamLayout.setVisibility(View.VISIBLE);

                    } else if (cartResponse.getStatus().equals("10300")) {

                        BaseResponse baseResponse = new BaseResponse();
                        bottamLayout.setVisibility(View.GONE);
                        recyclercart.setVisibility(View.GONE);

                        noDataFound.setVisibility(View.VISIBLE);
                        txtAddProduct.setOnClickListener(view -> {
                            Intent intent = new Intent(getActivity(), HomeActivity.class);
                            intent.putExtra(BOTTAM_TAB_POSITION, 0);
                            startActivity(intent);
                        });


                        // cart count update
                        if (getContext() instanceof HomeActivity) {
                            ((HomeActivity) getContext()).getCartCount();
                            Log.d(TAG, "onResponse: " + "cart");
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<CartResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    @OnClick(R.id.btnAddtocart)
    public void onViewClicked() {

        Intent intent = new Intent(getActivity(), CheckoutActivity.class);
        intent.putExtra(TITLE, CHECKOUT);
        intent.putExtra(TYPE, OUR_STORY);
        intent.putExtra(BOTTAM_TAB_POSITION, 2);
        startActivity(intent);
    }

    public void deleteCartItem(String cartId) {

        new AlertDialog.Builder(getActivity(), R.style.CustomDialogTheme)
                .setTitle("REMOVE ?")
                .setMessage("Are you sure want to Delete this Item.")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d("MainActivity", "Sending atomic bombs to Jupiter");

                        Call<CartDeleteResponse> cartDeleteResponseCall = RetrofitClient.getInstance().getApi().CartDelete(user_id, cartId, deviceId);
                        cartDeleteResponseCall.enqueue(new Callback<CartDeleteResponse>() {
                            @Override
                            public void onResponse(Call<CartDeleteResponse> call, Response<CartDeleteResponse> response) {
                                if (response.isSuccessful()) ;
                                CartDeleteResponse cartDeleteResponse = response.body();
                                if (cartDeleteResponse.getStatus().equals("10100")) {

                                    // refresh cart data
                                    cartData();

                                    // cart count update
                                    if (getContext() instanceof HomeActivity) {
                                        ((HomeActivity) getContext()).getCartCount();
                                    }

                                    Toast.makeText(getActivity(), cartDeleteResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                } else if (cartDeleteResponse.getStatus().equals("10200")) {

                                    Toast.makeText(getActivity(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                                }
//


                            }

                            @Override
                            public void onFailure(Call<CartDeleteResponse> call, Throwable t) {
                                Log.d(TAG, "onFailure: " + t.getMessage());
                            }
                        });

                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d("MainActivity", "Aborting mission...");
                        dialog.dismiss();
                    }
                })
                .show();


    }

    public void changeQuatity(final String id, final int qty) {

        Call<CartIncreaseResponse> cartIncreaseResponseCall = RetrofitClient.getInstance().getApi().CartIncrease(qty, id);
        cartIncreaseResponseCall.enqueue(new Callback<CartIncreaseResponse>() {
            @Override
            public void onResponse(Call<CartIncreaseResponse> call, Response<CartIncreaseResponse> response) {
                if (response.isSuccessful()) {
                    CartIncreaseResponse cartIncreaseResponse = response.body();
                    if (cartIncreaseResponse.getStatus().equals("10100")) {

                        cartData();
                    } else if (cartIncreaseResponse.getStatus().equals("10200")) {
                        Toast.makeText(getActivity(), cartIncreaseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<CartIncreaseResponse> call, Throwable t) {

            }
        });

    }

    private void saveCartCount(int cartCount){

        SharedPreferences preferences =getActivity().getSharedPreferences("CARTCOUNT",0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(CART_COUNT,cartCount);
        editor.apply();
    }

}
