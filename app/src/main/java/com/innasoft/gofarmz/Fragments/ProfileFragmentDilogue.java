package com.innasoft.gofarmz.Fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.Preferences.UserSessionManager;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.BaseResponse;
import com.innasoft.gofarmz.Utilis.Util;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Preferences.UserSessionManager.KEY_DEVICEID;
import static com.innasoft.gofarmz.Preferences.UserSessionManager.KEY_EMAIL;
import static com.innasoft.gofarmz.Preferences.UserSessionManager.KEY_GENDER;
import static com.innasoft.gofarmz.Preferences.UserSessionManager.KEY_ID;
import static com.innasoft.gofarmz.Preferences.UserSessionManager.KEY_MOBILE;
import static com.innasoft.gofarmz.Preferences.UserSessionManager.KEY_NAME;
import static com.innasoft.gofarmz.Preferences.UserSessionManager.KEY_TOKEN;

public class ProfileFragmentDilogue extends BottomSheetDialogFragment implements InternetConnectivityListener, RadioGroup.OnCheckedChangeListener {

    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    View rootView;
    UserSessionManager userSessionManager;
    private String deviceId, token, user_id, email, mobile, name, gender;
    boolean internet;

    @BindView(R.id.topLayout)
    CardView topLayout;
    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;
    @BindView(R.id.male)
    RadioButton male;
    @BindView(R.id.female)
    RadioButton female;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;
    @BindView(R.id.nameTill)
    TextInputLayout nameTill;
    @BindView(R.id.mobileTill)
    TextInputLayout mobileTill;
    @BindView(R.id.emailTill)
    TextInputLayout emailTill;
    @BindView(R.id.closeImage)
    ImageView closeImage;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.txtClear)
    TextView txtClear;
    @BindView(R.id.etName)
    TextInputEditText etName;
    @BindView(R.id.etMobile)
    TextInputEditText etMobile;
    @BindView(R.id.etEmail)
    TextInputEditText etEmail;
    @BindView(R.id.btnUpdate)
    Button btnUpdate;

    public ProfileFragmentDilogue() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_profile_dialog, container, false);
        ButterKnife.bind(this, rootView);
        progressLayout.setVisibility(View.GONE);

        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);

        userSessionManager = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();

        deviceId = userDetails.get(KEY_DEVICEID);
        token = userDetails.get(KEY_TOKEN);
        user_id = userDetails.get(KEY_ID);
        name = userDetails.get(KEY_NAME);
        email = userDetails.get(KEY_EMAIL);
        mobile = userDetails.get(KEY_MOBILE);
        gender = userDetails.get(KEY_GENDER);

        etName.setText(name);
        etEmail.setText(email);
        etMobile.setText(mobile);
        radioGroup.setOnCheckedChangeListener(this);

        if (gender != null) {
            if (gender.equalsIgnoreCase("Male")) {
                male.setChecked(true);
            } else if (gender.equalsIgnoreCase("Female")) {
                female.setChecked(true);
            }

        }

        etName.addTextChangedListener(new MyTextWatcher(etName));
        etMobile.addTextChangedListener(new MyTextWatcher(etMobile));
        etEmail.addTextChangedListener(new MyTextWatcher(etEmail));

        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);

    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
            internet = isConnected;

        } else {
            internet = false;
            Util.snackBar(coordinateLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
        }
    }


    @OnClick({R.id.closeImage, R.id.txtClear, R.id.btnUpdate})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.closeImage:
                dismiss();
                break;
            case R.id.txtClear:
                dismiss();
                break;
            case R.id.btnUpdate:
                Log.d(TAG, "onViewClicked: " + "clicked button");
                if (internet) {
                    updateProfile();

                } else {
                    internet = false;
                    Util.snackBar(coordinateLayout, getResources().getString(R.string.notconnected), Color.YELLOW);
                }
                break;
        }
    }


    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checked_id) {
        if (checked_id == R.id.male) {
            male.setChecked(true);
            gender = "Male";
        } else {
            female.setChecked(true);
            gender = "Female";
        }
        Log.d(TAG, "onCheckedChanged: "+checked_id);
    }


    private void updateProfile() {

        final String name = etName.getText().toString().trim();
        final String mobile = etMobile.getText().toString().trim();
        final String email = etEmail.getText().toString().trim();

        if ((!Util.isValidName(name, nameTill, etName, getActivity()))) {
            return;
        }
        if ((!Util.isValidPhoneNumber(mobile, mobileTill, etMobile, getActivity()))) {
            return;
        }
        if ((!Util.isValidEmail(email, emailTill, etEmail, getActivity()))) {
            return;
        }
        if (gender == null){
            Util.snackBar(coordinateLayout, "Please Select Gender", Color.YELLOW);
            return;
        }

        progressLayout.setVisibility(View.VISIBLE);
        Call<BaseResponse> call = RetrofitClient.getInstance().getApi().updateProfile(token, deviceId, user_id, etName.getText().toString(), etEmail.getText().toString(), etMobile.getText().toString(), gender);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);
                    BaseResponse baseResponse = response.body();

                    if (baseResponse.getStatus().equals("10100")) {

                        userSessionManager.createLogin(user_id, etName.getText().toString(), etEmail.getText().toString(),
                                etMobile.getText().toString(), null, deviceId, token, gender);

                        dismiss();
                        Toast.makeText(getActivity(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();


                    } else if (baseResponse.getStatus().equals("10200")) {

                        Toast.makeText(getActivity(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (baseResponse.getStatus().equals("10300")) {

                       // Toast.makeText(getActivity(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (baseResponse.getStatus().equals("10400")) {

                        Toast.makeText(getActivity(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }


    // text input layout class
    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.etName:
                    Util.isValidName(etName.getText().toString().trim(), nameTill, etName, getActivity());
                    break;
                case R.id.etMobile:
                    Util.isValidPhoneNumber(etMobile.getText().toString().trim(), mobileTill, etMobile, getActivity());
                    break;
                case R.id.etEmail:
                    Util.isValidEmail(etEmail.getText().toString().trim(), emailTill, etEmail, getActivity());
                    break;

            }
        }
    }

}