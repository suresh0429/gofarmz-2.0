package com.innasoft.gofarmz.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.innasoft.gofarmz.Adapter.BasketChildViewAdapter;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.CartResponse;
import com.innasoft.gofarmz.Response.MyOrderDetailsResponse;
import com.innasoft.gofarmz.models.ChildItemsModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.innasoft.gofarmz.Utilis.Constants.ARRAYLIST;
import static com.innasoft.gofarmz.Utilis.Constants.CART;
import static com.innasoft.gofarmz.Utilis.Constants.FRAGMENT_FROM;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;
import static com.innasoft.gofarmz.Utilis.Constants.TYPE;

public class ChildItemsBottomSheetFragment extends BottomSheetDialogFragment {
    View rootView;
    @BindView(R.id.childItemsRecycler)
    RecyclerView childItemsRecycler;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.closeImage)
    ImageView closeImage;
    @BindView(R.id.txtClear)
    TextView txtClear;

    ArrayList<ChildItemsModel> childItemsModels = new ArrayList<>();

    public ChildItemsBottomSheetFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_bottom_sheet_dialog, container, false);
        ButterKnife.bind(this, rootView);
        Bundle bundle = this.getArguments();
        if (bundle != null) {

            String title = bundle.getString(TITLE);
            String type = bundle.getString(TYPE);
            String fragmentType = bundle.getString(FRAGMENT_FROM);

            if (fragmentType.equalsIgnoreCase(CART)){

                if (type.equalsIgnoreCase("COMBO_CUSTOM")){

                    ArrayList<CartResponse.DataBean.RecordsBean.ADDONSBean> transactionList = (ArrayList<CartResponse.DataBean.RecordsBean.ADDONSBean>) getArguments().getSerializable(ARRAYLIST);
                    txtTitle.setText(title);

                    childItemsModels.clear();
                    for (CartResponse.DataBean.RecordsBean.ADDONSBean essentialBean :transactionList){
                        childItemsModels.add(new ChildItemsModel(essentialBean.getImages(),essentialBean.getPrice(),essentialBean.getQuantity(),essentialBean.getPdtName(),essentialBean.getUnitValue(),essentialBean.getUnitName()));
                    }
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
                    childItemsRecycler.setLayoutManager(linearLayoutManager);
                    BasketChildViewAdapter adapter = new BasketChildViewAdapter(childItemsModels, getActivity());
                    childItemsRecycler.setAdapter(adapter);

                }else {
                    ArrayList<CartResponse.DataBean.RecordsBean.ESSENTIALBean> transactionList = (ArrayList<CartResponse.DataBean.RecordsBean.ESSENTIALBean>) getArguments().getSerializable(ARRAYLIST);
                    txtTitle.setText(title);

                    childItemsModels.clear();
                    for (CartResponse.DataBean.RecordsBean.ESSENTIALBean essentialBean :transactionList){
                        childItemsModels.add(new ChildItemsModel(essentialBean.getImages(),essentialBean.getPrice(),null,essentialBean.getPdtName(),essentialBean.getUnitValue(),essentialBean.getUnitName()));
                    }
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
                    childItemsRecycler.setLayoutManager(linearLayoutManager);
                    BasketChildViewAdapter adapter = new BasketChildViewAdapter(childItemsModels, getActivity());
                    childItemsRecycler.setAdapter(adapter);
                }

            }else {

                ArrayList<MyOrderDetailsResponse.DataBean.ProductsBean.ChildProductsBean> transactionList = (ArrayList<MyOrderDetailsResponse.DataBean.ProductsBean.ChildProductsBean>) getArguments().getSerializable(ARRAYLIST);
                txtTitle.setText(title);

                childItemsModels.clear();
                for (MyOrderDetailsResponse.DataBean.ProductsBean.ChildProductsBean childProductsBean :transactionList){
                    childItemsModels.add(new ChildItemsModel(childProductsBean.getImages(),childProductsBean.getFinalPrice(),childProductsBean.getQuantity(),childProductsBean.getProductName(),childProductsBean.getUnitValue(),childProductsBean.getUnitName()));
                }

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
                childItemsRecycler.setLayoutManager(linearLayoutManager);
                BasketChildViewAdapter adapter = new BasketChildViewAdapter(childItemsModels, getActivity());
                childItemsRecycler.setAdapter(adapter);
            }


        }


        return rootView;
    }

    @OnClick({R.id.closeImage, R.id.txtClear})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.closeImage:
                dismiss();
                break;
            case R.id.txtClear:
                dismiss();
                break;
        }
    }
}
