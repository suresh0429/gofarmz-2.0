package com.innasoft.gofarmz.Fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.innasoft.gofarmz.Adapter.DeliverySlotAdapter;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.models.SlotItem;

import java.util.ArrayList;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.innasoft.gofarmz.Utilis.Constants.ARRAYLIST;
import static com.innasoft.gofarmz.Utilis.Constants.FRAGMENT_FROM;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;
import static com.innasoft.gofarmz.models.AccountItem.CHILD_TYPE;
import static com.innasoft.gofarmz.models.SlotItem.HEADER_TYPE;


/**
 * A simple {@link Fragment} subclass.
 */
public class DeliverySlotFragment extends BottomSheetDialogFragment implements DeliverySlotAdapter.ListAdapterListener {
    private DeliverySlotAdapter adapter;
    View rootView;
    @BindView(R.id.childItemsRecycler)
    RecyclerView childItemsRecycler;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.closeImage)
    ImageView closeImage;
    @BindView(R.id.txtClear)
    TextView txtClear;

    ArrayList<SlotItem> childItemsModels = new ArrayList<>();


    public DeliverySlotFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_delivery_slot, container, false);
        ButterKnife.bind(this, rootView);

        Bundle bundle = this.getArguments();
        if (bundle != null) {

            String title = bundle.getString(TITLE);
            String fragmentType = bundle.getString(FRAGMENT_FROM);
            ArrayList<Object> transactionList = (ArrayList<Object>) getArguments().getSerializable(ARRAYLIST);

            int i = 0;
            for (Object ob : transactionList){
                final Map<String, Object> mainArray = (Map<String, Object>) transactionList.get(i);
                final ArrayList<Object> slotArray = (ArrayList<Object>) mainArray.get("slots");

                childItemsModels.add(new SlotItem((String) mainArray.get("date"),"","","",HEADER_TYPE));

                int j = 0;
                for (Object slot : slotArray){
                    Map<String, Object> tmpObj = (Map<String, Object>) slotArray.get(j);
                    String id = (String) tmpObj.get("id");
                    String start_time = (String) tmpObj.get("start_time");
                    String end_time = (String) tmpObj.get("end_time");
                    childItemsModels.add(new SlotItem((String) mainArray.get("date"),start_time,end_time,id,CHILD_TYPE));
                    j++;
                }

               i++;
            }

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
            childItemsRecycler.setLayoutManager(linearLayoutManager);
            adapter = new DeliverySlotAdapter(childItemsModels, getActivity(),this);
            childItemsRecycler.setAdapter(adapter);

        }




        return rootView;
    }

    @OnClick({R.id.closeImage, R.id.txtClear})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.closeImage:
                dismiss();
                break;
            case R.id.txtClear:
                dismiss();
                break;
        }
    }

    @Override
    public void onClickAtOKButton(int position) {
        dismiss();
    }
}
