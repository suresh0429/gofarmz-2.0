package com.innasoft.gofarmz.Fragments;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.gofarmz.Activity.BulkHarvetsersActivity;
import com.innasoft.gofarmz.Activity.PreOrdersActivity;
import com.innasoft.gofarmz.Activity.VirtualShopsActivity;
import com.innasoft.gofarmz.Adapter.CatAdapter;
import com.innasoft.gofarmz.Adapter.FBHAdpter;
import com.innasoft.gofarmz.Adapter.SliderAdapter;
import com.innasoft.gofarmz.Api.RetrofitClient;
import com.innasoft.gofarmz.R;
import com.innasoft.gofarmz.Response.SliderTopBannerResponse;
import com.innasoft.gofarmz.models.CatItem;
import com.innasoft.gofarmz.models.FBhItem;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.IndicatorView.draw.controller.DrawController;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.gofarmz.Utilis.Constants.BOTTAM_TAB_POSITION;
import static com.innasoft.gofarmz.Utilis.Constants.COMMUNITY_SHOPS;
import static com.innasoft.gofarmz.Utilis.Constants.FARMER_BULK_HARVESTERS;
import static com.innasoft.gofarmz.Utilis.Constants.TITLE;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements InternetConnectivityListener {
    @BindView(R.id.txtRetry)
    TextView txtRetry;
    @BindView(R.id.no_internetLayout)
    LinearLayout noInternetLayout;
    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    @BindView(R.id.txtViewAllVirtual)
    TextView txtViewAllVirtual;
    @BindView(R.id.gvsRecycler)
    RecyclerView gvsRecycler;

    @BindView(R.id.txtViewAll)
    TextView txtViewAll;
    @BindView(R.id.fbhRecycler)
    RecyclerView fbhRecycler;
    @BindView(R.id.catRecycler)
    RecyclerView catRecycler;
    @BindView(R.id.slider_image)
    SliderView sliderImage;
    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;
    @BindView(R.id.nestedScroll)
    NestedScrollView nestedScroll;
    @BindView(R.id.preOrderLayout)
    RelativeLayout preOrderLayout;


    private List<CatItem> catItems = new ArrayList<>();
    private List<FBhItem> fBhItems = new ArrayList<>();
    private List<FBhItem> gvsItems = new ArrayList<>();
    View rootView;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, rootView);

        // check internet
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);


        // addBanners();
        catList();
        fbhList();
        gvsList();


        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);

    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.d(TAG, "onInternetConnectivityChanged: " + isConnected);
        if (isConnected) {
            addBanners();
            noInternetLayout.setVisibility(View.GONE);
        } else {
              noInternetLayout.setVisibility(View.VISIBLE);
             // Util.snackBar(progressLayout, getResources().getString(R.string.notconnected), Color.YELLOW);

        }
    }


    @SuppressLint("ShowToast")
    private void addBanners() {

        progressLayout.setVisibility(View.VISIBLE);
        nestedScroll.setAlpha(0.5f);

        Call<SliderTopBannerResponse> call = RetrofitClient.getInstance().getApi().TopBanners("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1Njg2MDgxMzMsImp0aSI6IklOSzROU0R4a1EwRVAxR3Z2TlhpWE5URnk5YTllZDJkdUowRHBGZVpVaG89IiwiaXNzIjoiaHR0cDpcL1wvaW5uYXNvZnQuaW5cL3BocC1qc29uXC8iLCJuYmYiOjE1Njg2MDgxMzQsImV4cCI6MTYwMDE0NDEzNCwiZGF0YSI6eyJ1c2VyX2lkIjoiMyJ9fQ.IBA2S4IhRpGHg7NRGOScv2aifzaEwlyo3ErAcNUWI_WjhzmpGYKFuiYSInvEgTFBMjPFD3TSM8GwMPjjoVF-2A");
        call.enqueue(new Callback<SliderTopBannerResponse>() {
            @Override
            public void onResponse(Call<SliderTopBannerResponse> call, Response<SliderTopBannerResponse> response) {
                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);
                    nestedScroll.setAlpha(1f);

                    SliderTopBannerResponse sliderTopBannerResponse = response.body();
                    if (sliderTopBannerResponse.getStatus().equals("10100")) {


                        List<SliderTopBannerResponse.DataBean> dataBeanList = sliderTopBannerResponse.getData();

                        final SliderAdapter adapter = new SliderAdapter(getActivity(), dataBeanList);
                        sliderImage.setSliderAdapter(adapter);

                        sliderImage.setIndicatorAnimation(IndicatorAnimations.THIN_WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
                        sliderImage.setSliderTransformAnimation(SliderAnimations.CUBEINROTATIONTRANSFORMATION);
                        sliderImage.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
                        sliderImage.setIndicatorSelectedColor(Color.WHITE);
                        sliderImage.setIndicatorUnselectedColor(Color.GRAY);
                        sliderImage.startAutoCycle();

                        sliderImage.setOnIndicatorClickListener(new DrawController.ClickListener() {
                            @Override
                            public void onIndicatorClicked(int position) {
                                sliderImage.setCurrentPagePosition(position);
                            }
                        });

                    } else if (sliderTopBannerResponse.getStatus().equals("10200")) {

                        Toast.makeText(getActivity(), sliderTopBannerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<SliderTopBannerResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                nestedScroll.setAlpha(1f);
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void catList() {

        GridLayoutManager manager = new GridLayoutManager(getActivity(), 3);
        catRecycler.setLayoutManager(manager);

        catRecycler.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        CatAdapter adapter = new CatAdapter(catItems, getActivity());
        catRecycler.setAdapter(adapter);

       /* manager.setOrientation(LinearLayoutManager.VERTICAL);
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (adapter != null) {
                    switch (adapter.getItemViewType(position)) {
                        case 1:
                            return 1;
                        case 0:
                            return 3; //number of columns of the grid
                        default:
                            return -1;
                    }
                } else {
                    return -1;
                }
            }
        });*/

        catItems.add(new CatItem("Vegetables","2", R.drawable.ic_vegetables));
        catItems.add(new CatItem("Fruits","1", R.drawable.ic_fruits));
        catItems.add(new CatItem("Millets","3", R.drawable.ic_millets));
        catItems.add(new CatItem("Grocery","", R.drawable.ic_grocerys));
        catItems.add(new CatItem("Dairy","", R.drawable.ic_dairy_products));
        catItems.add(new CatItem("Sustainable","", R.drawable.ic_sustainable));
       // catItems.add(new CatItem("Pre Orders","", R.drawable.ic_preorder));

        adapter.notifyDataSetChanged();

    }

    private void fbhList() {
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false);
        fbhRecycler.setLayoutManager(manager);
        FBHAdpter adapter = new FBHAdpter(fBhItems, getActivity());
        fbhRecycler.setAdapter(adapter);

        FBhItem fBhItem = new FBhItem("https://i0.wp.com/images-prod.healthline.com/hlcmsresource/images/AN_images/tomatoes-1296x728-feature.jpg?w=1155&h=1528", "Madhava Reddy");
        fBhItems.add(fBhItem);

        fBhItem = new FBhItem("https://cdn-prod.medicalnewstoday.com/content/images/articles/280/280579/potatoes-can-be-healthful.jpg", "Manmadha rao");
        fBhItems.add(fBhItem);

        fBhItem = new FBhItem("https://ichef.bbci.co.uk/wwfeatures/live/976_549/images/live/p0/7v/2w/p07v2wjn.jpg", "Mahesh");
        fBhItems.add(fBhItem);

        fBhItem = new FBhItem("https://img.etimg.com/thumb/width-640,height-480,imgsize-876498,resizemode-1,msid-67172581/despite-scheme-onions-plunge-to-rs-1-50-in-maharashtra.jpg", "Ramesh");
        fBhItems.add(fBhItem);

        fBhItem = new FBhItem("https://tiimg.tistatic.com/fp/1/004/859/fresh-green-chilli-mirchi--316.jpg", "Madhava Reddy");
        fBhItems.add(fBhItem);

    }

    private void gvsList() {
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false);
        gvsRecycler.setLayoutManager(manager);
        FBHAdpter adapter = new FBHAdpter(gvsItems, getActivity());
        gvsRecycler.setAdapter(adapter);

        FBhItem fBhItem = new FBhItem("https://www.ashianahousing.com/assets/images/homeall.jpg", "Manmadha shop");
        gvsItems.add(fBhItem);

        fBhItem = new FBhItem("https://assets-news.housing.com/news/wp-content/uploads/2018/02/24194719/The-pros-and-cons-of-gated-communities-and-standalone-buildings-FB-1200x628-compressed.jpg", "Madhava Shop");
        gvsItems.add(fBhItem);

        fBhItem = new FBhItem("https://www.myhomebhooja.com/img/banner2.png", "Mahesh shop");
        gvsItems.add(fBhItem);

        fBhItem = new FBhItem("https://c8.alamy.com/comp/MCX785/hyderabad-india-december-202016-children-at-play-area-inside-a-gated-community-with-highrise-buildings-in-hyderabadindia-MCX785.jpg", "Ramesh shop");
        gvsItems.add(fBhItem);

        fBhItem = new FBhItem("https://www.track2realty.track2media.com/wp-content/uploads/2016/03/Sobha-Group-launches-Sobha-Hartland-in-Dubai-high-rise-apartments.jpg", "Reddy shop");
        gvsItems.add(fBhItem);

    }


    @OnClick({R.id.txtViewAllVirtual, R.id.txtViewAll,R.id.preOrderLayout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtViewAllVirtual:
                Intent intent = new Intent(getContext(), VirtualShopsActivity.class);
                intent.putExtra(TITLE, COMMUNITY_SHOPS);
                intent.putExtra(BOTTAM_TAB_POSITION, 0);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            case R.id.txtViewAll:
                Intent intent1 = new Intent(getContext(), BulkHarvetsersActivity.class);
                intent1.putExtra(TITLE, FARMER_BULK_HARVESTERS);
                intent1.putExtra(BOTTAM_TAB_POSITION, 0);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent1);
                break;
            case R.id.preOrderLayout:
                Intent intent2 = new Intent(getContext(), PreOrdersActivity.class);
               // intent2.putExtra(TITLE, FARMER_BULK_HARVESTERS);
                intent2.putExtra(BOTTAM_TAB_POSITION, 0);
                intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent2);
                break;
        }
    }
}
