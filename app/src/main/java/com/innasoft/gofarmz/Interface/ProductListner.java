package com.innasoft.gofarmz.Interface;

import com.innasoft.gofarmz.Response.CustomBoxResponse;
import com.innasoft.gofarmz.models.Product;

public interface ProductListner {

    void OnItemClick(int position, Product product, CustomBoxResponse.DataBean.OPTIONALBean optionalBean);

    void onMinusClick(int position,Product product);

    void onPlusClick(int position,Product product);

    void onAddClick(int position,Product product);

}
