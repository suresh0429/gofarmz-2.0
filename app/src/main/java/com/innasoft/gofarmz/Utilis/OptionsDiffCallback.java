package com.innasoft.gofarmz.Utilis;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

import com.innasoft.gofarmz.models.Options;

import java.util.List;

public class OptionsDiffCallback extends DiffUtil.Callback {

    private final List<Options> mOldOptionsList;
    private final List<Options> mNewOptionsList;

    public OptionsDiffCallback(List<Options> oldOptionsList, List<Options> newOptionsList) {
        this.mOldOptionsList = oldOptionsList;
        this.mNewOptionsList = newOptionsList;
    }

    @Override
    public int getOldListSize() {
        return mOldOptionsList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewOptionsList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldOptionsList.get(oldItemPosition).getCartQty() == mNewOptionsList.get(
                newItemPosition).getCartQty();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        final Options oldOptions = mOldOptionsList.get(oldItemPosition);
        final Options newOptions = mNewOptionsList.get(newItemPosition);

        return oldOptions.getOptName().equals(newOptions.getOptName());
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        // Implement method if you're going to use ItemAnimator
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
}
