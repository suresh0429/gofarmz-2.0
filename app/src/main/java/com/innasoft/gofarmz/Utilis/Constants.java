package com.innasoft.gofarmz.Utilis;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Constants {

    public static final String MOBILE_KEY ="mobile_no";
    public static final String BOTTAM_TAB_POSITION ="position";
    public static final String ORDER_TAB_POSITION ="order_position";
    public static final String ORDER_STATUS ="status";
    public static final String ORDER_ID ="order_id";
    public static final String TITLE ="title";
    public static final String MODULE ="module";
    public static final String CATID ="catId";
    public static final String TYPE ="type";
    public static final String IMAGE ="image";
    public static final String PRODUCTID ="productId";
    public static final String ARRAYLIST ="array_list";
    public static final String ARRAYLIST1 ="array_list1";
    public static final String BUNDLE ="bundle";
    public static final String FRAGMENT_FROM ="fragment";
    public static final String CART ="cart";
    public static final String PRODUCT_DETAILS ="product_details";
    public static final String EDIT_ADDRESS ="Edit Address";
    public static final String LOCATIONNAME ="location_name";
    public static final String LOCATION_PREF ="locationPref";
    public static final String COMMUNITY_SHOPS ="Gofarmz Community Shops";
    public static final String FARMER_BULK_HARVESTERS ="Farmer Bulk harvests";
    public static final String CHECKOUT ="Checkout";


    // Cart
    public static final String CART_COUNT ="cartCount";
    public static final String PRE_ORDER_ID ="PreOrderId";
    public static final String COUNT ="COUNT";

    // Notification
    public static final String NOTIFY_COUNT ="notifyCount";

    //Checkout
    public static final String CHANGE_ADDRESS ="ChangeAddress";
    public static final String ADD_ADDRESS ="AddAddress";
    public static final String HOMEACTIVITY ="HomeActivity";
    public static final String PRE_ORDER_CART ="preOrderCart";
    public static final String ORDER_CART ="orderCart";


    // order stateData
    public static final String ORDER_TITLE ="Order Details";
    public static final String PRE_ORDER_TITLE ="PreOrder Details";

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String SHARED_PREF = "ah_firebase";


    public static final String PREFS_LOCATION = "LOCATION_PREF";

    public static final String KEY_LOCATIONFIRSTTIME="LocationFirstTime";
    public static final String KEY_PINCODE="pincode";
    public static final String KEY_AREA="area";
    public static final String KEY_SHIPPINGCHARGES="shipping_charges";



    public static String capitalize(String capString){
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()){
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();

    }


}
