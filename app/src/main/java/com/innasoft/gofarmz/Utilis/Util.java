package com.innasoft.gofarmz.Utilis;


import android.graphics.Color;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.innasoft.gofarmz.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {

    public static void snackBar(View view,String message, int color) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        /*snackbar.setAction("RETRY", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: "+"click");
            }
        });*/
        snackbar.setActionTextColor(Color.RED);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();
    }

    // validate name
    public static boolean isValidName(String name, TextInputLayout nameTill, TextInputEditText etName, FragmentActivity activity) {
        Pattern pattern = Pattern.compile("[a-zA-Z ]+");
        Matcher matcher = pattern.matcher(name);

        if (name.isEmpty()) {
            nameTill.setError("name is required");
            requestFocus(etName,activity);
            return false;
        } else if (!matcher.matches()) {
            nameTill.setError("Enter Alphabets Only");
            requestFocus(etName,activity);
            return false;
        }
        else if (name.length() < 5 || name.length() > 20) {
            nameTill.setError("Name Should be 5 to 20 characters");
            requestFocus(etName,activity);
            return false;
        } else {
            nameTill.setErrorEnabled(false);
        }
        return matcher.matches();
    }


    // validate phone
    public static boolean isValidPhoneNumber(String mobile, TextInputLayout mobileTill, TextInputEditText etMobile,FragmentActivity activity) {
        Pattern pattern = Pattern.compile("^[9876]\\d{9}$");
        Matcher matcher = pattern.matcher(mobile);

        if (mobile.isEmpty()) {
            mobileTill.setError("Phone no is required");
            requestFocus(etMobile, activity);

            return false;
        } else if (!matcher.matches()) {
            mobileTill.setError("Enter a valid mobile");
            requestFocus(etMobile, activity);
            return false;
        } else {
            mobileTill.setErrorEnabled(false);
        }

        return matcher.matches();
    }


    // validate your email address
    public static boolean isValidEmail(String email, TextInputLayout emailTill, TextInputEditText etEmail,FragmentActivity activity) {
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);

        if (email.isEmpty()) {
            emailTill.setError("Email is required");
            requestFocus(etEmail, activity);
            return false;
        } else if (!matcher.matches()) {
            emailTill.setError("Enter a valid email");
            requestFocus(etEmail, activity);
            return false;
        } else {
            emailTill.setErrorEnabled(false);
        }


        return matcher.matches();
    }


    // request focus
    public static void requestFocus(View view, FragmentActivity activity) {
        if (view.requestFocus()) {
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    public double ParseDouble(String strNumber) {
        if (strNumber != null && strNumber.length() > 0) {
            try {
                return Double.parseDouble(strNumber);
            } catch (Exception e) {
                return -1;   // or some value to mark this field is wrong. or make a function validates field first ...
            }
        } else return 0;
    }
}
