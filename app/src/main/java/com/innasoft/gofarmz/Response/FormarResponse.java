package com.innasoft.gofarmz.Response;

import java.util.List;

public class FormarResponse {


    /**
     * status : 10100
     * message : Data fetch successfully.
     * stateData : [{"id":"17","type":"Farmer","farm_name":"Shyambabu Farm","farmer_name":"Syam Babu","farm_photo":"images/farm_images/17/92a34-1.jpg"},{"id":"15","type":"Farmer","farm_name":"Venkateswarlu Farm","farmer_name":"Venkateswarlu","farm_photo":""},{"id":"13","type":"Farmer","farm_name":"Venkataiah Farms","farmer_name":"Venkataiah","farm_photo":"images/farm_images/13/b07eb-ba567f43-f669-476e-9cfc-d986738f87e6.jpg"},{"id":"2","type":"Farmer","farm_name":"Aishwarya Farms","farmer_name":"Krishna Kumar Nair","farm_photo":"images/farm_images/2/05572-whatsapp-image-2018-09-25-at-12.33.01-pm.jpeg"},{"id":"1","type":"Farmer","farm_name":"GoFarmz Exclusive","farmer_name":"L S Raju","farm_photo":"images/farm_images/1/bc3de-2.jpg"},{"id":"5","type":"Supplier","farm_name":"Sristi Naturals","farmer_name":"Satya Kodali","farm_photo":""}]
     */

    private String status;
    private String message;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 17
         * type : Farmer
         * farm_name : Shyambabu Farm
         * farmer_name : Syam Babu
         * farm_photo : images/farm_images/17/92a34-1.jpg
         */

        private String id;
        private String type;
        private String farm_name;
        private String farmer_name;
        private String farm_photo;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getFarm_name() {
            return farm_name;
        }

        public void setFarm_name(String farm_name) {
            this.farm_name = farm_name;
        }

        public String getFarmer_name() {
            return farmer_name;
        }

        public void setFarmer_name(String farmer_name) {
            this.farmer_name = farmer_name;
        }

        public String getFarm_photo() {
            return farm_photo;
        }

        public void setFarm_photo(String farm_photo) {
            this.farm_photo = farm_photo;
        }
    }
}
