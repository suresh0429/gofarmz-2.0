package com.innasoft.gofarmz.Response;

import java.util.List;

public class ViewDetailsResponse {

    /**
     * status : 10100
     * message : Data fetch successfully
     * stateData : {"currency":"Rs. ","records":[{"cart_product_addon_id":"2365","pdtId":"27","optId":"0","pdtName":"Ginger","images":"ginger.png","price":"104","quantity":"1","unitValue":"1","unitName":"Kg"}]}
     */

    private String status;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * currency : Rs.
         * records : [{"cart_product_addon_id":"2365","pdtId":"27","optId":"0","pdtName":"Ginger","images":"ginger.png","price":"104","quantity":"1","unitValue":"1","unitName":"Kg"}]
         */

        private String currency;
        private List<RecordsBean> records;

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public List<RecordsBean> getRecords() {
            return records;
        }

        public void setRecords(List<RecordsBean> records) {
            this.records = records;
        }

        public static class RecordsBean {
            /**
             * cart_product_addon_id : 2365
             * pdtId : 27
             * optId : 0
             * pdtName : Ginger
             * images : ginger.png
             * price : 104
             * quantity : 1
             * unitValue : 1
             * unitName : Kg
             */

            private String cart_product_addon_id;
            private String pdtId;
            private String optId;
            private String pdtName;
            private String images;
            private String price;
            private String quantity;
            private String unitValue;
            private String unitName;

            public String getCart_product_addon_id() {
                return cart_product_addon_id;
            }

            public void setCart_product_addon_id(String cart_product_addon_id) {
                this.cart_product_addon_id = cart_product_addon_id;
            }

            public String getPdtId() {
                return pdtId;
            }

            public void setPdtId(String pdtId) {
                this.pdtId = pdtId;
            }

            public String getOptId() {
                return optId;
            }

            public void setOptId(String optId) {
                this.optId = optId;
            }

            public String getPdtName() {
                return pdtName;
            }

            public void setPdtName(String pdtName) {
                this.pdtName = pdtName;
            }

            public String getImages() {
                return images;
            }

            public void setImages(String images) {
                this.images = images;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }

            public String getUnitValue() {
                return unitValue;
            }

            public void setUnitValue(String unitValue) {
                this.unitValue = unitValue;
            }

            public String getUnitName() {
                return unitName;
            }

            public void setUnitName(String unitName) {
                this.unitName = unitName;
            }
        }
    }
}
