package com.innasoft.gofarmz.Response;

import com.google.gson.annotations.SerializedName;

public class UpdatePreOrderCardDeleteResponse {


    /**
     * status : 10100
     * message : Deleted successfully.
     * cart_count : 1
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("cart_count")
    private int cartCount;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCartCount() {
        return cartCount;
    }

    public void setCartCount(int cartCount) {
        this.cartCount = cartCount;
    }
}
