package com.innasoft.gofarmz.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PreOrderCheckoutResponse {


    /**
     * status : 10100
     * message : Data fetch successfully.
     * stateData : {"mobile_verify_status":"1","mobile":"8019448098","address":[{"id":"388","user_id":"151","name":"Vijay Ravinootala","address_line1":"Srishti Tower","address_line2":" Hitech City Rd, VIP Hills, Silicon Valley, ","area":"Madhapur","city":"Hyderabad","state":"Telangana","country":"India","pincode":"500081","latitude":"17.4456","longitude":"78.3861","contact_no":"8019448098","alternate_contact_no":"","is_default":"Yes","created_on":"2019-04-03 12:30:45","updated_on":"2019-04-03 12:30:45","distance":11,"delivery_status":true,"delivery_charges":"50.00"}],"products":[{"product_id":"33","quantity":"2","pop_image":"http://gofarmzv2.learningslot.in/timthumb.php?src=http://gofarmzv2.learningslot.in/images/products/pre_order/1554184867294_McCann-Dodoni-03-440x320.jpg&h=160&w=220","pop_expected_delivery_date":"30-04-2019","pop_unit_price":"30.00","pop_unit_value":"1KG","pop_available_units":"2","product_name":"Beetroot","total_price":60,"grand_total":60},{"product_id":"65","quantity":"6","pop_image":"http://gofarmzv2.learningslot.in/timthumb.php?src=http://gofarmzv2.learningslot.in/images/products/pre_order/1554184948734_sunflower-wall-art-sunflower-wall-art-outdoor-metal.jpg&h=160&w=220","pop_expected_delivery_date":"26-04-2019","pop_unit_price":"40.00","pop_unit_value":"2KG","pop_available_units":"6","product_name":"Brinjal Purple Long","total_price":240,"grand_total":240}],"finalTotal":300,"payment_gateway":[{"id":"1","name":"Cash on Delivery","logo":"images/gateway/Cash-on-Delivery.jpg"}]}
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("stateData")
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * mobile_verify_status : 1
         * mobile : 8019448098
         * address : [{"id":"388","user_id":"151","name":"Vijay Ravinootala","address_line1":"Srishti Tower","address_line2":" Hitech City Rd, VIP Hills, Silicon Valley, ","area":"Madhapur","city":"Hyderabad","state":"Telangana","country":"India","pincode":"500081","latitude":"17.4456","longitude":"78.3861","contact_no":"8019448098","alternate_contact_no":"","is_default":"Yes","created_on":"2019-04-03 12:30:45","updated_on":"2019-04-03 12:30:45","distance":11,"delivery_status":true,"delivery_charges":"50.00"}]
         * products : [{"product_id":"33","quantity":"2","pop_image":"http://gofarmzv2.learningslot.in/timthumb.php?src=http://gofarmzv2.learningslot.in/images/products/pre_order/1554184867294_McCann-Dodoni-03-440x320.jpg&h=160&w=220","pop_expected_delivery_date":"30-04-2019","pop_unit_price":"30.00","pop_unit_value":"1KG","pop_available_units":"2","product_name":"Beetroot","total_price":60,"grand_total":60},{"product_id":"65","quantity":"6","pop_image":"http://gofarmzv2.learningslot.in/timthumb.php?src=http://gofarmzv2.learningslot.in/images/products/pre_order/1554184948734_sunflower-wall-art-sunflower-wall-art-outdoor-metal.jpg&h=160&w=220","pop_expected_delivery_date":"26-04-2019","pop_unit_price":"40.00","pop_unit_value":"2KG","pop_available_units":"6","product_name":"Brinjal Purple Long","total_price":240,"grand_total":240}]
         * finalTotal : 300
         * payment_gateway : [{"id":"1","name":"Cash on Delivery","logo":"images/gateway/Cash-on-Delivery.jpg"}]
         */

        @SerializedName("mobile_verify_status")
        private String mobileVerifyStatus;
        @SerializedName("mobile")
        private String mobile;
        @SerializedName("finalTotal")
        private int finalTotal;
        @SerializedName("address")
        private List<AddressBean> address;
        @SerializedName("products")
        private List<ProductsBean> products;
        @SerializedName("payment_gateway")
        private List<PaymentGatewayBean> paymentGateway;

        public String getMobileVerifyStatus() {
            return mobileVerifyStatus;
        }

        public void setMobileVerifyStatus(String mobileVerifyStatus) {
            this.mobileVerifyStatus = mobileVerifyStatus;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public int getFinalTotal() {
            return finalTotal;
        }

        public void setFinalTotal(int finalTotal) {
            this.finalTotal = finalTotal;
        }

        public List<AddressBean> getAddress() {
            return address;
        }

        public void setAddress(List<AddressBean> address) {
            this.address = address;
        }

        public List<ProductsBean> getProducts() {
            return products;
        }

        public void setProducts(List<ProductsBean> products) {
            this.products = products;
        }

        public List<PaymentGatewayBean> getPaymentGateway() {
            return paymentGateway;
        }

        public void setPaymentGateway(List<PaymentGatewayBean> paymentGateway) {
            this.paymentGateway = paymentGateway;
        }

        public static class AddressBean {
            /**
             * id : 388
             * user_id : 151
             * name : Vijay Ravinootala
             * address_line1 : Srishti Tower
             * address_line2 :  Hitech City Rd, VIP Hills, Silicon Valley,
             * area : Madhapur
             * city : Hyderabad
             * state : Telangana
             * country : India
             * pincode : 500081
             * latitude : 17.4456
             * longitude : 78.3861
             * contact_no : 8019448098
             * alternate_contact_no :
             * is_default : Yes
             * created_on : 2019-04-03 12:30:45
             * updated_on : 2019-04-03 12:30:45
             * distance : 11
             * delivery_status : true
             * delivery_charges : 50.00
             */

            @SerializedName("id")
            private String id;
            @SerializedName("user_id")
            private String userId;
            @SerializedName("name")
            private String name;
            @SerializedName("address_line1")
            private String addressLine1;
            @SerializedName("address_line2")
            private String addressLine2;
            @SerializedName("area")
            private String area;
            @SerializedName("city")
            private String city;
            @SerializedName("state")
            private String state;
            @SerializedName("country")
            private String country;
            @SerializedName("pincode")
            private String pincode;
            @SerializedName("latitude")
            private String latitude;
            @SerializedName("longitude")
            private String longitude;
            @SerializedName("contact_no")
            private String contactNo;
            @SerializedName("alternate_contact_no")
            private String alternateContactNo;
            @SerializedName("is_default")
            private String isDefault;
            @SerializedName("created_on")
            private String createdOn;
            @SerializedName("updated_on")
            private String updatedOn;
            @SerializedName("distance")
            private int distance;
            @SerializedName("delivery_status")
            private boolean deliveryStatus;
            @SerializedName("delivery_charges")
            private String deliveryCharges;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getUserId() {
                return userId;
            }

            public void setUserId(String userId) {
                this.userId = userId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getAddressLine1() {
                return addressLine1;
            }

            public void setAddressLine1(String addressLine1) {
                this.addressLine1 = addressLine1;
            }

            public String getAddressLine2() {
                return addressLine2;
            }

            public void setAddressLine2(String addressLine2) {
                this.addressLine2 = addressLine2;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getPincode() {
                return pincode;
            }

            public void setPincode(String pincode) {
                this.pincode = pincode;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public String getContactNo() {
                return contactNo;
            }

            public void setContactNo(String contactNo) {
                this.contactNo = contactNo;
            }

            public String getAlternateContactNo() {
                return alternateContactNo;
            }

            public void setAlternateContactNo(String alternateContactNo) {
                this.alternateContactNo = alternateContactNo;
            }

            public String getIsDefault() {
                return isDefault;
            }

            public void setIsDefault(String isDefault) {
                this.isDefault = isDefault;
            }

            public String getCreatedOn() {
                return createdOn;
            }

            public void setCreatedOn(String createdOn) {
                this.createdOn = createdOn;
            }

            public String getUpdatedOn() {
                return updatedOn;
            }

            public void setUpdatedOn(String updatedOn) {
                this.updatedOn = updatedOn;
            }

            public int getDistance() {
                return distance;
            }

            public void setDistance(int distance) {
                this.distance = distance;
            }

            public boolean isDeliveryStatus() {
                return deliveryStatus;
            }

            public void setDeliveryStatus(boolean deliveryStatus) {
                this.deliveryStatus = deliveryStatus;
            }

            public String getDeliveryCharges() {
                return deliveryCharges;
            }

            public void setDeliveryCharges(String deliveryCharges) {
                this.deliveryCharges = deliveryCharges;
            }
        }

        public static class ProductsBean {
            /**
             * product_id : 33
             * quantity : 2
             * pop_image : http://gofarmzv2.learningslot.in/timthumb.php?src=http://gofarmzv2.learningslot.in/images/products/pre_order/1554184867294_McCann-Dodoni-03-440x320.jpg&h=160&w=220
             * pop_expected_delivery_date : 30-04-2019
             * pop_unit_price : 30.00
             * pop_unit_value : 1KG
             * pop_available_units : 2
             * product_name : Beetroot
             * total_price : 60
             * grand_total : 60
             */

            @SerializedName("product_id")
            private String productId;
            @SerializedName("quantity")
            private String quantity;
            @SerializedName("pop_image")
            private String popImage;
            @SerializedName("pop_expected_delivery_date")
            private String popExpectedDeliveryDate;
            @SerializedName("pop_unit_price")
            private String popUnitPrice;
            @SerializedName("pop_unit_value")
            private String popUnitValue;
            @SerializedName("pop_available_units")
            private String popAvailableUnits;
            @SerializedName("product_name")
            private String productName;
            @SerializedName("total_price")
            private int totalPrice;
            @SerializedName("grand_total")
            private int grandTotal;

            public String getProductId() {
                return productId;
            }

            public void setProductId(String productId) {
                this.productId = productId;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }

            public String getPopImage() {
                return popImage;
            }

            public void setPopImage(String popImage) {
                this.popImage = popImage;
            }

            public String getPopExpectedDeliveryDate() {
                return popExpectedDeliveryDate;
            }

            public void setPopExpectedDeliveryDate(String popExpectedDeliveryDate) {
                this.popExpectedDeliveryDate = popExpectedDeliveryDate;
            }

            public String getPopUnitPrice() {
                return popUnitPrice;
            }

            public void setPopUnitPrice(String popUnitPrice) {
                this.popUnitPrice = popUnitPrice;
            }

            public String getPopUnitValue() {
                return popUnitValue;
            }

            public void setPopUnitValue(String popUnitValue) {
                this.popUnitValue = popUnitValue;
            }

            public String getPopAvailableUnits() {
                return popAvailableUnits;
            }

            public void setPopAvailableUnits(String popAvailableUnits) {
                this.popAvailableUnits = popAvailableUnits;
            }

            public String getProductName() {
                return productName;
            }

            public void setProductName(String productName) {
                this.productName = productName;
            }

            public int getTotalPrice() {
                return totalPrice;
            }

            public void setTotalPrice(int totalPrice) {
                this.totalPrice = totalPrice;
            }

            public int getGrandTotal() {
                return grandTotal;
            }

            public void setGrandTotal(int grandTotal) {
                this.grandTotal = grandTotal;
            }
        }

        public static class PaymentGatewayBean {
            /**
             * id : 1
             * name : Cash on Delivery
             * logo : images/gateway/Cash-on-Delivery.jpg
             */

            @SerializedName("id")
            private String id;
            @SerializedName("name")
            private String name;
            @SerializedName("logo")
            private String logo;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }
        }
    }
}
