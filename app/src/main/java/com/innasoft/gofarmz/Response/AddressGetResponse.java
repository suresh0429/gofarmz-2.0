package com.innasoft.gofarmz.Response;

public class AddressGetResponse {

    /**
     * status : 10100
     * message : Data fetch successfully.
     * stateData : {"id":"409","user_id":"400","name":"aaaa","address_line1":"aaaa","address_line2":"","area":"aaa","city":"aaa","state":"aaa","country":"India","pincode":"505302","latitude":"47.3374","longitude":"77.6647","contact_no":"155887222","alternate_contact_no":"887787887","is_default":"No","created_on":"2019-10-14 15:24:06","updated_on":"2019-10-14 15:26:15","distance":5340,"delivery_status":false,"delivery_charges":"0"}
     */

    private String status;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 409
         * user_id : 400
         * name : aaaa
         * address_line1 : aaaa
         * address_line2 :
         * area : aaa
         * city : aaa
         * state : aaa
         * country : India
         * pincode : 505302
         * latitude : 47.3374
         * longitude : 77.6647
         * contact_no : 155887222
         * alternate_contact_no : 887787887
         * is_default : No
         * created_on : 2019-10-14 15:24:06
         * updated_on : 2019-10-14 15:26:15
         * distance : 5340
         * delivery_status : false
         * delivery_charges : 0
         */

        private String id;
        private String user_id;
        private String name;
        private String address_line1;
        private String address_line2;
        private String area;
        private String city;
        private String state;
        private String country;
        private String pincode;
        private String latitude;
        private String longitude;
        private String contact_no;
        private String alternate_contact_no;
        private String is_default;
        private String created_on;
        private String updated_on;
        private int distance;
        private boolean delivery_status;
        private String delivery_charges;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress_line1() {
            return address_line1;
        }

        public void setAddress_line1(String address_line1) {
            this.address_line1 = address_line1;
        }

        public String getAddress_line2() {
            return address_line2;
        }

        public void setAddress_line2(String address_line2) {
            this.address_line2 = address_line2;
        }

        public String getArea() {
            return area;
        }

        public void setArea(String area) {
            this.area = area;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getContact_no() {
            return contact_no;
        }

        public void setContact_no(String contact_no) {
            this.contact_no = contact_no;
        }

        public String getAlternate_contact_no() {
            return alternate_contact_no;
        }

        public void setAlternate_contact_no(String alternate_contact_no) {
            this.alternate_contact_no = alternate_contact_no;
        }

        public String getIs_default() {
            return is_default;
        }

        public void setIs_default(String is_default) {
            this.is_default = is_default;
        }

        public String getCreated_on() {
            return created_on;
        }

        public void setCreated_on(String created_on) {
            this.created_on = created_on;
        }

        public String getUpdated_on() {
            return updated_on;
        }

        public void setUpdated_on(String updated_on) {
            this.updated_on = updated_on;
        }

        public int getDistance() {
            return distance;
        }

        public void setDistance(int distance) {
            this.distance = distance;
        }

        public boolean isDelivery_status() {
            return delivery_status;
        }

        public void setDelivery_status(boolean delivery_status) {
            this.delivery_status = delivery_status;
        }

        public String getDelivery_charges() {
            return delivery_charges;
        }

        public void setDelivery_charges(String delivery_charges) {
            this.delivery_charges = delivery_charges;
        }
    }
}
