package com.innasoft.gofarmz.Response;

import java.util.List;

public class PreOrderCheckoutDataResponse {


    /**
     * status : 10100
     * message : Data fetch successfully.
     * stateData : {"mobile_verify_status":"1","mobile":"9949905540","address":[{"id":"411","user_id":"400","name":"Jay","address_line1":"KnowTec","address_line2":"1-98/9/12c, M.N.R.J Residency, 3rd floor,Opp:FIITJEE School, Image Gardens Rd, ","area":"Madhapur","city":"Hyderabad","state":"Telangana","country":"India","pincode":"500081","latitude":"17.4494","longitude":"78.387","contact_no":"9949905540","alternate_contact_no":"","is_default":"Yes","created_on":"2019-10-14 16:26:53","updated_on":"2019-10-14 16:26:53","distance":10,"delivery_status":true,"delivery_charges":"0"}],"products":[{"product_id":"96","quantity":"1","pop_image":"http://gofarmzv2.learningslot.in/timthumb.php?src=http://gofarmzv2.learningslot.in/images/products/pre_order/1554816739944_2019-04-09.png&h=160&w=220","pop_expected_delivery_date":"24th Apr- 30th Apr","pop_unit_price":"1.00","pop_unit_value":"1 Litre","pop_available_units":"33","product_name":"Natural Cold Pressed Ground Nut Oil 1litre","total_price":1,"grand_total":1}],"finalTotal":1,"payment_gateway":[{"id":"1","name":"Cash on Delivery","logo":"images/gateway/Cash-on-Delivery.jpg"}]}
     */

    private String status;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * mobile_verify_status : 1
         * mobile : 9949905540
         * address : [{"id":"411","user_id":"400","name":"Jay","address_line1":"KnowTec","address_line2":"1-98/9/12c, M.N.R.J Residency, 3rd floor,Opp:FIITJEE School, Image Gardens Rd, ","area":"Madhapur","city":"Hyderabad","state":"Telangana","country":"India","pincode":"500081","latitude":"17.4494","longitude":"78.387","contact_no":"9949905540","alternate_contact_no":"","is_default":"Yes","created_on":"2019-10-14 16:26:53","updated_on":"2019-10-14 16:26:53","distance":10,"delivery_status":true,"delivery_charges":"0"}]
         * products : [{"product_id":"96","quantity":"1","pop_image":"http://gofarmzv2.learningslot.in/timthumb.php?src=http://gofarmzv2.learningslot.in/images/products/pre_order/1554816739944_2019-04-09.png&h=160&w=220","pop_expected_delivery_date":"24th Apr- 30th Apr","pop_unit_price":"1.00","pop_unit_value":"1 Litre","pop_available_units":"33","product_name":"Natural Cold Pressed Ground Nut Oil 1litre","total_price":1,"grand_total":1}]
         * finalTotal : 1
         * payment_gateway : [{"id":"1","name":"Cash on Delivery","logo":"images/gateway/Cash-on-Delivery.jpg"}]
         */

        private String mobile_verify_status;
        private String mobile;
        private int finalTotal;
        private Object address;
        private List<ProductsBean> products;
        private List<PaymentGatewayBean> payment_gateway;

        public String getMobile_verify_status() {
            return mobile_verify_status;
        }

        public void setMobile_verify_status(String mobile_verify_status) {
            this.mobile_verify_status = mobile_verify_status;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public int getFinalTotal() {
            return finalTotal;
        }

        public void setFinalTotal(int finalTotal) {
            this.finalTotal = finalTotal;
        }

        public Object getAddress() {
            return address;
        }

        public void setAddress(List<Object> address) {
            this.address = address;
        }

        public List<ProductsBean> getProducts() {
            return products;
        }

        public void setProducts(List<ProductsBean> products) {
            this.products = products;
        }

        public List<PaymentGatewayBean> getPayment_gateway() {
            return payment_gateway;
        }

        public void setPayment_gateway(List<PaymentGatewayBean> payment_gateway) {
            this.payment_gateway = payment_gateway;
        }

        public static class AddressBean {
            /**
             * id : 411
             * user_id : 400
             * name : Jay
             * address_line1 : KnowTec
             * address_line2 : 1-98/9/12c, M.N.R.J Residency, 3rd floor,Opp:FIITJEE School, Image Gardens Rd,
             * area : Madhapur
             * city : Hyderabad
             * state : Telangana
             * country : India
             * pincode : 500081
             * latitude : 17.4494
             * longitude : 78.387
             * contact_no : 9949905540
             * alternate_contact_no :
             * is_default : Yes
             * created_on : 2019-10-14 16:26:53
             * updated_on : 2019-10-14 16:26:53
             * distance : 10
             * delivery_status : true
             * delivery_charges : 0
             */

            private String id;
            private String user_id;
            private String name;
            private String address_line1;
            private String address_line2;
            private String area;
            private String city;
            private String state;
            private String country;
            private String pincode;
            private String latitude;
            private String longitude;
            private String contact_no;
            private String alternate_contact_no;
            private String is_default;
            private String created_on;
            private String updated_on;
            private int distance;
            private boolean delivery_status;
            private String delivery_charges;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getAddress_line1() {
                return address_line1;
            }

            public void setAddress_line1(String address_line1) {
                this.address_line1 = address_line1;
            }

            public String getAddress_line2() {
                return address_line2;
            }

            public void setAddress_line2(String address_line2) {
                this.address_line2 = address_line2;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getPincode() {
                return pincode;
            }

            public void setPincode(String pincode) {
                this.pincode = pincode;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public String getContact_no() {
                return contact_no;
            }

            public void setContact_no(String contact_no) {
                this.contact_no = contact_no;
            }

            public String getAlternate_contact_no() {
                return alternate_contact_no;
            }

            public void setAlternate_contact_no(String alternate_contact_no) {
                this.alternate_contact_no = alternate_contact_no;
            }

            public String getIs_default() {
                return is_default;
            }

            public void setIs_default(String is_default) {
                this.is_default = is_default;
            }

            public String getCreated_on() {
                return created_on;
            }

            public void setCreated_on(String created_on) {
                this.created_on = created_on;
            }

            public String getUpdated_on() {
                return updated_on;
            }

            public void setUpdated_on(String updated_on) {
                this.updated_on = updated_on;
            }

            public int getDistance() {
                return distance;
            }

            public void setDistance(int distance) {
                this.distance = distance;
            }

            public boolean isDelivery_status() {
                return delivery_status;
            }

            public void setDelivery_status(boolean delivery_status) {
                this.delivery_status = delivery_status;
            }

            public String getDelivery_charges() {
                return delivery_charges;
            }

            public void setDelivery_charges(String delivery_charges) {
                this.delivery_charges = delivery_charges;
            }
        }

        public static class ProductsBean {
            /**
             * product_id : 96
             * quantity : 1
             * pop_image : http://gofarmzv2.learningslot.in/timthumb.php?src=http://gofarmzv2.learningslot.in/images/products/pre_order/1554816739944_2019-04-09.png&h=160&w=220
             * pop_expected_delivery_date : 24th Apr- 30th Apr
             * pop_unit_price : 1.00
             * pop_unit_value : 1 Litre
             * pop_available_units : 33
             * product_name : Natural Cold Pressed Ground Nut Oil 1litre
             * total_price : 1
             * grand_total : 1
             */

            private String product_id;
            private String quantity;
            private String pop_image;
            private String pop_expected_delivery_date;
            private String pop_unit_price;
            private String pop_unit_value;
            private String pop_available_units;
            private String product_name;
            private int total_price;
            private int grand_total;

            public String getProduct_id() {
                return product_id;
            }

            public void setProduct_id(String product_id) {
                this.product_id = product_id;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }

            public String getPop_image() {
                return pop_image;
            }

            public void setPop_image(String pop_image) {
                this.pop_image = pop_image;
            }

            public String getPop_expected_delivery_date() {
                return pop_expected_delivery_date;
            }

            public void setPop_expected_delivery_date(String pop_expected_delivery_date) {
                this.pop_expected_delivery_date = pop_expected_delivery_date;
            }

            public String getPop_unit_price() {
                return pop_unit_price;
            }

            public void setPop_unit_price(String pop_unit_price) {
                this.pop_unit_price = pop_unit_price;
            }

            public String getPop_unit_value() {
                return pop_unit_value;
            }

            public void setPop_unit_value(String pop_unit_value) {
                this.pop_unit_value = pop_unit_value;
            }

            public String getPop_available_units() {
                return pop_available_units;
            }

            public void setPop_available_units(String pop_available_units) {
                this.pop_available_units = pop_available_units;
            }

            public String getProduct_name() {
                return product_name;
            }

            public void setProduct_name(String product_name) {
                this.product_name = product_name;
            }

            public int getTotal_price() {
                return total_price;
            }

            public void setTotal_price(int total_price) {
                this.total_price = total_price;
            }

            public int getGrand_total() {
                return grand_total;
            }

            public void setGrand_total(int grand_total) {
                this.grand_total = grand_total;
            }
        }

        public static class PaymentGatewayBean {
            /**
             * id : 1
             * name : Cash on Delivery
             * logo : images/gateway/Cash-on-Delivery.jpg
             */

            private String id;
            private String name;
            private String logo;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }
        }
    }
}
