package com.innasoft.gofarmz.Response;

public class BaseResponse {

    /**
     * status : 10100
     * message : Registration done successfully. OTP has been sent to your mobile/Email.
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
