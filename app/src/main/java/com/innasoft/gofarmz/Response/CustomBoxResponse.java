package com.innasoft.gofarmz.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustomBoxResponse {


    /**
     * status : 10100
     * content : Our Vegetable boxes come in three variants, Small, Medium and Large recommended according to your family size. You also have an option to customize the box as per your requirement.
     * versions : {"android_version":"1.9.24","ios_version":"1.4.4"}
     * message : Data fetch successfully
     * data : [{"id":"38","type":"COMBO_CUSTOM","pdtName":"Custom Veggie Box","unitValue":"3","price":"300","about":"Custom vegie Box","moreinfo":"","availability":"available","unitName":"Kg","images":"862860.png","isInCart":0,"cartQty":0,"OPTIONAL":[{"id":"21","pdtName":"Tomato","images":"tomato1.png","availability":"available","options":[{"optId":"0","pdtId":"21","optName":"1 Kg","price":"50","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"28","pdtId":"21","optName":"500 gms","price":"25","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"29","pdtId":"21","optName":"250 gms","price":"12","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"120","pdtId":"21","optName":"1.5 Kg","price":"75","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"141","pdtId":"21","optName":"2 Kg","price":"100","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"23","pdtName":"Onions","images":"onions.png","availability":"available","options":[{"optId":"0","pdtId":"23","optName":"1 Kg","price":"60","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"30","pdtId":"23","optName":"500 gms","price":"30","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"31","pdtId":"23","optName":"250 gms","price":"15","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"119","pdtId":"23","optName":"1.5 Kg","price":"90","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"124","pdtId":"23","optName":"2 Kg","price":"120","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"24","pdtName":"Green Chillies","images":"green_chillies.png","availability":"available","options":[{"optId":"0","pdtId":"24","optName":"1 Kg","price":"90","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"33","pdtId":"24","optName":"500 gms","price":"45","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"34","pdtId":"24","optName":"250 gms","price":"23","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"137","pdtId":"24","optName":"100 gms","price":"9","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"25","pdtName":"Potato","images":"potato1.png","availability":"available","options":[{"optId":"0","pdtId":"25","optName":"1 Kg","price":"52","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"35","pdtId":"25","optName":"500 gms","price":"30","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"36","pdtId":"25","optName":"250 gms","price":"15","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"143","pdtId":"25","optName":"2000 Gms","price":"120","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"26","pdtName":"Carrot","images":"carrot.png","availability":"available","options":[{"optId":"0","pdtId":"26","optName":"1 Kg","price":"104","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"20","pdtId":"26","optName":"500 gms","price":"52","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"37","pdtId":"26","optName":"250 gms","price":"26","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"27","pdtName":"Ginger","images":"ginger.png","availability":"available","options":[{"optId":"0","pdtId":"27","optName":"1 Kg","price":"200","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"38","pdtId":"27","optName":"500 gms","price":"100","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"39","pdtId":"27","optName":"250 gms","price":"50","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"132","pdtId":"27","optName":"200 Gms","price":"40","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"138","pdtId":"27","optName":"100 gms","price":"20","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"28","pdtName":"Garlic","images":"garlic.png","availability":"available","options":[{"optId":"0","pdtId":"28","optName":"1 Kg","price":"207","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"40","pdtId":"28","optName":"500 gms","price":"104","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"41","pdtId":"28","optName":"250 gms","price":"52","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"130","pdtId":"28","optName":"200 gms","price":"40","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"133","pdtId":"28","optName":"100 gms","price":"22","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"139","pdtId":"28","optName":"50 gms","price":"11","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"29","pdtName":"Lady Finger","images":"ladies_finger.png","availability":"available","options":[{"optId":"0","pdtId":"29","optName":"1 Kg","price":"68","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"51","pdtId":"29","optName":"500 gms","price":"34","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"52","pdtId":"29","optName":"250 gms","price":"17","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"30","pdtName":"Green Cucumber ","images":"cucumber.png","availability":"available","options":[{"optId":"0","pdtId":"30","optName":"1 Kg","price":"50","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"24","pdtId":"30","optName":"500 gms","price":"25","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"53","pdtId":"30","optName":"250 gms","price":"13","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"31","pdtName":"Cluster Beans","images":"cluster_beans.png","availability":"available","options":[{"optId":"0","pdtId":"31","optName":"1 Kg","price":"62","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"58","pdtId":"31","optName":"500 gms","price":"31","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"59","pdtId":"31","optName":"250 gms","price":"16","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"33","pdtName":"Beetroot","images":"beetroot.png","availability":"available","options":[{"optId":"0","pdtId":"33","optName":"1 Kg","price":"80","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"72","pdtId":"33","optName":"500 gms","price":"40","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"73","pdtId":"33","optName":"250 gms","price":"20","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"34","pdtName":"Curry Leaves","images":"curry_leaf.png","availability":"available","options":[{"optId":"0","pdtId":"34","optName":"1 Bunch","price":"10","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"44","pdtId":"34","optName":"5Bunches","price":"50","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"128","pdtId":"34","optName":"10 Bunches","price":"90","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"35","pdtName":"Coriander","images":"coriander.png","availability":"available","options":[{"optId":"0","pdtId":"35","optName":"1 Bunch","price":"10","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"45","pdtId":"35","optName":"10 Bunches","price":"100","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"46","pdtId":"35","optName":"5 Bunches","price":"50","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"36","pdtName":"Bottle Gourd","images":"bottle_gourd.png","availability":"available","options":[{"optId":"0","pdtId":"36","optName":"1 Pieces","price":"30","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"145","pdtId":"36","optName":"2 Pieces","price":"60","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"37","pdtName":"Little Gourd Donda","images":"little_gourd.png","availability":"available","options":[{"optId":"0","pdtId":"37","optName":"1 Kg","price":"52","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"68","pdtId":"37","optName":"500 gms","price":"26","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"69","pdtId":"37","optName":"250 gms","price":"13","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"121","pdtId":"37","optName":"750 gms","price":"39","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"39","pdtName":"Lemon","images":"lemon.png","availability":"available","options":[{"optId":"0","pdtId":"39","optName":"8 Pieces","price":"40","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"50","pdtId":"39","optName":"1 Piece","price":"5","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"140","pdtId":"39","optName":"2 Pieces","price":"10","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"142","pdtId":"39","optName":"4 Pieces","price":"20","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"49","pdtName":"Drum Sticks","images":"Drum_Sticks.jpg","availability":"available","options":[{"optId":"0","pdtId":"49","optName":"8 Pieces","price":"86","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"64","pdtId":"49","optName":"4 Pieces","price":"48","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"65","pdtId":"49","optName":"2 Pieces","price":"24","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"51","pdtName":"Raw Banana","images":"Raw_Banana.jpg","availability":"available","options":[{"optId":"0","pdtId":"51","optName":"4 Pieces","price":"64","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"70","pdtId":"51","optName":"3 Pieces","price":"48","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"71","pdtId":"51","optName":"2 Pieces","price":"32","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"152","pdtId":"51","optName":"1 Piece","price":"16","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"52","pdtName":"Bitter Gourd","images":"Bitter_Guard.jpg","availability":"available","options":[{"optId":"0","pdtId":"52","optName":"1 Kg","price":"72","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"74","pdtId":"52","optName":"500 gms","price":"36","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"75","pdtId":"52","optName":"250 gms","price":"18","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"56","pdtName":"Capsicum Green","images":"Capsicum_Green.jpg","availability":"available","options":[{"optId":"0","pdtId":"56","optName":"1 Kg","price":"96","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"82","pdtId":"56","optName":"500 gms","price":"48","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"58","pdtName":"Palakura(spinach)","images":"Spinach.jpg","availability":"available","options":[{"optId":"0","pdtId":"58","optName":"1 Bunch","price":"10","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"86","pdtId":"58","optName":"5Bunches","price":"50","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"87","pdtId":"58","optName":"10Bunches","price":"90","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"59","pdtName":" Thotakura(amaranthus)","images":"Thotakura(Amaranthus-min.png","availability":"available","options":[{"optId":"0","pdtId":"59","optName":"1 Bunch","price":"10","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"88","pdtId":"59","optName":"5Bunches","price":"50","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"89","pdtId":"59","optName":"10Bunches","price":"90","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"60","pdtName":"Chukkakura (sorrel Leaves)","images":"Chukkakura.jpg","availability":"available","options":[{"optId":"0","pdtId":"60","optName":"1 Bunch","price":"10","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"90","pdtId":"60","optName":"5Bunches","price":"50","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"91","pdtId":"60","optName":"10Bunches","price":"90","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"64","pdtName":"Brinjal Purple Round","images":"Brinjal_purple_Round.jpg","availability":"available","options":[{"optId":"0","pdtId":"64","optName":"1 Kg","price":"60","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"99","pdtId":"64","optName":"500 Gms","price":"30","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"100","pdtId":"64","optName":"250 Gms","price":"15","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"68","pdtName":"Cucumber Dosakaya","images":"Dosakaya.jpg","availability":"available","options":[{"optId":"0","pdtId":"68","optName":"1 Kg","price":"50","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"106","pdtId":"68","optName":"500 Gms","price":"25","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"107","pdtId":"68","optName":"250 Gms","price":"13","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"75","pdtName":"Ridge Gourd (birakaya)","images":"Ridge_gourd.jpg","availability":"available","options":[{"optId":"0","pdtId":"75","optName":"1 Kg","price":"64","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"117","pdtId":"75","optName":"500 gms","price":"32","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"118","pdtId":"75","optName":"250 gms","price":"16","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"136","pdtId":"75","optName":"1.5 Kg","price":"96","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"144","pdtId":"75","optName":"2 Kg","price":"128","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"77","pdtName":"Bachalakura(malabar Spinach)","images":"Bachalakura(Malabar_Spinach).png","availability":"available","options":[{"optId":"0","pdtId":"77","optName":"1 Bunch","price":"10","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"146","pdtId":"77","optName":"5Bunches","price":"50","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"147","pdtId":"77","optName":"10Bunches","price":"90","isInCart":0,"cartQty":0,"qtyPrice":0}]}]}]
     */

    @SerializedName("status")
    private String status;
    @SerializedName("content")
    private String content;
    @SerializedName("versions")
    private VersionsBean versions;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public VersionsBean getVersions() {
        return versions;
    }

    public void setVersions(VersionsBean versions) {
        this.versions = versions;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class VersionsBean {
        /**
         * android_version : 1.9.24
         * ios_version : 1.4.4
         */

        @SerializedName("android_version")
        private String androidVersion;
        @SerializedName("ios_version")
        private String iosVersion;

        public String getAndroidVersion() {
            return androidVersion;
        }

        public void setAndroidVersion(String androidVersion) {
            this.androidVersion = androidVersion;
        }

        public String getIosVersion() {
            return iosVersion;
        }

        public void setIosVersion(String iosVersion) {
            this.iosVersion = iosVersion;
        }
    }

    public static class DataBean {
        /**
         * id : 38
         * type : COMBO_CUSTOM
         * pdtName : Custom Veggie Box
         * unitValue : 3
         * price : 300
         * about : Custom vegie Box
         * moreinfo :
         * availability : available
         * unitName : Kg
         * images : 862860.png
         * isInCart : 0
         * cartQty : 0
         * OPTIONAL : [{"id":"21","pdtName":"Tomato","images":"tomato1.png","availability":"available","options":[{"optId":"0","pdtId":"21","optName":"1 Kg","price":"50","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"28","pdtId":"21","optName":"500 gms","price":"25","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"29","pdtId":"21","optName":"250 gms","price":"12","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"120","pdtId":"21","optName":"1.5 Kg","price":"75","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"141","pdtId":"21","optName":"2 Kg","price":"100","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"23","pdtName":"Onions","images":"onions.png","availability":"available","options":[{"optId":"0","pdtId":"23","optName":"1 Kg","price":"60","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"30","pdtId":"23","optName":"500 gms","price":"30","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"31","pdtId":"23","optName":"250 gms","price":"15","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"119","pdtId":"23","optName":"1.5 Kg","price":"90","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"124","pdtId":"23","optName":"2 Kg","price":"120","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"24","pdtName":"Green Chillies","images":"green_chillies.png","availability":"available","options":[{"optId":"0","pdtId":"24","optName":"1 Kg","price":"90","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"33","pdtId":"24","optName":"500 gms","price":"45","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"34","pdtId":"24","optName":"250 gms","price":"23","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"137","pdtId":"24","optName":"100 gms","price":"9","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"25","pdtName":"Potato","images":"potato1.png","availability":"available","options":[{"optId":"0","pdtId":"25","optName":"1 Kg","price":"52","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"35","pdtId":"25","optName":"500 gms","price":"30","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"36","pdtId":"25","optName":"250 gms","price":"15","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"143","pdtId":"25","optName":"2000 Gms","price":"120","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"26","pdtName":"Carrot","images":"carrot.png","availability":"available","options":[{"optId":"0","pdtId":"26","optName":"1 Kg","price":"104","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"20","pdtId":"26","optName":"500 gms","price":"52","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"37","pdtId":"26","optName":"250 gms","price":"26","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"27","pdtName":"Ginger","images":"ginger.png","availability":"available","options":[{"optId":"0","pdtId":"27","optName":"1 Kg","price":"200","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"38","pdtId":"27","optName":"500 gms","price":"100","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"39","pdtId":"27","optName":"250 gms","price":"50","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"132","pdtId":"27","optName":"200 Gms","price":"40","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"138","pdtId":"27","optName":"100 gms","price":"20","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"28","pdtName":"Garlic","images":"garlic.png","availability":"available","options":[{"optId":"0","pdtId":"28","optName":"1 Kg","price":"207","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"40","pdtId":"28","optName":"500 gms","price":"104","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"41","pdtId":"28","optName":"250 gms","price":"52","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"130","pdtId":"28","optName":"200 gms","price":"40","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"133","pdtId":"28","optName":"100 gms","price":"22","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"139","pdtId":"28","optName":"50 gms","price":"11","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"29","pdtName":"Lady Finger","images":"ladies_finger.png","availability":"available","options":[{"optId":"0","pdtId":"29","optName":"1 Kg","price":"68","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"51","pdtId":"29","optName":"500 gms","price":"34","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"52","pdtId":"29","optName":"250 gms","price":"17","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"30","pdtName":"Green Cucumber ","images":"cucumber.png","availability":"available","options":[{"optId":"0","pdtId":"30","optName":"1 Kg","price":"50","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"24","pdtId":"30","optName":"500 gms","price":"25","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"53","pdtId":"30","optName":"250 gms","price":"13","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"31","pdtName":"Cluster Beans","images":"cluster_beans.png","availability":"available","options":[{"optId":"0","pdtId":"31","optName":"1 Kg","price":"62","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"58","pdtId":"31","optName":"500 gms","price":"31","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"59","pdtId":"31","optName":"250 gms","price":"16","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"33","pdtName":"Beetroot","images":"beetroot.png","availability":"available","options":[{"optId":"0","pdtId":"33","optName":"1 Kg","price":"80","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"72","pdtId":"33","optName":"500 gms","price":"40","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"73","pdtId":"33","optName":"250 gms","price":"20","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"34","pdtName":"Curry Leaves","images":"curry_leaf.png","availability":"available","options":[{"optId":"0","pdtId":"34","optName":"1 Bunch","price":"10","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"44","pdtId":"34","optName":"5Bunches","price":"50","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"128","pdtId":"34","optName":"10 Bunches","price":"90","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"35","pdtName":"Coriander","images":"coriander.png","availability":"available","options":[{"optId":"0","pdtId":"35","optName":"1 Bunch","price":"10","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"45","pdtId":"35","optName":"10 Bunches","price":"100","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"46","pdtId":"35","optName":"5 Bunches","price":"50","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"36","pdtName":"Bottle Gourd","images":"bottle_gourd.png","availability":"available","options":[{"optId":"0","pdtId":"36","optName":"1 Pieces","price":"30","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"145","pdtId":"36","optName":"2 Pieces","price":"60","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"37","pdtName":"Little Gourd Donda","images":"little_gourd.png","availability":"available","options":[{"optId":"0","pdtId":"37","optName":"1 Kg","price":"52","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"68","pdtId":"37","optName":"500 gms","price":"26","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"69","pdtId":"37","optName":"250 gms","price":"13","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"121","pdtId":"37","optName":"750 gms","price":"39","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"39","pdtName":"Lemon","images":"lemon.png","availability":"available","options":[{"optId":"0","pdtId":"39","optName":"8 Pieces","price":"40","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"50","pdtId":"39","optName":"1 Piece","price":"5","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"140","pdtId":"39","optName":"2 Pieces","price":"10","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"142","pdtId":"39","optName":"4 Pieces","price":"20","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"49","pdtName":"Drum Sticks","images":"Drum_Sticks.jpg","availability":"available","options":[{"optId":"0","pdtId":"49","optName":"8 Pieces","price":"86","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"64","pdtId":"49","optName":"4 Pieces","price":"48","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"65","pdtId":"49","optName":"2 Pieces","price":"24","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"51","pdtName":"Raw Banana","images":"Raw_Banana.jpg","availability":"available","options":[{"optId":"0","pdtId":"51","optName":"4 Pieces","price":"64","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"70","pdtId":"51","optName":"3 Pieces","price":"48","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"71","pdtId":"51","optName":"2 Pieces","price":"32","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"152","pdtId":"51","optName":"1 Piece","price":"16","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"52","pdtName":"Bitter Gourd","images":"Bitter_Guard.jpg","availability":"available","options":[{"optId":"0","pdtId":"52","optName":"1 Kg","price":"72","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"74","pdtId":"52","optName":"500 gms","price":"36","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"75","pdtId":"52","optName":"250 gms","price":"18","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"56","pdtName":"Capsicum Green","images":"Capsicum_Green.jpg","availability":"available","options":[{"optId":"0","pdtId":"56","optName":"1 Kg","price":"96","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"82","pdtId":"56","optName":"500 gms","price":"48","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"58","pdtName":"Palakura(spinach)","images":"Spinach.jpg","availability":"available","options":[{"optId":"0","pdtId":"58","optName":"1 Bunch","price":"10","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"86","pdtId":"58","optName":"5Bunches","price":"50","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"87","pdtId":"58","optName":"10Bunches","price":"90","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"59","pdtName":" Thotakura(amaranthus)","images":"Thotakura(Amaranthus-min.png","availability":"available","options":[{"optId":"0","pdtId":"59","optName":"1 Bunch","price":"10","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"88","pdtId":"59","optName":"5Bunches","price":"50","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"89","pdtId":"59","optName":"10Bunches","price":"90","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"60","pdtName":"Chukkakura (sorrel Leaves)","images":"Chukkakura.jpg","availability":"available","options":[{"optId":"0","pdtId":"60","optName":"1 Bunch","price":"10","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"90","pdtId":"60","optName":"5Bunches","price":"50","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"91","pdtId":"60","optName":"10Bunches","price":"90","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"64","pdtName":"Brinjal Purple Round","images":"Brinjal_purple_Round.jpg","availability":"available","options":[{"optId":"0","pdtId":"64","optName":"1 Kg","price":"60","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"99","pdtId":"64","optName":"500 Gms","price":"30","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"100","pdtId":"64","optName":"250 Gms","price":"15","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"68","pdtName":"Cucumber Dosakaya","images":"Dosakaya.jpg","availability":"available","options":[{"optId":"0","pdtId":"68","optName":"1 Kg","price":"50","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"106","pdtId":"68","optName":"500 Gms","price":"25","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"107","pdtId":"68","optName":"250 Gms","price":"13","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"75","pdtName":"Ridge Gourd (birakaya)","images":"Ridge_gourd.jpg","availability":"available","options":[{"optId":"0","pdtId":"75","optName":"1 Kg","price":"64","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"117","pdtId":"75","optName":"500 gms","price":"32","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"118","pdtId":"75","optName":"250 gms","price":"16","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"136","pdtId":"75","optName":"1.5 Kg","price":"96","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"144","pdtId":"75","optName":"2 Kg","price":"128","isInCart":0,"cartQty":0,"qtyPrice":0}]},{"id":"77","pdtName":"Bachalakura(malabar Spinach)","images":"Bachalakura(Malabar_Spinach).png","availability":"available","options":[{"optId":"0","pdtId":"77","optName":"1 Bunch","price":"10","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"146","pdtId":"77","optName":"5Bunches","price":"50","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"147","pdtId":"77","optName":"10Bunches","price":"90","isInCart":0,"cartQty":0,"qtyPrice":0}]}]
         */

        @SerializedName("id")
        private String id;
        @SerializedName("type")
        private String type;
        @SerializedName("pdtName")
        private String pdtName;
        @SerializedName("unitValue")
        private String unitValue;
        @SerializedName("price")
        private String price;
        @SerializedName("about")
        private String about;
        @SerializedName("moreinfo")
        private String moreinfo;
        @SerializedName("availability")
        private String availability;
        @SerializedName("unitName")
        private String unitName;
        @SerializedName("images")
        private String images;
        @SerializedName("isInCart")
        private int isInCart;
        @SerializedName("cartQty")
        private int cartQty;
        @SerializedName("OPTIONAL")
        private List<OPTIONALBean> OPTIONAL;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getPdtName() {
            return pdtName;
        }

        public void setPdtName(String pdtName) {
            this.pdtName = pdtName;
        }

        public String getUnitValue() {
            return unitValue;
        }

        public void setUnitValue(String unitValue) {
            this.unitValue = unitValue;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getAbout() {
            return about;
        }

        public void setAbout(String about) {
            this.about = about;
        }

        public String getMoreinfo() {
            return moreinfo;
        }

        public void setMoreinfo(String moreinfo) {
            this.moreinfo = moreinfo;
        }

        public String getAvailability() {
            return availability;
        }

        public void setAvailability(String availability) {
            this.availability = availability;
        }

        public String getUnitName() {
            return unitName;
        }

        public void setUnitName(String unitName) {
            this.unitName = unitName;
        }

        public String getImages() {
            return images;
        }

        public void setImages(String images) {
            this.images = images;
        }

        public int getIsInCart() {
            return isInCart;
        }

        public void setIsInCart(int isInCart) {
            this.isInCart = isInCart;
        }

        public int getCartQty() {
            return cartQty;
        }

        public void setCartQty(int cartQty) {
            this.cartQty = cartQty;
        }

        public List<OPTIONALBean> getOPTIONAL() {
            return OPTIONAL;
        }

        public void setOPTIONAL(List<OPTIONALBean> OPTIONAL) {
            this.OPTIONAL = OPTIONAL;
        }

        public static class OPTIONALBean {
            /**
             * id : 21
             * pdtName : Tomato
             * images : tomato1.png
             * availability : available
             * options : [{"optId":"0","pdtId":"21","optName":"1 Kg","price":"50","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"28","pdtId":"21","optName":"500 gms","price":"25","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"29","pdtId":"21","optName":"250 gms","price":"12","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"120","pdtId":"21","optName":"1.5 Kg","price":"75","isInCart":0,"cartQty":0,"qtyPrice":0},{"optId":"141","pdtId":"21","optName":"2 Kg","price":"100","isInCart":0,"cartQty":0,"qtyPrice":0}]
             */

            @SerializedName("id")
            private String id;
            @SerializedName("pdtName")
            private String pdtName;
            @SerializedName("images")
            private String images;
            @SerializedName("availability")
            private String availability;
            @SerializedName("options")
            private List<OptionsBean> options;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getPdtName() {
                return pdtName;
            }

            public void setPdtName(String pdtName) {
                this.pdtName = pdtName;
            }

            public String getImages() {
                return images;
            }

            public void setImages(String images) {
                this.images = images;
            }

            public String getAvailability() {
                return availability;
            }

            public void setAvailability(String availability) {
                this.availability = availability;
            }

            public List<OptionsBean> getOptions() {
                return options;
            }

            public void setOptions(List<OptionsBean> options) {
                this.options = options;
            }

            public static class OptionsBean {
                /**
                 * optId : 0
                 * pdtId : 21
                 * optName : 1 Kg
                 * price : 50
                 * isInCart : 0
                 * cartQty : 0
                 * qtyPrice : 0
                 */

                @SerializedName("optId")
                private String optId;
                @SerializedName("pdtId")
                private String pdtId;
                @SerializedName("optName")
                private String optName;
                @SerializedName("price")
                private String price;
                @SerializedName("isInCart")
                private int isInCart;
                @SerializedName("cartQty")
                private int cartQty;
                @SerializedName("qtyPrice")
                private int qtyPrice;

                public String getOptId() {
                    return optId;
                }

                public void setOptId(String optId) {
                    this.optId = optId;
                }

                public String getPdtId() {
                    return pdtId;
                }

                public void setPdtId(String pdtId) {
                    this.pdtId = pdtId;
                }

                public String getOptName() {
                    return optName;
                }

                public void setOptName(String optName) {
                    this.optName = optName;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }

                public int getIsInCart() {
                    return isInCart;
                }

                public void setIsInCart(int isInCart) {
                    this.isInCart = isInCart;
                }

                public int getCartQty() {
                    return cartQty;
                }

                public void setCartQty(int cartQty) {
                    this.cartQty = cartQty;
                }

                public int getQtyPrice() {
                    return qtyPrice;
                }

                public void setQtyPrice(int qtyPrice) {
                    this.qtyPrice = qtyPrice;
                }
            }
        }
    }
}
