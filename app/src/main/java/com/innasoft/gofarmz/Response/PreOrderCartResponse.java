package com.innasoft.gofarmz.Response;

import java.util.List;

public class PreOrderCartResponse {


    /**
     * status : 10100
     * message : Cart products
     * stateData : [{"product_id":"96","quantity":"2","pop_image":"http://gofarmzv2.learningslot.in/timthumb.php?src=http://gofarmzv2.learningslot.in/images/products/pre_order/1554816739944_2019-04-09.png&h=160&w=220","pop_expected_delivery_date":"24th Apr- 30th Apr","pop_unit_price":"1.00","pop_unit_value":"1 Litre","pop_available_units":"33","product_name":"Natural Cold Pressed Ground Nut Oil 1litre","total_price":"2"}]
     * total : 2
     */

    private String status;
    private String message;
    private int total;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * product_id : 96
         * quantity : 2
         * pop_image : http://gofarmzv2.learningslot.in/timthumb.php?src=http://gofarmzv2.learningslot.in/images/products/pre_order/1554816739944_2019-04-09.png&h=160&w=220
         * pop_expected_delivery_date : 24th Apr- 30th Apr
         * pop_unit_price : 1.00
         * pop_unit_value : 1 Litre
         * pop_available_units : 33
         * product_name : Natural Cold Pressed Ground Nut Oil 1litre
         * total_price : 2
         */

        private String product_id;
        private String quantity;
        private String pop_image;
        private String pop_expected_delivery_date;
        private String pop_unit_price;
        private String pop_unit_value;
        private String pop_available_units;
        private String product_name;
        private String total_price;

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getPop_image() {
            return pop_image;
        }

        public void setPop_image(String pop_image) {
            this.pop_image = pop_image;
        }

        public String getPop_expected_delivery_date() {
            return pop_expected_delivery_date;
        }

        public void setPop_expected_delivery_date(String pop_expected_delivery_date) {
            this.pop_expected_delivery_date = pop_expected_delivery_date;
        }

        public String getPop_unit_price() {
            return pop_unit_price;
        }

        public void setPop_unit_price(String pop_unit_price) {
            this.pop_unit_price = pop_unit_price;
        }

        public String getPop_unit_value() {
            return pop_unit_value;
        }

        public void setPop_unit_value(String pop_unit_value) {
            this.pop_unit_value = pop_unit_value;
        }

        public String getPop_available_units() {
            return pop_available_units;
        }

        public void setPop_available_units(String pop_available_units) {
            this.pop_available_units = pop_available_units;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getTotal_price() {
            return total_price;
        }

        public void setTotal_price(String total_price) {
            this.total_price = total_price;
        }
    }
}
