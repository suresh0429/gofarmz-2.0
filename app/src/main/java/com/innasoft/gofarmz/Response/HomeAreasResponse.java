package com.innasoft.gofarmz.Response;

import java.util.List;

public class HomeAreasResponse {


    /**
     * status : 10100
     * message : Data fetch successfully.
     * stateData : [{"pincode":"500038","area":"Ameerpet"},{"pincode":"500054","area":"Balanagar"},{"pincode":"500034","area":"BanjaraHills"},{"pincode":"502032","area":"Beeramguda"},{"pincode":"500016","area":"Begumpet"},{"pincode":"500050","area":"Chandanagar"},{"pincode":"500096","area":"Film nagar"},{"pincode":"500032","area":"Gachibowli"},{"pincode":"500020","area":"Gandhi Nagar"},{"pincode":"500029","area":"Himayatnagar"},{"pincode":"500085","area":"Jntu Kukatpally"},{"pincode":"500033","area":"Jublee Hills"},{"pincode":"500004","area":"Khairatabad"},{"pincode":"500084","area":"Kondapur"},{"pincode":"500072","area":"Kukatpally"},{"pincode":"500019","area":"Lingampally"},{"pincode":"500081","area":"Madhapur"},{"pincode":"500089","area":"Manikonda"},{"pincode":"500057","area":"Masabtank"},{"pincode":"500028","area":"Mehdipatnam"},{"pincode":"500049","area":"Miyapur"},{"pincode":"500008","area":"Nanakramguda"},{"pincode":"500033","area":"Nandagiri Hills "},{"pincode":"500011","area":"New Bowenpally"},{"pincode":"500090","area":"Nizampet"},{"pincode":"500025","area":"Padmarao Nagar "},{"pincode":"500082","area":"Panjagutta"},{"pincode":"500071","area":"Rail Nilayam"},{"pincode":"500045","area":"Rajeev Nagar, Shaikpet"},{"pincode":"500018","area":"Sanathnagar"},{"pincode":"500073","area":"Srinagar Colony"},{"pincode":"500007","area":"Tarnaka"},{"pincode":"500015","area":"Thirumalagiri"},{"pincode":"500061","area":"Warasiguda"},{"pincode":"500026","area":"West Marredpally"}]
     */

    private String status;
    private String message;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * pincode : 500038
         * area : Ameerpet
         */

        private String pincode;
        private String area;

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getArea() {
            return area;
        }

        public void setArea(String area) {
            this.area = area;
        }
    }
}
