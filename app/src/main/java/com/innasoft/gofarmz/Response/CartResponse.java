package com.innasoft.gofarmz.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CartResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : {"currency":"Rs. ","records":[{"id":"16284","product_name":"Custom Veggie Box","total_price":372,"grand_total":372,"product_id":"38","images":"862860.png","mrp_price":"300","type":"COMBO_CUSTOM","purchase_quantity":"1","unit_name":"Kg","ADDONS":[{"cart_product_addon_id":"102976","pdtId":"21","optId":"29","pdtName":"Tomato: 250 gms","images":"tomato1.png","price":"12","quantity":"1","unitValue":"250","unitName":"gms"},{"cart_product_addon_id":"102977","pdtId":"23","optId":"124","pdtName":"Onions: 2 Kg","images":"onions.png","price":"360","quantity":"3","unitValue":"2","unitName":"Kg"}],"discount":0},{"id":"16277","product_name":"Small Basket","total_price":300,"grand_total":300,"product_id":"20","images":"vegetable_box.png","mrp_price":"300","type":"COMBO","purchase_quantity":"1","unit_name":"Kg","ESSENTIAL":[{"pdtId":"21","optId":"0","pdtName":"Tomato","images":"tomato1.png","unitValue":"1","unitName":"Kg","price":"50"},{"pdtId":"23","optId":"0","pdtName":"Onions","images":"onions.png","unitValue":"1","unitName":"Kg","price":"60"},{"pdtId":"24","optId":"137","pdtName":"Green Chillies: 100 gms","images":"green_chillies.png","unitValue":"100","unitName":"gms","price":"9"},{"pdtId":"25","optId":"35","pdtName":"Potato: 500 gms","images":"potato1.png","unitValue":"500","unitName":"gms","price":"30"},{"pdtId":"26","optId":"37","pdtName":"Carrot: 250 gms","images":"carrot.png","unitValue":"250","unitName":"gms","price":"26"},{"pdtId":"27","optId":"138","pdtName":"Ginger: 100 gms","images":"ginger.png","unitValue":"100","unitName":"gms","price":"20"},{"pdtId":"28","optId":"139","pdtName":"Garlic: 50 gms","images":"garlic.png","unitValue":"50","unitName":"gms","price":"11"},{"pdtId":"29","optId":"51","pdtName":"Lady Finger: 500 gms","images":"ladies_finger.png","unitValue":"500","unitName":"gms","price":"34"},{"pdtId":"31","optId":"58","pdtName":"Cluster Beans: 500 gms","images":"cluster_beans.png","unitValue":"500","unitName":"gms","price":"31"},{"pdtId":"34","optId":"0","pdtName":"Curry Leaves","images":"curry_leaf.png","unitValue":"1","unitName":"Bunch","price":"10"},{"pdtId":"36","optId":"0","pdtName":"Bottle Gourd","images":"bottle_gourd.png","unitValue":"1","unitName":"Pieces","price":"30"},{"pdtId":"75","optId":"117","pdtName":"Ridge Gourd (birakaya): 500 gms","images":"Ridge_gourd.jpg","unitValue":"500","unitName":"gms","price":"32"}],"discount":0}],"finalTotal":672,"finalDiscount":0}
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * currency : Rs.
         * records : [{"id":"16284","product_name":"Custom Veggie Box","total_price":372,"grand_total":372,"product_id":"38","images":"862860.png","mrp_price":"300","type":"COMBO_CUSTOM","purchase_quantity":"1","unit_name":"Kg","ADDONS":[{"cart_product_addon_id":"102976","pdtId":"21","optId":"29","pdtName":"Tomato: 250 gms","images":"tomato1.png","price":"12","quantity":"1","unitValue":"250","unitName":"gms"},{"cart_product_addon_id":"102977","pdtId":"23","optId":"124","pdtName":"Onions: 2 Kg","images":"onions.png","price":"360","quantity":"3","unitValue":"2","unitName":"Kg"}],"discount":0},{"id":"16277","product_name":"Small Basket","total_price":300,"grand_total":300,"product_id":"20","images":"vegetable_box.png","mrp_price":"300","type":"COMBO","purchase_quantity":"1","unit_name":"Kg","ESSENTIAL":[{"pdtId":"21","optId":"0","pdtName":"Tomato","images":"tomato1.png","unitValue":"1","unitName":"Kg","price":"50"},{"pdtId":"23","optId":"0","pdtName":"Onions","images":"onions.png","unitValue":"1","unitName":"Kg","price":"60"},{"pdtId":"24","optId":"137","pdtName":"Green Chillies: 100 gms","images":"green_chillies.png","unitValue":"100","unitName":"gms","price":"9"},{"pdtId":"25","optId":"35","pdtName":"Potato: 500 gms","images":"potato1.png","unitValue":"500","unitName":"gms","price":"30"},{"pdtId":"26","optId":"37","pdtName":"Carrot: 250 gms","images":"carrot.png","unitValue":"250","unitName":"gms","price":"26"},{"pdtId":"27","optId":"138","pdtName":"Ginger: 100 gms","images":"ginger.png","unitValue":"100","unitName":"gms","price":"20"},{"pdtId":"28","optId":"139","pdtName":"Garlic: 50 gms","images":"garlic.png","unitValue":"50","unitName":"gms","price":"11"},{"pdtId":"29","optId":"51","pdtName":"Lady Finger: 500 gms","images":"ladies_finger.png","unitValue":"500","unitName":"gms","price":"34"},{"pdtId":"31","optId":"58","pdtName":"Cluster Beans: 500 gms","images":"cluster_beans.png","unitValue":"500","unitName":"gms","price":"31"},{"pdtId":"34","optId":"0","pdtName":"Curry Leaves","images":"curry_leaf.png","unitValue":"1","unitName":"Bunch","price":"10"},{"pdtId":"36","optId":"0","pdtName":"Bottle Gourd","images":"bottle_gourd.png","unitValue":"1","unitName":"Pieces","price":"30"},{"pdtId":"75","optId":"117","pdtName":"Ridge Gourd (birakaya): 500 gms","images":"Ridge_gourd.jpg","unitValue":"500","unitName":"gms","price":"32"}],"discount":0}]
         * finalTotal : 672
         * finalDiscount : 0
         */

        @SerializedName("currency")
        private String currency;
        @SerializedName("finalTotal")
        private int finalTotal;
        @SerializedName("finalDiscount")
        private int finalDiscount;
        @SerializedName("records")
        private List<RecordsBean> records;

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public int getFinalTotal() {
            return finalTotal;
        }

        public void setFinalTotal(int finalTotal) {
            this.finalTotal = finalTotal;
        }

        public int getFinalDiscount() {
            return finalDiscount;
        }

        public void setFinalDiscount(int finalDiscount) {
            this.finalDiscount = finalDiscount;
        }

        public List<RecordsBean> getRecords() {
            return records;
        }

        public void setRecords(List<RecordsBean> records) {
            this.records = records;
        }

        public static class RecordsBean {
            /**
             * id : 16284
             * product_name : Custom Veggie Box
             * total_price : 372
             * grand_total : 372
             * product_id : 38
             * images : 862860.png
             * mrp_price : 300
             * type : COMBO_CUSTOM
             * purchase_quantity : 1
             * unit_name : Kg
             * ADDONS : [{"cart_product_addon_id":"102976","pdtId":"21","optId":"29","pdtName":"Tomato: 250 gms","images":"tomato1.png","price":"12","quantity":"1","unitValue":"250","unitName":"gms"},{"cart_product_addon_id":"102977","pdtId":"23","optId":"124","pdtName":"Onions: 2 Kg","images":"onions.png","price":"360","quantity":"3","unitValue":"2","unitName":"Kg"}]
             * discount : 0
             * ESSENTIAL : [{"pdtId":"21","optId":"0","pdtName":"Tomato","images":"tomato1.png","unitValue":"1","unitName":"Kg","price":"50"},{"pdtId":"23","optId":"0","pdtName":"Onions","images":"onions.png","unitValue":"1","unitName":"Kg","price":"60"},{"pdtId":"24","optId":"137","pdtName":"Green Chillies: 100 gms","images":"green_chillies.png","unitValue":"100","unitName":"gms","price":"9"},{"pdtId":"25","optId":"35","pdtName":"Potato: 500 gms","images":"potato1.png","unitValue":"500","unitName":"gms","price":"30"},{"pdtId":"26","optId":"37","pdtName":"Carrot: 250 gms","images":"carrot.png","unitValue":"250","unitName":"gms","price":"26"},{"pdtId":"27","optId":"138","pdtName":"Ginger: 100 gms","images":"ginger.png","unitValue":"100","unitName":"gms","price":"20"},{"pdtId":"28","optId":"139","pdtName":"Garlic: 50 gms","images":"garlic.png","unitValue":"50","unitName":"gms","price":"11"},{"pdtId":"29","optId":"51","pdtName":"Lady Finger: 500 gms","images":"ladies_finger.png","unitValue":"500","unitName":"gms","price":"34"},{"pdtId":"31","optId":"58","pdtName":"Cluster Beans: 500 gms","images":"cluster_beans.png","unitValue":"500","unitName":"gms","price":"31"},{"pdtId":"34","optId":"0","pdtName":"Curry Leaves","images":"curry_leaf.png","unitValue":"1","unitName":"Bunch","price":"10"},{"pdtId":"36","optId":"0","pdtName":"Bottle Gourd","images":"bottle_gourd.png","unitValue":"1","unitName":"Pieces","price":"30"},{"pdtId":"75","optId":"117","pdtName":"Ridge Gourd (birakaya): 500 gms","images":"Ridge_gourd.jpg","unitValue":"500","unitName":"gms","price":"32"}]
             */

            @SerializedName("id")
            private String id;
            @SerializedName("product_name")
            private String productName;
            @SerializedName("total_price")
            private int totalPrice;
            @SerializedName("grand_total")
            private int grandTotal;
            @SerializedName("product_id")
            private String productId;
            @SerializedName("images")
            private String images;
            @SerializedName("mrp_price")
            private String mrpPrice;
            @SerializedName("type")
            private String type;
            @SerializedName("purchase_quantity")
            private String purchaseQuantity;
            @SerializedName("unit_name")
            private String unitName;
            @SerializedName("discount")
            private int discount;
            @SerializedName("ADDONS")
            private List<ADDONSBean> ADDONS;
            @SerializedName("ESSENTIAL")
            private List<ESSENTIALBean> ESSENTIAL;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getProductName() {
                return productName;
            }

            public void setProductName(String productName) {
                this.productName = productName;
            }

            public int getTotalPrice() {
                return totalPrice;
            }

            public void setTotalPrice(int totalPrice) {
                this.totalPrice = totalPrice;
            }

            public int getGrandTotal() {
                return grandTotal;
            }

            public void setGrandTotal(int grandTotal) {
                this.grandTotal = grandTotal;
            }

            public String getProductId() {
                return productId;
            }

            public void setProductId(String productId) {
                this.productId = productId;
            }

            public String getImages() {
                return images;
            }

            public void setImages(String images) {
                this.images = images;
            }

            public String getMrpPrice() {
                return mrpPrice;
            }

            public void setMrpPrice(String mrpPrice) {
                this.mrpPrice = mrpPrice;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getPurchaseQuantity() {
                return purchaseQuantity;
            }

            public void setPurchaseQuantity(String purchaseQuantity) {
                this.purchaseQuantity = purchaseQuantity;
            }

            public String getUnitName() {
                return unitName;
            }

            public void setUnitName(String unitName) {
                this.unitName = unitName;
            }

            public int getDiscount() {
                return discount;
            }

            public void setDiscount(int discount) {
                this.discount = discount;
            }

            public List<ADDONSBean> getADDONS() {
                return ADDONS;
            }

            public void setADDONS(List<ADDONSBean> ADDONS) {
                this.ADDONS = ADDONS;
            }

            public List<ESSENTIALBean> getESSENTIAL() {
                return ESSENTIAL;
            }

            public void setESSENTIAL(List<ESSENTIALBean> ESSENTIAL) {
                this.ESSENTIAL = ESSENTIAL;
            }

            public static class ADDONSBean {
                /**
                 * cart_product_addon_id : 102976
                 * pdtId : 21
                 * optId : 29
                 * pdtName : Tomato: 250 gms
                 * images : tomato1.png
                 * price : 12
                 * quantity : 1
                 * unitValue : 250
                 * unitName : gms
                 */

                @SerializedName("cart_product_addon_id")
                private String cartProductAddonId;
                @SerializedName("pdtId")
                private String pdtId;
                @SerializedName("optId")
                private String optId;
                @SerializedName("pdtName")
                private String pdtName;
                @SerializedName("images")
                private String images;
                @SerializedName("price")
                private String price;
                @SerializedName("quantity")
                private String quantity;
                @SerializedName("unitValue")
                private String unitValue;
                @SerializedName("unitName")
                private String unitName;

                public String getCartProductAddonId() {
                    return cartProductAddonId;
                }

                public void setCartProductAddonId(String cartProductAddonId) {
                    this.cartProductAddonId = cartProductAddonId;
                }

                public String getPdtId() {
                    return pdtId;
                }

                public void setPdtId(String pdtId) {
                    this.pdtId = pdtId;
                }

                public String getOptId() {
                    return optId;
                }

                public void setOptId(String optId) {
                    this.optId = optId;
                }

                public String getPdtName() {
                    return pdtName;
                }

                public void setPdtName(String pdtName) {
                    this.pdtName = pdtName;
                }

                public String getImages() {
                    return images;
                }

                public void setImages(String images) {
                    this.images = images;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }

                public String getQuantity() {
                    return quantity;
                }

                public void setQuantity(String quantity) {
                    this.quantity = quantity;
                }

                public String getUnitValue() {
                    return unitValue;
                }

                public void setUnitValue(String unitValue) {
                    this.unitValue = unitValue;
                }

                public String getUnitName() {
                    return unitName;
                }

                public void setUnitName(String unitName) {
                    this.unitName = unitName;
                }
            }

            public static class ESSENTIALBean {
                /**
                 * pdtId : 21
                 * optId : 0
                 * pdtName : Tomato
                 * images : tomato1.png
                 * unitValue : 1
                 * unitName : Kg
                 * price : 50
                 */

                @SerializedName("pdtId")
                private String pdtId;
                @SerializedName("optId")
                private String optId;
                @SerializedName("pdtName")
                private String pdtName;
                @SerializedName("images")
                private String images;
                @SerializedName("unitValue")
                private String unitValue;
                @SerializedName("unitName")
                private String unitName;
                @SerializedName("price")
                private String price;

                public String getPdtId() {
                    return pdtId;
                }

                public void setPdtId(String pdtId) {
                    this.pdtId = pdtId;
                }

                public String getOptId() {
                    return optId;
                }

                public void setOptId(String optId) {
                    this.optId = optId;
                }

                public String getPdtName() {
                    return pdtName;
                }

                public void setPdtName(String pdtName) {
                    this.pdtName = pdtName;
                }

                public String getImages() {
                    return images;
                }

                public void setImages(String images) {
                    this.images = images;
                }

                public String getUnitValue() {
                    return unitValue;
                }

                public void setUnitValue(String unitValue) {
                    this.unitValue = unitValue;
                }

                public String getUnitName() {
                    return unitName;
                }

                public void setUnitName(String unitName) {
                    this.unitName = unitName;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }
            }
        }
    }
}
