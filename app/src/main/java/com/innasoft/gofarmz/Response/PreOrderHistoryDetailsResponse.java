package com.innasoft.gofarmz.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PreOrderHistoryDetailsResponse {

    /**
     * status : 10100
     * message : Data fetch successfully
     * stateData : {"currency":"Rs. ","total_products":4,"order_details":{"ref_no":"PRE08042019001","total_price":"466","shipping_charges":"40","grand_total":"506","pincode":"500081","medium":"WEB","pre_order_status":"Confirmed","getaway_name":"N/A","order_date":"08-04-2019 10:14 am"},"address":{"name":"Vijay Ravinootala","mobile":"8019448098","alternate_contact_no":"","country":"India","state":"Telangana","city":"Hyderabad","area":"Madhapur","address":"Unnamed Road,VIP Hills, Jaihind Enclave,","pincode":"500081"},"products":[{"product_id":"72","product_name":"Raw Mango","image":"1554465208423_istockphoto-679591552-640x640.jpg","expected_delivery_date":"2019-04-27","unit_price":"66.00","unit_value":"1KG","product_quantity":"1","total_price":"66.00","pre_order_status":"Confirmed"},{"product_id":"52","product_name":"Bitter Gourd","image":"1554465236797_Grape_02.jpg","expected_delivery_date":"2019-04-26","unit_price":"30.00","unit_value":"2KG","product_quantity":"2","total_price":"60.00","pre_order_status":"Confirmed"},{"product_id":"36","product_name":"Bottle Gourd","image":"pre-order-default-product-image.jpg","expected_delivery_date":"2019-04-27","unit_price":"50.00","unit_value":"1KG","product_quantity":"4","total_price":"200.00","pre_order_status":"Confirmed"},{"product_id":"63","product_name":"Broad Beans","image":"1554465207772_Ghee2.jpg","expected_delivery_date":"2019-04-29","unit_price":"70.00","unit_value":"2 Liters","product_quantity":"2","total_price":"140.00","pre_order_status":"Confirmed"}]}
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("stateData")
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * currency : Rs.
         * total_products : 4
         * order_details : {"ref_no":"PRE08042019001","total_price":"466","shipping_charges":"40","grand_total":"506","pincode":"500081","medium":"WEB","pre_order_status":"Confirmed","getaway_name":"N/A","order_date":"08-04-2019 10:14 am"}
         * address : {"name":"Vijay Ravinootala","mobile":"8019448098","alternate_contact_no":"","country":"India","state":"Telangana","city":"Hyderabad","area":"Madhapur","address":"Unnamed Road,VIP Hills, Jaihind Enclave,","pincode":"500081"}
         * products : [{"product_id":"72","product_name":"Raw Mango","image":"1554465208423_istockphoto-679591552-640x640.jpg","expected_delivery_date":"2019-04-27","unit_price":"66.00","unit_value":"1KG","product_quantity":"1","total_price":"66.00","pre_order_status":"Confirmed"},{"product_id":"52","product_name":"Bitter Gourd","image":"1554465236797_Grape_02.jpg","expected_delivery_date":"2019-04-26","unit_price":"30.00","unit_value":"2KG","product_quantity":"2","total_price":"60.00","pre_order_status":"Confirmed"},{"product_id":"36","product_name":"Bottle Gourd","image":"pre-order-default-product-image.jpg","expected_delivery_date":"2019-04-27","unit_price":"50.00","unit_value":"1KG","product_quantity":"4","total_price":"200.00","pre_order_status":"Confirmed"},{"product_id":"63","product_name":"Broad Beans","image":"1554465207772_Ghee2.jpg","expected_delivery_date":"2019-04-29","unit_price":"70.00","unit_value":"2 Liters","product_quantity":"2","total_price":"140.00","pre_order_status":"Confirmed"}]
         */

        @SerializedName("currency")
        private String currency;
        @SerializedName("total_products")
        private int totalProducts;
        @SerializedName("order_details")
        private OrderDetailsBean orderDetails;
        @SerializedName("address")
        private AddressBean address;
        @SerializedName("products")
        private List<ProductsBean> products;

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public int getTotalProducts() {
            return totalProducts;
        }

        public void setTotalProducts(int totalProducts) {
            this.totalProducts = totalProducts;
        }

        public OrderDetailsBean getOrderDetails() {
            return orderDetails;
        }

        public void setOrderDetails(OrderDetailsBean orderDetails) {
            this.orderDetails = orderDetails;
        }

        public AddressBean getAddress() {
            return address;
        }

        public void setAddress(AddressBean address) {
            this.address = address;
        }

        public List<ProductsBean> getProducts() {
            return products;
        }

        public void setProducts(List<ProductsBean> products) {
            this.products = products;
        }

        public static class OrderDetailsBean {
            /**
             * ref_no : PRE08042019001
             * total_price : 466
             * shipping_charges : 40
             * grand_total : 506
             * pincode : 500081
             * medium : WEB
             * pre_order_status : Confirmed
             * getaway_name : N/A
             * order_date : 08-04-2019 10:14 am
             */

            @SerializedName("ref_no")
            private String refNo;
            @SerializedName("total_price")
            private String totalPrice;
            @SerializedName("shipping_charges")
            private String shippingCharges;
            @SerializedName("grand_total")
            private String grandTotal;
            @SerializedName("pincode")
            private String pincode;
            @SerializedName("medium")
            private String medium;
            @SerializedName("pre_order_status")
            private String preOrderStatus;
            @SerializedName("getaway_name")
            private String getawayName;
            @SerializedName("order_date")
            private String orderDate;

            public String getRefNo() {
                return refNo;
            }

            public void setRefNo(String refNo) {
                this.refNo = refNo;
            }

            public String getTotalPrice() {
                return totalPrice;
            }

            public void setTotalPrice(String totalPrice) {
                this.totalPrice = totalPrice;
            }

            public String getShippingCharges() {
                return shippingCharges;
            }

            public void setShippingCharges(String shippingCharges) {
                this.shippingCharges = shippingCharges;
            }

            public String getGrandTotal() {
                return grandTotal;
            }

            public void setGrandTotal(String grandTotal) {
                this.grandTotal = grandTotal;
            }

            public String getPincode() {
                return pincode;
            }

            public void setPincode(String pincode) {
                this.pincode = pincode;
            }

            public String getMedium() {
                return medium;
            }

            public void setMedium(String medium) {
                this.medium = medium;
            }

            public String getPreOrderStatus() {
                return preOrderStatus;
            }

            public void setPreOrderStatus(String preOrderStatus) {
                this.preOrderStatus = preOrderStatus;
            }

            public String getGetawayName() {
                return getawayName;
            }

            public void setGetawayName(String getawayName) {
                this.getawayName = getawayName;
            }

            public String getOrderDate() {
                return orderDate;
            }

            public void setOrderDate(String orderDate) {
                this.orderDate = orderDate;
            }
        }

        public static class AddressBean {
            /**
             * name : Vijay Ravinootala
             * mobile : 8019448098
             * alternate_contact_no :
             * country : India
             * state : Telangana
             * city : Hyderabad
             * area : Madhapur
             * address : Unnamed Road,VIP Hills, Jaihind Enclave,
             * pincode : 500081
             */

            @SerializedName("name")
            private String name;
            @SerializedName("mobile")
            private String mobile;
            @SerializedName("alternate_contact_no")
            private String alternateContactNo;
            @SerializedName("country")
            private String country;
            @SerializedName("state")
            private String state;
            @SerializedName("city")
            private String city;
            @SerializedName("area")
            private String area;
            @SerializedName("address")
            private String address;
            @SerializedName("pincode")
            private String pincode;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getMobile() {
                return mobile;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public String getAlternateContactNo() {
                return alternateContactNo;
            }

            public void setAlternateContactNo(String alternateContactNo) {
                this.alternateContactNo = alternateContactNo;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getPincode() {
                return pincode;
            }

            public void setPincode(String pincode) {
                this.pincode = pincode;
            }
        }

        public static class ProductsBean {
            /**
             * product_id : 72
             * product_name : Raw Mango
             * image : 1554465208423_istockphoto-679591552-640x640.jpg
             * expected_delivery_date : 2019-04-27
             * unit_price : 66.00
             * unit_value : 1KG
             * product_quantity : 1
             * total_price : 66.00
             * pre_order_status : Confirmed
             */

            @SerializedName("product_id")
            private String productId;
            @SerializedName("product_name")
            private String productName;
            @SerializedName("image")
            private String image;
            @SerializedName("expected_delivery_date")
            private String expectedDeliveryDate;
            @SerializedName("unit_price")
            private String unitPrice;
            @SerializedName("unit_value")
            private String unitValue;
            @SerializedName("product_quantity")
            private String productQuantity;
            @SerializedName("total_price")
            private String totalPrice;
            @SerializedName("pre_order_status")
            private String preOrderStatus;

            public String getProductId() {
                return productId;
            }

            public void setProductId(String productId) {
                this.productId = productId;
            }

            public String getProductName() {
                return productName;
            }

            public void setProductName(String productName) {
                this.productName = productName;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getExpectedDeliveryDate() {
                return expectedDeliveryDate;
            }

            public void setExpectedDeliveryDate(String expectedDeliveryDate) {
                this.expectedDeliveryDate = expectedDeliveryDate;
            }

            public String getUnitPrice() {
                return unitPrice;
            }

            public void setUnitPrice(String unitPrice) {
                this.unitPrice = unitPrice;
            }

            public String getUnitValue() {
                return unitValue;
            }

            public void setUnitValue(String unitValue) {
                this.unitValue = unitValue;
            }

            public String getProductQuantity() {
                return productQuantity;
            }

            public void setProductQuantity(String productQuantity) {
                this.productQuantity = productQuantity;
            }

            public String getTotalPrice() {
                return totalPrice;
            }

            public void setTotalPrice(String totalPrice) {
                this.totalPrice = totalPrice;
            }

            public String getPreOrderStatus() {
                return preOrderStatus;
            }

            public void setPreOrderStatus(String preOrderStatus) {
                this.preOrderStatus = preOrderStatus;
            }
        }
    }
}
