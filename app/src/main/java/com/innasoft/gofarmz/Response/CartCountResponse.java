package com.innasoft.gofarmz.Response;

public class CartCountResponse {


    /**
     * status : 10100
     * message : Data save successfully
     * stateData : 1
     */

    private String status;
    private String message;
    private int data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }
}
