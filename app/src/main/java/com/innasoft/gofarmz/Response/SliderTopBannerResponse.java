package com.innasoft.gofarmz.Response;

import java.util.List;

public class SliderTopBannerResponse {


    /**
     * status : 10100
     * message : Data fetch successfully.
     * p_banner : images/banners/app/promo/614f6-untitled-1.png
     * stateData : [{"id":"6","highlighted_text":"Pre Order","image":"images/banners/app/e5f3d-pre-order-banner.png"},{"id":"3","highlighted_text":"Our Tailored Baskets make Placing Orders Easy.","image":"images/banners/app/e8e28-2.png"},{"id":"2","highlighted_text":"Fresh Naturally Grown Products at Never Before Prices.","image":"images/banners/app/73219-1.png"},{"id":"5","highlighted_text":"Know Your Farmer Who is Producing Your Food","image":"images/banners/app/52e6e-3.png"},{"id":"4","highlighted_text":"Collect the Produce right at Your Doorstep","image":"images/banners/app/95c5f-4.png"}]
     */

    private String status;
    private String message;
    private String p_banner;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getP_banner() {
        return p_banner;
    }

    public void setP_banner(String p_banner) {
        this.p_banner = p_banner;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 6
         * highlighted_text : Pre Order
         * image : images/banners/app/e5f3d-pre-order-banner.png
         */

        private String id;
        private String highlighted_text;
        private String image;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getHighlighted_text() {
            return highlighted_text;
        }

        public void setHighlighted_text(String highlighted_text) {
            this.highlighted_text = highlighted_text;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}
