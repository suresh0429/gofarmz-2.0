package com.innasoft.gofarmz.Response;

import java.util.List;

public class HomeCategoriesResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * stateData : [{"id":"5","name":"Browse Products","image":"images/category/main/bada5-basket3.png","type_avail":"COMBO_CUSTOM"},{"id":"2","name":"Vegetables","image":"images/category/main/b4004-vegetables.png","type_avail":"COMBO"},{"id":"1","name":"Fruits","image":"images/category/main/0b776-fruits.png","type_avail":"COMBO_CUSTOM"},{"id":"3","name":"Millets","image":"images/category/main/dbb62-millets.png","type_avail":"COMBO_CUSTOM"}]
     */

    private String status;
    private String message;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 5
         * name : Browse Products
         * image : images/category/main/bada5-basket3.png
         * type_avail : COMBO_CUSTOM
         */

        private String id;
        private String name;
        private String image;
        private String type_avail;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getType_avail() {
            return type_avail;
        }

        public void setType_avail(String type_avail) {
            this.type_avail = type_avail;
        }
    }
}
