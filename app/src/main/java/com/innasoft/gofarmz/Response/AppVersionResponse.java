package com.innasoft.gofarmz.Response;

public class AppVersionResponse {


    /**
     * status : 10100
     * message : Data fetch successfully.
     * stateData : {"android_version":"1.9.1","ios_version":"1.9.3"}
     */

    private String status;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * android_version : 1.9.1
         * ios_version : 1.9.3
         */

        private String android_version;
        private String ios_version;

        public String getAndroid_version() {
            return android_version;
        }

        public void setAndroid_version(String android_version) {
            this.android_version = android_version;
        }

        public String getIos_version() {
            return ios_version;
        }

        public void setIos_version(String ios_version) {
            this.ios_version = ios_version;
        }
    }
}
