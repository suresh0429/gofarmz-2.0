package com.innasoft.gofarmz.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommunityResponse {


    /**
     * status : 10100
     * message : Data fetch successfully.
     * stateData : [{"id":"1","leader_id":"7453","community":"my home hub","leader_name":"suresh"},{"id":"2","leader_id":"0","community":"Aparna Constructions","leader_name":""}]
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("stateData")
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * leader_id : 7453
         * community : my home hub
         * leader_name : suresh
         */

        @SerializedName("id")
        private String id;
        @SerializedName("leader_id")
        private String leaderId;
        @SerializedName("community")
        private String community;
        @SerializedName("leader_name")
        private String leaderName;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLeaderId() {
            return leaderId;
        }

        public void setLeaderId(String leaderId) {
            this.leaderId = leaderId;
        }

        public String getCommunity() {
            return community;
        }

        public void setCommunity(String community) {
            this.community = community;
        }

        public String getLeaderName() {
            return leaderName;
        }

        public void setLeaderName(String leaderName) {
            this.leaderName = leaderName;
        }
    }
}
