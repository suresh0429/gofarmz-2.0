package com.innasoft.gofarmz.Response;

import java.util.List;

public class BottomOfferAddResponse {


    /**
     * status : 10100
     * message : Data fetch successfully.
     * stateData : [{"id":"5","title":"Pre Order","description":"<p>\r\n\tLorem Ipsum is simply dummy text of the printing and typesetting industry.<\/p>\r\n","image":"images/banners/app/promo/824f7-istockphoto-679591552-640x640.jpg"},{"id":"4","title":"Refer a Farmer","description":"<pre courier=\"\" font-size:=\"\" style=\"color: rgb(0, 0, 0); font-family: \">\r\nKindly refer ZBNF, Natural or Organic farmers you know to GoFarmz<\/pre>\r\n","image":"images/banners/app/promo/1ae68-untitled-6.png"},{"id":"2","title":"Welcome Offer","description":"<p>\r\n\t<span style=\"color: rgb(38, 50, 56); font-family: Roboto, sans-serif; font-size: 13px;\">Use GOFIRST coupon code to get 10% discount on your first order.<\/span><\/p>\r\n","image":"images/banners/app/promo/9a348-app-banner.png"}]
     */

    private String status;
    private String message;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 5
         * title : Pre Order
         * description : <p>
         Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
         * image : images/banners/app/promo/824f7-istockphoto-679591552-640x640.jpg
         */

        private String id;
        private String title;
        private String description;
        private String image;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}
