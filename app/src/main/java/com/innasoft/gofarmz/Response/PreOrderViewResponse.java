package com.innasoft.gofarmz.Response;

import com.google.gson.annotations.SerializedName;

public class PreOrderViewResponse {


    /**
     * status : 10100
     * versions : {"android_version":"1.9.13","ios_version":"1.4.3"}
     * message : Pre-order product details
     * stateData : {"product_id":"33","pop_image":"http://gofarmzv2.learningslot.in/timthumb.php?src=http://gofarmzv2.learningslot.in/images/products/pre_order/1554184867294_McCann-Dodoni-03-440x320.jpg&h=160&w=220","pop_expected_delivery_date":"30-04-2019","pop_description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.","pop_unit_price":"30.00","pop_unit_value":"1KG","pop_available_units":"1","priority":"0","product_name":"Beetroot","quantity":"2"}
     * cart_count : 3
     */

    @SerializedName("status")
    private String status;
    @SerializedName("versions")
    private VersionsBean versions;
    @SerializedName("message")
    private String message;
    @SerializedName("stateData")
    private DataBean data;
    @SerializedName("cart_count")
    private int cartCount;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public VersionsBean getVersions() {
        return versions;
    }

    public void setVersions(VersionsBean versions) {
        this.versions = versions;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public int getCartCount() {
        return cartCount;
    }

    public void setCartCount(int cartCount) {
        this.cartCount = cartCount;
    }

    public static class VersionsBean {
        /**
         * android_version : 1.9.13
         * ios_version : 1.4.3
         */

        @SerializedName("android_version")
        private String androidVersion;
        @SerializedName("ios_version")
        private String iosVersion;

        public String getAndroidVersion() {
            return androidVersion;
        }

        public void setAndroidVersion(String androidVersion) {
            this.androidVersion = androidVersion;
        }

        public String getIosVersion() {
            return iosVersion;
        }

        public void setIosVersion(String iosVersion) {
            this.iosVersion = iosVersion;
        }
    }

    public static class DataBean {
        /**
         * product_id : 33
         * pop_image : http://gofarmzv2.learningslot.in/timthumb.php?src=http://gofarmzv2.learningslot.in/images/products/pre_order/1554184867294_McCann-Dodoni-03-440x320.jpg&h=160&w=220
         * pop_expected_delivery_date : 30-04-2019
         * pop_description : Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.
         * pop_unit_price : 30.00
         * pop_unit_value : 1KG
         * pop_available_units : 1
         * priority : 0
         * product_name : Beetroot
         * quantity : 2
         */

        @SerializedName("product_id")
        private String productId;
        @SerializedName("pop_image")
        private String popImage;
        @SerializedName("pop_expected_delivery_date")
        private String popExpectedDeliveryDate;
        @SerializedName("pop_description")
        private String popDescription;
        @SerializedName("pop_unit_price")
        private String popUnitPrice;
        @SerializedName("pop_unit_value")
        private String popUnitValue;
        @SerializedName("pop_available_units")
        private String popAvailableUnits;
        @SerializedName("priority")
        private String priority;
        @SerializedName("product_name")
        private String productName;
        @SerializedName("quantity")
        private String quantity;

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getPopImage() {
            return popImage;
        }

        public void setPopImage(String popImage) {
            this.popImage = popImage;
        }

        public String getPopExpectedDeliveryDate() {
            return popExpectedDeliveryDate;
        }

        public void setPopExpectedDeliveryDate(String popExpectedDeliveryDate) {
            this.popExpectedDeliveryDate = popExpectedDeliveryDate;
        }

        public String getPopDescription() {
            return popDescription;
        }

        public void setPopDescription(String popDescription) {
            this.popDescription = popDescription;
        }

        public String getPopUnitPrice() {
            return popUnitPrice;
        }

        public void setPopUnitPrice(String popUnitPrice) {
            this.popUnitPrice = popUnitPrice;
        }

        public String getPopUnitValue() {
            return popUnitValue;
        }

        public void setPopUnitValue(String popUnitValue) {
            this.popUnitValue = popUnitValue;
        }

        public String getPopAvailableUnits() {
            return popAvailableUnits;
        }

        public void setPopAvailableUnits(String popAvailableUnits) {
            this.popAvailableUnits = popAvailableUnits;
        }

        public String getPriority() {
            return priority;
        }

        public void setPriority(String priority) {
            this.priority = priority;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }
    }
}
