package com.innasoft.gofarmz.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CheckoutDataResponse {


    /**
     * status : 10100
     * message : Data fetch successfully.
     * data : {"mobile_verify_status":"1","mobile":"9542410774","address":[{"id":"2902","user_id":"3211","name":"suresh kumar","address_line1":"223-rtutcycyvuvu","address_line2":"tdtdycycuvugugi","area":"Kukatpally","city":"Hyderabad","state":"Telangana","country":"India","pincode":"500072","latitude":"17.4875","longitude":"78.3953","contact_no":"2828283839","alternate_contact_no":"","is_default":"Yes","created_on":"2020-01-11 17:46:03","updated_on":"2020-01-11 18:36:04","distance":3,"delivery_status":true,"delivery_charges":"25"}],"enable_order_closing":0,"order_closing_txt":"Orders closed for this week. You can start placing your orders again from Sunday.  ","products":[{"id":"16192","product_name":"Small Basket","total_price":600,"grand_total":600,"product_id":"20","images":"vegetable_box.png","mrp_price":"300","type":"COMBO","purchase_quantity":"2","unit_name":"Kg","ESSENTIAL":[{"pdtId":"21","optId":"0","pdtName":"Tomato","images":"tomato1.png","unitValue":"1","unitName":"Kg","price":"50"},{"pdtId":"23","optId":"0","pdtName":"Onions","images":"onions.png","unitValue":"1","unitName":"Kg","price":"60"},{"pdtId":"24","optId":"137","pdtName":"Green Chillies: 100 gms","images":"green_chillies.png","unitValue":"100","unitName":"gms","price":"9"},{"pdtId":"25","optId":"35","pdtName":"Potato: 500 gms","images":"potato1.png","unitValue":"500","unitName":"gms","price":"30"},{"pdtId":"26","optId":"37","pdtName":"Carrot: 250 gms","images":"carrot.png","unitValue":"250","unitName":"gms","price":"26"},{"pdtId":"27","optId":"138","pdtName":"Ginger: 100 gms","images":"ginger.png","unitValue":"100","unitName":"gms","price":"20"},{"pdtId":"28","optId":"139","pdtName":"Garlic: 50 gms","images":"garlic.png","unitValue":"50","unitName":"gms","price":"11"},{"pdtId":"29","optId":"51","pdtName":"Lady Finger: 500 gms","images":"ladies_finger.png","unitValue":"500","unitName":"gms","price":"34"},{"pdtId":"31","optId":"58","pdtName":"Cluster Beans: 500 gms","images":"cluster_beans.png","unitValue":"500","unitName":"gms","price":"31"},{"pdtId":"34","optId":"0","pdtName":"Curry Leaves","images":"curry_leaf.png","unitValue":"1","unitName":"Bunch","price":"10"},{"pdtId":"36","optId":"0","pdtName":"Bottle Gourd","images":"bottle_gourd.png","unitValue":"1","unitName":"Pieces","price":"30"},{"pdtId":"75","optId":"117","pdtName":"Ridge Gourd (birakaya): 500 gms","images":"Ridge_gourd.jpg","unitValue":"500","unitName":"gms","price":"32"}],"discount":0}],"finalTotal":600,"finalDiscount":0,"wallet_balance":"0.00","payment_gateway":[{"id":"6","name":"Pay Online","logo":"images/gateway/1896px-Razorpay_logo.svg.png"},{"id":"1","name":"Cash on Delivery","logo":"images/gateway/Cash-on-Delivery.jpg"}]}
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * mobile_verify_status : 1
         * mobile : 9542410774
         * address : [{"id":"2902","user_id":"3211","name":"suresh kumar","address_line1":"223-rtutcycyvuvu","address_line2":"tdtdycycuvugugi","area":"Kukatpally","city":"Hyderabad","state":"Telangana","country":"India","pincode":"500072","latitude":"17.4875","longitude":"78.3953","contact_no":"2828283839","alternate_contact_no":"","is_default":"Yes","created_on":"2020-01-11 17:46:03","updated_on":"2020-01-11 18:36:04","distance":3,"delivery_status":true,"delivery_charges":"25"}]
         * enable_order_closing : 0
         * order_closing_txt : Orders closed for this week. You can start placing your orders again from Sunday.
         * products : [{"id":"16192","product_name":"Small Basket","total_price":600,"grand_total":600,"product_id":"20","images":"vegetable_box.png","mrp_price":"300","type":"COMBO","purchase_quantity":"2","unit_name":"Kg","ESSENTIAL":[{"pdtId":"21","optId":"0","pdtName":"Tomato","images":"tomato1.png","unitValue":"1","unitName":"Kg","price":"50"},{"pdtId":"23","optId":"0","pdtName":"Onions","images":"onions.png","unitValue":"1","unitName":"Kg","price":"60"},{"pdtId":"24","optId":"137","pdtName":"Green Chillies: 100 gms","images":"green_chillies.png","unitValue":"100","unitName":"gms","price":"9"},{"pdtId":"25","optId":"35","pdtName":"Potato: 500 gms","images":"potato1.png","unitValue":"500","unitName":"gms","price":"30"},{"pdtId":"26","optId":"37","pdtName":"Carrot: 250 gms","images":"carrot.png","unitValue":"250","unitName":"gms","price":"26"},{"pdtId":"27","optId":"138","pdtName":"Ginger: 100 gms","images":"ginger.png","unitValue":"100","unitName":"gms","price":"20"},{"pdtId":"28","optId":"139","pdtName":"Garlic: 50 gms","images":"garlic.png","unitValue":"50","unitName":"gms","price":"11"},{"pdtId":"29","optId":"51","pdtName":"Lady Finger: 500 gms","images":"ladies_finger.png","unitValue":"500","unitName":"gms","price":"34"},{"pdtId":"31","optId":"58","pdtName":"Cluster Beans: 500 gms","images":"cluster_beans.png","unitValue":"500","unitName":"gms","price":"31"},{"pdtId":"34","optId":"0","pdtName":"Curry Leaves","images":"curry_leaf.png","unitValue":"1","unitName":"Bunch","price":"10"},{"pdtId":"36","optId":"0","pdtName":"Bottle Gourd","images":"bottle_gourd.png","unitValue":"1","unitName":"Pieces","price":"30"},{"pdtId":"75","optId":"117","pdtName":"Ridge Gourd (birakaya): 500 gms","images":"Ridge_gourd.jpg","unitValue":"500","unitName":"gms","price":"32"}],"discount":0}]
         * finalTotal : 600
         * finalDiscount : 0
         * wallet_balance : 0.00
         * payment_gateway : [{"id":"6","name":"Pay Online","logo":"images/gateway/1896px-Razorpay_logo.svg.png"},{"id":"1","name":"Cash on Delivery","logo":"images/gateway/Cash-on-Delivery.jpg"}]
         */

        @SerializedName("mobile_verify_status")
        private String mobileVerifyStatus;
        @SerializedName("mobile")
        private String mobile;
        @SerializedName("enable_order_closing")
        private int enableOrderClosing;
        @SerializedName("order_closing_txt")
        private String orderClosingTxt;
        @SerializedName("finalTotal")
        private int finalTotal;
        @SerializedName("finalDiscount")
        private int finalDiscount;
        @SerializedName("wallet_balance")
        private String walletBalance;
        @SerializedName("address")
        private Object address;
        @SerializedName("products")
        private List<ProductsBean> products;
        @SerializedName("payment_gateway")
        private List<PaymentGatewayBean> paymentGateway;

        public String getMobileVerifyStatus() {
            return mobileVerifyStatus;
        }

        public void setMobileVerifyStatus(String mobileVerifyStatus) {
            this.mobileVerifyStatus = mobileVerifyStatus;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public int getEnableOrderClosing() {
            return enableOrderClosing;
        }

        public void setEnableOrderClosing(int enableOrderClosing) {
            this.enableOrderClosing = enableOrderClosing;
        }

        public String getOrderClosingTxt() {
            return orderClosingTxt;
        }

        public void setOrderClosingTxt(String orderClosingTxt) {
            this.orderClosingTxt = orderClosingTxt;
        }

        public int getFinalTotal() {
            return finalTotal;
        }

        public void setFinalTotal(int finalTotal) {
            this.finalTotal = finalTotal;
        }

        public int getFinalDiscount() {
            return finalDiscount;
        }

        public void setFinalDiscount(int finalDiscount) {
            this.finalDiscount = finalDiscount;
        }

        public String getWalletBalance() {
            return walletBalance;
        }

        public void setWalletBalance(String walletBalance) {
            this.walletBalance = walletBalance;
        }

        public Object getAddress() {
            return address;
        }

        public void setAddress(List<AddressBean> address) {
            this.address = address;
        }

        public List<ProductsBean> getProducts() {
            return products;
        }

        public void setProducts(List<ProductsBean> products) {
            this.products = products;
        }

        public List<PaymentGatewayBean> getPaymentGateway() {
            return paymentGateway;
        }

        public void setPaymentGateway(List<PaymentGatewayBean> paymentGateway) {
            this.paymentGateway = paymentGateway;
        }

        public static class AddressBean {
            /**
             * id : 2902
             * user_id : 3211
             * name : suresh kumar
             * address_line1 : 223-rtutcycyvuvu
             * address_line2 : tdtdycycuvugugi
             * area : Kukatpally
             * city : Hyderabad
             * state : Telangana
             * country : India
             * pincode : 500072
             * latitude : 17.4875
             * longitude : 78.3953
             * contact_no : 2828283839
             * alternate_contact_no :
             * is_default : Yes
             * created_on : 2020-01-11 17:46:03
             * updated_on : 2020-01-11 18:36:04
             * distance : 3
             * delivery_status : true
             * delivery_charges : 25
             */

            @SerializedName("id")
            private String id;
            @SerializedName("user_id")
            private String userId;
            @SerializedName("name")
            private String name;
            @SerializedName("address_line1")
            private String addressLine1;
            @SerializedName("address_line2")
            private String addressLine2;
            @SerializedName("area")
            private String area;
            @SerializedName("city")
            private String city;
            @SerializedName("state")
            private String state;
            @SerializedName("country")
            private String country;
            @SerializedName("pincode")
            private String pincode;
            @SerializedName("latitude")
            private String latitude;
            @SerializedName("longitude")
            private String longitude;
            @SerializedName("contact_no")
            private String contactNo;
            @SerializedName("alternate_contact_no")
            private String alternateContactNo;
            @SerializedName("is_default")
            private String isDefault;
            @SerializedName("created_on")
            private String createdOn;
            @SerializedName("updated_on")
            private String updatedOn;
            @SerializedName("distance")
            private int distance;
            @SerializedName("delivery_status")
            private boolean deliveryStatus;
            @SerializedName("delivery_charges")
            private String deliveryCharges;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getUserId() {
                return userId;
            }

            public void setUserId(String userId) {
                this.userId = userId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getAddressLine1() {
                return addressLine1;
            }

            public void setAddressLine1(String addressLine1) {
                this.addressLine1 = addressLine1;
            }

            public String getAddressLine2() {
                return addressLine2;
            }

            public void setAddressLine2(String addressLine2) {
                this.addressLine2 = addressLine2;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getPincode() {
                return pincode;
            }

            public void setPincode(String pincode) {
                this.pincode = pincode;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public String getContactNo() {
                return contactNo;
            }

            public void setContactNo(String contactNo) {
                this.contactNo = contactNo;
            }

            public String getAlternateContactNo() {
                return alternateContactNo;
            }

            public void setAlternateContactNo(String alternateContactNo) {
                this.alternateContactNo = alternateContactNo;
            }

            public String getIsDefault() {
                return isDefault;
            }

            public void setIsDefault(String isDefault) {
                this.isDefault = isDefault;
            }

            public String getCreatedOn() {
                return createdOn;
            }

            public void setCreatedOn(String createdOn) {
                this.createdOn = createdOn;
            }

            public String getUpdatedOn() {
                return updatedOn;
            }

            public void setUpdatedOn(String updatedOn) {
                this.updatedOn = updatedOn;
            }

            public int getDistance() {
                return distance;
            }

            public void setDistance(int distance) {
                this.distance = distance;
            }

            public boolean isDeliveryStatus() {
                return deliveryStatus;
            }

            public void setDeliveryStatus(boolean deliveryStatus) {
                this.deliveryStatus = deliveryStatus;
            }

            public String getDeliveryCharges() {
                return deliveryCharges;
            }

            public void setDeliveryCharges(String deliveryCharges) {
                this.deliveryCharges = deliveryCharges;
            }
        }

        public static class ProductsBean {
            /**
             * id : 16192
             * product_name : Small Basket
             * total_price : 600
             * grand_total : 600
             * product_id : 20
             * images : vegetable_box.png
             * mrp_price : 300
             * type : COMBO
             * purchase_quantity : 2
             * unit_name : Kg
             * ESSENTIAL : [{"pdtId":"21","optId":"0","pdtName":"Tomato","images":"tomato1.png","unitValue":"1","unitName":"Kg","price":"50"},{"pdtId":"23","optId":"0","pdtName":"Onions","images":"onions.png","unitValue":"1","unitName":"Kg","price":"60"},{"pdtId":"24","optId":"137","pdtName":"Green Chillies: 100 gms","images":"green_chillies.png","unitValue":"100","unitName":"gms","price":"9"},{"pdtId":"25","optId":"35","pdtName":"Potato: 500 gms","images":"potato1.png","unitValue":"500","unitName":"gms","price":"30"},{"pdtId":"26","optId":"37","pdtName":"Carrot: 250 gms","images":"carrot.png","unitValue":"250","unitName":"gms","price":"26"},{"pdtId":"27","optId":"138","pdtName":"Ginger: 100 gms","images":"ginger.png","unitValue":"100","unitName":"gms","price":"20"},{"pdtId":"28","optId":"139","pdtName":"Garlic: 50 gms","images":"garlic.png","unitValue":"50","unitName":"gms","price":"11"},{"pdtId":"29","optId":"51","pdtName":"Lady Finger: 500 gms","images":"ladies_finger.png","unitValue":"500","unitName":"gms","price":"34"},{"pdtId":"31","optId":"58","pdtName":"Cluster Beans: 500 gms","images":"cluster_beans.png","unitValue":"500","unitName":"gms","price":"31"},{"pdtId":"34","optId":"0","pdtName":"Curry Leaves","images":"curry_leaf.png","unitValue":"1","unitName":"Bunch","price":"10"},{"pdtId":"36","optId":"0","pdtName":"Bottle Gourd","images":"bottle_gourd.png","unitValue":"1","unitName":"Pieces","price":"30"},{"pdtId":"75","optId":"117","pdtName":"Ridge Gourd (birakaya): 500 gms","images":"Ridge_gourd.jpg","unitValue":"500","unitName":"gms","price":"32"}]
             * discount : 0
             */

            @SerializedName("id")
            private String id;
            @SerializedName("product_name")
            private String productName;
            @SerializedName("total_price")
            private int totalPrice;
            @SerializedName("grand_total")
            private int grandTotal;
            @SerializedName("product_id")
            private String productId;
            @SerializedName("images")
            private String images;
            @SerializedName("mrp_price")
            private String mrpPrice;
            @SerializedName("type")
            private String type;
            @SerializedName("purchase_quantity")
            private String purchaseQuantity;
            @SerializedName("unit_name")
            private String unitName;
            @SerializedName("discount")
            private int discount;
            @SerializedName("ESSENTIAL")
            private List<ESSENTIALBean> ESSENTIAL;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getProductName() {
                return productName;
            }

            public void setProductName(String productName) {
                this.productName = productName;
            }

            public int getTotalPrice() {
                return totalPrice;
            }

            public void setTotalPrice(int totalPrice) {
                this.totalPrice = totalPrice;
            }

            public int getGrandTotal() {
                return grandTotal;
            }

            public void setGrandTotal(int grandTotal) {
                this.grandTotal = grandTotal;
            }

            public String getProductId() {
                return productId;
            }

            public void setProductId(String productId) {
                this.productId = productId;
            }

            public String getImages() {
                return images;
            }

            public void setImages(String images) {
                this.images = images;
            }

            public String getMrpPrice() {
                return mrpPrice;
            }

            public void setMrpPrice(String mrpPrice) {
                this.mrpPrice = mrpPrice;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getPurchaseQuantity() {
                return purchaseQuantity;
            }

            public void setPurchaseQuantity(String purchaseQuantity) {
                this.purchaseQuantity = purchaseQuantity;
            }

            public String getUnitName() {
                return unitName;
            }

            public void setUnitName(String unitName) {
                this.unitName = unitName;
            }

            public int getDiscount() {
                return discount;
            }

            public void setDiscount(int discount) {
                this.discount = discount;
            }

            public List<ESSENTIALBean> getESSENTIAL() {
                return ESSENTIAL;
            }

            public void setESSENTIAL(List<ESSENTIALBean> ESSENTIAL) {
                this.ESSENTIAL = ESSENTIAL;
            }

            public static class ESSENTIALBean {
                /**
                 * pdtId : 21
                 * optId : 0
                 * pdtName : Tomato
                 * images : tomato1.png
                 * unitValue : 1
                 * unitName : Kg
                 * price : 50
                 */

                @SerializedName("pdtId")
                private String pdtId;
                @SerializedName("optId")
                private String optId;
                @SerializedName("pdtName")
                private String pdtName;
                @SerializedName("images")
                private String images;
                @SerializedName("unitValue")
                private String unitValue;
                @SerializedName("unitName")
                private String unitName;
                @SerializedName("price")
                private String price;

                public String getPdtId() {
                    return pdtId;
                }

                public void setPdtId(String pdtId) {
                    this.pdtId = pdtId;
                }

                public String getOptId() {
                    return optId;
                }

                public void setOptId(String optId) {
                    this.optId = optId;
                }

                public String getPdtName() {
                    return pdtName;
                }

                public void setPdtName(String pdtName) {
                    this.pdtName = pdtName;
                }

                public String getImages() {
                    return images;
                }

                public void setImages(String images) {
                    this.images = images;
                }

                public String getUnitValue() {
                    return unitValue;
                }

                public void setUnitValue(String unitValue) {
                    this.unitValue = unitValue;
                }

                public String getUnitName() {
                    return unitName;
                }

                public void setUnitName(String unitName) {
                    this.unitName = unitName;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }
            }
        }

        public static class PaymentGatewayBean {
            /**
             * id : 6
             * name : Pay Online
             * logo : images/gateway/1896px-Razorpay_logo.svg.png
             */

            @SerializedName("id")
            private String id;
            @SerializedName("name")
            private String name;
            @SerializedName("logo")
            private String logo;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }
        }
    }
}
