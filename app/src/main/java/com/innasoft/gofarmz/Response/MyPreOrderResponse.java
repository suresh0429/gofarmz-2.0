package com.innasoft.gofarmz.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyPreOrderResponse {


    /**
     * status : 10100
     * data : {"currency":"Rs. ","recordTotalCnt":42,"recordData":[{"id":"159","ref_no":"PRE27122019073","total_price":"699","shipping_charges":"40","grand_total":"739","pre_order_status":"Confirmed","created_date_time":"2019-12-27 18:33:42","images":"natufamily1.jpg"},{"id":"158","ref_no":"PRE27122019072","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 18:23:10","images":"natufamily.jpg"},{"id":"157","ref_no":"PRE27122019071","total_price":"699","shipping_charges":"40","grand_total":"739","pre_order_status":"Confirmed","created_date_time":"2019-12-27 18:04:25","images":"natufamily1.jpg"},{"id":"156","ref_no":"PRE27122019070","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 17:50:09","images":null},{"id":"155","ref_no":"PRE27122019069","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 17:47:23","images":null},{"id":"154","ref_no":"PRE27122019068","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 17:34:48","images":null},{"id":"153","ref_no":"PRE27122019067","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 17:19:49","images":null},{"id":"152","ref_no":"PRE27122019066","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 17:18:05","images":null},{"id":"151","ref_no":"PRE27122019065","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 17:16:08","images":null},{"id":"150","ref_no":"PRE27122019064","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 17:14:18","images":null},{"id":"149","ref_no":"PRE27122019063","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 17:12:58","images":null},{"id":"147","ref_no":"PRE27122019061","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 17:05:11","images":null},{"id":"146","ref_no":"PRE27122019060","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 17:00:34","images":null},{"id":"145","ref_no":"PRE27122019059","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:46:37","images":null},{"id":"144","ref_no":"PRE27122019058","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:45:28","images":null},{"id":"143","ref_no":"PRE27122019057","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:42:13","images":null},{"id":"142","ref_no":"PRE27122019056","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:29:17","images":null},{"id":"141","ref_no":"PRE27122019055","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:27:44","images":null},{"id":"140","ref_no":"PRE27122019054","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:26:41","images":null},{"id":"139","ref_no":"PRE27122019053","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:25:47","images":null},{"id":"138","ref_no":"PRE27122019052","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:24:50","images":null},{"id":"137","ref_no":"PRE27122019051","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:23:45","images":null},{"id":"135","ref_no":"PRE27122019049","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:15:54","images":null},{"id":"132","ref_no":"PRE27122019046","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:07:13","images":null},{"id":"130","ref_no":"PRE27122019044","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:04:35","images":null},{"id":"129","ref_no":"PRE27122019043","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:02:28","images":null},{"id":"127","ref_no":"PRE27122019041","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:00:41","images":null},{"id":"125","ref_no":"PRE27122019039","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 14:58:15","images":null},{"id":"123","ref_no":"PRE27122019037","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 14:49:42","images":null},{"id":"122","ref_no":"PRE27122019036","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 14:47:50","images":null},{"id":"107","ref_no":"PRE27122019021","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 13:12:20","images":null},{"id":"106","ref_no":"PRE27122019020","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 13:10:31","images":null},{"id":"105","ref_no":"PRE27122019019","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 13:05:48","images":null},{"id":"104","ref_no":"PRE27122019018","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 13:05:21","images":null},{"id":"100","ref_no":"PRE27122019014","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 12:50:48","images":null},{"id":"99","ref_no":"PRE27122019013","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 12:50:42","images":null},{"id":"98","ref_no":"PRE27122019012","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 12:50:40","images":null},{"id":"97","ref_no":"PRE27122019011","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 12:50:12","images":null},{"id":"94","ref_no":"PRE27122019008","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 12:38:47","images":null},{"id":"93","ref_no":"PRE27122019007","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 12:33:51","images":null},{"id":"90","ref_no":"PRE27122019004","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 11:52:38","images":null},{"id":"87","ref_no":"PRE27122019001","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 11:30:53","images":null}]}
     */

    @SerializedName("status")
    private String status;
    @SerializedName("data")
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * currency : Rs.
         * recordTotalCnt : 42
         * recordData : [{"id":"159","ref_no":"PRE27122019073","total_price":"699","shipping_charges":"40","grand_total":"739","pre_order_status":"Confirmed","created_date_time":"2019-12-27 18:33:42","images":"natufamily1.jpg"},{"id":"158","ref_no":"PRE27122019072","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 18:23:10","images":"natufamily.jpg"},{"id":"157","ref_no":"PRE27122019071","total_price":"699","shipping_charges":"40","grand_total":"739","pre_order_status":"Confirmed","created_date_time":"2019-12-27 18:04:25","images":"natufamily1.jpg"},{"id":"156","ref_no":"PRE27122019070","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 17:50:09","images":null},{"id":"155","ref_no":"PRE27122019069","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 17:47:23","images":null},{"id":"154","ref_no":"PRE27122019068","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 17:34:48","images":null},{"id":"153","ref_no":"PRE27122019067","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 17:19:49","images":null},{"id":"152","ref_no":"PRE27122019066","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 17:18:05","images":null},{"id":"151","ref_no":"PRE27122019065","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 17:16:08","images":null},{"id":"150","ref_no":"PRE27122019064","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 17:14:18","images":null},{"id":"149","ref_no":"PRE27122019063","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 17:12:58","images":null},{"id":"147","ref_no":"PRE27122019061","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 17:05:11","images":null},{"id":"146","ref_no":"PRE27122019060","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 17:00:34","images":null},{"id":"145","ref_no":"PRE27122019059","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:46:37","images":null},{"id":"144","ref_no":"PRE27122019058","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:45:28","images":null},{"id":"143","ref_no":"PRE27122019057","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:42:13","images":null},{"id":"142","ref_no":"PRE27122019056","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:29:17","images":null},{"id":"141","ref_no":"PRE27122019055","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:27:44","images":null},{"id":"140","ref_no":"PRE27122019054","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:26:41","images":null},{"id":"139","ref_no":"PRE27122019053","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:25:47","images":null},{"id":"138","ref_no":"PRE27122019052","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:24:50","images":null},{"id":"137","ref_no":"PRE27122019051","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:23:45","images":null},{"id":"135","ref_no":"PRE27122019049","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:15:54","images":null},{"id":"132","ref_no":"PRE27122019046","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:07:13","images":null},{"id":"130","ref_no":"PRE27122019044","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:04:35","images":null},{"id":"129","ref_no":"PRE27122019043","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:02:28","images":null},{"id":"127","ref_no":"PRE27122019041","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 15:00:41","images":null},{"id":"125","ref_no":"PRE27122019039","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 14:58:15","images":null},{"id":"123","ref_no":"PRE27122019037","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 14:49:42","images":null},{"id":"122","ref_no":"PRE27122019036","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 14:47:50","images":null},{"id":"107","ref_no":"PRE27122019021","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 13:12:20","images":null},{"id":"106","ref_no":"PRE27122019020","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 13:10:31","images":null},{"id":"105","ref_no":"PRE27122019019","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 13:05:48","images":null},{"id":"104","ref_no":"PRE27122019018","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 13:05:21","images":null},{"id":"100","ref_no":"PRE27122019014","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 12:50:48","images":null},{"id":"99","ref_no":"PRE27122019013","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 12:50:42","images":null},{"id":"98","ref_no":"PRE27122019012","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 12:50:40","images":null},{"id":"97","ref_no":"PRE27122019011","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 12:50:12","images":null},{"id":"94","ref_no":"PRE27122019008","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 12:38:47","images":null},{"id":"93","ref_no":"PRE27122019007","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 12:33:51","images":null},{"id":"90","ref_no":"PRE27122019004","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 11:52:38","images":null},{"id":"87","ref_no":"PRE27122019001","total_price":"499","shipping_charges":"40","grand_total":"539","pre_order_status":"Confirmed","created_date_time":"2019-12-27 11:30:53","images":null}]
         */

        @SerializedName("currency")
        private String currency;
        @SerializedName("recordTotalCnt")
        private int recordTotalCnt;
        @SerializedName("recordData")
        private List<RecordDataBean> recordData;

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public int getRecordTotalCnt() {
            return recordTotalCnt;
        }

        public void setRecordTotalCnt(int recordTotalCnt) {
            this.recordTotalCnt = recordTotalCnt;
        }

        public List<RecordDataBean> getRecordData() {
            return recordData;
        }

        public void setRecordData(List<RecordDataBean> recordData) {
            this.recordData = recordData;
        }

        public static class RecordDataBean {
            /**
             * id : 159
             * ref_no : PRE27122019073
             * total_price : 699
             * shipping_charges : 40
             * grand_total : 739
             * pre_order_status : Confirmed
             * created_date_time : 2019-12-27 18:33:42
             * images : natufamily1.jpg
             */

            @SerializedName("id")
            private String id;
            @SerializedName("ref_no")
            private String refNo;
            @SerializedName("total_price")
            private String totalPrice;
            @SerializedName("shipping_charges")
            private String shippingCharges;
            @SerializedName("grand_total")
            private String grandTotal;
            @SerializedName("pre_order_status")
            private String preOrderStatus;
            @SerializedName("created_date_time")
            private String createdDateTime;
            @SerializedName("images")
            private String images;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getRefNo() {
                return refNo;
            }

            public void setRefNo(String refNo) {
                this.refNo = refNo;
            }

            public String getTotalPrice() {
                return totalPrice;
            }

            public void setTotalPrice(String totalPrice) {
                this.totalPrice = totalPrice;
            }

            public String getShippingCharges() {
                return shippingCharges;
            }

            public void setShippingCharges(String shippingCharges) {
                this.shippingCharges = shippingCharges;
            }

            public String getGrandTotal() {
                return grandTotal;
            }

            public void setGrandTotal(String grandTotal) {
                this.grandTotal = grandTotal;
            }

            public String getPreOrderStatus() {
                return preOrderStatus;
            }

            public void setPreOrderStatus(String preOrderStatus) {
                this.preOrderStatus = preOrderStatus;
            }

            public String getCreatedDateTime() {
                return createdDateTime;
            }

            public void setCreatedDateTime(String createdDateTime) {
                this.createdDateTime = createdDateTime;
            }

            public String getImages() {
                return images;
            }

            public void setImages(String images) {
                this.images = images;
            }
        }
    }
}
