package com.innasoft.gofarmz.Response;

import java.util.List;

public class MyPreOrderDetailsResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * stateData : {"currency":"Rs. ","total_products":1,"order_details":{"ref_no":"PRE14062019007","total_price":"1","shipping_charges":"0","grand_total":"1","pincode":"500090","medium":"WEB","pre_order_status":"Confirmed","getaway_name":"Payu","order_date":"14-06-2019 06:31 pm"},"address":{"name":"Jay","mobile":"9949905540","alternate_contact_no":"","country":"India","state":"Telangana","city":"Hyderabad","area":"Nizampet","address":"Unnamed Road ","pincode":"500090"},"products":[{"product_id":"96","product_name":"Natural Cold Pressed Ground Nut Oil 1litre","image":"http://gofarmzv2.learningslot.in/timthumb.php?src=http://gofarmzv2.learningslot.in/images/products/pre_order/1554816739944_2019-04-09.png&h=120&w=165","expected_delivery_date":"24th Apr- 30th Apr","unit_price":"1.00","unit_value":"1 Litre","product_quantity":"1","total_price":"1.00","pre_order_status":"Confirmed"}]}
     */

    private String status;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * currency : Rs.
         * total_products : 1
         * order_details : {"ref_no":"PRE14062019007","total_price":"1","shipping_charges":"0","grand_total":"1","pincode":"500090","medium":"WEB","pre_order_status":"Confirmed","getaway_name":"Payu","order_date":"14-06-2019 06:31 pm"}
         * address : {"name":"Jay","mobile":"9949905540","alternate_contact_no":"","country":"India","state":"Telangana","city":"Hyderabad","area":"Nizampet","address":"Unnamed Road ","pincode":"500090"}
         * products : [{"product_id":"96","product_name":"Natural Cold Pressed Ground Nut Oil 1litre","image":"http://gofarmzv2.learningslot.in/timthumb.php?src=http://gofarmzv2.learningslot.in/images/products/pre_order/1554816739944_2019-04-09.png&h=120&w=165","expected_delivery_date":"24th Apr- 30th Apr","unit_price":"1.00","unit_value":"1 Litre","product_quantity":"1","total_price":"1.00","pre_order_status":"Confirmed"}]
         */

        private String currency;
        private int total_products;
        private OrderDetailsBean order_details;
        private AddressBean address;
        private List<ProductsBean> products;

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public int getTotal_products() {
            return total_products;
        }

        public void setTotal_products(int total_products) {
            this.total_products = total_products;
        }

        public OrderDetailsBean getOrder_details() {
            return order_details;
        }

        public void setOrder_details(OrderDetailsBean order_details) {
            this.order_details = order_details;
        }

        public AddressBean getAddress() {
            return address;
        }

        public void setAddress(AddressBean address) {
            this.address = address;
        }

        public List<ProductsBean> getProducts() {
            return products;
        }

        public void setProducts(List<ProductsBean> products) {
            this.products = products;
        }

        public static class OrderDetailsBean {
            /**
             * ref_no : PRE14062019007
             * total_price : 1
             * shipping_charges : 0
             * grand_total : 1
             * pincode : 500090
             * medium : WEB
             * pre_order_status : Confirmed
             * getaway_name : Payu
             * order_date : 14-06-2019 06:31 pm
             */

            private String ref_no;
            private String total_price;
            private String shipping_charges;
            private String grand_total;
            private String pincode;
            private String medium;
            private String pre_order_status;
            private String getaway_name;
            private String order_date;

            public String getRef_no() {
                return ref_no;
            }

            public void setRef_no(String ref_no) {
                this.ref_no = ref_no;
            }

            public String getTotal_price() {
                return total_price;
            }

            public void setTotal_price(String total_price) {
                this.total_price = total_price;
            }

            public String getShipping_charges() {
                return shipping_charges;
            }

            public void setShipping_charges(String shipping_charges) {
                this.shipping_charges = shipping_charges;
            }

            public String getGrand_total() {
                return grand_total;
            }

            public void setGrand_total(String grand_total) {
                this.grand_total = grand_total;
            }

            public String getPincode() {
                return pincode;
            }

            public void setPincode(String pincode) {
                this.pincode = pincode;
            }

            public String getMedium() {
                return medium;
            }

            public void setMedium(String medium) {
                this.medium = medium;
            }

            public String getPre_order_status() {
                return pre_order_status;
            }

            public void setPre_order_status(String pre_order_status) {
                this.pre_order_status = pre_order_status;
            }

            public String getGetaway_name() {
                return getaway_name;
            }

            public void setGetaway_name(String getaway_name) {
                this.getaway_name = getaway_name;
            }

            public String getOrder_date() {
                return order_date;
            }

            public void setOrder_date(String order_date) {
                this.order_date = order_date;
            }
        }

        public static class AddressBean {
            /**
             * name : Jay
             * mobile : 9949905540
             * alternate_contact_no :
             * country : India
             * state : Telangana
             * city : Hyderabad
             * area : Nizampet
             * address : Unnamed Road
             * pincode : 500090
             */

            private String name;
            private String mobile;
            private String alternate_contact_no;
            private String country;
            private String state;
            private String city;
            private String area;
            private String address;
            private String pincode;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getMobile() {
                return mobile;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public String getAlternate_contact_no() {
                return alternate_contact_no;
            }

            public void setAlternate_contact_no(String alternate_contact_no) {
                this.alternate_contact_no = alternate_contact_no;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getPincode() {
                return pincode;
            }

            public void setPincode(String pincode) {
                this.pincode = pincode;
            }
        }

        public static class ProductsBean {
            /**
             * product_id : 96
             * product_name : Natural Cold Pressed Ground Nut Oil 1litre
             * image : http://gofarmzv2.learningslot.in/timthumb.php?src=http://gofarmzv2.learningslot.in/images/products/pre_order/1554816739944_2019-04-09.png&h=120&w=165
             * expected_delivery_date : 24th Apr- 30th Apr
             * unit_price : 1.00
             * unit_value : 1 Litre
             * product_quantity : 1
             * total_price : 1.00
             * pre_order_status : Confirmed
             */

            private String product_id;
            private String product_name;
            private String image;
            private String expected_delivery_date;
            private String unit_price;
            private String unit_value;
            private String product_quantity;
            private String total_price;
            private String pre_order_status;

            public String getProduct_id() {
                return product_id;
            }

            public void setProduct_id(String product_id) {
                this.product_id = product_id;
            }

            public String getProduct_name() {
                return product_name;
            }

            public void setProduct_name(String product_name) {
                this.product_name = product_name;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getExpected_delivery_date() {
                return expected_delivery_date;
            }

            public void setExpected_delivery_date(String expected_delivery_date) {
                this.expected_delivery_date = expected_delivery_date;
            }

            public String getUnit_price() {
                return unit_price;
            }

            public void setUnit_price(String unit_price) {
                this.unit_price = unit_price;
            }

            public String getUnit_value() {
                return unit_value;
            }

            public void setUnit_value(String unit_value) {
                this.unit_value = unit_value;
            }

            public String getProduct_quantity() {
                return product_quantity;
            }

            public void setProduct_quantity(String product_quantity) {
                this.product_quantity = product_quantity;
            }

            public String getTotal_price() {
                return total_price;
            }

            public void setTotal_price(String total_price) {
                this.total_price = total_price;
            }

            public String getPre_order_status() {
                return pre_order_status;
            }

            public void setPre_order_status(String pre_order_status) {
                this.pre_order_status = pre_order_status;
            }
        }
    }
}
