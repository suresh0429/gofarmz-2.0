package com.innasoft.gofarmz.Response;

public class PreOrderProductResponse {


    /**
     * status : 10100
     * versions : {"android_version":"1.9.13","ios_version":"1.4.3"}
     * message : Pre-order product details
     * stateData : {"product_id":"96","pop_image":"http://gofarmzv2.learningslot.in/timthumb.php?src=http://gofarmzv2.learningslot.in/images/products/pre_order/1554816739944_2019-04-09.png&h=400","pop_expected_delivery_date":"24th Apr- 30th Apr","pop_description":"This Natural Cold Pressed Groundnut Oils are from Anantha Naturals collective. It is completely produced by Natural, Chemical-free groundnuts using low RPM method preserving the nutrients, aroma and taste. Highly recommended for Indian cooking. No preservatives added, nil chemical treatment.\r\n\r\nAnantha Naturals is working towards promoting Zero Budget Natural Farming (Desi cow based methods by Padmashri Sri Subhash Palekarji) and Ecologically Sustainable Agriculture through innovative systems, education and through favorable marketing of natural farming produce. Who are we: We are a team of ZBNF farmers and professionals from various sectors like IT, Engineering, Finance and Service. We are all from agriculture families and have passion to revitalize agriculture. We are working full time to make agriculture a profitable, dignified and joyable occupation especially for small and marginable farmers.","pop_unit_price":"1.00","pop_unit_value":"1 Litre","pop_available_units":"33","priority":"2","product_name":"Natural Cold Pressed Ground Nut Oil 1litre","quantity":"0"}
     * cart_count : 0
     */

    private String status;
    private VersionsBean versions;
    private String message;
    private DataBean data;
    private int cart_count;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public VersionsBean getVersions() {
        return versions;
    }

    public void setVersions(VersionsBean versions) {
        this.versions = versions;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public int getCart_count() {
        return cart_count;
    }

    public void setCart_count(int cart_count) {
        this.cart_count = cart_count;
    }

    public static class VersionsBean {
        /**
         * android_version : 1.9.13
         * ios_version : 1.4.3
         */

        private String android_version;
        private String ios_version;

        public String getAndroid_version() {
            return android_version;
        }

        public void setAndroid_version(String android_version) {
            this.android_version = android_version;
        }

        public String getIos_version() {
            return ios_version;
        }

        public void setIos_version(String ios_version) {
            this.ios_version = ios_version;
        }
    }

    public static class DataBean {
        /**
         * product_id : 96
         * pop_image : http://gofarmzv2.learningslot.in/timthumb.php?src=http://gofarmzv2.learningslot.in/images/products/pre_order/1554816739944_2019-04-09.png&h=400
         * pop_expected_delivery_date : 24th Apr- 30th Apr
         * pop_description : This Natural Cold Pressed Groundnut Oils are from Anantha Naturals collective. It is completely produced by Natural, Chemical-free groundnuts using low RPM method preserving the nutrients, aroma and taste. Highly recommended for Indian cooking. No preservatives added, nil chemical treatment.

         Anantha Naturals is working towards promoting Zero Budget Natural Farming (Desi cow based methods by Padmashri Sri Subhash Palekarji) and Ecologically Sustainable Agriculture through innovative systems, education and through favorable marketing of natural farming produce. Who are we: We are a team of ZBNF farmers and professionals from various sectors like IT, Engineering, Finance and Service. We are all from agriculture families and have passion to revitalize agriculture. We are working full time to make agriculture a profitable, dignified and joyable occupation especially for small and marginable farmers.
         * pop_unit_price : 1.00
         * pop_unit_value : 1 Litre
         * pop_available_units : 33
         * priority : 2
         * product_name : Natural Cold Pressed Ground Nut Oil 1litre
         * quantity : 0
         */

        private String product_id;
        private String pop_image;
        private String pop_expected_delivery_date;
        private String pop_description;
        private String pop_unit_price;
        private String pop_unit_value;
        private String pop_available_units;
        private String priority;
        private String product_name;
        private String quantity;

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getPop_image() {
            return pop_image;
        }

        public void setPop_image(String pop_image) {
            this.pop_image = pop_image;
        }

        public String getPop_expected_delivery_date() {
            return pop_expected_delivery_date;
        }

        public void setPop_expected_delivery_date(String pop_expected_delivery_date) {
            this.pop_expected_delivery_date = pop_expected_delivery_date;
        }

        public String getPop_description() {
            return pop_description;
        }

        public void setPop_description(String pop_description) {
            this.pop_description = pop_description;
        }

        public String getPop_unit_price() {
            return pop_unit_price;
        }

        public void setPop_unit_price(String pop_unit_price) {
            this.pop_unit_price = pop_unit_price;
        }

        public String getPop_unit_value() {
            return pop_unit_value;
        }

        public void setPop_unit_value(String pop_unit_value) {
            this.pop_unit_value = pop_unit_value;
        }

        public String getPop_available_units() {
            return pop_available_units;
        }

        public void setPop_available_units(String pop_available_units) {
            this.pop_available_units = pop_available_units;
        }

        public String getPriority() {
            return priority;
        }

        public void setPriority(String priority) {
            this.priority = priority;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }
    }
}
