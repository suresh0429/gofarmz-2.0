package com.innasoft.gofarmz.Response;

public class PreOrderUpdateCartResponse {

    /**
     * status : 10100
     * message : Cart updated successfully.
     * cart_count : 1
     */

    private String status;
    private String message;
    private int cart_count;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCart_count() {
        return cart_count;
    }

    public void setCart_count(int cart_count) {
        this.cart_count = cart_count;
    }
}
