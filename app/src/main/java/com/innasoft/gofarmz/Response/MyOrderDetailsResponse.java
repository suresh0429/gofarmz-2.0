package com.innasoft.gofarmz.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyOrderDetailsResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : {"currency":"Rs. ","total_products":1,"order_details":{"reference_id":"REF20200121025514","order_confirmation_reference_id":"GFZ2020012102551492","total_mrp_price":"300","discount":"0","shipping_charges":"25","final_price":"325","cancel_charges":"0","refund_price":"0","pincode":"500072","medium":"ANDROID","order_status":"Confirmed","status":"1","getaway_name":"Cash on Delivery","ip_address":"157.48.62.73","expected_delivery":"28-01-2020","expected_delivery_slot":"08:00 AM - 12:30 PM","order_date":"21-01-2020 02:55 pm"},"address":{"name":"suresh kumar","mobile":"2828283839","alternate_contact_no":"","country":"India","state":"Telangana","city":"Hyderabad","area":"Kukatpally","address":"223-rtutcycyvuvutdtdycycuvugugi","pincode":"500072"},"status":[{"order_status":"Confirmed","message":"Order has been Confirmed","created_on":"2020-01-21 14:55:14"}],"products":[{"id":"3406","orders_pid":"3359","product_sku_id":"PQQE85137","product_id":"20","product_name":"Small Basket","count_name":"","images":"vegetable_box.png","product_mrp_price":"300","purchase_quantity":"1","total_price":"300","grand_total":"300","status":"1","child_products":[{"type":"ESSENTIAL","product_name":"Tomato","unit_name":"Kg","unit_value":"1","price":"50","discount":"0","final_price":"50","quantity":"1","created_on":"2020-01-21 14:55:14","images":"tomato1.png"},{"type":"ESSENTIAL","product_name":"Onions","unit_name":"Kg","unit_value":"1","price":"60","discount":"0","final_price":"60","quantity":"1","created_on":"2020-01-21 14:55:14","images":"onions.png"},{"type":"ESSENTIAL","product_name":"Green Chillies (100 gms)","unit_name":"gms","unit_value":"100","price":"9","discount":"0","final_price":"9","quantity":"1","created_on":"2020-01-21 14:55:14","images":"green_chillies.png"},{"type":"ESSENTIAL","product_name":"Potato (500 gms)","unit_name":"gms","unit_value":"500","price":"30","discount":"0","final_price":"30","quantity":"1","created_on":"2020-01-21 14:55:14","images":"potato1.png"},{"type":"ESSENTIAL","product_name":"Carrot (250 gms)","unit_name":"gms","unit_value":"250","price":"26","discount":"0","final_price":"26","quantity":"1","created_on":"2020-01-21 14:55:14","images":"carrot.png"},{"type":"ESSENTIAL","product_name":"Ginger (100 gms)","unit_name":"gms","unit_value":"100","price":"20","discount":"0","final_price":"20","quantity":"1","created_on":"2020-01-21 14:55:14","images":"ginger.png"},{"type":"ESSENTIAL","product_name":"Garlic (50 gms)","unit_name":"gms","unit_value":"50","price":"11","discount":"0","final_price":"11","quantity":"1","created_on":"2020-01-21 14:55:14","images":"garlic.png"},{"type":"ESSENTIAL","product_name":"Lady Finger (500 gms)","unit_name":"gms","unit_value":"500","price":"34","discount":"0","final_price":"34","quantity":"1","created_on":"2020-01-21 14:55:14","images":"ladies_finger.png"},{"type":"ESSENTIAL","product_name":"Cluster Beans (500 gms)","unit_name":"gms","unit_value":"500","price":"31","discount":"0","final_price":"31","quantity":"1","created_on":"2020-01-21 14:55:14","images":"cluster_beans.png"},{"type":"ESSENTIAL","product_name":"Curry Leaves","unit_name":"Bunch","unit_value":"1","price":"10","discount":"0","final_price":"10","quantity":"1","created_on":"2020-01-21 14:55:14","images":"curry_leaf.png"},{"type":"ESSENTIAL","product_name":"Bottle Gourd","unit_name":"Pieces","unit_value":"1","price":"30","discount":"0","final_price":"30","quantity":"1","created_on":"2020-01-21 14:55:14","images":"bottle_gourd.png"},{"type":"ESSENTIAL","product_name":"Ridge Gourd (birakaya) (500 gms)","unit_name":"gms","unit_value":"500","price":"32","discount":"0","final_price":"32","quantity":"1","created_on":"2020-01-21 14:55:14","images":"Ridge_gourd.jpg"}]}]}
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * currency : Rs.
         * total_products : 1
         * order_details : {"reference_id":"REF20200121025514","order_confirmation_reference_id":"GFZ2020012102551492","total_mrp_price":"300","discount":"0","shipping_charges":"25","final_price":"325","cancel_charges":"0","refund_price":"0","pincode":"500072","medium":"ANDROID","order_status":"Confirmed","status":"1","getaway_name":"Cash on Delivery","ip_address":"157.48.62.73","expected_delivery":"28-01-2020","expected_delivery_slot":"08:00 AM - 12:30 PM","order_date":"21-01-2020 02:55 pm"}
         * address : {"name":"suresh kumar","mobile":"2828283839","alternate_contact_no":"","country":"India","state":"Telangana","city":"Hyderabad","area":"Kukatpally","address":"223-rtutcycyvuvutdtdycycuvugugi","pincode":"500072"}
         * status : [{"order_status":"Confirmed","message":"Order has been Confirmed","created_on":"2020-01-21 14:55:14"}]
         * products : [{"id":"3406","orders_pid":"3359","product_sku_id":"PQQE85137","product_id":"20","product_name":"Small Basket","count_name":"","images":"vegetable_box.png","product_mrp_price":"300","purchase_quantity":"1","total_price":"300","grand_total":"300","status":"1","child_products":[{"type":"ESSENTIAL","product_name":"Tomato","unit_name":"Kg","unit_value":"1","price":"50","discount":"0","final_price":"50","quantity":"1","created_on":"2020-01-21 14:55:14","images":"tomato1.png"},{"type":"ESSENTIAL","product_name":"Onions","unit_name":"Kg","unit_value":"1","price":"60","discount":"0","final_price":"60","quantity":"1","created_on":"2020-01-21 14:55:14","images":"onions.png"},{"type":"ESSENTIAL","product_name":"Green Chillies (100 gms)","unit_name":"gms","unit_value":"100","price":"9","discount":"0","final_price":"9","quantity":"1","created_on":"2020-01-21 14:55:14","images":"green_chillies.png"},{"type":"ESSENTIAL","product_name":"Potato (500 gms)","unit_name":"gms","unit_value":"500","price":"30","discount":"0","final_price":"30","quantity":"1","created_on":"2020-01-21 14:55:14","images":"potato1.png"},{"type":"ESSENTIAL","product_name":"Carrot (250 gms)","unit_name":"gms","unit_value":"250","price":"26","discount":"0","final_price":"26","quantity":"1","created_on":"2020-01-21 14:55:14","images":"carrot.png"},{"type":"ESSENTIAL","product_name":"Ginger (100 gms)","unit_name":"gms","unit_value":"100","price":"20","discount":"0","final_price":"20","quantity":"1","created_on":"2020-01-21 14:55:14","images":"ginger.png"},{"type":"ESSENTIAL","product_name":"Garlic (50 gms)","unit_name":"gms","unit_value":"50","price":"11","discount":"0","final_price":"11","quantity":"1","created_on":"2020-01-21 14:55:14","images":"garlic.png"},{"type":"ESSENTIAL","product_name":"Lady Finger (500 gms)","unit_name":"gms","unit_value":"500","price":"34","discount":"0","final_price":"34","quantity":"1","created_on":"2020-01-21 14:55:14","images":"ladies_finger.png"},{"type":"ESSENTIAL","product_name":"Cluster Beans (500 gms)","unit_name":"gms","unit_value":"500","price":"31","discount":"0","final_price":"31","quantity":"1","created_on":"2020-01-21 14:55:14","images":"cluster_beans.png"},{"type":"ESSENTIAL","product_name":"Curry Leaves","unit_name":"Bunch","unit_value":"1","price":"10","discount":"0","final_price":"10","quantity":"1","created_on":"2020-01-21 14:55:14","images":"curry_leaf.png"},{"type":"ESSENTIAL","product_name":"Bottle Gourd","unit_name":"Pieces","unit_value":"1","price":"30","discount":"0","final_price":"30","quantity":"1","created_on":"2020-01-21 14:55:14","images":"bottle_gourd.png"},{"type":"ESSENTIAL","product_name":"Ridge Gourd (birakaya) (500 gms)","unit_name":"gms","unit_value":"500","price":"32","discount":"0","final_price":"32","quantity":"1","created_on":"2020-01-21 14:55:14","images":"Ridge_gourd.jpg"}]}]
         */

        @SerializedName("currency")
        private String currency;
        @SerializedName("total_products")
        private int totalProducts;
        @SerializedName("order_details")
        private OrderDetailsBean orderDetails;
        @SerializedName("address")
        private AddressBean address;
        @SerializedName("status")
        private List<StatusBean> status;
        @SerializedName("products")
        private List<ProductsBean> products;

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public int getTotalProducts() {
            return totalProducts;
        }

        public void setTotalProducts(int totalProducts) {
            this.totalProducts = totalProducts;
        }

        public OrderDetailsBean getOrderDetails() {
            return orderDetails;
        }

        public void setOrderDetails(OrderDetailsBean orderDetails) {
            this.orderDetails = orderDetails;
        }

        public AddressBean getAddress() {
            return address;
        }

        public void setAddress(AddressBean address) {
            this.address = address;
        }

        public List<StatusBean> getStatus() {
            return status;
        }

        public void setStatus(List<StatusBean> status) {
            this.status = status;
        }

        public List<ProductsBean> getProducts() {
            return products;
        }

        public void setProducts(List<ProductsBean> products) {
            this.products = products;
        }

        public static class OrderDetailsBean {
            /**
             * reference_id : REF20200121025514
             * order_confirmation_reference_id : GFZ2020012102551492
             * total_mrp_price : 300
             * discount : 0
             * shipping_charges : 25
             * final_price : 325
             * cancel_charges : 0
             * refund_price : 0
             * pincode : 500072
             * medium : ANDROID
             * order_status : Confirmed
             * status : 1
             * getaway_name : Cash on Delivery
             * ip_address : 157.48.62.73
             * expected_delivery : 28-01-2020
             * expected_delivery_slot : 08:00 AM - 12:30 PM
             * order_date : 21-01-2020 02:55 pm
             */

            @SerializedName("reference_id")
            private String referenceId;
            @SerializedName("order_confirmation_reference_id")
            private String orderConfirmationReferenceId;
            @SerializedName("total_mrp_price")
            private String totalMrpPrice;
            @SerializedName("discount")
            private String discount;
            @SerializedName("shipping_charges")
            private String shippingCharges;
            @SerializedName("final_price")
            private String finalPrice;
            @SerializedName("cancel_charges")
            private String cancelCharges;
            @SerializedName("refund_price")
            private String refundPrice;
            @SerializedName("pincode")
            private String pincode;
            @SerializedName("medium")
            private String medium;
            @SerializedName("order_status")
            private String orderStatus;
            @SerializedName("status")
            private String status;
            @SerializedName("getaway_name")
            private String getawayName;
            @SerializedName("ip_address")
            private String ipAddress;
            @SerializedName("expected_delivery")
            private String expectedDelivery;
            @SerializedName("expected_delivery_slot")
            private String expectedDeliverySlot;
            @SerializedName("order_date")
            private String orderDate;

            public String getReferenceId() {
                return referenceId;
            }

            public void setReferenceId(String referenceId) {
                this.referenceId = referenceId;
            }

            public String getOrderConfirmationReferenceId() {
                return orderConfirmationReferenceId;
            }

            public void setOrderConfirmationReferenceId(String orderConfirmationReferenceId) {
                this.orderConfirmationReferenceId = orderConfirmationReferenceId;
            }

            public String getTotalMrpPrice() {
                return totalMrpPrice;
            }

            public void setTotalMrpPrice(String totalMrpPrice) {
                this.totalMrpPrice = totalMrpPrice;
            }

            public String getDiscount() {
                return discount;
            }

            public void setDiscount(String discount) {
                this.discount = discount;
            }

            public String getShippingCharges() {
                return shippingCharges;
            }

            public void setShippingCharges(String shippingCharges) {
                this.shippingCharges = shippingCharges;
            }

            public String getFinalPrice() {
                return finalPrice;
            }

            public void setFinalPrice(String finalPrice) {
                this.finalPrice = finalPrice;
            }

            public String getCancelCharges() {
                return cancelCharges;
            }

            public void setCancelCharges(String cancelCharges) {
                this.cancelCharges = cancelCharges;
            }

            public String getRefundPrice() {
                return refundPrice;
            }

            public void setRefundPrice(String refundPrice) {
                this.refundPrice = refundPrice;
            }

            public String getPincode() {
                return pincode;
            }

            public void setPincode(String pincode) {
                this.pincode = pincode;
            }

            public String getMedium() {
                return medium;
            }

            public void setMedium(String medium) {
                this.medium = medium;
            }

            public String getOrderStatus() {
                return orderStatus;
            }

            public void setOrderStatus(String orderStatus) {
                this.orderStatus = orderStatus;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getGetawayName() {
                return getawayName;
            }

            public void setGetawayName(String getawayName) {
                this.getawayName = getawayName;
            }

            public String getIpAddress() {
                return ipAddress;
            }

            public void setIpAddress(String ipAddress) {
                this.ipAddress = ipAddress;
            }

            public String getExpectedDelivery() {
                return expectedDelivery;
            }

            public void setExpectedDelivery(String expectedDelivery) {
                this.expectedDelivery = expectedDelivery;
            }

            public String getExpectedDeliverySlot() {
                return expectedDeliverySlot;
            }

            public void setExpectedDeliverySlot(String expectedDeliverySlot) {
                this.expectedDeliverySlot = expectedDeliverySlot;
            }

            public String getOrderDate() {
                return orderDate;
            }

            public void setOrderDate(String orderDate) {
                this.orderDate = orderDate;
            }
        }

        public static class AddressBean {
            /**
             * name : suresh kumar
             * mobile : 2828283839
             * alternate_contact_no :
             * country : India
             * state : Telangana
             * city : Hyderabad
             * area : Kukatpally
             * address : 223-rtutcycyvuvutdtdycycuvugugi
             * pincode : 500072
             */

            @SerializedName("name")
            private String name;
            @SerializedName("mobile")
            private String mobile;
            @SerializedName("alternate_contact_no")
            private String alternateContactNo;
            @SerializedName("country")
            private String country;
            @SerializedName("state")
            private String state;
            @SerializedName("city")
            private String city;
            @SerializedName("area")
            private String area;
            @SerializedName("address")
            private String address;
            @SerializedName("pincode")
            private String pincode;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getMobile() {
                return mobile;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public String getAlternateContactNo() {
                return alternateContactNo;
            }

            public void setAlternateContactNo(String alternateContactNo) {
                this.alternateContactNo = alternateContactNo;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getPincode() {
                return pincode;
            }

            public void setPincode(String pincode) {
                this.pincode = pincode;
            }
        }

        public static class StatusBean {
            /**
             * order_status : Confirmed
             * message : Order has been Confirmed
             * created_on : 2020-01-21 14:55:14
             */

            @SerializedName("order_status")
            private String orderStatus;
            @SerializedName("message")
            private String message;
            @SerializedName("created_on")
            private String createdOn;

            public String getOrderStatus() {
                return orderStatus;
            }

            public void setOrderStatus(String orderStatus) {
                this.orderStatus = orderStatus;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public String getCreatedOn() {
                return createdOn;
            }

            public void setCreatedOn(String createdOn) {
                this.createdOn = createdOn;
            }
        }

        public static class ProductsBean {
            /**
             * id : 3406
             * orders_pid : 3359
             * product_sku_id : PQQE85137
             * product_id : 20
             * product_name : Small Basket
             * count_name :
             * images : vegetable_box.png
             * product_mrp_price : 300
             * purchase_quantity : 1
             * total_price : 300
             * grand_total : 300
             * status : 1
             * child_products : [{"type":"ESSENTIAL","product_name":"Tomato","unit_name":"Kg","unit_value":"1","price":"50","discount":"0","final_price":"50","quantity":"1","created_on":"2020-01-21 14:55:14","images":"tomato1.png"},{"type":"ESSENTIAL","product_name":"Onions","unit_name":"Kg","unit_value":"1","price":"60","discount":"0","final_price":"60","quantity":"1","created_on":"2020-01-21 14:55:14","images":"onions.png"},{"type":"ESSENTIAL","product_name":"Green Chillies (100 gms)","unit_name":"gms","unit_value":"100","price":"9","discount":"0","final_price":"9","quantity":"1","created_on":"2020-01-21 14:55:14","images":"green_chillies.png"},{"type":"ESSENTIAL","product_name":"Potato (500 gms)","unit_name":"gms","unit_value":"500","price":"30","discount":"0","final_price":"30","quantity":"1","created_on":"2020-01-21 14:55:14","images":"potato1.png"},{"type":"ESSENTIAL","product_name":"Carrot (250 gms)","unit_name":"gms","unit_value":"250","price":"26","discount":"0","final_price":"26","quantity":"1","created_on":"2020-01-21 14:55:14","images":"carrot.png"},{"type":"ESSENTIAL","product_name":"Ginger (100 gms)","unit_name":"gms","unit_value":"100","price":"20","discount":"0","final_price":"20","quantity":"1","created_on":"2020-01-21 14:55:14","images":"ginger.png"},{"type":"ESSENTIAL","product_name":"Garlic (50 gms)","unit_name":"gms","unit_value":"50","price":"11","discount":"0","final_price":"11","quantity":"1","created_on":"2020-01-21 14:55:14","images":"garlic.png"},{"type":"ESSENTIAL","product_name":"Lady Finger (500 gms)","unit_name":"gms","unit_value":"500","price":"34","discount":"0","final_price":"34","quantity":"1","created_on":"2020-01-21 14:55:14","images":"ladies_finger.png"},{"type":"ESSENTIAL","product_name":"Cluster Beans (500 gms)","unit_name":"gms","unit_value":"500","price":"31","discount":"0","final_price":"31","quantity":"1","created_on":"2020-01-21 14:55:14","images":"cluster_beans.png"},{"type":"ESSENTIAL","product_name":"Curry Leaves","unit_name":"Bunch","unit_value":"1","price":"10","discount":"0","final_price":"10","quantity":"1","created_on":"2020-01-21 14:55:14","images":"curry_leaf.png"},{"type":"ESSENTIAL","product_name":"Bottle Gourd","unit_name":"Pieces","unit_value":"1","price":"30","discount":"0","final_price":"30","quantity":"1","created_on":"2020-01-21 14:55:14","images":"bottle_gourd.png"},{"type":"ESSENTIAL","product_name":"Ridge Gourd (birakaya) (500 gms)","unit_name":"gms","unit_value":"500","price":"32","discount":"0","final_price":"32","quantity":"1","created_on":"2020-01-21 14:55:14","images":"Ridge_gourd.jpg"}]
             */

            @SerializedName("id")
            private String id;
            @SerializedName("orders_pid")
            private String ordersPid;
            @SerializedName("product_sku_id")
            private String productSkuId;
            @SerializedName("product_id")
            private String productId;
            @SerializedName("product_name")
            private String productName;
            @SerializedName("count_name")
            private String countName;
            @SerializedName("images")
            private String images;
            @SerializedName("product_mrp_price")
            private String productMrpPrice;
            @SerializedName("purchase_quantity")
            private String purchaseQuantity;
            @SerializedName("total_price")
            private String totalPrice;
            @SerializedName("grand_total")
            private String grandTotal;
            @SerializedName("status")
            private String status;
            @SerializedName("child_products")
            private List<ChildProductsBean> childProducts;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getOrdersPid() {
                return ordersPid;
            }

            public void setOrdersPid(String ordersPid) {
                this.ordersPid = ordersPid;
            }

            public String getProductSkuId() {
                return productSkuId;
            }

            public void setProductSkuId(String productSkuId) {
                this.productSkuId = productSkuId;
            }

            public String getProductId() {
                return productId;
            }

            public void setProductId(String productId) {
                this.productId = productId;
            }

            public String getProductName() {
                return productName;
            }

            public void setProductName(String productName) {
                this.productName = productName;
            }

            public String getCountName() {
                return countName;
            }

            public void setCountName(String countName) {
                this.countName = countName;
            }

            public String getImages() {
                return images;
            }

            public void setImages(String images) {
                this.images = images;
            }

            public String getProductMrpPrice() {
                return productMrpPrice;
            }

            public void setProductMrpPrice(String productMrpPrice) {
                this.productMrpPrice = productMrpPrice;
            }

            public String getPurchaseQuantity() {
                return purchaseQuantity;
            }

            public void setPurchaseQuantity(String purchaseQuantity) {
                this.purchaseQuantity = purchaseQuantity;
            }

            public String getTotalPrice() {
                return totalPrice;
            }

            public void setTotalPrice(String totalPrice) {
                this.totalPrice = totalPrice;
            }

            public String getGrandTotal() {
                return grandTotal;
            }

            public void setGrandTotal(String grandTotal) {
                this.grandTotal = grandTotal;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public List<ChildProductsBean> getChildProducts() {
                return childProducts;
            }

            public void setChildProducts(List<ChildProductsBean> childProducts) {
                this.childProducts = childProducts;
            }

            public static class ChildProductsBean {
                /**
                 * type : ESSENTIAL
                 * product_name : Tomato
                 * unit_name : Kg
                 * unit_value : 1
                 * price : 50
                 * discount : 0
                 * final_price : 50
                 * quantity : 1
                 * created_on : 2020-01-21 14:55:14
                 * images : tomato1.png
                 */

                @SerializedName("type")
                private String type;
                @SerializedName("product_name")
                private String productName;
                @SerializedName("unit_name")
                private String unitName;
                @SerializedName("unit_value")
                private String unitValue;
                @SerializedName("price")
                private String price;
                @SerializedName("discount")
                private String discount;
                @SerializedName("final_price")
                private String finalPrice;
                @SerializedName("quantity")
                private String quantity;
                @SerializedName("created_on")
                private String createdOn;
                @SerializedName("images")
                private String images;

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public String getProductName() {
                    return productName;
                }

                public void setProductName(String productName) {
                    this.productName = productName;
                }

                public String getUnitName() {
                    return unitName;
                }

                public void setUnitName(String unitName) {
                    this.unitName = unitName;
                }

                public String getUnitValue() {
                    return unitValue;
                }

                public void setUnitValue(String unitValue) {
                    this.unitValue = unitValue;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }

                public String getDiscount() {
                    return discount;
                }

                public void setDiscount(String discount) {
                    this.discount = discount;
                }

                public String getFinalPrice() {
                    return finalPrice;
                }

                public void setFinalPrice(String finalPrice) {
                    this.finalPrice = finalPrice;
                }

                public String getQuantity() {
                    return quantity;
                }

                public void setQuantity(String quantity) {
                    this.quantity = quantity;
                }

                public String getCreatedOn() {
                    return createdOn;
                }

                public void setCreatedOn(String createdOn) {
                    this.createdOn = createdOn;
                }

                public String getImages() {
                    return images;
                }

                public void setImages(String images) {
                    this.images = images;
                }
            }
        }
    }
}
