package com.innasoft.gofarmz.Response;

import java.util.List;

public class PromoCodeResponse {

    /**
     * status : 10100
     * message : Data fetch successfully.
     * stateData : [{"code":"GOFIRST","name":"10% discount on first order","description":"<p>\n\t1. Get 10%&nbsp; discount<\/p>\n<p>\n\t2. Only 1 time / user<\/p>\n"}]
     */

    private String status;
    private String message;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * code : GOFIRST
         * name : 10% discount on first order
         * description : <p>
         1. Get 10%&nbsp; discount</p>
         <p>
         2. Only 1 time / user</p>

         */

        private String code;
        private String name;
        private String description;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}
