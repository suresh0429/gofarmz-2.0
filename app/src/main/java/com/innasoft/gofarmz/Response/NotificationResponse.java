package com.innasoft.gofarmz.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationResponse {


    /**
     * status : 10100
     * message : Data fetch successfully.
     * data : [{"id":"774","type":"PUSH","msg_title":"test","msg_txt":"cfdvdf","msg_image":"https://www.innasoft.in/gofarmz/uploads/8012d-7c06979.png","users":"7456","is_read":"0","response":"","created_on":"2020-01-28 15:19:06"},{"id":"772","type":"PUSH","msg_title":"test","msg_txt":"test","msg_image":"","users":"7456","is_read":"0","response":"","created_on":"2020-01-28 11:23:22"},{"id":"771","type":"PUSH","msg_title":"testre","msg_txt":"xcxc","msg_image":"","users":"7456","is_read":"0","response":"","created_on":"2020-01-28 11:22:01"},{"id":"770","type":"PUSH","msg_title":"testre","msg_txt":"xcxc","msg_image":"","users":"7456","is_read":"0","response":"","created_on":"2020-01-28 11:22:01"},{"id":"769","type":"PUSH","msg_title":"test","msg_txt":"asasas","msg_image":"","users":"7456","is_read":"0","response":"","created_on":"2020-01-28 11:13:16"},{"id":"767","type":"PUSH","msg_title":"test","msg_txt":"test","msg_image":"https://www.innasoft.in/gofarmz/uploads/48295-7c06979.png","users":"7456","is_read":"0","response":"","created_on":"2020-01-27 17:42:48"},{"id":"766","type":"PUSH","msg_title":"suressh","msg_txt":"dd","msg_image":"","users":"7456","is_read":"0","response":"","created_on":"2020-01-27 17:07:19"},{"id":"765","type":"PUSH","msg_title":"test","msg_txt":"test","msg_image":"","users":"7456","is_read":"0","response":"","created_on":"2020-01-27 16:05:32"},{"id":"764","type":"PUSH","msg_title":"title","msg_txt":"Hi suresh..........","msg_image":"","users":"7456","is_read":"0","response":"","created_on":"2020-01-23 15:07:36"}]
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 774
         * type : PUSH
         * msg_title : test
         * msg_txt : cfdvdf
         * msg_image : https://www.innasoft.in/gofarmz/uploads/8012d-7c06979.png
         * users : 7456
         * is_read : 0
         * response :
         * created_on : 2020-01-28 15:19:06
         */

        @SerializedName("id")
        private String id;
        @SerializedName("type")
        private String type;
        @SerializedName("msg_title")
        private String msgTitle;
        @SerializedName("msg_txt")
        private String msgTxt;
        @SerializedName("msg_image")
        private String msgImage;
        @SerializedName("users")
        private String users;
        @SerializedName("is_read")
        private String isRead;
        @SerializedName("response")
        private String response;
        @SerializedName("created_on")
        private String createdOn;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getMsgTitle() {
            return msgTitle;
        }

        public void setMsgTitle(String msgTitle) {
            this.msgTitle = msgTitle;
        }

        public String getMsgTxt() {
            return msgTxt;
        }

        public void setMsgTxt(String msgTxt) {
            this.msgTxt = msgTxt;
        }

        public String getMsgImage() {
            return msgImage;
        }

        public void setMsgImage(String msgImage) {
            this.msgImage = msgImage;
        }

        public String getUsers() {
            return users;
        }

        public void setUsers(String users) {
            this.users = users;
        }

        public String getIsRead() {
            return isRead;
        }

        public void setIsRead(String isRead) {
            this.isRead = isRead;
        }

        public String getResponse() {
            return response;
        }

        public void setResponse(String response) {
            this.response = response;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }
    }
}
