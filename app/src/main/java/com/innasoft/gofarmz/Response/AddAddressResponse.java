package com.innasoft.gofarmz.Response;

public class AddAddressResponse {


    /**
     * status : 10100
     * message : Address added successfully.
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
