package com.innasoft.gofarmz.Response;

public class ImageNotificationResponse {


    /**
     * status : 10100
     * message : Notification details.
     * stateData : {"id":"2180","title":"Testing","message":"Testing","created_on":"2019-06-19 18:36:34","is_read":"1","image":"http://gofarmzv2.learningslot.in/uploads/5a8b2-5.jpg"}
     */

    private String status;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 2180
         * title : Testing
         * message : Testing
         * created_on : 2019-06-19 18:36:34
         * is_read : 1
         * image : http://gofarmzv2.learningslot.in/uploads/5a8b2-5.jpg
         */

        private String id;
        private String title;
        private String message;
        private String created_on;
        private String is_read;
        private String image;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getCreated_on() {
            return created_on;
        }

        public void setCreated_on(String created_on) {
            this.created_on = created_on;
        }

        public String getIs_read() {
            return is_read;
        }

        public void setIs_read(String is_read) {
            this.is_read = is_read;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}
