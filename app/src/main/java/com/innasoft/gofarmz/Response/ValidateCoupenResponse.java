package com.innasoft.gofarmz.Response;

public class ValidateCoupenResponse {


    /**
     * status : 10100
     * message : Coupon applied successfully.
     * stateData : 35.3
     */

    private String status;
    private String message;
    private double data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public double getData() {
        return data;
    }

    public void setData(double data) {
        this.data = data;
    }
}
