package com.innasoft.gofarmz.Response;

public class ViewCartIncreaseResponse {

    /**
     * status : 10100
     * message : Ginger quantity updated successfully.
     * price : 208.00
     */

    private String status;
    private String message;
    private String price;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
