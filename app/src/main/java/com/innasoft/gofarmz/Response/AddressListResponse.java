package com.innasoft.gofarmz.Response;

import java.util.List;

public class AddressListResponse {


    /**
     * status : 10100
     * message : Data fetch successfully.
     * stateData : [{"id":"406","name":"Jay","address_line1":"KnowTec","address_line2":"1-98/9/12c, M.N.R.J Residency, 3rd floor,Opp:FIITJEE School, Image Gardens Rd, ","area":"Madhapur","city":"Hyderabad","state":"Telangana","pincode":"500081","country":"India","contact_no":"9949905540","alternate_contact_no":"1234567890","latitude":"17.4494","longitude":"78.387","is_default":"Yes","distance":10,"delivery_status":true,"delivery_charges":"0"},{"id":"405","name":"aaaa","address_line1":"aaaa","address_line2":"","area":"aaa","city":"aaa","state":"aaa","pincode":"505302","country":"India","contact_no":"155887222","alternate_contact_no":"887787887","latitude":"47.3374","longitude":"77.6647","is_default":"No","distance":5340,"delivery_status":false,"delivery_charges":"0"},{"id":"399","name":"Jay","address_line1":"KnowTec","address_line2":"1-98/9/12c, M.N.R.J Residency, 3rd floor,Opp:FIITJEE School, Image Gardens Rd, ","area":"Madhapur","city":"Hyderabad","state":"Telangana","pincode":"500081","country":"India","contact_no":"9949905540","alternate_contact_no":"","latitude":"17.4494","longitude":"78.387","is_default":"No","distance":10,"delivery_status":true,"delivery_charges":"0"}]
     */

    private String status;
    private String message;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 406
         * name : Jay
         * address_line1 : KnowTec
         * address_line2 : 1-98/9/12c, M.N.R.J Residency, 3rd floor,Opp:FIITJEE School, Image Gardens Rd,
         * area : Madhapur
         * city : Hyderabad
         * state : Telangana
         * pincode : 500081
         * country : India
         * contact_no : 9949905540
         * alternate_contact_no : 1234567890
         * latitude : 17.4494
         * longitude : 78.387
         * is_default : Yes
         * distance : 10
         * delivery_status : true
         * delivery_charges : 0
         */

        private String id;
        private String name;
        private String address_line1;
        private String address_line2;
        private String area;
        private String city;
        private String state;
        private String pincode;
        private String country;
        private String contact_no;
        private String alternate_contact_no;
        private String latitude;
        private String longitude;
        private String is_default;
        private int distance;
        private boolean delivery_status;
        private String delivery_charges;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress_line1() {
            return address_line1;
        }

        public void setAddress_line1(String address_line1) {
            this.address_line1 = address_line1;
        }

        public String getAddress_line2() {
            return address_line2;
        }

        public void setAddress_line2(String address_line2) {
            this.address_line2 = address_line2;
        }

        public String getArea() {
            return area;
        }

        public void setArea(String area) {
            this.area = area;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getContact_no() {
            return contact_no;
        }

        public void setContact_no(String contact_no) {
            this.contact_no = contact_no;
        }

        public String getAlternate_contact_no() {
            return alternate_contact_no;
        }

        public void setAlternate_contact_no(String alternate_contact_no) {
            this.alternate_contact_no = alternate_contact_no;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getIs_default() {
            return is_default;
        }

        public void setIs_default(String is_default) {
            this.is_default = is_default;
        }

        public int getDistance() {
            return distance;
        }

        public void setDistance(int distance) {
            this.distance = distance;
        }

        public boolean isDelivery_status() {
            return delivery_status;
        }

        public void setDelivery_status(boolean delivery_status) {
            this.delivery_status = delivery_status;
        }

        public String getDelivery_charges() {
            return delivery_charges;
        }

        public void setDelivery_charges(String delivery_charges) {
            this.delivery_charges = delivery_charges;
        }
    }
}
