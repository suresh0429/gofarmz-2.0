package com.innasoft.gofarmz.Response;

import java.util.List;

public class MyOrderResponse {


    /**
     * status : 10100
     * stateData : {"currency":"Rs. ","recordTotalCnt":17,"recordData":[{"id":"3152","reference_id":"REF20191115025200","order_id":"GFZ2019111502520052","final_price":"325","order_status":"Confirmed","order_date":"2019-11-15","order_time":"14:52:00","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"25","images":"vegetable_box.png"},{"id":"3151","reference_id":"REF20191115025046","order_id":"GFZ2019111502504618","final_price":"325","order_status":"Confirmed","order_date":"2019-11-15","order_time":"14:50:46","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"25","images":"vegetable_box.png"},{"id":"3150","reference_id":"REF20191115024946","order_id":"GFZ2019111502494621","final_price":"325","order_status":"Confirmed","order_date":"2019-11-15","order_time":"14:49:46","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"25","images":"vegetable_box.png"},{"id":"3149","reference_id":"REF20191115024739","order_id":"GFZ2019111502473961","final_price":"325","order_status":"Confirmed","order_date":"2019-11-15","order_time":"14:47:39","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"25","images":"vegetable_box.png"},{"id":"3135","reference_id":"REF20191115011619","order_id":"GFZ2019111501161992","final_price":"325","order_status":"Confirmed","order_date":"2019-11-15","order_time":"13:16:19","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"25","images":"vegetable_box.png"},{"id":"3131","reference_id":"REF20191113123758","order_id":"GFZ2019111312375824","final_price":"425","order_status":"Confirmed","order_date":"2019-11-13","order_time":"12:37:58","discount":"0","total_mrp_price":"400","coupon_code":null,"status":"1","shipping_charges":"25","images":"8628601.png"},{"id":"3130","reference_id":"REF20191113122231","order_id":"GFZ2019111312223133","final_price":"340","order_status":"Confirmed","order_date":"2019-11-13","order_time":"12:22:31","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"40","images":"vegetable_box.png"},{"id":"3129","reference_id":"REF20191113121217","order_id":"GFZ2019111312121777","final_price":"340","order_status":"Confirmed","order_date":"2019-11-13","order_time":"12:12:17","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"40","images":"vegetable_box.png"},{"id":"3127","reference_id":"REF20191113112804","order_id":"GFZ2019111311280468","final_price":"325","order_status":"Confirmed","order_date":"2019-11-13","order_time":"11:28:04","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"25","images":"8628601.png"},{"id":"3124","reference_id":"REF20191112032540","order_id":"GFZ2019111203254084","final_price":"629","order_status":"Confirmed","order_date":"2019-11-12","order_time":"15:25:40","discount":"0","total_mrp_price":"604","coupon_code":null,"status":"1","shipping_charges":"25","images":"vegetable_box.png"},{"id":"3123","reference_id":"REF20191112030510","order_id":"GFZ2019111203051087","final_price":"329","order_status":"Confirmed","order_date":"2019-11-12","order_time":"15:05:10","discount":"0","total_mrp_price":"304","coupon_code":null,"status":"1","shipping_charges":"25","images":"8628601.png"},{"id":"3122","reference_id":"REF20191112030118","order_id":"GFZ2019111203011911","final_price":"633","order_status":"Confirmed","order_date":"2019-11-12","order_time":"15:01:18","discount":"0","total_mrp_price":"608","coupon_code":null,"status":"1","shipping_charges":"25","images":"8628601.png"},{"id":"3121","reference_id":"REF20191112025351","order_id":"GFZ2019111202535270","final_price":"325","order_status":"Confirmed","order_date":"2019-11-12","order_time":"14:53:51","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"25","images":"vegetable_box.png"},{"id":"3119","reference_id":"REF20191108052958","order_id":"GFZ2019110805295845","final_price":"325","order_status":"Confirmed","order_date":"2019-11-08","order_time":"17:29:58","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"25","images":"vegetable_box.png"},{"id":"3118","reference_id":"REF20191108051427","order_id":"GFZ2019110805142794","final_price":"325","order_status":"Confirmed","order_date":"2019-11-08","order_time":"17:14:27","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"25","images":"vegetable_box.png"},{"id":"3117","reference_id":"REF20191108050851","order_id":"GFZ2019110805085182","final_price":"325","order_status":"Confirmed","order_date":"2019-11-08","order_time":"17:08:51","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"25","images":"vegetable_box.png"},{"id":"3116","reference_id":"REF20191108044804","order_id":"GFZ2019110804480436","final_price":"325","order_status":"Confirmed","order_date":"2019-11-08","order_time":"16:48:04","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"25","images":"vegetable_box.png"}]}
     */

    private String status;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * currency : Rs.
         * recordTotalCnt : 17
         * recordData : [{"id":"3152","reference_id":"REF20191115025200","order_id":"GFZ2019111502520052","final_price":"325","order_status":"Confirmed","order_date":"2019-11-15","order_time":"14:52:00","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"25","images":"vegetable_box.png"},{"id":"3151","reference_id":"REF20191115025046","order_id":"GFZ2019111502504618","final_price":"325","order_status":"Confirmed","order_date":"2019-11-15","order_time":"14:50:46","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"25","images":"vegetable_box.png"},{"id":"3150","reference_id":"REF20191115024946","order_id":"GFZ2019111502494621","final_price":"325","order_status":"Confirmed","order_date":"2019-11-15","order_time":"14:49:46","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"25","images":"vegetable_box.png"},{"id":"3149","reference_id":"REF20191115024739","order_id":"GFZ2019111502473961","final_price":"325","order_status":"Confirmed","order_date":"2019-11-15","order_time":"14:47:39","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"25","images":"vegetable_box.png"},{"id":"3135","reference_id":"REF20191115011619","order_id":"GFZ2019111501161992","final_price":"325","order_status":"Confirmed","order_date":"2019-11-15","order_time":"13:16:19","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"25","images":"vegetable_box.png"},{"id":"3131","reference_id":"REF20191113123758","order_id":"GFZ2019111312375824","final_price":"425","order_status":"Confirmed","order_date":"2019-11-13","order_time":"12:37:58","discount":"0","total_mrp_price":"400","coupon_code":null,"status":"1","shipping_charges":"25","images":"8628601.png"},{"id":"3130","reference_id":"REF20191113122231","order_id":"GFZ2019111312223133","final_price":"340","order_status":"Confirmed","order_date":"2019-11-13","order_time":"12:22:31","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"40","images":"vegetable_box.png"},{"id":"3129","reference_id":"REF20191113121217","order_id":"GFZ2019111312121777","final_price":"340","order_status":"Confirmed","order_date":"2019-11-13","order_time":"12:12:17","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"40","images":"vegetable_box.png"},{"id":"3127","reference_id":"REF20191113112804","order_id":"GFZ2019111311280468","final_price":"325","order_status":"Confirmed","order_date":"2019-11-13","order_time":"11:28:04","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"25","images":"8628601.png"},{"id":"3124","reference_id":"REF20191112032540","order_id":"GFZ2019111203254084","final_price":"629","order_status":"Confirmed","order_date":"2019-11-12","order_time":"15:25:40","discount":"0","total_mrp_price":"604","coupon_code":null,"status":"1","shipping_charges":"25","images":"vegetable_box.png"},{"id":"3123","reference_id":"REF20191112030510","order_id":"GFZ2019111203051087","final_price":"329","order_status":"Confirmed","order_date":"2019-11-12","order_time":"15:05:10","discount":"0","total_mrp_price":"304","coupon_code":null,"status":"1","shipping_charges":"25","images":"8628601.png"},{"id":"3122","reference_id":"REF20191112030118","order_id":"GFZ2019111203011911","final_price":"633","order_status":"Confirmed","order_date":"2019-11-12","order_time":"15:01:18","discount":"0","total_mrp_price":"608","coupon_code":null,"status":"1","shipping_charges":"25","images":"8628601.png"},{"id":"3121","reference_id":"REF20191112025351","order_id":"GFZ2019111202535270","final_price":"325","order_status":"Confirmed","order_date":"2019-11-12","order_time":"14:53:51","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"25","images":"vegetable_box.png"},{"id":"3119","reference_id":"REF20191108052958","order_id":"GFZ2019110805295845","final_price":"325","order_status":"Confirmed","order_date":"2019-11-08","order_time":"17:29:58","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"25","images":"vegetable_box.png"},{"id":"3118","reference_id":"REF20191108051427","order_id":"GFZ2019110805142794","final_price":"325","order_status":"Confirmed","order_date":"2019-11-08","order_time":"17:14:27","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"25","images":"vegetable_box.png"},{"id":"3117","reference_id":"REF20191108050851","order_id":"GFZ2019110805085182","final_price":"325","order_status":"Confirmed","order_date":"2019-11-08","order_time":"17:08:51","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"25","images":"vegetable_box.png"},{"id":"3116","reference_id":"REF20191108044804","order_id":"GFZ2019110804480436","final_price":"325","order_status":"Confirmed","order_date":"2019-11-08","order_time":"16:48:04","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"25","images":"vegetable_box.png"}]
         */

        private String currency;
        private int recordTotalCnt;
        private List<RecordDataBean> recordData;

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public int getRecordTotalCnt() {
            return recordTotalCnt;
        }

        public void setRecordTotalCnt(int recordTotalCnt) {
            this.recordTotalCnt = recordTotalCnt;
        }

        public List<RecordDataBean> getRecordData() {
            return recordData;
        }

        public void setRecordData(List<RecordDataBean> recordData) {
            this.recordData = recordData;
        }

        public static class RecordDataBean {
            /**
             * id : 3152
             * reference_id : REF20191115025200
             * order_id : GFZ2019111502520052
             * final_price : 325
             * order_status : Confirmed
             * order_date : 2019-11-15
             * order_time : 14:52:00
             * discount : 0
             * total_mrp_price : 300
             * coupon_code : null
             * status : 1
             * shipping_charges : 25
             * images : vegetable_box.png
             */

            private String id;
            private String reference_id;
            private String order_id;
            private String final_price;
            private String order_status;
            private String order_date;
            private String order_time;
            private String discount;
            private String total_mrp_price;
            private String coupon_code;
            private String status;
            private String shipping_charges;
            private String images;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getReference_id() {
                return reference_id;
            }

            public void setReference_id(String reference_id) {
                this.reference_id = reference_id;
            }

            public String getOrder_id() {
                return order_id;
            }

            public void setOrder_id(String order_id) {
                this.order_id = order_id;
            }

            public String getFinal_price() {
                return final_price;
            }

            public void setFinal_price(String final_price) {
                this.final_price = final_price;
            }

            public String getOrder_status() {
                return order_status;
            }

            public void setOrder_status(String order_status) {
                this.order_status = order_status;
            }

            public String getOrder_date() {
                return order_date;
            }

            public void setOrder_date(String order_date) {
                this.order_date = order_date;
            }

            public String getOrder_time() {
                return order_time;
            }

            public void setOrder_time(String order_time) {
                this.order_time = order_time;
            }

            public String getDiscount() {
                return discount;
            }

            public void setDiscount(String discount) {
                this.discount = discount;
            }

            public String getTotal_mrp_price() {
                return total_mrp_price;
            }

            public void setTotal_mrp_price(String total_mrp_price) {
                this.total_mrp_price = total_mrp_price;
            }

            public String getCoupon_code() {
                return coupon_code;
            }

            public void setCoupon_code(String coupon_code) {
                this.coupon_code = coupon_code;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getShipping_charges() {
                return shipping_charges;
            }

            public void setShipping_charges(String shipping_charges) {
                this.shipping_charges = shipping_charges;
            }

            public String getImages() {
                return images;
            }

            public void setImages(String images) {
                this.images = images;
            }
        }
    }
}
