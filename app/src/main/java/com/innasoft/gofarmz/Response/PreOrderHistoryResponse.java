package com.innasoft.gofarmz.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PreOrderHistoryResponse {


    /**
     * status : 10100
     * stateData : {"currency":"Rs. ","recordTotalCnt":1,"recordData":[{"id":"1","ref_no":"PRE08042019001","total_price":"466","shipping_charges":"40","grand_total":"506","pre_order_status":"Confirmed","created_date_time":"2019-04-08 10:14:45","images":"Raw_Mango.png"}]}
     */

    @SerializedName("status")
    private String status;
    @SerializedName("stateData")
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * currency : Rs.
         * recordTotalCnt : 1
         * recordData : [{"id":"1","ref_no":"PRE08042019001","total_price":"466","shipping_charges":"40","grand_total":"506","pre_order_status":"Confirmed","created_date_time":"2019-04-08 10:14:45","images":"Raw_Mango.png"}]
         */

        @SerializedName("currency")
        private String currency;
        @SerializedName("recordTotalCnt")
        private int recordTotalCnt;
        @SerializedName("recordData")
        private List<RecordDataBean> recordData;

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public int getRecordTotalCnt() {
            return recordTotalCnt;
        }

        public void setRecordTotalCnt(int recordTotalCnt) {
            this.recordTotalCnt = recordTotalCnt;
        }

        public List<RecordDataBean> getRecordData() {
            return recordData;
        }

        public void setRecordData(List<RecordDataBean> recordData) {
            this.recordData = recordData;
        }

        public static class RecordDataBean {
            /**
             * id : 1
             * ref_no : PRE08042019001
             * total_price : 466
             * shipping_charges : 40
             * grand_total : 506
             * pre_order_status : Confirmed
             * created_date_time : 2019-04-08 10:14:45
             * images : Raw_Mango.png
             */

            @SerializedName("id")
            private String id;
            @SerializedName("ref_no")
            private String refNo;
            @SerializedName("total_price")
            private String totalPrice;
            @SerializedName("shipping_charges")
            private String shippingCharges;
            @SerializedName("grand_total")
            private String grandTotal;
            @SerializedName("pre_order_status")
            private String preOrderStatus;
            @SerializedName("created_date_time")
            private String createdDateTime;
            @SerializedName("images")
            private String images;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getRefNo() {
                return refNo;
            }

            public void setRefNo(String refNo) {
                this.refNo = refNo;
            }

            public String getTotalPrice() {
                return totalPrice;
            }

            public void setTotalPrice(String totalPrice) {
                this.totalPrice = totalPrice;
            }

            public String getShippingCharges() {
                return shippingCharges;
            }

            public void setShippingCharges(String shippingCharges) {
                this.shippingCharges = shippingCharges;
            }

            public String getGrandTotal() {
                return grandTotal;
            }

            public void setGrandTotal(String grandTotal) {
                this.grandTotal = grandTotal;
            }

            public String getPreOrderStatus() {
                return preOrderStatus;
            }

            public void setPreOrderStatus(String preOrderStatus) {
                this.preOrderStatus = preOrderStatus;
            }

            public String getCreatedDateTime() {
                return createdDateTime;
            }

            public void setCreatedDateTime(String createdDateTime) {
                this.createdDateTime = createdDateTime;
            }

            public String getImages() {
                return images;
            }

            public void setImages(String images) {
                this.images = images;
            }
        }
    }
}
