package com.innasoft.gofarmz.Response;

import com.google.gson.annotations.SerializedName;

public class PreOrderPlaceResponse {


    /**
     * status : 10400
     * message : Order created successfully
     * data : {"amount":539,"order_id":107,"encrypted_orderid":"MzQwBwA=","order_ref_no":"PRE27122019021","payment_url":"https://www.innasoft.in/gofarmz/api/preorder_payment/MzQwBwA="}
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * amount : 539
         * order_id : 107
         * encrypted_orderid : MzQwBwA=
         * order_ref_no : PRE27122019021
         * payment_url : https://www.innasoft.in/gofarmz/api/preorder_payment/MzQwBwA=
         */

        @SerializedName("amount")
        private int amount;
        @SerializedName("order_id")
        private int orderId;
        @SerializedName("encrypted_orderid")
        private String encryptedOrderid;
        @SerializedName("order_ref_no")
        private String orderRefNo;
        @SerializedName("payment_url")
        private String paymentUrl;

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public int getOrderId() {
            return orderId;
        }

        public void setOrderId(int orderId) {
            this.orderId = orderId;
        }

        public String getEncryptedOrderid() {
            return encryptedOrderid;
        }

        public void setEncryptedOrderid(String encryptedOrderid) {
            this.encryptedOrderid = encryptedOrderid;
        }

        public String getOrderRefNo() {
            return orderRefNo;
        }

        public void setOrderRefNo(String orderRefNo) {
            this.orderRefNo = orderRefNo;
        }

        public String getPaymentUrl() {
            return paymentUrl;
        }

        public void setPaymentUrl(String paymentUrl) {
            this.paymentUrl = paymentUrl;
        }
    }
}
