package com.innasoft.gofarmz.Response;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {


    /**
     * status : 10100
     * message : You have been logged in successfully.
     * data : {"jwt":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1NzgzNzE4NzEsImp0aSI6IjJmS0dnYzQ5eTc1VXJqQmFIbFZCMlh6N1JZcUN1Z0dkS0hkUHVXbGtZSk09IiwiaXNzIjoiaHR0cDpcL1wvd3d3LmlubmFzb2Z0LmluXC9waHAtanNvblwvIiwibmJmIjoxNTc4MzcxODcyLCJleHAiOjE2MDk5MDc4NzIsImRhdGEiOnsidXNlcl9pZCI6IjMyMTEiLCJ1c2VyX25hbWUiOiJCaGF2YW5pIiwiZW1haWwiOiJiaGF2YW5pQGlubmFzb2Z0LmluIiwiZ2VuZGVyIjoiRmVtYWxlIiwibW9iaWxlIjoiOTU0MjQxMDc3NCIsImFjY291bnRfc3RhdHVzIjoiMSIsImJyb3dzZXJfc2Vzc2lvbl9pZCI6IjJmS0dnYzQ5eTc1VXJqQmFIbFZCMlh6N1JZcUN1Z0dkS0hkUHVXbGtZSk09In19.v9oea4Y7jAPUGQxRHG0gBknKV_ok1LNjOGW7SREn-325_Uq4Zd5VQnjrJ--gLiADBoEWiMkhY64GQajqitaPqA","user_id":"3211","user_name":"Bhavani","email":"bhavani@innasoft.in","mobile":"9542410774","gender":"Female"}
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * jwt : eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1NzgzNzE4NzEsImp0aSI6IjJmS0dnYzQ5eTc1VXJqQmFIbFZCMlh6N1JZcUN1Z0dkS0hkUHVXbGtZSk09IiwiaXNzIjoiaHR0cDpcL1wvd3d3LmlubmFzb2Z0LmluXC9waHAtanNvblwvIiwibmJmIjoxNTc4MzcxODcyLCJleHAiOjE2MDk5MDc4NzIsImRhdGEiOnsidXNlcl9pZCI6IjMyMTEiLCJ1c2VyX25hbWUiOiJCaGF2YW5pIiwiZW1haWwiOiJiaGF2YW5pQGlubmFzb2Z0LmluIiwiZ2VuZGVyIjoiRmVtYWxlIiwibW9iaWxlIjoiOTU0MjQxMDc3NCIsImFjY291bnRfc3RhdHVzIjoiMSIsImJyb3dzZXJfc2Vzc2lvbl9pZCI6IjJmS0dnYzQ5eTc1VXJqQmFIbFZCMlh6N1JZcUN1Z0dkS0hkUHVXbGtZSk09In19.v9oea4Y7jAPUGQxRHG0gBknKV_ok1LNjOGW7SREn-325_Uq4Zd5VQnjrJ--gLiADBoEWiMkhY64GQajqitaPqA
         * user_id : 3211
         * user_name : Bhavani
         * email : bhavani@innasoft.in
         * mobile : 9542410774
         * gender : Female
         */

        @SerializedName("jwt")
        private String jwt;
        @SerializedName("user_id")
        private String userId;
        @SerializedName("user_name")
        private String userName;
        @SerializedName("email")
        private String email;
        @SerializedName("mobile")
        private String mobile;
        @SerializedName("gender")
        private String gender;

        public String getJwt() {
            return jwt;
        }

        public void setJwt(String jwt) {
            this.jwt = jwt;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }
    }
}
