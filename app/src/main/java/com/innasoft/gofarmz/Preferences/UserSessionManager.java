package com.innasoft.gofarmz.Preferences;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class UserSessionManager {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "gofarmz";

    // All Shared Preferences Keys
    public static final String KEY_IS_WAITING_FOR_SMS = "IsWaitingForSms";
    public static final String KEY_MOBILE_NUMBER = "mobile_number";
    public static final String KEY_IS_LOGGED_IN = "isLoggedIn";
    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_MOBILE = "mobile";
    public static final String KEY_PROFILE_PIC = "profilepic";
    public static final String KEY_DEVICEID="deviceId";
    public static final String KEY_TOKEN="AccessToken";
    public static final String KEY_GENDER="gender";



    public UserSessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setIsWaitingForSms(boolean isWaiting) {
        editor.putBoolean(KEY_IS_WAITING_FOR_SMS, isWaiting);
        editor.commit();
    }

    public boolean isWaitingForSms() {
        return pref.getBoolean(KEY_IS_WAITING_FOR_SMS, false);
    }

    public void setMobileNumber(String mobileNumber) {
        editor.putString(KEY_MOBILE_NUMBER, mobileNumber);
        editor.commit();
    }

    public String getMobileNumber() {
        return pref.getString(KEY_MOBILE_NUMBER, null);
    }

    public void createLogin(String id,String name, String email, String mobile,String profilepic,String device_id,String token,String gender) {
        editor.putString(KEY_ID, id);
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_MOBILE, mobile);
        editor.putString(KEY_PROFILE_PIC, profilepic);
        editor.putString(KEY_DEVICEID,device_id);
        editor.putString(KEY_TOKEN,token);
        editor.putString(KEY_GENDER,gender);
        editor.putBoolean(KEY_IS_LOGGED_IN, true);

        editor.commit();
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(KEY_IS_LOGGED_IN, false);
    }

    public void clearSession() {
        editor.clear();
        editor.commit();
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> profile = new HashMap<>();
        profile.put("id", pref.getString(KEY_ID, null));
        profile.put("name", pref.getString(KEY_NAME, null));
        profile.put("email", pref.getString(KEY_EMAIL, null));
        profile.put("mobile", pref.getString(KEY_MOBILE, null));
        profile.put("profilepic", pref.getString(KEY_PROFILE_PIC, null));
        profile.put("deviceId",pref.getString(KEY_DEVICEID,null));
        profile.put("AccessToken",pref.getString(KEY_TOKEN,null));
        profile.put("gender",pref.getString(KEY_GENDER,null));

        return profile;
    }

}
